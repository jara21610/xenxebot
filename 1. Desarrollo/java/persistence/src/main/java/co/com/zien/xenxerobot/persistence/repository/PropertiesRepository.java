package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.jpa.repository.*;

public interface PropertiesRepository extends JpaRepository<Properties, String>
{
    @Query(name = "Properties.findByName")
    String findByName(@Param("name") final String p0);
}
