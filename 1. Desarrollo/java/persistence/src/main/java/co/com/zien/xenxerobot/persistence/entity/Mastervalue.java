package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

@Entity
@Table(name = "MASTERVALUE")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Mastervalue.findAll", query = "SELECT m FROM Mastervalue m"), @NamedQuery(name = "Mastervalue.findByIdmastertype", query = "SELECT m FROM Mastervalue m WHERE m.mastervaluePK.idmastertype = :idmastertype"), @NamedQuery(name = "Mastervalue.findByValue", query = "SELECT m FROM Mastervalue m WHERE m.mastervaluePK.value = :value"), @NamedQuery(name = "Mastervalue.findBymastertypename", query = "SELECT m.mastervaluePK.value FROM Mastervalue m WHERE m.mastertype.name = :name") })
public class Mastervalue implements Serializable
{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MastervaluePK mastervaluePK;
    @JoinColumn(name = "idmastertype", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mastertype mastertype;
    
    public Mastervalue() {
    }
    
    public Mastervalue(final MastervaluePK mastervaluePK) {
        this.mastervaluePK = mastervaluePK;
    }
    
    public Mastervalue(final int idmastertype, final String value) {
        this.mastervaluePK = new MastervaluePK(idmastertype, value);
    }
    
    public MastervaluePK getMastervaluePK() {
        return this.mastervaluePK;
    }
    
    public void setMastervaluePK(final MastervaluePK mastervaluePK) {
        this.mastervaluePK = mastervaluePK;
    }
    
    public Mastertype getMastertype() {
        return this.mastertype;
    }
    
    public void setMastertype(final Mastertype mastertype) {
        this.mastertype = mastertype;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.mastervaluePK != null) ? this.mastervaluePK.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Mastervalue)) {
            return false;
        }
        final Mastervalue other = (Mastervalue)object;
        return (this.mastervaluePK != null || other.mastervaluePK == null) && (this.mastervaluePK == null || this.mastervaluePK.equals(other.mastervaluePK));
    }
    
    public String toString() {
        return "javaapplication1.Mastervalue[ mastervaluePK=" + this.mastervaluePK + " ]";
    }
}
