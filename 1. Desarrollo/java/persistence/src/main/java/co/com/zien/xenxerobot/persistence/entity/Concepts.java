package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

@Entity
@Table(name = "CONCEPTS")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Concepts.findAll", query = "SELECT c FROM Concepts c"), @NamedQuery(name = "Concepts.findByConceptType", query = "SELECT c FROM Concepts c WHERE c.conceptType = :conceptType"), @NamedQuery(name = "Concepts.findByName", query = "SELECT c FROM Concepts c WHERE c.name = :name"), @NamedQuery(name = "Concepts.findByHumanSentiment", query = "SELECT c FROM Concepts c WHERE c.humanSentiment = :humanSentiment"), @NamedQuery(name = "Concepts.findByMachineLearningSentiment", query = "SELECT c FROM Concepts c WHERE c.machineLearningSentiment = :machineLearningSentiment"), @NamedQuery(name = "Concepts.findById", query = "SELECT c FROM Concepts c WHERE c.id = :id") })
public class Concepts implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Column(name = "CONCEPT_TYPE")
    private String conceptType;
    @Column(name = "NAME")
    private String name;
    @Column(name = "HUMAN_SENTIMENT")
    private String humanSentiment;
    @Column(name = "MACHINE_LEARNING_SENTIMENT")
    private String machineLearningSentiment;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private String id;
    @JoinColumn(name = "ID_POST", referencedColumnName = "IDPOST")
    @ManyToOne
    private Post idPost;
    
    public Concepts() {
    }
    
    public Concepts(final String id) {
        this.id = id;
    }
    
    public String getConceptType() {
        return this.conceptType;
    }
    
    public void setConceptType(final String conceptType) {
        this.conceptType = conceptType;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getHumanSentiment() {
        return this.humanSentiment;
    }
    
    public void setHumanSentiment(final String humanSentiment) {
        this.humanSentiment = humanSentiment;
    }
    
    public String getMachineLearningSentiment() {
        return this.machineLearningSentiment;
    }
    
    public void setMachineLearningSentiment(final String machineLearningSentiment) {
        this.machineLearningSentiment = machineLearningSentiment;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public Post getIdPost() {
        return this.idPost;
    }
    
    public void setIdPost(final Post idPost) {
        this.idPost = idPost;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Concepts)) {
            return false;
        }
        final Concepts other = (Concepts)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication1.Concepts[ id=" + this.id + " ]";
    }
}
