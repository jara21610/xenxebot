package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import java.math.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "PLAN")
@XmlRootElement
@NamedQueries({ 
	@NamedQuery(name = "Plan.findAll", query = "SELECT p FROM Plan p"), 
	@NamedQuery(name = "Plan.findByPlanid", query = "SELECT p FROM Plan p WHERE p.planid = :planid"), 
	@NamedQuery(name = "Plan.findByTxplancapacity", query = "SELECT p FROM Plan p WHERE p.txplancapacity = :txplancapacity"), 
	@NamedQuery(name = "Plan.findByTimeunit", query = "SELECT p FROM Plan p WHERE p.timeunit = :timeunit"),
	@NamedQuery(name = "Plan.findByIdAndProductid", query = "SELECT p FROM Plan p WHERE p.planid = :planid and p.productid = :productid"), 
	@NamedQuery(name = "Plan.findByTimequantity", query = "SELECT p FROM Plan p WHERE p.timequantity = :timequantity") })
public class Plan implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PLANID")
    private String planid;
    @Column(name = "PRODUCTID")
    private String productid;
    @Basic(optional = false)
    @Column(name = "TXPLANCAPACITY")
    private int txplancapacity;
    @Column(name = "TIMEUNIT")
    private String timeunit;
    @Column(name = "TIMEQUANTITY")
    private String timequantity;
    @Column(name = "PRICE")
    private BigInteger price;
    @OneToMany(cascade = { CascadeType.ALL }, mappedBy = "idplan")
    private Collection<Userplan> userplanCollection;
    
    public Plan() {
    }
    
    public Plan(final String planid) {
        this.planid = planid;
    }
    
    public Plan(final String planid, final int txplancapacity) {
        this.planid = planid;
        this.txplancapacity = txplancapacity;
    }
    
    public String getPlanid() {
        return this.planid;
    }
    
    public void setPlanid(final String planid) {
        this.planid = planid;
    }
    
    public int getTxplancapacity() {
        return this.txplancapacity;
    }
    
    public void setTxplancapacity(final int txplancapacity) {
        this.txplancapacity = txplancapacity;
    }
    
    public String getTimeunit() {
        return this.timeunit;
    }
    
    public void setTimeunit(final String timeunit) {
        this.timeunit = timeunit;
    }
    
    public String getTimequantity() {
        return this.timequantity;
    }
    
    public void setTimequantity(final String timequantity) {
        this.timequantity = timequantity;
    }
    
    @XmlTransient
    public Collection<Userplan> getUserplanCollection() {
        return this.userplanCollection;
    }
    
    public void setUserplanCollection(final Collection<Userplan> userplanCollection) {
        this.userplanCollection = userplanCollection;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.planid != null) ? this.planid.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Plan)) {
            return false;
        }
        final Plan other = (Plan)object;
        return (this.planid != null || other.planid == null) && (this.planid == null || this.planid.equals(other.planid));
    }
    
    public String toString() {
        return "javaapplication2.Plan[ planid=" + this.planid + " ]";
    }
    
    public BigInteger getPrice() {
        return this.price;
    }
    
    public void setPrice(final BigInteger price) {
        this.price = price;
    }
    
    public String getProductid() {
  		return productid;
  	}

  	public void setProductid(String productid) {
  		this.productid = productid;
  	}
}
