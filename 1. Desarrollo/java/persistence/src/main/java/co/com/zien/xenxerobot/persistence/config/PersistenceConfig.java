package co.com.zien.xenxerobot.persistence.config;

import org.springframework.transaction.annotation.*;
import org.springframework.data.jpa.repository.config.*;
import javax.sql.*;
import org.springframework.core.env.*;
import org.springframework.orm.jpa.vendor.*;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.PropertySource;

import javax.persistence.*;
import org.springframework.orm.jpa.*;
import com.zaxxer.hikari.*;
import java.util.*;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "co.com.zien.xenxerobot.persistence.repository" })
@PropertySource({ "file:///${XENXE_ROBOT}/XenxeApplication.properties" })
public class PersistenceConfig
{
    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource, final Environment env) {
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter((JpaVendorAdapter)new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(new String[] { "co.com.zien.xenxerobot.persistence.entity" });
        final Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
        jpaProperties.put("hibernate.ejb.naming_strategy", env.getRequiredProperty("hibernate.ejb.naming_strategy"));
        jpaProperties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
        jpaProperties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
        jpaProperties.put("hibernate.id.new_generator_mappings", "false");
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        return entityManagerFactoryBean;
    }
    
    @Bean
    JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
    
    @Bean(destroyMethod = "close")
    DataSource dataSource(final Environment env) {
        final HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName(env.getRequiredProperty("db.driver"));
        dataSourceConfig.setJdbcUrl(env.getRequiredProperty("db.url"));
        dataSourceConfig.setUsername(env.getRequiredProperty("db.username"));
        dataSourceConfig.setPassword(env.getRequiredProperty("db.password"));
        dataSourceConfig.setMaximumPoolSize((int)new Integer(env.getRequiredProperty("db.maximumpoolsize").toString()));
        return (DataSource)new HikariDataSource(dataSourceConfig);
    }
}
