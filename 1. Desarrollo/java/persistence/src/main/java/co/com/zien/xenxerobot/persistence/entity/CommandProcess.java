package co.com.zien.xenxerobot.persistence.entity;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "COMAND_PROCCESS")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "CommandProcess.findMyRunningProcess", query = "SELECT cp FROM CommandProcess cp INNER JOIN cp.user u where cp.state = :state and u.chatid = :chatid and cp.productid = :productid") })
public class CommandProcess
{
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "comand")
    private String comand;
    @Column(name = "arguments")
    private String arguments;
    @Column(name = "state")
    private String state;
    @Column(name = "channel")
    private String channel;
    @Column(name = "exception")
    private String exception;
    @Column(name = "reportdetail")
    private String reportdetail;
    @Column(name = "finishdate")
    private Date finishdate;
    @Column(name = "begindate")
    private Date begindate;
    @ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinColumn(name = "userid")
    private Users user;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "commandprocess")
    private Set<Post> posts;
    @Column(name = "PRODUCTID")
    private String productid;    
    
    public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public CommandProcess() {
        this.posts = new HashSet<Post>();
    }
    
    public Date getFinishdate() {
        return this.finishdate;
    }
    
    public void setFinishdate(final Date finishdate) {
        this.finishdate = finishdate;
    }
    
    public Date getBegindate() {
        return this.begindate;
    }
    
    public void setBegindate(final Date begindate) {
        this.begindate = begindate;
    }
    
    public String getException() {
        return this.exception;
    }
    
    public void setException(final String exception) {
        this.exception = exception;
    }
    
    public Users getUser() {
        return this.user;
    }
    
    public void setUser(final Users user) {
        this.user = user;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getComand() {
        return this.comand;
    }
    
    public void setComand(final String comand) {
        this.comand = comand;
    }
    
    public String getArguments() {
        return this.arguments;
    }
    
    public void setArguments(final String arguments) {
        this.arguments = arguments;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public String getChannel() {
        return this.channel;
    }
    
    public void setChannel(final String channel) {
        this.channel = channel;
    }
    
    public Set<Post> getPosts() {
        return this.posts;
    }
    
    public void setPosts(final Set<Post> posts) {
        this.posts = posts;
    }
    
    public String getReportdetail() {
        return this.reportdetail;
    }
    
    public void setReportdetail(final String reportdetail) {
        this.reportdetail = reportdetail;
    }
}
