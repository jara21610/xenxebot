package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import java.math.*;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "USERPLAN")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Userplan.findAll", query = "SELECT u FROM Userplan u"), 
	@NamedQuery(name = "Userplan.findById", query = "SELECT u FROM Userplan u WHERE u.id = :id"), 
	@NamedQuery(name = "Userplan.findByState", query = "SELECT u FROM Userplan u WHERE u.state = :state"), 
	@NamedQuery(name = "Userplan.findByStartdate", query = "SELECT u FROM Userplan u WHERE u.startdate = :startdate"), 
	@NamedQuery(name = "Userplan.findByFinishdate", query = "SELECT u FROM Userplan u WHERE u.finishdate = :finishdate"), 
	@NamedQuery(name = "Userplan.findByTxplancapacity", query = "SELECT u FROM Userplan u WHERE u.txplancapacity = :txplancapacity"), 
	@NamedQuery(name = "Userplan.historicByUserid", query = "SELECT u FROM Userplan u WHERE u.iduser.id = :userid and u.productid = :productid order by u.finishdate desc"), 
	@NamedQuery(name = "Userplan.activePlanByUseridandstate", query = "SELECT u FROM Userplan u WHERE u.iduser.id = :userid and u.state = :state and u.productid = :productid"), 
	@NamedQuery(name = "Userplan.findPlanstoFinish", query = "SELECT u FROM Userplan u WHERE u.finishdate = CURRENT_DATE"),
	@NamedQuery(name = "Userplan.findPlansbyProductid", query = "SELECT u FROM Userplan u WHERE u.productid = :productid and u.iduser.id = :userid"),
	@NamedQuery(name = "Userplan.findByTxpalused", query = "SELECT u FROM Userplan u WHERE u.txpalused = :txpalused") })
public class Userplan implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private String id;
    @Basic(optional = false)
    @Column(name = "STATE")
    private String state;
    @Column(name = "PRODUCTID")
    private String productid;    
	@Column(name = "STARTDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startdate;
    @Column(name = "FINISHDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishdate;
    @Column(name = "TXPLANCAPACITY")
    private Integer txplancapacity;
    @Column(name = "TXPALUSED")
    private Integer txpalused;
    @Column(name = "PRICE")
    private BigInteger price;
    @Column(name = "TEXTLICENCEACEPTED")                    
    private String testlicenceacepted;
	@JoinColumn(name = "IDPLAN", referencedColumnName = "PLANID")
    @ManyToOne(optional = false)
    private Plan idplan;
    @JoinColumn(name = "IDUSER", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Users iduser;
    @OneToMany(mappedBy = "userplanid")
    private Collection<Paymenthistory> paymenthistoryCollection;
    
    public Userplan() {
    }
    
    public Userplan(final String id) {
        this.id = id;
    }
    
    public Userplan(final String id, final String state) {
        this.id = id;
        this.state = state;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public Date getStartdate() {
        return this.startdate;
    }
    
    public void setStartdate(final Date startdate) {
        this.startdate = startdate;
    }
    
    public Date getFinishdate() {
        return this.finishdate;
    }
    
    public void setFinishdate(final Date finishdate) {
        this.finishdate = finishdate;
    }
    
    public Integer getTxplancapacity() {
        return this.txplancapacity;
    }
    
    public void setTxplancapacity(final Integer txplancapacity) {
        this.txplancapacity = txplancapacity;
    }
    
    public Integer getTxpalused() {
        return this.txpalused;
    }
    
    public void setTxpalused(final Integer txpalused) {
        this.txpalused = txpalused;
    }
    
    public Plan getIdplan() {
        return this.idplan;
    }
    
    public void setIdplan(final Plan idplan) {
        this.idplan = idplan;
    }
    
    public Users getIduser() {
        return this.iduser;
    }
    
    public void setIduser(final Users iduser) {
        this.iduser = iduser;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Userplan)) {
            return false;
        }
        final Userplan other = (Userplan)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication2.Userplan[ id=" + this.id + " ]";
    }
    
    public Collection<Paymenthistory> getPaymenthistoryCollection() {
        return this.paymenthistoryCollection;
    }
    
    public void setPaymenthistoryCollection(final Collection<Paymenthistory> paymenthistoryCollection) {
        this.paymenthistoryCollection = paymenthistoryCollection;
    }
    
    public BigInteger getPrice() {
        return this.price;
    }
    
    public void setPrice(final BigInteger price) {
        this.price = price;
    }
    
    public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}
	
    public String getTestlicenceacepted() {
		return testlicenceacepted;
	}

	public void setTestlicenceacepted(String testlicenceacepted) {
		this.testlicenceacepted = testlicenceacepted;
	}
}
