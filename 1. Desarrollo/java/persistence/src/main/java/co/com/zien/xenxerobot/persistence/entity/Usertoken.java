package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "USERTOKEN")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Usertoken.findAll", query = "SELECT u FROM Usertoken u"), @NamedQuery(name = "Usertoken.findByToken", query = "SELECT u FROM Usertoken u WHERE u.token = :token"), @NamedQuery(name = "Usertoken.findByUntilvaliddate", query = "SELECT u FROM Usertoken u WHERE u.untilvaliddate = :untilvaliddate") })
public class Usertoken implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "UNTILVALIDDATE")
    private Date untilvaliddate;
    @Column(name = "PRODUCTID")
    private String productid;    
	@JoinColumn(name = "IDUSER", referencedColumnName = "ID")
    @ManyToOne
    private Users iduser;
    
    public Usertoken() {
    }
    
    public Usertoken(final String token) {
        this.token = token;
    }
    
    public String getToken() {
        return this.token;
    }
    
    public void setToken(final String token) {
        this.token = token;
    }
    
    public Date getUntilvaliddate() {
        return this.untilvaliddate;
    }
    
    public void setUntilvaliddate(final Date untilvaliddate) {
        this.untilvaliddate = untilvaliddate;
    }
    
    public Users getIduser() {
        return this.iduser;
    }
    
    public void setIduser(final Users iduser) {
        this.iduser = iduser;
    }
    
    public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.token != null) ? this.token.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Usertoken)) {
            return false;
        }
        final Usertoken other = (Usertoken)object;
        return (this.token != null || other.token == null) && (this.token == null || this.token.equals(other.token));
    }
    
    public String toString() {
        return "javaapplication2.Usertoken[ token=" + this.token + " ]";
    }
}
