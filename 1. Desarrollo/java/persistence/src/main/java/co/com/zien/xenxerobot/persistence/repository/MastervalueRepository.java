package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface MastervalueRepository extends JpaRepository<Mastervalue, MastervaluePK>
{
    @Query(name = "Mastervalue.findBymastertypename")
    List<String> findBymastertypename(@Param("name") final String p0);
}
