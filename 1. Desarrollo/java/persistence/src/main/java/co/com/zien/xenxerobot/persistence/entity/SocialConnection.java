package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "SOCIALCONECCTION")
@XmlRootElement
public class SocialConnection implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "windowsizeminutes")
    private int windowsizeminutes;
    @Column(name = "limitcallsallowed")
    private int limitcallsallowed;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "socialconnection")
    private Set<SocialConnectionDetails> socialconecctiondetails;
    
    public SocialConnection() {
        this.socialconecctiondetails = new HashSet<SocialConnectionDetails>();
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public int getWindowsizeminutes() {
        return this.windowsizeminutes;
    }
    
    public void setWindowsizeminutes(final int windowsizeminutes) {
        this.windowsizeminutes = windowsizeminutes;
    }
    
    public int getLimitcallsallowed() {
        return this.limitcallsallowed;
    }
    
    public void setLimitcallsallowed(final int limitcallsallowed) {
        this.limitcallsallowed = limitcallsallowed;
    }
    
    public Set<SocialConnectionDetails> getSocialconecctiondetails() {
        return this.socialconecctiondetails;
    }
    
    public void setSocialconecctiondetails(final Set<SocialConnectionDetails> socialconecctiondetails) {
        this.socialconecctiondetails = socialconecctiondetails;
    }
}
