package co.com.zien.xenxerobot.persistence.repository;

import org.springframework.data.repository.query.*;
import org.springframework.data.domain.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;
import java.math.*;
import co.com.zien.xenxerobot.persistence.entity.*;

public interface PostRepository extends JpaRepository<Post, String>
{
    @Query(name = "Post.posttopackage")
    List<Object[]> posttopackage(@Param("id") final String p0, final Pageable p1);
    
    @Query(name = "Post.posttochange")
    List<Post> posttochange(@Param("id") final BigDecimal p0, final Pageable p1);
    
    @Query(name = "Post.postbypackage")
    List<Post> postbypackage(@Param("id") final BigDecimal p0);
    
    @Query(name = "Post.nextworkitembypackage")
    List<Post> nextworkitembypackage(@Param("id") final BigDecimal p0);
    
    @Query(name = "Post.packageSize")
    Integer packageSize(@Param("id") final BigDecimal p0);
    
    @Query(name = "Post.totalPost")
    Integer totalPost(@Param("id") final String p0);
    
    @Query(name = "Post.totalXSentiment")
    Integer totalXSentiment(@Param("id") final String p0, @Param("sentiment") final String p1);
    
    @Query(name = "Post.sentimentXCategory")
    List<ElementSentimentDTO> sentimentXCategory(@Param("id") final String p0);
    
    @Query(name = "Post.topfrecuencyXconceptType")
    List<Object[]> topfrecuencyXconceptType(@Param("id") final String p0, @Param("type") final String p1, final Pageable p2);
    
    @Query(name = "Post.sentimentXbyConceptName")
    ElementSentimentDTO sentimentXbyConceptName(@Param("id") final String p0, @Param("type") final String p1, @Param("name") final String p2);
}
