package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "POST")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Post.posttopackage", query = "select  max(post.idpost), post.originalmessage from Post post where post.commandprocess.id = :id and post.idPackage is null and post.discartedtopack is null and post.optiontochange is null group by post.originalmessage"), @NamedQuery(name = "Post.posttochange", query = "SELECT p FROM Post p where p.idPackage.id = :id and p.optiontochange = 'Y'"), @NamedQuery(name = "Post.postbypackage", query = "SELECT p FROM Post p where p.idPackage.id = :id and p.worked = 'Y'"), @NamedQuery(name = "Post.nextworkitembypackage", query = "SELECT p FROM Post p where p.idPackage.id = :id and p.worked IS NULL and p.optiontochange is null order by p.idpost DESC"), @NamedQuery(name = "Post.packageSize", query = "SELECT count(p) FROM Post p where p.idPackage.id = :id and p.optiontochange is null"), @NamedQuery(name = "Post.totalPost", query = "SELECT count(p) FROM Post p where p.commandprocess.id = :id and p.sentiment is not null"), @NamedQuery(name = "Post.totalXSentiment", query = "SELECT count(p) FROM Post p where p.commandprocess.id = :id and p.sentiment = :sentiment"), @NamedQuery(name = "Post.sentimentXCategory", query = "SELECT new co.com.zien.xenxerobot.persistence.entity.ElementSentimentDTO (p.topic, ((SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic and p1.sentiment='P')*1.00/(SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic))*100, ((SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic and p1.sentiment='N')*1.00/(SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic))*100, ((SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic and p1.sentiment='NEU')*1.00/(SELECT count(p1.topic) FROM co.com.zien.xenxerobot.persistence.entity.Post p1 where p1.commandprocess.id = :id and p1.topic = p.topic))*100) FROM co.com.zien.xenxerobot.persistence.entity.Post p where p.commandprocess.id = :id and p.topic is not null GROUP BY p.topic"), @NamedQuery(name = "Post.topfrecuencyXconceptType", query = "SELECT c.name, count(c.name) as total FROM Post p INNER JOIN p.conceptsCollection c where p.commandprocess.id = :id and c.conceptType = :type GROUP BY c.name ORDER BY total DESC, c.name"), @NamedQuery(name = "Post.sentimentXbyConceptName", query = "SELECT new co.com.zien.xenxerobot.persistence.entity.ElementSentimentDTO (c.name, (SELECT count(c1.machineLearningSentiment) FROM Post p1 INNER JOIN p1.conceptsCollection c1 where p1.commandprocess.id = :id and c1.conceptType = :type and c1.name = :name and c1.machineLearningSentiment='P')*1.00/(count(c.machineLearningSentiment)), (SELECT count(c1.machineLearningSentiment) FROM Post p1 INNER JOIN p1.conceptsCollection c1 where p1.commandprocess.id = :id and c1.conceptType = :type and c1.name = :name and c1.machineLearningSentiment='N')*1.00/(count(c.machineLearningSentiment)), (SELECT count(c1.machineLearningSentiment) FROM Post p1 INNER JOIN p1.conceptsCollection c1 where p1.commandprocess.id = :id and c1.conceptType = :type and c1.name = :name and c1.machineLearningSentiment='NEU')*1.00/(count(c.machineLearningSentiment))) FROM Post p INNER JOIN p.conceptsCollection c where p.commandprocess.id = :id and c.conceptType = :type and c.name = :name GROUP BY c.name") })
public class Post implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "idpost")
    private String idpost;
    @Column(name = "languagepost")
    private String languagepost;
    @Column(name = "loaddate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loaddate;
    @Column(name = "postdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date postdate;
    @Column(name = "originalmessage")
    private String originalmessage;
    @Column(name = "userpost")
    private String userpost;
    @Column(name = "formattedmessage")
    private String formattedmessage;
    @Column(name = "topic")
    private String topic;
    @Column(name = "sentiment")
    private String sentiment;
    @Column(name = "HUMAN_SENTIMENT")
    private String humanSentiment;
    @Column(name = "HUMAN_CATEGORY")
    private String humanCategory;
    @Column(name = "WORKED")
    private String worked;
    @Column(name = "DISCARTEDTOPACK")
    private String discartedtopack;
    @Column(name = "OPTIONTOCHANGE")
    private String optiontochange;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idcomandproccess")
    private CommandProcess commandprocess;
    @JoinColumn(name = "ID_PACKAGE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Packages idPackage;
    @OneToMany(mappedBy = "idPost")
    private Collection<Concepts> conceptsCollection;
    
    public String getTopic() {
        return this.topic;
    }
    
    public void setTopic(final String topic) {
        this.topic = topic;
    }
    
    public String getSentiment() {
        return this.sentiment;
    }
    
    public void setSentiment(final String sentiment) {
        this.sentiment = sentiment;
    }
    
    public CommandProcess getCommandprocess() {
        return this.commandprocess;
    }
    
    public void setCommandprocess(final CommandProcess commandprocess) {
        this.commandprocess = commandprocess;
    }
    
    public String getIdpost() {
        return this.idpost;
    }
    
    public void setIdpost(final String idpost) {
        this.idpost = idpost;
    }
    
    public String getLanguagepost() {
        return this.languagepost;
    }
    
    public void setLanguagepost(final String languagepost) {
        this.languagepost = languagepost;
    }
    
    public Date getLoaddate() {
        return this.loaddate;
    }
    
    public void setLoaddate(final Date loaddate) {
        this.loaddate = loaddate;
    }
    
    public Date getPostdate() {
        return this.postdate;
    }
    
    public void setPostdate(final Date postdate) {
        this.postdate = postdate;
    }
    
    public String getOriginalmessage() {
        return this.originalmessage;
    }
    
    public void setOriginalmessage(final String originalmessage) {
        this.originalmessage = originalmessage;
    }
    
    public String getUserpost() {
        return this.userpost;
    }
    
    public void setUserpost(final String userpost) {
        this.userpost = userpost;
    }
    
    public String getFormattedmessage() {
        return this.formattedmessage;
    }
    
    public void setFormattedmessage(final String formattedmessage) {
        this.formattedmessage = formattedmessage;
    }
    
    public Packages getIdPackage() {
        return this.idPackage;
    }
    
    public void setIdPackage(final Packages idPackage) {
        this.idPackage = idPackage;
    }
    
    public Collection<Concepts> getConceptsCollection() {
        return this.conceptsCollection;
    }
    
    public void setConceptsCollection(final Collection<Concepts> conceptsCollection) {
        this.conceptsCollection = conceptsCollection;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.idpost != null) ? this.idpost.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Post)) {
            return false;
        }
        final Post other = (Post)object;
        return (this.idpost != null || other.idpost == null) && (this.idpost == null || this.idpost.equals(other.idpost));
    }
    
    public String toString() {
        return "javaapplication1.Post[ idpost=" + this.idpost + " ]";
    }
    
    public String getHumanSentiment() {
        return this.humanSentiment;
    }
    
    public void setHumanSentiment(final String humanSentiment) {
        this.humanSentiment = humanSentiment;
    }
    
    public String getHumanCategory() {
        return this.humanCategory;
    }
    
    public void setHumanCategory(final String humanCategory) {
        this.humanCategory = humanCategory;
    }
    
    public String getWorked() {
        return this.worked;
    }
    
    public void setWorked(final String worked) {
        this.worked = worked;
    }
    
    public String getDiscartedtopack() {
        return this.discartedtopack;
    }
    
    public void setDiscartedtopack(final String discartedtopack) {
        this.discartedtopack = discartedtopack;
    }
    
    public String getOptiontochange() {
        return this.optiontochange;
    }
    
    public void setOptiontochange(final String optiontochange) {
        this.optiontochange = optiontochange;
    }
}
