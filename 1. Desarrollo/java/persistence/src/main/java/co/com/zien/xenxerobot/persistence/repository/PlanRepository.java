package co.com.zien.xenxerobot.persistence.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface PlanRepository extends JpaRepository<Plan, String>
{
	
	@Query(name = "Plan.findByIdAndProductid")
    Plan findByIdAndProductid(@Param("planid") final String planid, @Param("productid") final String productid);
}
