package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.jpa.repository.*;

public interface UserRepository extends JpaRepository<Users, String>
{
    @Query(name = "Users.isUserActivebyEmail")
    Users isUserActivebyEmail(@Param("email") final String p0);
    
    @Query(name = "Users.isUserActivebyChatid")
    Users isUserActivebyChatid(@Param("chatid") final String p0);
    
    @Query(name = "Users.isUserActivebyMobilPhone")
    Users isUserActivebyMobilPhone(@Param("mobilphone") final String p0);
}
