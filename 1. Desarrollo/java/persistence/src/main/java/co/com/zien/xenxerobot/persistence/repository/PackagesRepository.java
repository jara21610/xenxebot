package co.com.zien.xenxerobot.persistence.repository;

import java.math.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface PackagesRepository extends JpaRepository<Packages, BigDecimal>
{
    @Query(name = "Packages.findByWorker")
    List<Packages> findByWorker(@Param("worker") final Worker p0);
    
    @Query(name = "Packages.findByState")
    List<Packages> findByState(@Param("state") final String p0);
}
