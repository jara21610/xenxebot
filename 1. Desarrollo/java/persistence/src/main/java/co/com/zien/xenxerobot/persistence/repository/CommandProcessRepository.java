package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface CommandProcessRepository extends JpaRepository<CommandProcess, String>
{
    @Query(name = "CommandProcess.findMyRunningProcess")
    List<CommandProcess> findMyRunningProcess(@Param("state") final String p0, @Param("chatid") final String p1,  @Param("productid") final String p2);
}
