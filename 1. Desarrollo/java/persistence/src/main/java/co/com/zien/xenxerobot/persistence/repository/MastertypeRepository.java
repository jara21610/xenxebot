package co.com.zien.xenxerobot.persistence.repository;

import org.springframework.data.jpa.repository.*;
import co.com.zien.xenxerobot.persistence.entity.*;

public interface MastertypeRepository extends JpaRepository<Mastertype, Integer>
{
}
