package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "WORKER")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Worker.findAll", query = "SELECT w FROM Worker w"), @NamedQuery(name = "Worker.findById", query = "SELECT w FROM Worker w WHERE w.id = :id"), @NamedQuery(name = "Worker.findByName", query = "SELECT w FROM Worker w WHERE w.name = :name"), @NamedQuery(name = "Worker.findByLastname", query = "SELECT w FROM Worker w WHERE w.lastname = :lastname"), @NamedQuery(name = "Worker.findByEmail", query = "SELECT w FROM Worker w WHERE w.email = :email"), @NamedQuery(name = "Worker.findByPassword", query = "SELECT w FROM Worker w WHERE w.password = :password") })
public class Worker implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private String id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LASTNAME")
    private String lastname;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PASSWORD")
    private String password;
    @OneToMany(mappedBy = "idWorker")
    private Collection<Packages> packagesCollection;
    
    public Worker() {
    }
    
    public Worker(final String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getLastname() {
        return this.lastname;
    }
    
    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @XmlTransient
    public Collection<Packages> getPackagesCollection() {
        return this.packagesCollection;
    }
    
    public void setPackagesCollection(final Collection<Packages> packagesCollection) {
        this.packagesCollection = packagesCollection;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Worker)) {
            return false;
        }
        final Worker other = (Worker)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication1.Worker[ id=" + this.id + " ]";
    }
}
