package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.persistence.*;

@Embeddable
public class MastervaluePK implements Serializable
{
    @Basic(optional = false)
    @Column(name = "idmastertype")
    private int idmastertype;
    @Basic(optional = false)
    @Column(name = "value")
    private String value;
    
    public MastervaluePK() {
    }
    
    public MastervaluePK(final int idmastertype, final String value) {
        this.idmastertype = idmastertype;
        this.value = value;
    }
    
    public int getIdmastertype() {
        return this.idmastertype;
    }
    
    public void setIdmastertype(final int idmastertype) {
        this.idmastertype = idmastertype;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += this.idmastertype;
        hash += ((this.value != null) ? this.value.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof MastervaluePK)) {
            return false;
        }
        final MastervaluePK other = (MastervaluePK)object;
        return this.idmastertype == other.idmastertype && (this.value != null || other.value == null) && (this.value == null || this.value.equals(other.value));
    }
    
    public String toString() {
        return "javaapplication1.MastervaluePK[ idmastertype=" + this.idmastertype + ", value=" + this.value + " ]";
    }
}
