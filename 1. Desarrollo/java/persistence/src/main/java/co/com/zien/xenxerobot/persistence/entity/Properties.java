package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

@Entity
@Table(name = "PROPERTIES")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Properties.findAll", query = "SELECT p FROM Properties p"), @NamedQuery(name = "Properties.findByName", query = "SELECT p.value FROM Properties p WHERE p.name = :name"), @NamedQuery(name = "Properties.findByValue", query = "SELECT p FROM Properties p WHERE p.value = :value") })
public class Properties implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "value")
    private String value;
    
    public Properties() {
    }
    
    public Properties(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.name != null) ? this.name.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Properties)) {
            return false;
        }
        final Properties other = (Properties)object;
        return (this.name != null || other.name == null) && (this.name == null || this.name.equals(other.name));
    }
    
    public String toString() {
        return "javaapplication1.Properties[ name=" + this.name + " ]";
    }
}
