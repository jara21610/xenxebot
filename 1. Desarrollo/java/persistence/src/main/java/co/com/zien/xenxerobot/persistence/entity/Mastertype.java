package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "MASTERTYPE")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Mastertype.findAll", query = "SELECT m FROM Mastertype m"), @NamedQuery(name = "Mastertype.findById", query = "SELECT m FROM Mastertype m WHERE m.id = :id"), @NamedQuery(name = "Mastertype.findByName", query = "SELECT m FROM Mastertype m WHERE m.name = :name") })
public class Mastertype implements Serializable
{
    @OneToMany(cascade = { CascadeType.ALL }, mappedBy = "mastertype")
    private Collection<Mastervalue> mastervalueCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    
    public Mastertype() {
    }
    
    public Mastertype(final Integer id) {
        this.id = id;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Mastertype)) {
            return false;
        }
        final Mastertype other = (Mastertype)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication1.Mastertype[ id=" + this.id + " ]";
    }
    
    @XmlTransient
    public Collection<Mastervalue> getMastervalueCollection() {
        return this.mastervalueCollection;
    }
    
    public void setMastervalueCollection(final Collection<Mastervalue> mastervalueCollection) {
        this.mastervalueCollection = mastervalueCollection;
    }
}
