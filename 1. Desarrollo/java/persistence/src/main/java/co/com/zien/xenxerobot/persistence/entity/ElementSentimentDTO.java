package co.com.zien.xenxerobot.persistence.entity;

import java.math.BigDecimal;

public class ElementSentimentDTO
{
    private String name;
    private BigDecimal positivo;
    private BigDecimal negativo;
    private BigDecimal neutral;
    private BigDecimal maxsentiment;    

	public ElementSentimentDTO(final String name, final double positivo, final double negativo, final double neutral) {
        this.name = name;
        this.positivo = new BigDecimal(positivo);
        this.negativo = new BigDecimal(negativo);
        this.neutral = new BigDecimal(neutral);
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public BigDecimal getPositivo() {
        return this.positivo;
    }
    
    public void setPositivo(final BigDecimal positivo) {
        this.positivo = positivo;
    }
    
    public BigDecimal getNegativo() {
        return this.negativo;
    }
    
    public void setNegativo(final BigDecimal negativo) {
        this.negativo = negativo;
    }
    
    public BigDecimal getNeutral() {
        return this.neutral;
    }
    
    public void setNeutral(final BigDecimal neutral) {
        this.neutral = neutral;
    }
    

    public BigDecimal getMaxsentiment() {
		return maxsentiment;
	}

	public void setMaxsentiment(BigDecimal maxsentiment) {
		this.maxsentiment = maxsentiment;
	}
}
