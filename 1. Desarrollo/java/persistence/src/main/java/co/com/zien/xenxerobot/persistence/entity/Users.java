package co.com.zien.xenxerobot.persistence.entity;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u WHERE u.state = 'activo'"), @NamedQuery(name = "Users.isUserActivebyEmail", query = "SELECT u FROM Users u WHERE u.email = :email AND u.state = 'activo'"), @NamedQuery(name = "Users.isUserActivebyMobilPhone", query = "SELECT u FROM Users u WHERE u.mobilphone = :mobilphone AND u.state = 'activo'"), @NamedQuery(name = "Users.isUserActivebyChatid", query = "SELECT u FROM Users u WHERE u.chatid = :chatid AND u.state = 'activo'") })
public class Users
{
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "middlename")
    private String middlename;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "email")
    private String email;
    @Column(name = "mobilphone")
    private String mobilphone;
    @Column(name = "state")
    private String state;
    @Column(name = "chatid")
    private String chatid;
    @Column(name = "texttestlicence")
    private String texttestlicence;
    @Column(name = "testlicenceacepted")
    private String testlicenceacepted;
    @Column(name = "facebooktoken")
    private String facebooktoken;    
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "user")
    private Set<CommandProcess> commandprocesses;
    @OneToMany(cascade = { CascadeType.ALL }, mappedBy = "iduser")
    private Collection<Userplan> userplanCollection;
    @OneToMany(mappedBy = "iduser")
    private Collection<Usertoken> usertokenCollection;
    @OneToMany(mappedBy = "userid")
    private Collection<Paymentmethod> paymentmethodCollection;
    
    public Users() {
        this.commandprocesses = new HashSet<CommandProcess>();
    }
    
    public String getChatid() {
        return this.chatid;
    }
    
    public void setChatid(final String chatid) {
        this.chatid = chatid;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public Set<CommandProcess> getCommandprocesses() {
        return this.commandprocesses;
    }
    
    public void setCommandprocesses(final Set<CommandProcess> commandprocesses) {
        this.commandprocesses = commandprocesses;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getMiddlename() {
        return this.middlename;
    }
    
    public void setMiddlename(final String middlename) {
        this.middlename = middlename;
    }
    
    public String getLastname() {
        return this.lastname;
    }
    
    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getMobilphone() {
        return this.mobilphone;
    }
    
    public void setMobilphone(final String mobilphone) {
        this.mobilphone = mobilphone;
    }
    
    public String getTexttestlicence() {
        return this.texttestlicence;
    }
    
    public void setTexttestlicence(final String texttestlicence) {
        this.texttestlicence = texttestlicence;
    }
    
    public String getTestlicenceacepted() {
        return this.testlicenceacepted;
    }
    
    public void setTestlicenceacepted(final String testlicenceacepted) {
        this.testlicenceacepted = testlicenceacepted;
    }
    
    public Collection<Userplan> getUserplanCollection() {
        return this.userplanCollection;
    }
    
    public void setUserplanCollection(final Collection<Userplan> userplanCollection) {
        this.userplanCollection = userplanCollection;
    }
    
    public Collection<Usertoken> getUsertokenCollection() {
        return this.usertokenCollection;
    }
    
    public void setUsertokenCollection(final Collection<Usertoken> usertokenCollection) {
        this.usertokenCollection = usertokenCollection;
    }
    
    public Collection<Paymentmethod> getPaymentmethodCollection() {
        return this.paymentmethodCollection;
    }
    
    public void setPaymentmethodCollection(final Collection<Paymentmethod> paymentmethodCollection) {
        this.paymentmethodCollection = paymentmethodCollection;
    }
    
    public String getFacebooktoken() {
		return facebooktoken;
	}

	public void setFacebooktoken(String facebooktoken) {
		this.facebooktoken = facebooktoken;
	}
}
