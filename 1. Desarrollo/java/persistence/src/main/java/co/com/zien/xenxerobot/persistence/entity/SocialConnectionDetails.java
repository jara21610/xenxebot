package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import javax.persistence.*;

@Entity
@Table(name = "SOCIALCONECCTIONDETAILS")
@XmlRootElement
public class SocialConnectionDetails implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "keydata1")
    private String keydata1;
    @Column(name = "keydata2")
    private String keydata2;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "socialname")
    private SocialConnection socialconnection;
    
    public String getKeydata1() {
        return this.keydata1;
    }
    
    public void setKeydata1(final String keydata1) {
        this.keydata1 = keydata1;
    }
    
    public String getKeydata2() {
        return this.keydata2;
    }
    
    public void setKeydata2(final String keydata2) {
        this.keydata2 = keydata2;
    }
    
    public SocialConnection getSocialconnection() {
        return this.socialconnection;
    }
    
    public void setSocialconnection(final SocialConnection socialconnection) {
        this.socialconnection = socialconnection;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
}
