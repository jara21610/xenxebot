package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.jpa.repository.*;

public interface PaymentmethodRepository extends JpaRepository<Paymentmethod, String>
{
    @Query(name = "Paymentmethod.findPrederminateMethod")
    Paymentmethod findPrederminateMethod(@Param("userid") final String p0);
}
