package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import javax.xml.bind.annotation.*;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "PAYMENTHISTORY")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Paymenthistory.findAll", query = "SELECT p FROM Paymenthistory p"), @NamedQuery(name = "Paymenthistory.findById", query = "SELECT p FROM Paymenthistory p WHERE p.id = :id"), @NamedQuery(name = "Paymenthistory.findByStatus", query = "SELECT p FROM Paymenthistory p WHERE p.status = :status"), @NamedQuery(name = "Paymenthistory.findByPaymentdate", query = "SELECT p FROM Paymenthistory p WHERE p.paymentdate = :paymentdate"), @NamedQuery(name = "Paymenthistory.findByDetail", query = "SELECT p FROM Paymenthistory p WHERE p.detail = :detail") })
public class Paymenthistory implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private String id;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "PAYMENTDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentdate;
    @Column(name = "DETAIL")
    private String detail;
    @JoinColumn(name = "PAYMENTMETHODID", referencedColumnName = "ID")
    @ManyToOne
    private Paymentmethod paymentmethodid;
    @JoinColumn(name = "USERPLANID", referencedColumnName = "ID")
    @ManyToOne
    private Userplan userplanid;
    
    public Paymenthistory() {
    }
    
    public Paymenthistory(final String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(final String status) {
        this.status = status;
    }
    
    public Date getPaymentdate() {
        return this.paymentdate;
    }
    
    public void setPaymentdate(final Date paymentdate) {
        this.paymentdate = paymentdate;
    }
    
    public String getDetail() {
        return this.detail;
    }
    
    public void setDetail(final String detail) {
        this.detail = detail;
    }
    
    public Paymentmethod getPaymentmethodid() {
        return this.paymentmethodid;
    }
    
    public void setPaymentmethodid(final Paymentmethod paymentmethodid) {
        this.paymentmethodid = paymentmethodid;
    }
    
    public Userplan getUserplanid() {
        return this.userplanid;
    }
    
    public void setUserplanid(final Userplan userplanid) {
        this.userplanid = userplanid;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Paymenthistory)) {
            return false;
        }
        final Paymenthistory other = (Paymenthistory)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication2.Paymenthistory[ id=" + this.id + " ]";
    }
}
