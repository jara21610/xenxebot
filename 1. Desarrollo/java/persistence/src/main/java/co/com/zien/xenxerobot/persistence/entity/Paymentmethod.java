package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "PAYMENTMETHOD")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Paymentmethod.findAll", query = "SELECT p FROM Paymentmethod p"), @NamedQuery(name = "Paymentmethod.findById", query = "SELECT p FROM Paymentmethod p WHERE p.id = :id"), @NamedQuery(name = "Paymentmethod.findPrederminateMethod", query = "SELECT p FROM Paymentmethod p WHERE p.userid.id = :userid and p.predeterminate = 'Y'"), @NamedQuery(name = "Paymentmethod.findByCreditcardtoken", query = "SELECT p FROM Paymentmethod p WHERE p.creditcardtoken = :creditcardtoken") })
public class Paymentmethod implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private String id;
    @Column(name = "CREDITCARDTOKEN")
    private String creditcardtoken;
    @Column(name = "PAYMENTMETHOD")
    private String paymentmethod;
    @Column(name = "MASKEDNUMBER")
    private String maskednumber;
    @Column(name = "CARDNAME")
    private String cardname;
    @Column(name = "DNINUMBER")
    private String dninumber;
    @Column(name = "PREDETERMINATE")
    private String predeterminate;
    @JoinColumn(name = "USERID", referencedColumnName = "ID")
    @ManyToOne
    private Users userid;
    @OneToMany(mappedBy = "paymentmethodid")
    private Collection<Paymenthistory> paymenthistoryCollection;
    
    public Paymentmethod() {
    }
    
    public Paymentmethod(final String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getCreditcardtoken() {
        return this.creditcardtoken;
    }
    
    public void setCreditcardtoken(final String creditcardtoken) {
        this.creditcardtoken = creditcardtoken;
    }
    
    public Users getUserid() {
        return this.userid;
    }
    
    public void setUserid(final Users userid) {
        this.userid = userid;
    }
    
    @XmlTransient
    public Collection<Paymenthistory> getPaymenthistoryCollection() {
        return this.paymenthistoryCollection;
    }
    
    public void setPaymenthistoryCollection(final Collection<Paymenthistory> paymenthistoryCollection) {
        this.paymenthistoryCollection = paymenthistoryCollection;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Paymentmethod)) {
            return false;
        }
        final Paymentmethod other = (Paymentmethod)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication2.Paymentmethod[ id=" + this.id + " ]";
    }
    
    public String getPaymentmethod() {
        return this.paymentmethod;
    }
    
    public void setPaymentmethod(final String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }
    
    public String getMaskednumber() {
        return this.maskednumber;
    }
    
    public void setMaskednumber(final String maskednumber) {
        this.maskednumber = maskednumber;
    }
    
    public String getCardname() {
        return this.cardname;
    }
    
    public void setCardname(final String cardname) {
        this.cardname = cardname;
    }
    
    public String getDninumber() {
        return this.dninumber;
    }
    
    public void setDninumber(final String dninumber) {
        this.dninumber = dninumber;
    }
    
    public String getPredeterminate() {
        return this.predeterminate;
    }
    
    public void setPredeterminate(final String predeterminate) {
        this.predeterminate = predeterminate;
    }
}
