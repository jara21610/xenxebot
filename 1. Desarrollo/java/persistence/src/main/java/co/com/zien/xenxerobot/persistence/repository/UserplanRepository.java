package co.com.zien.xenxerobot.persistence.repository;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.domain.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface UserplanRepository extends JpaRepository<Userplan, String>
{
    @Query(name = "Userplan.historicByUserid")
    List<Userplan> historicByUserid(@Param("userid") final String p0, @Param("productid") final String p1,  final Pageable p2);
    
    @Query(name = "Userplan.activePlanByUseridandstate")
    Userplan activePlanByUseridandstate(@Param("userid") final String p0, @Param("state") final String p1, @Param("productid") final String p2);
    
    @Query(name = "Userplan.findPlansbyProductid")
    Userplan findPlansbyProductid(@Param("productid") final String p0, @Param("userid") final String p1);
    
    @Query(name = "Userplan.findPlanstoFinish")
    List<Userplan> findPlanstoFinish(final Pageable p1);
}
