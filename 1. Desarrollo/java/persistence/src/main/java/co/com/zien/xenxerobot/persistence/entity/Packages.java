package co.com.zien.xenxerobot.persistence.entity;

import java.io.*;
import java.math.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "PACKAGES")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Packages.findAll", query = "SELECT p FROM Packages p"), @NamedQuery(name = "Packages.findById", query = "SELECT p FROM Packages p WHERE p.id = :id"), @NamedQuery(name = "Packages.findByName", query = "SELECT p FROM Packages p WHERE p.name = :name"), @NamedQuery(name = "Packages.findByState", query = "SELECT p FROM Packages p WHERE p.state = :state"), @NamedQuery(name = "Packages.findByWorker", query = "SELECT p FROM Packages p WHERE p.idWorker = :worker") })
public class Packages implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "STATE")
    private String state;
    @OneToMany(mappedBy = "idPackage")
    private Collection<Post> postCollection;
    @JoinColumn(name = "ID_WORKER", referencedColumnName = "ID")
    @ManyToOne
    private Worker idWorker;
    
    public Packages() {
    }
    
    public Packages(final BigDecimal id) {
        this.id = id;
    }
    
    public BigDecimal getId() {
        return this.id;
    }
    
    public void setId(final BigDecimal id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    @XmlTransient
    public Collection<Post> getPostCollection() {
        return this.postCollection;
    }
    
    public void setPostCollection(final Collection<Post> postCollection) {
        this.postCollection = postCollection;
    }
    
    public Worker getIdWorker() {
        return this.idWorker;
    }
    
    public void setIdWorker(final Worker idWorker) {
        this.idWorker = idWorker;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(final Object object) {
        if (!(object instanceof Packages)) {
            return false;
        }
        final Packages other = (Packages)object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }
    
    public String toString() {
        return "javaapplication1.Packages[ id=" + this.id + " ]";
    }
}
