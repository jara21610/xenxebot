/*    */ package co.com.zien.xenxerobot.frontend.controllers;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.services.PlanManagementService;
/*    */ import co.com.zien.xenxerobot.backend.services.TokenService;
/*    */ import co.com.zien.xenxerobot.backend.services.UpgradePlanDTO;
/*    */ import co.com.zien.xenxerobot.persistence.repository.PlanRepository;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.web.bind.annotation.CrossOrigin;
/*    */ import org.springframework.web.bind.annotation.RequestBody;
/*    */ import org.springframework.web.bind.annotation.RequestMapping;
/*    */ import org.springframework.web.bind.annotation.RestController;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @RestController
/*    */ @RequestMapping({"/planmanagement"})
/*    */ public class PlanManagementServiceController
/*    */ {
/*    */   @Autowired
/*    */   TokenService ts;
/*    */   @Autowired
/*    */   PlanManagementService pms;
/*    */   @Autowired
/*    */   UserplanRepository upr;
/*    */   @Autowired
/*    */   PlanRepository planr;
/*    */   @Autowired
/*    */   UserRepository ur;
/*    */   
/*    */   @CrossOrigin(methods={org.springframework.web.bind.annotation.RequestMethod.POST})
/*    */   @RequestMapping({"/upgradePlanWithPaymentMethod"})
/*    */   public ResponseDTO upgradePlanWithPaymentMethod(@RequestBody UpgradePlanDTO updto)
/*    */   {
/* 40 */     ResponseDTO response = new ResponseDTO();
/*    */     try
/*    */     {
/* 43 */       this.pms.upgradePlanWithPaymentMethod(updto);
/*    */       
/* 45 */       response.setCode("0");
/* 46 */       response.setMessage("SUCCESS");
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 50 */       response.setCode("-1");
/* 51 */       response.setMessage("Error en la aplicación, vuelva a intentar más tarde o contacte al administrador");
/*    */     }
/* 53 */     return response;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\PlanManagementServiceController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */