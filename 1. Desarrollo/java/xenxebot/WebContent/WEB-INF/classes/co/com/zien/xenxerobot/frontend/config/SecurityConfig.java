/*    */ package co.com.zien.xenxerobot.frontend.config;
/*    */ 
/*    */ import co.com.zien.xenxerobot.security.UserRealm;
/*    */ import org.apache.shiro.authc.credential.CredentialsMatcher;
/*    */ import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
/*    */ import org.apache.shiro.cache.CacheManager;
/*    */ import org.apache.shiro.cache.MemoryConstrainedCacheManager;
/*    */ import org.apache.shiro.spring.LifecycleBeanPostProcessor;
/*    */ import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
/*    */ import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
/*    */ import org.apache.shiro.web.mgt.WebSecurityManager;
/*    */ import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
/*    */ import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
/*    */ import org.springframework.context.annotation.Bean;
/*    */ import org.springframework.context.annotation.Configuration;
/*    */ import org.springframework.context.annotation.DependsOn;
/*    */ 
/*    */ 
/*    */ @Configuration
/*    */ public class SecurityConfig
/*    */ {
/*    */   @Bean
/*    */   public CredentialsMatcher credentialsMatcher()
/*    */   {
/* 25 */     HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
/* 26 */     matcher.setHashAlgorithmName("SHA-256");
/* 27 */     matcher.setHashIterations(2);
/* 28 */     return matcher;
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public UserRealm customSecurityRealm() {
/* 33 */     UserRealm ur = new UserRealm();
/* 34 */     ur.setCredentialsMatcher(credentialsMatcher());
/* 35 */     return ur;
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public WebSecurityManager securityManager() {
/* 40 */     DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
/* 41 */     securityManager.setRealm(customSecurityRealm());
/* 42 */     securityManager.setCacheManager(cacheManager());
/* 43 */     return securityManager;
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public CacheManager cacheManager()
/*    */   {
/* 49 */     MemoryConstrainedCacheManager cacheManager = new MemoryConstrainedCacheManager();
/* 50 */     return cacheManager;
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
/* 55 */     return new LifecycleBeanPostProcessor();
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public MethodInvokingFactoryBean methodInvokingFactoryBean() {
/* 60 */     MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
/* 61 */     methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
/* 62 */     methodInvokingFactoryBean.setArguments(new Object[] { securityManager() });
/* 63 */     return methodInvokingFactoryBean;
/*    */   }
/*    */   
/*    */   @Bean
/*    */   @DependsOn({"lifecycleBeanPostProcessor"})
/*    */   public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
/* 69 */     return new DefaultAdvisorAutoProxyCreator();
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
/* 74 */     AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
/* 75 */     authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
/* 76 */     return authorizationAttributeSourceAdvisor;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\config\SecurityConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */