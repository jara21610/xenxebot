/*    */ package co.com.zien.xenxerobot.audit;
/*    */ 
/*    */ import javax.servlet.ServletRequest;
/*    */ import org.apache.shiro.SecurityUtils;
/*    */ import org.apache.shiro.subject.Subject;
/*    */ import org.audit4j.core.MetaData;
/*    */ import org.springframework.web.context.request.RequestContextHolder;
/*    */ import org.springframework.web.context.request.ServletRequestAttributes;
/*    */ 
/*    */ public class AuditMetaData implements MetaData
/*    */ {
/*    */   private static final long serialVersionUID = 7243065407615627372L;
/*    */   
/*    */   public String getOrigin()
/*    */   {
/*    */     try
/*    */     {
/* 18 */       return ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
/*    */     } catch (Exception e) {
/* 20 */       e.printStackTrace();
/*    */     }
/* 22 */     return "unidentified";
/*    */   }
/*    */   
/*    */   public String getActor()
/*    */   {
/* 27 */     Subject currentUser = SecurityUtils.getSubject();
/* 28 */     if (currentUser != null) {
/* 29 */       return currentUser.getPrincipal().toString();
/*    */     }
/* 31 */     return "system";
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\audit\AuditMetaData.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */