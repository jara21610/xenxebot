/*    */ package co.com.zien.xenxerobot.frontend.controllers;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.config.Config;
/*    */ import co.com.zien.xenxerobot.backend.services.BotLauncher;
/*    */ import javax.servlet.ServletConfig;
/*    */ import javax.servlet.ServletException;
/*    */ import javax.servlet.annotation.WebServlet;
/*    */ import javax.servlet.http.HttpServlet;
/*    */ import org.springframework.context.ApplicationContext;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @WebServlet(value={"/ProcessLauncher"}, loadOnStartup=0)
/*    */ public class ProcessLauncher
/*    */   extends HttpServlet
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   
/*    */   public void init(ServletConfig config)
/*    */     throws ServletException
/*    */   {
/* 36 */     BotLauncher bl = (BotLauncher)Config.getContext().getBean(BotLauncher.class);
/* 37 */     bl.launchBots();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\ProcessLauncher.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */