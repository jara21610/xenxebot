/*    */ package co.com.zien.xenxerobot.frontend.config;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.config.Config;
/*    */ import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
/*    */ import org.apache.shiro.web.mgt.WebSecurityManager;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.annotation.Bean;
/*    */ import org.springframework.context.annotation.ComponentScan;
/*    */ import org.springframework.context.annotation.Configuration;
/*    */ import org.springframework.context.annotation.Import;
/*    */ import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
/*    */ import org.springframework.web.servlet.config.annotation.EnableWebMvc;
/*    */ import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Configuration
/*    */ @ComponentScan({"co.com.zien.xenxerobot.frontend.controllers"})
/*    */ @EnableWebMvc
/*    */ @Import({Config.class})
/*    */ public class WebConfig
/*    */   extends WebMvcConfigurerAdapter
/*    */ {
/*    */   @Autowired
/*    */   private WebSecurityManager securityManager;
/*    */   
/*    */   public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
/*    */   {
/* 38 */     configurer.enable();
/*    */   }
/*    */   
/*    */   @Bean
/*    */   public ShiroFilterFactoryBean shiroFilterBean() {
/* 43 */     ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
/* 44 */     shiroFilter.setLoginUrl("/ui/security/login.xhtml");
/* 45 */     shiroFilter.setSecurityManager(this.securityManager);
/* 46 */     return shiroFilter;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\config\WebConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */