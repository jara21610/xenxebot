/*    */ package co.com.zien.xenxerobot.frontend.controllers;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.services.RegisterUserDTO;
/*    */ import co.com.zien.xenxerobot.backend.services.RegistryService;
/*    */ import co.com.zien.xenxerobot.backend.services.UserAlreadyExistException;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.web.bind.annotation.CrossOrigin;
/*    */ import org.springframework.web.bind.annotation.RequestBody;
/*    */ import org.springframework.web.bind.annotation.RequestMapping;
/*    */ import org.springframework.web.bind.annotation.RestController;
/*    */ 
/*    */ 
/*    */ 
/*    */ @RestController
/*    */ @RequestMapping({"/registry"})
/*    */ public class RegistryServiceController
/*    */ {
/*    */   @Autowired
/*    */   RegistryService rs;
/*    */   
/*    */   @CrossOrigin(methods={org.springframework.web.bind.annotation.RequestMethod.POST})
/*    */   @RequestMapping({"/getUserLicence"})
/*    */   public LicenceDTO getUserLicence()
/*    */   {
/* 25 */     LicenceDTO license = new LicenceDTO();
/* 26 */     license.setLicense(this.rs.getUserLicence());
/* 27 */     return license;
/*    */   }
/*    */   
/*    */   @CrossOrigin(methods={org.springframework.web.bind.annotation.RequestMethod.POST})
/*    */   @RequestMapping({"/registryUser"})
/*    */   public ResponseDTO registryUser(@RequestBody RegisterUserDTO dto)
/*    */   {
/* 34 */     ResponseDTO response = new ResponseDTO();
/*    */     try
/*    */     {
/* 37 */       this.rs.registerUser(dto);
/*    */       
/* 39 */       response.setCode("0");
/*    */     } catch (UserAlreadyExistException e) {
/* 41 */       response.setCode("-1");
/* 42 */       response.setMessage("Usted ya posee una cuenta registrada");
/*    */     } catch (Exception e) {
/* 44 */       response.setCode("-2");
/* 45 */       response.setMessage("Ha ocurrido en la  aplicación en este momento no podemos atenderlo");
/*    */     }
/*    */     
/* 48 */     return response;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\RegistryServiceController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */