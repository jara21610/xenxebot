/*    */ package co.com.zien.xenxerobot.frontend.config;
/*    */ 
/*    */ import co.com.zien.xenxerobot.audit.AuditMetaData;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import org.audit4j.core.handler.ConsoleAuditHandler;
/*    */ import org.audit4j.core.handler.Handler;
/*    */ import org.audit4j.core.layout.SimpleLayout;
/*    */ import org.audit4j.handler.db.DatabaseAuditHandler;
/*    */ import org.audit4j.integration.spring.AuditAspect;
/*    */ import org.audit4j.integration.spring.SpringAudit4jConfig;
/*    */ import org.springframework.context.annotation.Bean;
/*    */ import org.springframework.context.annotation.Configuration;
/*    */ import org.springframework.context.annotation.EnableAspectJAutoProxy;
/*    */ 
/*    */ @Configuration
/*    */ @EnableAspectJAutoProxy
/*    */ public class AuditConfig
/*    */ {
/*    */   @Bean
/*    */   public AuditAspect auditAspect()
/*    */   {
/* 23 */     AuditAspect auditAspect = new AuditAspect();
/* 24 */     return auditAspect;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   @Bean
/*    */   public DatabaseAuditHandler databaseHandler()
/*    */   {
/* 32 */     DatabaseAuditHandler dbHandler = new DatabaseAuditHandler();
/* 33 */     dbHandler.setEmbedded("true");
/* 34 */     return dbHandler;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   @Bean
/*    */   public SpringAudit4jConfig springAudit4jConfig()
/*    */   {
/* 42 */     SpringAudit4jConfig springAudit4jConfig = new SpringAudit4jConfig();
/* 43 */     springAudit4jConfig.setLayout(new SimpleLayout());
/* 44 */     List<Handler> handlers = new ArrayList();
/* 45 */     handlers.add(new ConsoleAuditHandler());
/*    */     
/* 47 */     handlers.add(databaseHandler());
/* 48 */     springAudit4jConfig.setHandlers(handlers);
/* 49 */     springAudit4jConfig.setMetaData(new AuditMetaData());
/* 50 */     return springAudit4jConfig;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\config\AuditConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */