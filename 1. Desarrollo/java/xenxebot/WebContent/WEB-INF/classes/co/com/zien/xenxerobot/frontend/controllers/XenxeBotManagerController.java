/*    */ package co.com.zien.xenxerobot.frontend.controllers;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.services.SocialPoolConectionManager;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.beans.factory.annotation.Qualifier;
/*    */ import org.springframework.web.bind.annotation.PathVariable;
/*    */ import org.springframework.web.bind.annotation.RequestMapping;
/*    */ import org.springframework.web.bind.annotation.RestController;
/*    */ 
/*    */ @RestController
/*    */ @RequestMapping({"/xenxebotmanager"})
/*    */ public class XenxeBotManagerController
/*    */ {
/*    */   @Autowired
/*    */   @Qualifier("TwitterPoolConectionManager")
/*    */   SocialPoolConectionManager tpcm;
/*    */   @Autowired
/*    */   @Qualifier("FacebookPoolConectionManager")
/*    */   SocialPoolConectionManager fpcm;
/*    */   
/*    */   @RequestMapping({"/socialpoolcurrentstate/{socialname}"})
/*    */   public java.util.List<co.com.zien.xenxerobot.backend.services.SocialConnectionDTO> socialpoolmanagercurrentstate(@PathVariable String socialname)
/*    */   {
/* 24 */     if (socialname.equals("TWITTER")) {
/* 25 */       return this.tpcm.getCurrentState();
/*    */     }
/* 27 */     return null;
/*    */   }
/*    */   
/*    */   @RequestMapping({"/socialconectionlimitcurrentstate/{socialname}"})
/*    */   public Object socialconectionlimitcurrentstate(@PathVariable String socialname)
/*    */   {
/* 33 */     if (socialname.equals("TWITTER"))
/* 34 */       return this.tpcm.getSocialServerLimitStatus();
/* 35 */     if (socialname.equals("FACEBOOK")) {
/* 36 */       return this.fpcm.getSocialServerLimitStatus();
/*    */     }
/* 38 */     return null;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\XenxeBotManagerController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */