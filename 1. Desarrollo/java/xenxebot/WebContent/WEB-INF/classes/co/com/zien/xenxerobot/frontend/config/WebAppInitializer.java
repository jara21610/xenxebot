/*    */ package co.com.zien.xenxerobot.frontend.config;
/*    */ 
/*    */ import java.util.EnumSet;
/*    */ import javax.servlet.DispatcherType;
/*    */ import javax.servlet.FilterRegistration.Dynamic;
/*    */ import javax.servlet.ServletContext;
/*    */ import javax.servlet.ServletException;
/*    */ import javax.servlet.ServletRegistration.Dynamic;
/*    */ import org.springframework.web.WebApplicationInitializer;
/*    */ import org.springframework.web.context.ContextLoaderListener;
/*    */ import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
/*    */ import org.springframework.web.filter.DelegatingFilterProxy;
/*    */ import org.springframework.web.servlet.DispatcherServlet;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class WebAppInitializer
/*    */   implements WebApplicationInitializer
/*    */ {
/*    */   public void onStartup(ServletContext servletContext)
/*    */     throws ServletException
/*    */   {
/* 23 */     AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
/* 24 */     rootContext.setServletContext(servletContext);
/* 25 */     rootContext.register(new Class[] { WebConfig.class, SecurityConfig.class });
/*    */     
/*    */ 
/* 28 */     servletContext.addListener(new ContextLoaderListener(rootContext));
/*    */     
/*    */ 
/* 31 */     ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcher", new DispatcherServlet(rootContext));
/* 32 */     dynamic.addMapping(new String[] { "/" });
/* 33 */     dynamic.setLoadOnStartup(1);
/*    */     
/* 35 */     servletContext.addFilter("shiroFilter", new DelegatingFilterProxy("shiroFilterBean", rootContext))
/* 36 */       .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ERROR), false, new String[] { "/*" });
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\config\WebAppInitializer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */