/*    */ package co.com.zien.xenxerobot.frontend.controllers;
/*    */ 
/*    */ public class ResponseDTO
/*    */ {
/*    */   private String code;
/*    */   private String message;
/*    */   
/*    */   public String getCode() {
/*  9 */     return this.code;
/*    */   }
/*    */   
/* 12 */   public void setCode(String code) { this.code = code; }
/*    */   
/*    */   public String getMessage() {
/* 15 */     return this.message;
/*    */   }
/*    */   
/* 18 */   public void setMessage(String message) { this.message = message; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\ResponseDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */