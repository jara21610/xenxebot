/*    */ package co.com.zien.xenxerobot.frontend.controllers.selectworker;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.PackagesStateEnum;
/*    */ import co.com.zien.xenxerobot.backend.services.dataset.PackagesService;
/*    */ import co.com.zien.xenxerobot.backend.services.dataset.WorkerService;
/*    */ import co.com.zien.xenxerobot.frontend.controllers.packagelist.PackageListBean;
/*    */ import co.com.zien.xenxerobot.persistence.entity.Worker;
/*    */ import java.util.List;
/*    */ import javax.faces.bean.ManagedBean;
/*    */ import javax.faces.bean.ManagedProperty;
/*    */ import javax.faces.bean.SessionScoped;
/*    */ import javax.faces.event.ComponentSystemEvent;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @ManagedBean
/*    */ @SessionScoped
/*    */ public class SelectWorkerBean
/*    */ {
/*    */   private List<Worker> workerList;
/*    */   private String selectworker;
/*    */   @ManagedProperty("#{workerService}")
/*    */   private WorkerService workerService;
/*    */   @ManagedProperty("#{packagesService}")
/*    */   private PackagesService packagesService;
/*    */   @ManagedProperty("#{packageListBean}")
/*    */   private PackageListBean packageListBean;
/*    */   private Worker currentworker;
/*    */   
/*    */   public void preRenderView(ComponentSystemEvent event)
/*    */   {
/* 34 */     this.workerList = this.workerService.getavailableWorkers();
/*    */   }
/*    */   
/*    */   public List<Worker> getWorkerList() {
/* 38 */     return this.workerList;
/*    */   }
/*    */   
/*    */   public void setWorkerList(List<Worker> workerList) {
/* 42 */     this.workerList = workerList;
/*    */   }
/*    */   
/*    */   public String getSelectworker() {
/* 46 */     return this.selectworker;
/*    */   }
/*    */   
/*    */   public void setSelectworker(String selectworker) {
/* 50 */     this.selectworker = selectworker;
/*    */   }
/*    */   
/*    */   public WorkerService getWorkerService() {
/* 54 */     return this.workerService;
/*    */   }
/*    */   
/*    */   public void setWorkerService(WorkerService workerService) {
/* 58 */     this.workerService = workerService;
/*    */   }
/*    */   
/*    */   public String getWorkerPakage() {
/* 62 */     this.currentworker = null;
/* 63 */     for (Worker w : this.workerList) {
/* 64 */       if (w.getId().equals(this.selectworker)) {
/* 65 */         this.currentworker = w;
/*    */       }
/*    */     }
/* 68 */     this.packageListBean.setPackageList(this.packagesService.findByWorker(this.currentworker));
/* 69 */     this.packageListBean.getPackageList().addAll(this.packagesService.findByState(PackagesStateEnum.AVAILABLE.getState()));
/* 70 */     return "myavailablepackages.xhtml";
/*    */   }
/*    */   
/*    */   public PackageListBean getPackageListBean() {
/* 74 */     return this.packageListBean;
/*    */   }
/*    */   
/*    */   public void setPackageListBean(PackageListBean packageListBean) {
/* 78 */     this.packageListBean = packageListBean;
/*    */   }
/*    */   
/*    */   public PackagesService getPackagesService() {
/* 82 */     return this.packagesService;
/*    */   }
/*    */   
/*    */   public void setPackagesService(PackagesService packagesService) {
/* 86 */     this.packagesService = packagesService;
/*    */   }
/*    */   
/*    */   public Worker getCurrentworker() {
/* 90 */     return this.currentworker;
/*    */   }
/*    */   
/*    */   public void setCurrentworker(Worker currentworker) {
/* 94 */     this.currentworker = currentworker;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\zien\xenxerobot\frontend\controllers\selectworker\SelectWorkerBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */