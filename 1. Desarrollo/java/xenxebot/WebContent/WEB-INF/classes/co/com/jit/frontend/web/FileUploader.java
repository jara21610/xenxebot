/*    */ package co.com.jit.frontend.web;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.io.InputStream;
/*    */ import java.io.PrintWriter;
/*    */ import java.util.UUID;
/*    */ import javax.servlet.ServletConfig;
/*    */ import javax.servlet.ServletException;
/*    */ import javax.servlet.http.HttpServlet;
/*    */ import javax.servlet.http.HttpServletRequest;
/*    */ import javax.servlet.http.HttpServletResponse;
/*    */ import org.apache.log4j.Logger;
/*    */ import org.springframework.web.context.support.SpringBeanAutowiringSupport;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public abstract class FileUploader
/*    */   extends HttpServlet
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/* 25 */   private static final Logger logger = Logger.getLogger(FileUploader.class);
/*    */   
/*    */   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
/*    */   {
/* 29 */     String mimeType = request.getContentType();
/* 30 */     String extension = null;
/* 31 */     String[][] supportedFormatTable = {
/* 32 */       { "image/png", "png" }, 
/* 33 */       { "image/jpeg", "jpg" }, 
/* 34 */       { "image/gif", "gif" } };
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 39 */     for (int i = 0; i < supportedFormatTable.length; i++) {
/* 40 */       if (supportedFormatTable[i][0].equals(mimeType)) {
/* 41 */         extension = supportedFormatTable[i][1];
/*    */         
/* 43 */         break;
/*    */       }
/*    */     }
/*    */     
/* 47 */     if (extension == null) {
/* 48 */       throw new IOException("Unsupported data type: " + mimeType);
/*    */     }
/*    */     
/*    */ 
/* 52 */     String filename = UUID.randomUUID() + "." + extension;
/* 53 */     request.setAttribute("extension", extension);
/* 54 */     request.setAttribute("filename", filename);
/* 55 */     File saveTo = new File(filename);
/* 56 */     FileOutputStream os = new FileOutputStream(saveTo);
/* 57 */     InputStream is = request.getInputStream();
/* 58 */     byte[] buff = new byte['Ā'];
/*    */     
/*    */     int len;
/* 61 */     while ((len = is.read(buff)) > 0) { int len;
/* 62 */       os.write(buff, 0, len);
/*    */     }
/* 64 */     os.close();
/*    */     
/* 66 */     action(request);
/*    */     
/*    */ 
/* 69 */     PrintWriter out = response.getWriter();
/*    */     
/* 71 */     out.println("Saved image to: " + saveTo.getAbsolutePath());
/*    */   }
/*    */   
/*    */   public void init(ServletConfig config) {
/*    */     try {
/* 76 */       super.init(config);
/*    */     } catch (ServletException e) {
/* 78 */       logger.error(e);
/*    */     }
/* 80 */     SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, 
/* 81 */       config.getServletContext());
/*    */   }
/*    */   
/*    */   protected abstract void action(HttpServletRequest paramHttpServletRequest);
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\frontend\frontend.jar.zip!\co\com\jit\frontend\web\FileUploader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */