package co.com.zien.xenxerobot.frontend.controllers;

public class LicenceDTO
{
    private String license;
    private String productid;
    
    public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getLicense() {
        return this.license;
    }
    
    public void setLicense(final String license) {
        this.license = license;
    }
}
