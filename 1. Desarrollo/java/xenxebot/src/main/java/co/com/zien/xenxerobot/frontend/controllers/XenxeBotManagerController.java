package co.com.zien.xenxerobot.frontend.controllers;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import co.com.zien.xenxerobot.backend.services.*;

@RestController
@RequestMapping({ "/xenxebotmanager" })
public class XenxeBotManagerController
{
    @Autowired
    @Qualifier("TwitterPoolConectionManager")
    SocialPoolConectionManager tpcm;
    @Autowired
    @Qualifier("FacebookPoolConectionManager")
    SocialPoolConectionManager fpcm;
    
    @RequestMapping({ "/socialpoolcurrentstate/{socialname}" })
    public List<SocialConnectionDTO> socialpoolmanagercurrentstate(@PathVariable final String socialname) {
        if (socialname.equals("TWITTER")) {
            return (List<SocialConnectionDTO>)this.tpcm.getCurrentState();
        }
        return null;
    }
    
    @RequestMapping({ "/socialconectionlimitcurrentstate/{socialname}" })
    public Object socialconectionlimitcurrentstate(@PathVariable final String socialname) {
        if (socialname.equals("TWITTER")) {
            return this.tpcm.getSocialServerLimitStatus();
        }
        if (socialname.equals("FACEBOOK")) {
            return this.fpcm.getSocialServerLimitStatus();
        }
        return null;
    }
}
