package co.com.zien.xenxerobot.frontend.controllers;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
import co.com.zien.xenxerobot.backend.services.*;

@RestController
@RequestMapping({ "/registry" })
public class RegistryServiceController
{
    @Autowired
    RegistryService rs;
    
    @CrossOrigin(methods = { RequestMethod.POST })
    @RequestMapping({ "/getUserLicence" })
    public LicenceDTO getUserLicence(@RequestBody final LicenceDTO dto ) {
        String productname = "";
        if (dto.getProductid().equals(ServiceTypeEnum.XENXE.getServicetype())){
        	productname = ServiceTypeEnum.XENXE.getServicetype().toString().toUpperCase();
        } else {
        	productname = ServiceTypeEnum.PREXTIGE.getServicetype().toString().toUpperCase();
        }
        dto.setLicense(this.rs.getUserLicence().replace("%productid%", productname));
        return dto;
    }
    
    @CrossOrigin(methods = { RequestMethod.POST })
    @RequestMapping({ "/registryUser" })
    public ResponseDTO registryUser(@RequestBody final RegisterUserDTO dto) {
        final ResponseDTO response = new ResponseDTO();
        try {
            this.rs.registerUser(dto);
            response.setCode("0");
        }
        catch (UserAlreadyExistException e) {
            response.setCode("-1");
            response.setMessage("Usted ya posee una cuenta registrada");
        }
        catch (Exception e2) {
            response.setCode("-2");
            response.setMessage("Ha ocurrido en la  aplicaci\u00f3n en este momento no podemos atenderlo");
        }
        return response;
    }
}
