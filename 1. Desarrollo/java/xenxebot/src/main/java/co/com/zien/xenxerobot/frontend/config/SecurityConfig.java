package co.com.zien.xenxerobot.frontend.config;

import org.apache.shiro.authc.credential.*;
import co.com.zien.xenxerobot.security.*;
import org.apache.shiro.web.mgt.*;
import org.apache.shiro.realm.*;
import org.apache.shiro.cache.*;
import org.apache.shiro.spring.*;
import org.springframework.beans.factory.config.*;
import org.springframework.aop.framework.autoproxy.*;
import org.springframework.context.annotation.*;
import org.apache.shiro.spring.security.interceptor.*;
import org.apache.shiro.mgt.*;
import org.apache.shiro.mgt.SecurityManager;

@Configuration
public class SecurityConfig
{
    @Bean
    public CredentialsMatcher credentialsMatcher() {
        final HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("SHA-256");
        matcher.setHashIterations(2);
        return (CredentialsMatcher)matcher;
    }
    
    @Bean
    public UserRealm customSecurityRealm() {
        final UserRealm ur = new UserRealm();
        ur.setCredentialsMatcher(this.credentialsMatcher());
        return ur;
    }
    
    @Bean
    public WebSecurityManager securityManager() {
        final DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm((Realm)this.customSecurityRealm());
        securityManager.setCacheManager(this.cacheManager());
        return (WebSecurityManager)securityManager;
    }
    
    @Bean
    public CacheManager cacheManager() {
        final MemoryConstrainedCacheManager cacheManager = new MemoryConstrainedCacheManager();
        return (CacheManager)cacheManager;
    }
    
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }
    
    @Bean
    public MethodInvokingFactoryBean methodInvokingFactoryBean() {
        final MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
        methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
        methodInvokingFactoryBean.setArguments(new Object[] { this.securityManager() });
        return methodInvokingFactoryBean;
    }
    
    @Bean
    @DependsOn({ "lifecycleBeanPostProcessor" })
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        return new DefaultAdvisorAutoProxyCreator();
    }
    
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        final AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager((SecurityManager)this.securityManager());
        return authorizationAttributeSourceAdvisor;
    }
}
