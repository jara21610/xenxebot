package co.com.zien.xenxerobot.frontend.controllers;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import co.com.zien.xenxerobot.backend.services.*;
import co.com.zien.xenxerobot.backend.config.*;
import javax.servlet.*;

@WebServlet(value = { "/ProcessLauncher" }, loadOnStartup = 0)
public class ProcessLauncher extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    
    public void init(final ServletConfig config) throws ServletException {
        final BotLauncher bl = (BotLauncher)Config.getContext().getBean((Class)BotLauncher.class);
        bl.launchBots();
    }
}
