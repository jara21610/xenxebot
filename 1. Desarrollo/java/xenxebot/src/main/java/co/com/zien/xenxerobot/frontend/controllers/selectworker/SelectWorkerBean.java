package co.com.zien.xenxerobot.frontend.controllers.selectworker;

import javax.faces.bean.*;
import co.com.zien.xenxerobot.backend.services.dataset.*;
import co.com.zien.xenxerobot.frontend.controllers.packagelist.*;
import javax.faces.event.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.*;

@ManagedBean
@SessionScoped
public class SelectWorkerBean
{
    private List<Worker> workerList;
    private String selectworker;
    @ManagedProperty("#{workerService}")
    private WorkerService workerService;
    @ManagedProperty("#{packagesService}")
    private PackagesService packagesService;
    @ManagedProperty("#{packageListBean}")
    private PackageListBean packageListBean;
    private Worker currentworker;
    
    public void preRenderView(final ComponentSystemEvent event) {
        this.workerList = (List<Worker>)this.workerService.getavailableWorkers();
    }
    
    public List<Worker> getWorkerList() {
        return this.workerList;
    }
    
    public void setWorkerList(final List<Worker> workerList) {
        this.workerList = workerList;
    }
    
    public String getSelectworker() {
        return this.selectworker;
    }
    
    public void setSelectworker(final String selectworker) {
        this.selectworker = selectworker;
    }
    
    public WorkerService getWorkerService() {
        return this.workerService;
    }
    
    public void setWorkerService(final WorkerService workerService) {
        this.workerService = workerService;
    }
    
    public String getWorkerPakage() {
        this.currentworker = null;
        for (final Worker w : this.workerList) {
            if (w.getId().equals(this.selectworker)) {
                this.currentworker = w;
            }
        }
        this.packageListBean.setPackageList(this.packagesService.findByWorker(this.currentworker));
        this.packageListBean.getPackageList().addAll(this.packagesService.findByState(PackagesStateEnum.AVAILABLE.getState()));
        return "myavailablepackages.xhtml";
    }
    
    public PackageListBean getPackageListBean() {
        return this.packageListBean;
    }
    
    public void setPackageListBean(final PackageListBean packageListBean) {
        this.packageListBean = packageListBean;
    }
    
    public PackagesService getPackagesService() {
        return this.packagesService;
    }
    
    public void setPackagesService(final PackagesService packagesService) {
        this.packagesService = packagesService;
    }
    
    public Worker getCurrentworker() {
        return this.currentworker;
    }
    
    public void setCurrentworker(final Worker currentworker) {
        this.currentworker = currentworker;
    }
}
