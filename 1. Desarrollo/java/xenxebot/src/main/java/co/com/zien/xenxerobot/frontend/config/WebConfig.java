package co.com.zien.xenxerobot.frontend.config;

import co.com.zien.xenxerobot.backend.config.*;
import org.apache.shiro.web.mgt.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.servlet.config.annotation.*;
import org.apache.shiro.spring.web.*;
import org.apache.shiro.mgt.*;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan({ "co.com.zien.xenxerobot.frontend.controllers" })
@EnableWebMvc
@Import({ Config.class })
public class WebConfig extends WebMvcConfigurerAdapter
{
    @Autowired
    private WebSecurityManager securityManager;
    
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    
    @Bean
    public ShiroFilterFactoryBean shiroFilterBean() {
        final ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setLoginUrl("/ui/security/login.xhtml");
        shiroFilter.setSecurityManager((SecurityManager)this.securityManager);
        return shiroFilter;
    }        
}
