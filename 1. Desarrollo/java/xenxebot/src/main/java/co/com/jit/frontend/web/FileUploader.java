package co.com.jit.frontend.web;

import org.apache.log4j.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import org.springframework.web.context.support.*;

public abstract class FileUploader extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger;
    
    static {
        logger = Logger.getLogger((Class)FileUploader.class);
    }
    
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String mimeType = request.getContentType();
        String extension = null;
        final String[][] supportedFormatTable = { { "image/png", "png" }, { "image/jpeg", "jpg" }, { "image/gif", "gif" } };
        for (int i = 0; i < supportedFormatTable.length; ++i) {
            if (supportedFormatTable[i][0].equals(mimeType)) {
                extension = supportedFormatTable[i][1];
                break;
            }
        }
        if (extension == null) {
            throw new IOException("Unsupported data type: " + mimeType);
        }
        final String filename = UUID.randomUUID() + "." + extension;
        request.setAttribute("extension", (Object)extension);
        request.setAttribute("filename", (Object)filename);
        final File saveTo = new File(filename);
        final FileOutputStream os = new FileOutputStream(saveTo);
        final InputStream is = (InputStream)request.getInputStream();
        final byte[] buff = new byte[256];
        int len;
        while ((len = is.read(buff)) > 0) {
            os.write(buff, 0, len);
        }
        os.close();
        this.action(request);
        final PrintWriter out = response.getWriter();
        out.println("Saved image to: " + saveTo.getAbsolutePath());
    }
    
    public void init(final ServletConfig config) {
        try {
            super.init(config);
        }
        catch (ServletException e) {
            FileUploader.logger.error((Object)e);
        }
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext((Object)this, config.getServletContext());
    }
    
    protected abstract void action(final HttpServletRequest p0);
}
