package co.com.zien.xenxerobot.frontend.controllers;

public class ResponseDTO
{
    private String code;
    private String message;
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(final String message) {
        this.message = message;
    }
}
