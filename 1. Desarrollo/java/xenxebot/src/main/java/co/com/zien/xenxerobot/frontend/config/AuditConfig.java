package co.com.zien.xenxerobot.frontend.config;

import org.springframework.context.annotation.*;
import org.springframework.context.annotation.Configuration;
import org.audit4j.handler.db.*;
import org.audit4j.integration.spring.*;
import org.audit4j.core.layout.*;
import org.audit4j.core.handler.*;
import java.util.*;
import co.com.zien.xenxerobot.audit.*;
import org.audit4j.core.*;

@Configuration
@EnableAspectJAutoProxy
public class AuditConfig
{
    @Bean
    public AuditAspect auditAspect() {
        final AuditAspect auditAspect = new AuditAspect();
        return auditAspect;
    }
    
    @Bean
    public DatabaseAuditHandler databaseHandler() {
        final DatabaseAuditHandler dbHandler = new DatabaseAuditHandler();
        dbHandler.setEmbedded("true");
        return dbHandler;
    }
    
    @Bean
    public SpringAudit4jConfig springAudit4jConfig() {
        final SpringAudit4jConfig springAudit4jConfig = new SpringAudit4jConfig();
        springAudit4jConfig.setLayout((Layout)new SimpleLayout());
        final List<Handler> handlers = new ArrayList<Handler>();
        handlers.add((Handler)new ConsoleAuditHandler());
        handlers.add((Handler)this.databaseHandler());
        springAudit4jConfig.setHandlers((List)handlers);
        springAudit4jConfig.setMetaData((MetaData)new AuditMetaData());
        return springAudit4jConfig;
    }
}
