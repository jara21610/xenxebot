package co.com.zien.xenxerobot.frontend.controllers;

import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import co.com.zien.xenxerobot.backend.services.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({ "/planmanagement" })
public class PlanManagementServiceController
{
    @Autowired
    TokenService ts;
    @Autowired
    PlanManagementService pms;
    @Autowired
    UserplanRepository upr;
    @Autowired
    PlanRepository planr;
    @Autowired
    UserRepository ur;
    
    @CrossOrigin(methods = { RequestMethod.POST })
    @RequestMapping({ "/upgradePlanWithPaymentMethod" })
    public ResponseDTO upgradePlanWithPaymentMethod(@RequestBody final UpgradePlanDTO updto) {
        final ResponseDTO response = new ResponseDTO();
        try {
            this.pms.upgradePlanWithPaymentMethod(updto);
            response.setCode("0");
            response.setMessage("SUCCESS");
        }
        catch (Exception e) {
            response.setCode("-1");
            response.setMessage("Error en la aplicaci\u00f3n, vuelva a intentar m\u00e1s tarde o contacte al administrador");
        }
        return response;
    }
}
