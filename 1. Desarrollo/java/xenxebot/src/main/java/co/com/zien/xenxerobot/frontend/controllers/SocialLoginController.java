package co.com.zien.xenxerobot.frontend.controllers;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import co.com.zien.xenxerobot.backend.config.Config;
import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
import co.com.zien.xenxerobot.backend.services.*;
import co.com.zien.xenxerobot.backend.telegram.bot.ServiceBot;
import co.com.zien.xenxerobot.persistence.entity.Users;
import co.com.zien.xenxerobot.persistence.repository.UserRepository;

@RestController
@RequestMapping({ "/sociallogin" })
public class SocialLoginController
{

	@Autowired
    TokenService ts;
	@Autowired
	FacebookService fs;
	@Autowired
	UserRepository ur;
	
		
    @CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*",methods = { RequestMethod.POST })
    @RequestMapping({ "/facebook" })
    public ResponseDTO registryUser(@RequestBody final FacebookLoginDTO dto) throws Exception{
    	final ResponseDTO response = new ResponseDTO();
    	Users u = null;
    	final ServiceBot xb = (ServiceBot)Config.getContext().getBean(dto.getServicetype());
        try {
        	u = this.ts.validateUserToken(dto.getUsertoken());
        	String extendedToken = fs.getExtendedToken(dto.getToken());
        	u.setFacebooktoken(extendedToken);
        	ur.saveAndFlush(u);        	
        	xb.handleFacebookSelectAccount(fs.getAccountList(extendedToken), u);
            response.setCode("0");
        }
        catch (TokenNotExistException ex) {}
        catch (TokenExpiredException e) {
            u = this.ts.getUserOwnerToken(dto.getUsertoken());
            xb.handleUserTokenException(u.getChatid());            
            response.setCode("-1");
            response.setMessage("El token a expirado");
        }
        catch (Exception e2) {
            response.setCode("-2");
            response.setMessage("Ha ocurrido en la  aplicaci\u00f3n en este momento no podemos atenderlo");
        }
        return response;
    }    
}
