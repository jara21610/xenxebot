package co.com.zien.xenxerobot.frontend.controllers.packagelist;

//import co.com.zien.xenxerobot.frontend.controllers.humantexttask.*;
import javax.faces.bean.*;
import co.com.zien.xenxerobot.backend.services.dataset.*;
import java.math.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.util.*;
import javax.faces.context.*;
import co.com.zien.xenxerobot.frontend.controllers.selectworker.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import javax.el.*;

@ManagedBean
@SessionScoped
public class PackageListBean
{
    private List<Packages> packageList;
    //@ManagedProperty("#{humanTextTaskBean}")
    //private HumanTextTaskBean humanTextTaskBean;
    @ManagedProperty("#{packagesService}")
    private PackagesService packagesService;
    private boolean error;
    private String errorMessage;
    
    public PackageListBean() {
        this.error = false;
        this.errorMessage = "";
    }
    
    public List<Packages> getPackageList() {
        return this.packageList;
    }
    
    public void setPackageList(final List<Packages> packageList) {
        this.packageList = packageList;
    }
    
    public String openpackage(final BigDecimal id) {
        this.error = false;
        this.errorMessage = "";
        final List<Post> workitems = (List<Post>)this.packagesService.nextworkitembypackage(id);
        /*this.humanTextTaskBean.setCurrentPost(workitems.get(0));
        this.humanTextTaskBean.setTotalpost(this.packagesService.packageSize(id));
        this.humanTextTaskBean.setWorkingpost(this.humanTextTaskBean.getTotalpost() - workitems.size() + 1);
        this.humanTextTaskBean.getCurrentPost().setConceptsCollection((Collection)new ArrayList());
        final Concepts concept = new Concepts();
        concept.setId(UUID.randomUUID().toString());
        this.humanTextTaskBean.setCurrentConcept(concept);
        this.humanTextTaskBean.setPackageid(id);*/
        return "humantexttask.xhtml";
    }
    
    public String selectpackage(final BigDecimal id) {
        this.error = false;
        this.errorMessage = "";
        final Packages selected = this.packageList.stream().filter(p -> p.getId().equals(id)).findAny().orElse(null);
        final FacesContext context = FacesContext.getCurrentInstance();
        final ELContext elContext = context.getELContext();
        final SelectWorkerBean selectWorkerBean = (SelectWorkerBean)elContext.getELResolver().getValue(elContext, (Object)null, (Object)"selectWorkerBean");
        selected.setIdWorker(selectWorkerBean.getCurrentworker());
        selected.setState(PackagesStateEnum.PROCCESS.getState());
        this.packagesService.savePackage(selected);
        this.packageList = null;
        (this.packageList = (List<Packages>)this.packagesService.findByWorker(selectWorkerBean.getCurrentworker())).addAll(this.packagesService.findByState(PackagesStateEnum.AVAILABLE.getState()));
        return "myavailablepackages.xhtml";
    }
    
    public PackagesService getPackagesService() {
        return this.packagesService;
    }
    
    public void setPackagesService(final PackagesService packagesService) {
        this.packagesService = packagesService;
    }
    
    /*public HumanTextTaskBean getHumanTextTaskBean() {
        return this.humanTextTaskBean;
    }
    
    public void setHumanTextTaskBean(final HumanTextTaskBean humanTextTaskBean) {
        this.humanTextTaskBean = humanTextTaskBean;
    }*/
    
    public boolean isError() {
        return this.error;
    }
    
    public void setError(final boolean error) {
        this.error = error;
    }
    
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
