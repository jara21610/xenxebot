package co.com.zien.xenxerobot.frontend.config;

import org.springframework.web.*;
import org.springframework.web.context.support.*;
import org.springframework.web.context.*;
import org.springframework.web.servlet.*;
import org.springframework.web.filter.*;
import java.util.*;
import javax.servlet.*;

public class WebAppInitializer implements WebApplicationInitializer
{
    public void onStartup(final ServletContext servletContext) throws ServletException {
        final AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.setServletContext(servletContext);
        rootContext.register(new Class[] { WebConfig.class, SecurityConfig.class });
        servletContext.addListener((EventListener)new ContextLoaderListener((WebApplicationContext)rootContext));
        final ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcher", (Servlet)new DispatcherServlet((WebApplicationContext)rootContext));
        dynamic.addMapping(new String[] { "/" });
        dynamic.setLoadOnStartup(1);
        servletContext.addFilter("shiroFilter", (Filter)new DelegatingFilterProxy("shiroFilterBean", (WebApplicationContext)rootContext)).addMappingForUrlPatterns((EnumSet)EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ERROR), false, new String[] { "/*" });
    }
}
