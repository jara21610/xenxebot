package co.com.zien.xenxerobot.audit;

import org.audit4j.core.*;
import org.springframework.web.context.request.*;
import org.apache.shiro.*;
import org.apache.shiro.subject.*;
import javax.servlet.*;

public class AuditMetaData implements MetaData
{
    private static final long serialVersionUID = 7243065407615627372L;
    
    public String getOrigin() {
        try {
            return ((ServletRequest)((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest()).getRemoteAddr();
        }
        catch (Exception e) {
            e.printStackTrace();
            return "unidentified";
        }
    }
    
    public String getActor() {
        final Subject currentUser = SecurityUtils.getSubject();
        if (currentUser != null) {
            return currentUser.getPrincipal().toString();
        }
        return "system";
    }
}
