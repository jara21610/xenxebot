/*     */ package co.com.jit.backend.util;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.config.Config;
/*     */ import com.artofsolving.jodconverter.DocumentConverter;
/*     */ import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
/*     */ import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
/*     */ import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
/*     */ import com.vdurmont.emoji.Emoji;
/*     */ import com.vdurmont.emoji.EmojiParser;
/*     */ import com.vdurmont.emoji.EmojiParser.EmojiTransformer;
/*     */ import com.vdurmont.emoji.EmojiParser.UnicodeCandidate;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.InputStreamReader;
/*     */ import java.io.PrintStream;
/*     */ import java.io.StringWriter;
/*     */ import java.net.ConnectException;
/*     */ import java.net.HttpURLConnection;
/*     */ import java.net.URL;
/*     */ import java.text.DateFormat;
/*     */ import java.text.ParseException;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.GregorianCalendar;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import org.apache.poi.hssf.usermodel.HSSFCell;
/*     */ import org.apache.poi.hssf.usermodel.HSSFRow;
/*     */ import org.apache.poi.hssf.usermodel.HSSFSheet;
/*     */ import org.apache.poi.hssf.usermodel.HSSFWorkbook;
/*     */ import org.apache.poi.ss.util.CellRangeAddress;
/*     */ import org.apache.velocity.Template;
/*     */ import org.apache.velocity.VelocityContext;
/*     */ import org.apache.velocity.app.VelocityEngine;
/*     */ import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
/*     */ import org.jsoup.Connection;
/*     */ import org.jsoup.Jsoup;
/*     */ import org.jsoup.nodes.Document;
/*     */ import org.springframework.context.ApplicationContext;
/*     */ import org.springframework.core.env.Environment;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Utils
/*     */ {
/*  69 */   private static Environment env = (Environment)Config.getContext().getBean(Environment.class);
/*     */   
/*     */ 
/*     */   public static String ConvertDateToString(String formato, Date date)
/*     */   {
/*  74 */     DateFormat fecha = new SimpleDateFormat(formato);
/*  75 */     String convertido = fecha.format(date);
/*  76 */     return convertido;
/*     */   }
/*     */   
/*     */   public static GregorianCalendar convertStringToDate(String fec)
/*     */   {
/*  81 */     DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
/*     */     
/*  83 */     GregorianCalendar cal = new GregorianCalendar();
/*     */     try {
/*  85 */       Date date = format.parse(fec);
/*  86 */       cal.setTime(date);
/*     */     } catch (ParseException e) {
/*  88 */       e.printStackTrace();
/*     */     }
/*  90 */     return cal;
/*     */   }
/*     */   
/*     */   public static int calculateAge(GregorianCalendar fecnac)
/*     */   {
/*  95 */     Calendar now = new GregorianCalendar();
/*  96 */     int res = now.get(1) - fecnac.get(1);
/*  97 */     if ((fecnac.get(2) > now.get(2)) || ((fecnac.get(2) == now.get(2)) && 
/*  98 */       (fecnac.get(5) > now.get(5))))
/*  99 */       res--;
/* 100 */     return res;
/*     */   }
/*     */   
/*     */   public static void convertDocuments(String source, String dst) throws ConnectException {
/* 104 */     File inputFile = new File(source);
/* 105 */     File outputFile = new File(dst);
/*     */     
/*     */ 
/* 108 */     OpenOfficeConnection connection = new SocketOpenOfficeConnection(8100);
/* 109 */     connection.connect();
/*     */     
/*     */ 
/* 112 */     DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
/* 113 */     converter.convert(inputFile, outputFile);
/*     */     
/*     */ 
/* 116 */     connection.disconnect();
/* 117 */     inputFile.delete();
/*     */   }
/*     */   
/*     */   public static void coverthtmltojpeg(String source, String dst, int width, int height) throws Exception {
/* 121 */     String command = env.getRequiredProperty("wkhtmltoimage.path") + " --width " + width + " --height " + height + " " + source + " " + dst;
/* 122 */     System.out.println(command);
/* 123 */     Runtime.getRuntime().exec(command);
/*     */   }
/*     */   
/*     */   public static void trasformTemplate(String sourcetemplate, String dstfilename, HashMap<String, Object> context) throws Exception
/*     */   {
/* 128 */     VelocityEngine ve = new VelocityEngine();
/*     */     
/* 130 */     ve.setProperty("resource.loader", "classpath");
/* 131 */     ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
/*     */     
/* 133 */     ve.init();
/*     */     
/* 135 */     Template t = ve.getTemplate(sourcetemplate);
/*     */     
/* 137 */     VelocityContext vc = new VelocityContext();
/*     */     
/* 139 */     for (String key : context.keySet()) {
/* 140 */       vc.put(key, context.get(key));
/*     */     }
/*     */     
/* 143 */     StringWriter sw = new StringWriter();
/* 144 */     t.merge(vc, sw);
/*     */     
/* 146 */     FileWriter fw = new FileWriter(dstfilename);
/* 147 */     fw.write(sw.toString());
/* 148 */     fw.close();
/*     */   }
/*     */   
/*     */ 
/*     */   public static String sendHttpRequest(String targetURL)
/*     */   {
/* 154 */     HttpURLConnection connection = null;
/* 155 */     StringBuilder response = new StringBuilder();
/*     */     try {
/* 157 */       URL url = new URL(targetURL);
/* 158 */       connection = (HttpURLConnection)url.openConnection();
/*     */       
/* 160 */       connection.setUseCaches(false);
/*     */       
/*     */ 
/* 163 */       InputStream is = connection.getInputStream();
/* 164 */       BufferedReader rd = new BufferedReader(new InputStreamReader(is));
/*     */       String line;
/* 166 */       while ((line = rd.readLine()) != null) { String line;
/* 167 */         response.append(line);
/* 168 */         response.append('\r');
/*     */       }
/* 170 */       rd.close();
/*     */     } catch (Exception e) {
/* 172 */       e.printStackTrace();
/*     */     }
/* 174 */     return response.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String deleteaccentmarkdieresis(String input)
/*     */   {
/* 185 */     String original = "áàäéèëíìïóòöúüÁÀÄÉÈËÍÌÏÓÒÖÚÙÜ";
/*     */     
/* 187 */     String ascii = "aaaeeeiiiooouuuAAAEEEIIIOOOUUU";
/* 188 */     String output = input;
/* 189 */     for (int i = 0; i < original.length(); i++)
/*     */     {
/* 191 */       output = output.replace(original.charAt(i), ascii.charAt(i));
/*     */     }
/* 193 */     return output;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String replacePunctuationMarks(String input)
/*     */   {
/* 204 */     input = input.replace("¿", " signointerrogacion ");
/* 205 */     input = input.replace("?", " signointerrogacion ");
/* 206 */     input = input.replace("¡", " signoadmiracion ");
/* 207 */     input = input.replace("!", " signoadmiracion ");
/* 208 */     input = input.replace("...", " signopuntossuspensivos ");
/* 209 */     input = input.replace(".", " signopunto ");
/* 210 */     input = input.replace(";", " signopuntoycoma ");
/* 211 */     input = input.replace(",", " signocoma ");
/*     */     
/* 213 */     return input;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String cleanString(List<String> regextoclean, String input)
/*     */   {
/* 223 */     for (String regex : regextoclean) {
/* 224 */       input = input.replaceAll(regex, "");
/*     */     }
/* 226 */     return input;
/*     */   }
/*     */   
/*     */   public static String cleanRepeatedChars(String input)
/*     */   {
/* 231 */     input = input.replaceAll("(.)\\1+", "$1");
/* 232 */     return input;
/*     */   }
/*     */   
/*     */   public static boolean isHashtag(String posibleHastag)
/*     */   {
/* 237 */     return posibleHastag.substring(0, 1).equals("#");
/*     */   }
/*     */   
/*     */   public static boolean isUserMention(String posibleHastag)
/*     */   {
/* 242 */     return posibleHastag.substring(0, 1).equals("@");
/*     */   }
/*     */   
/*     */   public static void copyRow(HSSFWorkbook workbook, HSSFSheet worksheet, int sourceRowNum, int destinationRowNum)
/*     */   {
/* 247 */     HSSFRow newRow = worksheet.getRow(destinationRowNum);
/* 248 */     HSSFRow sourceRow = worksheet.getRow(sourceRowNum);
/*     */     
/*     */ 
/* 251 */     if (newRow != null) {
/* 252 */       worksheet.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
/*     */     } else {
/* 254 */       newRow = worksheet.createRow(destinationRowNum);
/*     */     }
/*     */     
/*     */ 
/* 258 */     for (int i = 0; i < sourceRow.getLastCellNum(); i++)
/*     */     {
/* 260 */       HSSFCell oldCell = sourceRow.getCell(i);
/* 261 */       HSSFCell newCell = newRow.createCell(i);
/*     */       
/*     */ 
/* 264 */       if (oldCell == null) {
/* 265 */         newCell = null;
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 272 */         newCell.setCellStyle(oldCell.getCellStyle());
/*     */         
/*     */ 
/* 275 */         if (oldCell.getCellComment() != null) {
/* 276 */           newCell.setCellComment(oldCell.getCellComment());
/*     */         }
/*     */         
/*     */ 
/* 280 */         if (oldCell.getHyperlink() != null) {
/* 281 */           newCell.setHyperlink(oldCell.getHyperlink());
/*     */         }
/*     */         
/*     */ 
/* 285 */         newCell.setCellType(oldCell.getCellType());
/*     */         
/*     */ 
/* 288 */         switch (oldCell.getCellType()) {
/*     */         case 3: 
/* 290 */           newCell.setCellValue(oldCell.getStringCellValue());
/* 291 */           break;
/*     */         case 4: 
/* 293 */           newCell.setCellValue(oldCell.getBooleanCellValue());
/* 294 */           break;
/*     */         case 5: 
/* 296 */           newCell.setCellErrorValue(oldCell.getErrorCellValue());
/* 297 */           break;
/*     */         case 2: 
/* 299 */           newCell.setCellFormula(oldCell.getCellFormula());
/* 300 */           break;
/*     */         case 0: 
/* 302 */           newCell.setCellValue(oldCell.getNumericCellValue());
/* 303 */           break;
/*     */         case 1: 
/* 305 */           newCell.setCellValue(oldCell.getRichStringCellValue());
/*     */         }
/*     */         
/*     */       }
/*     */     }
/*     */     
/* 311 */     for (int i = 0; i < worksheet.getNumMergedRegions(); i++) {
/* 312 */       CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
/* 313 */       if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
/* 314 */         CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(), 
/* 315 */           newRow.getRowNum() + (
/* 316 */           cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow()), 
/*     */           
/* 318 */           cellRangeAddress.getFirstColumn(), 
/* 319 */           cellRangeAddress.getLastColumn());
/* 320 */         worksheet.addMergedRegion(newCellRangeAddress);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public static Date addMinutesToDate(int minutes, Date beforeTime) {
/* 326 */     long ONE_MINUTE_IN_MILLIS = 60000L;
/*     */     
/* 328 */     long curTimeInMs = beforeTime.getTime();
/* 329 */     Date afterAddingMins = new Date(curTimeInMs + minutes * 60000L);
/* 330 */     return afterAddingMins;
/*     */   }
/*     */   
/*     */   public static String getOriginalResponseTweet(String url) throws IOException {
/* 334 */     Document doc = Jsoup.connect(url).get();
/*     */     
/* 336 */     String text = doc.title();
/* 337 */     System.out.println(text);
/* 338 */     return text;
/*     */   }
/*     */   
/*     */   public static String getSentimentWordFromEmoticon(String text)
/*     */     throws IOException
/*     */   {
/* 344 */     EmojiParser.EmojiTransformer emojiTransformer = new EmojiParser.EmojiTransformer()
/*     */     {
/*     */       public String transform(EmojiParser.UnicodeCandidate unicodeCandidate) {
/* 347 */         return 
/*     */         
/*     */ 
/* 350 */           " " + (String)unicodeCandidate.getEmoji().getAliases().get(0) + unicodeCandidate.getFitzpatrickType() + " ";
/*     */       }
/*     */       
/* 353 */     };
/* 354 */     return EmojiParser.parseFromUnicode(text, emojiTransformer);
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\jit\backend\util\Utils.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */