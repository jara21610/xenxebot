/*    */ package co.com.jit.backend.util;
/*    */ 
/*    */ import java.security.SecureRandom;
/*    */ import java.util.Random;
/*    */ 
/*    */ 
/*    */ public class RandomPassword
/*    */ {
/*  9 */   private static final Random RANDOM = new SecureRandom();
/*    */   
/*    */   public static final int PASSWORD_LENGTH = 6;
/*    */   
/*    */   public static String generateRandomPassword()
/*    */   {
/* 15 */     String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
/*    */     
/* 17 */     String pw = "";
/* 18 */     for (int i = 0; i < 6; i++)
/*    */     {
/* 20 */       int index = (int)(RANDOM.nextDouble() * letters.length());
/* 21 */       pw = pw + letters.substring(index, index + 1);
/*    */     }
/* 23 */     return pw;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\jit\backend\util\RandomPassword.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */