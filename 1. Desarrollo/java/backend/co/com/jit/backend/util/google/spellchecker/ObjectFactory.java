/*    */ package co.com.jit.backend.util.google.spellchecker;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlRegistry;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlRegistry
/*    */ public class ObjectFactory
/*    */ {
/*    */   public Toplevel createToplevel()
/*    */   {
/* 44 */     return new Toplevel();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public Toplevel.CompleteSuggestion createToplevelCompleteSuggestion()
/*    */   {
/* 52 */     return new Toplevel.CompleteSuggestion();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public Toplevel.CompleteSuggestion.Suggestion createToplevelCompleteSuggestionSuggestion()
/*    */   {
/* 60 */     return new Toplevel.CompleteSuggestion.Suggestion();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\jit\backend\util\google\spellchecker\ObjectFactory.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */