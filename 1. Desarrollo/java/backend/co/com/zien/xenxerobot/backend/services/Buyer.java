/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class Buyer
/*    */ {
/*    */   private String merchantBuyerId;
/*    */   private String fullName;
/*    */   private String emailAddress;
/*    */   private String contactPhone;
/*    */   private String dniNumber;
/*    */   private ShippingAddress shippingAddress;
/*    */   
/*    */   public String getMerchantBuyerId() {
/* 13 */     return this.merchantBuyerId;
/*    */   }
/*    */   
/*    */   public void setMerchantBuyerId(String merchantBuyerId) {
/* 17 */     this.merchantBuyerId = merchantBuyerId;
/*    */   }
/*    */   
/*    */   public String getFullName() {
/* 21 */     return this.fullName;
/*    */   }
/*    */   
/*    */   public void setFullName(String fullName) {
/* 25 */     this.fullName = fullName;
/*    */   }
/*    */   
/*    */   public String getEmailAddress() {
/* 29 */     return this.emailAddress;
/*    */   }
/*    */   
/*    */   public void setEmailAddress(String emailAddress) {
/* 33 */     this.emailAddress = emailAddress;
/*    */   }
/*    */   
/*    */   public String getContactPhone() {
/* 37 */     return this.contactPhone;
/*    */   }
/*    */   
/*    */   public void setContactPhone(String contactPhone) {
/* 41 */     this.contactPhone = contactPhone;
/*    */   }
/*    */   
/*    */   public String getDniNumber() {
/* 45 */     return this.dniNumber;
/*    */   }
/*    */   
/*    */   public void setDniNumber(String dniNumber) {
/* 49 */     this.dniNumber = dniNumber;
/*    */   }
/*    */   
/*    */   public ShippingAddress getShippingAddress() {
/* 53 */     return this.shippingAddress;
/*    */   }
/*    */   
/*    */   public void setShippingAddress(ShippingAddress shippingAddress) {
/* 57 */     this.shippingAddress = shippingAddress;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\Buyer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */