/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*    */ 
/*    */ @org.springframework.stereotype.Service
/*    */ public class ResponseBotServiceFactoryImpl implements ResponseBotServiceFactory
/*    */ {
/*    */   @org.springframework.beans.factory.annotation.Autowired
/*    */   @org.springframework.beans.factory.annotation.Qualifier("EmailResponseBot")
/*    */   ResponseBotService erbs;
/*    */   @org.springframework.beans.factory.annotation.Autowired
/*    */   @org.springframework.beans.factory.annotation.Qualifier("TelegramResponseBot")
/*    */   ResponseBotService trbs;
/*    */   
/*    */   public ResponseBotService getResponseBotService(String channel) {
/* 16 */     if (channel.equals(ChannelEnum.TELEGRAM.getChannel()))
/* 17 */       return this.trbs;
/* 18 */     if (channel.equals(ChannelEnum.EMAIL.getChannel())) {
/* 19 */       return this.erbs;
/*    */     }
/* 21 */     return this.trbs;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ResponseBotServiceFactoryImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */