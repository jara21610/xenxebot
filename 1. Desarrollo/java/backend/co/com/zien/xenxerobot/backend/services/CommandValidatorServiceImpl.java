/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.TwitterSearchTypeEnum;
/*    */ import java.text.DateFormat;
/*    */ import java.text.ParseException;
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Arrays;
/*    */ import java.util.HashMap;
/*    */ import java.util.List;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.MessageSource;
/*    */ import org.springframework.context.i18n.LocaleContextHolder;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ @Service
/*    */ public class CommandValidatorServiceImpl
/*    */   implements CommandValidatorService
/*    */ {
/*    */   @Autowired
/*    */   MessageSource messageSource;
/*    */   
/*    */   public ComandDTO validateCommand(String command)
/*    */     throws CommandMalformedException
/*    */   {
/*    */     try
/*    */     {
/* 31 */       List<String> commanddetail = new ArrayList(Arrays.asList(command.split(" ")));
/* 32 */       ComandDTO cdto = new ComandDTO();
/*    */       
/* 34 */       if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(commanddetail.get(0))) {
/* 35 */         cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/* 36 */         if (SocialMediaSourceEnum.TWITTER.getSocialmediasource().equals(commanddetail.get(1))) {
/* 37 */           if ((TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype().equals(commanddetail.get(2))) || 
/* 38 */             (TwitterSearchTypeEnum.USUARIO.getTwittersearchtype().equals(commanddetail.get(2))))
/*    */           {
/* 40 */             DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/* 41 */             HashMap<String, Object> arguments = new HashMap();
/* 42 */             arguments.put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), 
/* 43 */               commanddetail.get(1));
/* 44 */             arguments.put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), commanddetail.get(2));
/* 45 */             String searchcriteria = "";
/* 46 */             for (int i = 3; i < commanddetail.size() - 4; i++) {
/* 47 */               searchcriteria = searchcriteria + (String)commanddetail.get(i) + " ";
/*    */             }
/* 49 */             arguments.put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), searchcriteria);
/* 50 */             arguments.put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), dateFormat.parse((String)commanddetail.get(commanddetail.size() - 4) + " " + (String)commanddetail.get(commanddetail.size() - 3)));
/* 51 */             arguments.put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), dateFormat.parse((String)commanddetail.get(commanddetail.size() - 2) + " " + (String)commanddetail.get(commanddetail.size() - 1)));
/* 52 */             cdto.setArguments(arguments);
/* 53 */             return cdto;
/*    */           }
/* 55 */           throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidtwittersearchtype", null, LocaleContextHolder.getLocale()));
/*    */         }
/*    */         
/* 58 */         throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidsocialmediasource", null, LocaleContextHolder.getLocale()));
/*    */       }
/*    */       
/* 61 */       if (CommandEnum.HELP.getCommand().equals(commanddetail.get(0))) {
/* 62 */         cdto.setCommand(CommandEnum.HELP.getCommand());
/* 63 */         return cdto;
/*    */       }
/* 65 */       throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidcommand", null, LocaleContextHolder.getLocale()));
/*    */     }
/*    */     catch (ParseException e) {
/* 68 */       throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidcommand", null, LocaleContextHolder.getLocale()));
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\CommandValidatorServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */