/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*    */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*    */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*    */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import org.apache.log4j.Logger;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.MessageSource;
/*    */ import org.springframework.context.i18n.LocaleContextHolder;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ public class EmailBotImpl
/*    */   implements EmailBot
/*    */ {
/* 33 */   private static final Logger logger = Logger.getLogger(EmailBotImpl.class);
/*    */   
/*    */   @Autowired
/*    */   EmailService es;
/*    */   @Autowired
/*    */   UserRepository ur;
/*    */   @Autowired
/*    */   CommandProcessRepository cpr;
/*    */   
/*    */   public void checkNewCommands()
/*    */   {
/*    */     for (;;)
/*    */     {
/* 46 */       List<EmailMessageDTO> messages = this.es.getNewEmailsImap();
/* 47 */       if (messages != null) {
/* 48 */         for (EmailMessageDTO message : messages) {
/* 49 */           String useremail = message.getFrom();
/* 50 */           if (useremail != null) {
/* 51 */             Users u = this.ur.isUserActivebyEmail(useremail);
/* 52 */             if (u != null)
/*    */             {
/* 54 */               ComandDTO cdto = null;
/*    */               try {
/* 56 */                 message.setText(message.getText().replace("\n", "").replace("\r", ""));
/* 57 */                 cdto = this.cvs.validateCommand(message.getText());
/* 58 */                 if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(cdto.getCommand())) {
/* 59 */                   cdto.setChannel(ChannelEnum.EMAIL.getChannel());
/* 60 */                   cdto.setCommunicationKey(useremail);
/* 61 */                   cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), 
/* 62 */                     ServiceTypeEnum.XENXE.getServicetype());
/* 63 */                   this.pps.proccess(cdto);
/* 64 */                 } else if (CommandEnum.HELP.getCommand().equals(cdto.getCommand())) {
/* 65 */                   this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, 
/* 66 */                     this.messageSource.getMessage("message.emailbot.help.subject", null, LocaleContextHolder.getLocale()), 
/* 67 */                     this.messageSource.getMessage("message.emailbot.help.text", null, LocaleContextHolder.getLocale()));
/*    */                 }
/*    */               }
/*    */               catch (CommandMalformedException e) {
/* 71 */                 this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, 
/* 72 */                   this.messageSource.getMessage("message.commandmalformed.subject", new Object[] { message.getText() }, LocaleContextHolder.getLocale()), 
/* 73 */                   this.messageSource.getMessage("message.commandmalformed.text", null, LocaleContextHolder.getLocale()));
/*    */               }
/*    */               
/*    */             }
/*    */             else
/*    */             {
/* 79 */               this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, 
/* 80 */                 this.messageSource.getMessage("message.notvalidemailsource.subject", null, LocaleContextHolder.getLocale()), 
/* 81 */                 this.messageSource.getMessage("message.notvalidemailsource.text", null, LocaleContextHolder.getLocale()));
/*    */             }
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   @Autowired
/*    */   PropertiesRepository pr;
/*    */   @Autowired
/*    */   MessageSource messageSource;
/*    */   @Autowired
/*    */   CommandValidatorService cvs;
/*    */   @Autowired
/*    */   PostProcessingService pps;
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\EmailBotImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */