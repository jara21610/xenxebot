/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class BillingAddress
/*    */ {
/*    */   private String street1;
/*    */   private String street2;
/*    */   private String city;
/*    */   private String state;
/*    */   private String country;
/*    */   private String postalCode;
/*    */   private String phone;
/*    */   
/*    */   public String getStreet1() {
/* 14 */     return this.street1;
/*    */   }
/*    */   
/*    */   public void setStreet1(String street1) {
/* 18 */     this.street1 = street1;
/*    */   }
/*    */   
/*    */   public String getStreet2() {
/* 22 */     return this.street2;
/*    */   }
/*    */   
/*    */   public void setStreet2(String street2) {
/* 26 */     this.street2 = street2;
/*    */   }
/*    */   
/*    */   public String getCity() {
/* 30 */     return this.city;
/*    */   }
/*    */   
/*    */   public void setCity(String city) {
/* 34 */     this.city = city;
/*    */   }
/*    */   
/*    */   public String getState() {
/* 38 */     return this.state;
/*    */   }
/*    */   
/*    */   public void setState(String state) {
/* 42 */     this.state = state;
/*    */   }
/*    */   
/*    */   public String getCountry() {
/* 46 */     return this.country;
/*    */   }
/*    */   
/*    */   public void setCountry(String country) {
/* 50 */     this.country = country;
/*    */   }
/*    */   
/*    */   public String getPostalCode() {
/* 54 */     return this.postalCode;
/*    */   }
/*    */   
/*    */   public void setPostalCode(String postalCode) {
/* 58 */     this.postalCode = postalCode;
/*    */   }
/*    */   
/*    */   public String getPhone() {
/* 62 */     return this.phone;
/*    */   }
/*    */   
/*    */   public void setPhone(String phone) {
/* 66 */     this.phone = phone;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\BillingAddress.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */