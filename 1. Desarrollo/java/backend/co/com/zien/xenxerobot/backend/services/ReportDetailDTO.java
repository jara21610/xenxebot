/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class ReportDetailDTO
/*    */ {
/*    */   private String type;
/*  9 */   private Map<String, Object> reportDetails = new HashMap();
/*    */   
/*    */   public String getType() {
/* 12 */     return this.type;
/*    */   }
/*    */   
/* 15 */   public void setType(String type) { this.type = type; }
/*    */   
/*    */   public Map<String, Object> getReportDetails() {
/* 18 */     return this.reportDetails;
/*    */   }
/*    */   
/* 21 */   public void setReportDetails(Map<String, Object> reportDetails) { this.reportDetails = reportDetails; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ReportDetailDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */