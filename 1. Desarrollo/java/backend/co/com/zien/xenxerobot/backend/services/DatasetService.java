package co.com.zien.xenxerobot.backend.services;

import java.math.BigDecimal;

public abstract interface DatasetService
{
  public abstract void export(BigDecimal... paramVarArgs)
    throws Exception;
  
  public abstract void generatePackgage(BigDecimal paramBigDecimal, String... paramVarArgs)
    throws Exception;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\DatasetService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */