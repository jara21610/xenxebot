/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.config.Config;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*    */ import co.com.zien.xenxerobot.backend.enumerator.TwitterSearchTypeEnum;
/*    */ import java.util.Calendar;
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ import org.junit.Test;
/*    */ import org.junit.runner.RunWith;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.test.context.ContextConfiguration;
/*    */ import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/*    */ import org.springframework.test.context.support.AnnotationConfigContextLoader;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @RunWith(SpringJUnit4ClassRunner.class)
/*    */ @ContextConfiguration(classes={Config.class}, loader=AnnotationConfigContextLoader.class)
/*    */ public class PostProcessingServiceImplTest
/*    */ {
/*    */   @Autowired
/*    */   PostProcessingService pps;
/*    */   
/*    */   @Test
/*    */   public void testPerformanceProccess()
/*    */   {
/* 33 */     int threads = 1;
/* 34 */     for (int i = 0; i < threads; i++) {
/* 35 */       ComandDTO cdto = new ComandDTO();
/*    */       
/* 37 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 38 */       cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/* 39 */       cdto.setArguments(new HashMap());
/* 40 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), "@petrogustavo");
/* 41 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/* 42 */       cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
/* 43 */       cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
/* 44 */       Calendar cal = Calendar.getInstance();
/* 45 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/* 46 */         cal.getTime());
/* 47 */       cal.add(5, -8);
/* 48 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/* 49 */         cal.getTime());
/*    */       
/*    */ 
/* 52 */       this.pps.proccess(cdto);
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PostProcessingServiceImplTest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */