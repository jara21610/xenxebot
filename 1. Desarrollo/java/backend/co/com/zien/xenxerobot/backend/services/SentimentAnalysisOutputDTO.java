/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ public class SentimentAnalysisOutputDTO
/*    */ {
/*    */   private String sentiment;
/*    */   private String category;
/*    */   private List<SentimentAnalysisConceptOutputDTO> conceptlist;
/*    */   
/*    */   public String getSentiment()
/*    */   {
/* 13 */     return this.sentiment;
/*    */   }
/*    */   
/*    */   public void setSentiment(String sentiment) {
/* 17 */     this.sentiment = sentiment;
/*    */   }
/*    */   
/*    */   public String getCategory() {
/* 21 */     return this.category;
/*    */   }
/*    */   
/*    */   public void setCategory(String category) {
/* 25 */     this.category = category;
/*    */   }
/*    */   
/*    */   public List<SentimentAnalysisConceptOutputDTO> getConceptlist() {
/* 29 */     return this.conceptlist;
/*    */   }
/*    */   
/*    */   public void setConceptlist(List<SentimentAnalysisConceptOutputDTO> conceptlist) {
/* 33 */     this.conceptlist = conceptlist;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SentimentAnalysisOutputDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */