/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.Date;
/*    */ 
/*    */ public class SocialConnectionDTO
/*    */ {
/*    */   private String keyData1;
/*    */   private String keyData2;
/*    */   private int remaining;
/*    */   private int limitcallsize;
/*    */   private int windowminutesize;
/*    */   private int secondsuntilreset;
/*    */   private Date windowStart;
/* 14 */   private Date windowFinish = null;
/*    */   private Object connection;
/*    */   private boolean active;
/*    */   
/*    */   public String getKeyData1() {
/* 19 */     return this.keyData1;
/*    */   }
/*    */   
/* 22 */   public void setKeyData1(String keyData1) { this.keyData1 = keyData1; }
/*    */   
/*    */   public String getKeyData2() {
/* 25 */     return this.keyData2;
/*    */   }
/*    */   
/* 28 */   public void setKeyData2(String keyData2) { this.keyData2 = keyData2; }
/*    */   
/*    */   public Date getWindowStart()
/*    */   {
/* 32 */     return this.windowStart;
/*    */   }
/*    */   
/* 35 */   public void setWindowStart(Date windowStart) { this.windowStart = windowStart; }
/*    */   
/*    */   public Date getWindowFinish() {
/* 38 */     return this.windowFinish;
/*    */   }
/*    */   
/* 41 */   public void setWindowFinish(Date windowFinish) { this.windowFinish = windowFinish; }
/*    */   
/*    */   public int getSecondsuntilreset() {
/* 44 */     return this.secondsuntilreset;
/*    */   }
/*    */   
/* 47 */   public void setSecondsuntilreset(int secondsuntilreset) { this.secondsuntilreset = secondsuntilreset; }
/*    */   
/*    */   public Object getConnection() {
/* 50 */     return this.connection;
/*    */   }
/*    */   
/* 53 */   public void setConnection(Object connection) { this.connection = connection; }
/*    */   
/*    */   public int getRemaining() {
/* 56 */     return this.remaining;
/*    */   }
/*    */   
/* 59 */   public void setRemaining(int remaining) { this.remaining = remaining; }
/*    */   
/*    */   public boolean isActive() {
/* 62 */     return this.active;
/*    */   }
/*    */   
/* 65 */   public void setActive(boolean active) { this.active = active; }
/*    */   
/*    */   public int getWindowminutesize() {
/* 68 */     return this.windowminutesize;
/*    */   }
/*    */   
/* 71 */   public void setWindowminutesize(int windowminutesize) { this.windowminutesize = windowminutesize; }
/*    */   
/*    */   public int getLimitcallsize() {
/* 74 */     return this.limitcallsize;
/*    */   }
/*    */   
/* 77 */   public void setLimitcallsize(int limitcallsize) { this.limitcallsize = limitcallsize; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SocialConnectionDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */