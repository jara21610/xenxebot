/*     */ package co.com.zien.xenxerobot.backend.config;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.telegram.bot.PreXtigeBot;
/*     */ import co.com.zien.xenxerobot.backend.telegram.bot.StressLoadTestBot;
/*     */ import co.com.zien.xenxerobot.backend.telegram.bot.XenxeBot;
/*     */ import co.com.zien.xenxerobot.persistence.config.PersistenceConfig;
/*     */ import java.util.concurrent.Executor;
/*     */ import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
/*     */ import org.springframework.beans.BeansException;
/*     */ import org.springframework.context.ApplicationContext;
/*     */ import org.springframework.context.ApplicationContextAware;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.context.annotation.Bean;
/*     */ import org.springframework.context.annotation.ComponentScan;
/*     */ import org.springframework.context.annotation.Configuration;
/*     */ import org.springframework.context.annotation.Import;
/*     */ import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
/*     */ import org.springframework.context.support.ReloadableResourceBundleMessageSource;
/*     */ import org.springframework.scheduling.annotation.AsyncConfigurer;
/*     */ import org.springframework.scheduling.annotation.EnableAsync;
/*     */ import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
/*     */ import org.telegram.telegrambots.ApiContextInitializer;
/*     */ import org.telegram.telegrambots.TelegramBotsApi;
/*     */ import org.telegram.telegrambots.exceptions.TelegramApiException;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Configuration
/*     */ @EnableAsync
/*     */ @ComponentScan({"co.com.zien.xenxerobot.backend", "co.com.jit.backend"})
/*     */ @Import({PersistenceConfig.class})
/*     */ public class Config
/*     */   implements AsyncConfigurer, ApplicationContextAware
/*     */ {
/*  43 */   static ApplicationContext applicationContext = null;
/*  44 */   private PreXtigeBot PreXtigeBot = null;
/*  45 */   private XenxeBot XenxeBot = null;
/*  46 */   private StressLoadTestBot StressLoadTestBot = null;
/*  47 */   TelegramBotsApi botsApi = new TelegramBotsApi();
/*     */   
/*  49 */   static { ApiContextInitializer.init(); }
/*     */   
/*     */   @Bean(name={"messageSource"})
/*     */   public MessageSource messageSource()
/*     */   {
/*  54 */     ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
/*  55 */     messageSource.setBasename("classpath:/resources/usermsg");
/*  56 */     messageSource.setDefaultEncoding("UTF-8");
/*  57 */     return messageSource;
/*     */   }
/*     */   
/*     */   public Executor getAsyncExecutor()
/*     */   {
/*  62 */     ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
/*  63 */     executor.setCorePoolSize(10);
/*  64 */     executor.setMaxPoolSize(Integer.MAX_VALUE);
/*  65 */     executor.setQueueCapacity(0);
/*  66 */     executor.setThreadNamePrefix("Spring-");
/*  67 */     executor.initialize();
/*  68 */     return executor;
/*     */   }
/*     */   
/*     */   @Bean(name={"PreXtige"})
/*     */   public PreXtigeBot PreXtigeBot()
/*     */   {
/*  74 */     if (this.PreXtigeBot == null)
/*     */     {
/*  76 */       this.PreXtigeBot = new PreXtigeBot();
/*     */       try {
/*  78 */         this.botsApi.registerBot(this.PreXtigeBot);
/*     */       }
/*     */       catch (TelegramApiException e)
/*     */       {
/*  82 */         e.printStackTrace();
/*     */       }
/*  84 */       return this.PreXtigeBot;
/*     */     }
/*  86 */     return this.PreXtigeBot;
/*     */   }
/*     */   
/*     */ 
/*     */   @Bean(name={"Xenxe"})
/*     */   public XenxeBot xenxeBot()
/*     */   {
/*  93 */     if (this.XenxeBot == null) {
/*  94 */       this.XenxeBot = new XenxeBot();
/*     */       try {
/*  96 */         this.botsApi.registerBot(this.XenxeBot);
/*     */       }
/*     */       catch (TelegramApiException e)
/*     */       {
/* 100 */         e.printStackTrace();
/*     */       }
/* 102 */       return this.XenxeBot;
/*     */     }
/* 104 */     return this.XenxeBot;
/*     */   }
/*     */   
/*     */ 
/*     */   @Bean(name={"StressLoadTestBot"})
/*     */   public StressLoadTestBot stressLoadTestBot()
/*     */   {
/* 111 */     if (this.StressLoadTestBot == null) {
/* 112 */       this.StressLoadTestBot = new StressLoadTestBot();
/*     */       try {
/* 114 */         this.botsApi.registerBot(this.StressLoadTestBot);
/*     */       }
/*     */       catch (TelegramApiException e)
/*     */       {
/* 118 */         e.printStackTrace();
/*     */       }
/* 120 */       return this.StressLoadTestBot;
/*     */     }
/* 122 */     return this.StressLoadTestBot;
/*     */   }
/*     */   
/*     */   @Bean
/*     */   public PropertySourcesPlaceholderConfigurer placeHolderConfigurer()
/*     */   {
/* 128 */     return new PropertySourcesPlaceholderConfigurer();
/*     */   }
/*     */   
/*     */ 
/*     */   public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler()
/*     */   {
/* 134 */     return null;
/*     */   }
/*     */   
/*     */   public void setApplicationContext(ApplicationContext context) throws BeansException {
/* 138 */     applicationContext = context;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static ApplicationContext getContext()
/*     */   {
/* 145 */     return applicationContext;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\config\Config.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */