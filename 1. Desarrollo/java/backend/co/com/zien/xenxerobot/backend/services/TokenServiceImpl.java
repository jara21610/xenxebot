/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*    */ import co.com.zien.xenxerobot.persistence.entity.Usertoken;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UsertokenRepository;
/*    */ import java.util.Calendar;
/*    */ import java.util.Date;
/*    */ import java.util.UUID;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ public class TokenServiceImpl
/*    */   implements TokenService
/*    */ {
/*    */   @Autowired
/*    */   UsertokenRepository utr;
/*    */   
/*    */   public String generateUserToken(Users userId)
/*    */   {
/* 24 */     Usertoken ut = new Usertoken();
/* 25 */     ut.setIduser(userId);
/* 26 */     String token = UUID.randomUUID().toString();
/* 27 */     ut.setToken(token);
/* 28 */     Calendar cal = Calendar.getInstance();
/* 29 */     cal.add(12, 10);
/* 30 */     ut.setUntilvaliddate(cal.getTime());
/* 31 */     this.utr.saveAndFlush(ut);
/* 32 */     return token;
/*    */   }
/*    */   
/*    */   public Users validateUserToken(String usertoken)
/*    */     throws TokenNotExistException, TokenExpiredException
/*    */   {
/* 38 */     Usertoken ut = (Usertoken)this.utr.findOne(usertoken);
/*    */     
/* 40 */     if (ut != null)
/*    */     {
/* 42 */       if (ut.getUntilvaliddate().after(new Date())) {
/* 43 */         return ut.getIduser();
/*    */       }
/* 45 */       throw new TokenExpiredException();
/*    */     }
/*    */     
/*    */ 
/* 49 */     throw new TokenNotExistException();
/*    */   }
/*    */   
/*    */   public Users getUserOwnerToken(String usertoken)
/*    */     throws TokenNotExistException
/*    */   {
/* 55 */     Usertoken ut = (Usertoken)this.utr.findOne(usertoken);
/*    */     
/* 57 */     if (ut != null) {
/* 58 */       return ut.getIduser();
/*    */     }
/* 60 */     throw new TokenNotExistException();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TokenServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */