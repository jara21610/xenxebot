/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum ConceptsEnum
/*    */ {
/*  6 */   ENTIDAD("ENTIDAD"),  TOPICO("TOPICO");
/*    */   
/*    */   private String concept;
/*    */   
/*    */   public String getConcept() {
/* 11 */     return this.concept;
/*    */   }
/*    */   
/*    */   private ConceptsEnum(String concept) {
/* 15 */     this.concept = concept;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\ConceptsEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */