/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum ChannelEnum
/*    */ {
/*  6 */   EMAIL("EMAIL"),  TELEGRAM("TELEGRAM");
/*    */   
/*    */   private String channel;
/*    */   
/*    */   public String getChannel() {
/* 11 */     return this.channel;
/*    */   }
/*    */   
/*    */   private ChannelEnum(String channel) {
/* 15 */     this.channel = channel;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\ChannelEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */