/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.entity.Userplan;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ @Service
/*    */ public class TransactionCounterServiceImpl
/*    */   implements TransactionCounterService
/*    */ {
/*    */   @Autowired
/*    */   UserplanRepository upr;
/*    */   
/*    */   public synchronized int addUsedTransactionPlan(String userid, int tx)
/*    */     throws TxLimitException
/*    */   {
/* 19 */     Userplan up = this.upr.activePlanByUseridandstate(userid, "ACTIVO");
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 24 */     if (up != null) {
/* 25 */       if (up.getTxplancapacity().intValue() != -1) {
/* 26 */         int txavailable = up.getTxplancapacity().intValue() - up.getTxpalused().intValue();
/* 27 */         if (txavailable <= 0) {
/* 28 */           up.setState("FINALIZADO");
/* 29 */           this.upr.saveAndFlush(up);
/* 30 */           throw new TxLimitException();
/*    */         }
/* 32 */         if (txavailable >= tx) {
/* 33 */           up.setTxpalused(Integer.valueOf(up.getTxpalused().intValue() + tx));
/* 34 */           this.upr.saveAndFlush(up);
/* 35 */           return tx;
/*    */         }
/* 37 */         up.setTxpalused(Integer.valueOf(up.getTxpalused().intValue() + txavailable));
/* 38 */         this.upr.saveAndFlush(up);
/* 39 */         return txavailable;
/*    */       }
/*    */       
/*    */ 
/* 43 */       up.setTxpalused(Integer.valueOf(up.getTxpalused().intValue() + tx));
/* 44 */       this.upr.saveAndFlush(up);
/* 45 */       return tx;
/*    */     }
/*    */     
/* 48 */     throw new TxLimitException();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TransactionCounterServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */