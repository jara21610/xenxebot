/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import com.google.common.collect.Lists;
/*     */ import com.restfb.Connection;
/*     */ import com.restfb.FacebookClient;
/*     */ import com.restfb.Parameter;
/*     */ import com.restfb.types.Comment;
/*     */ import com.restfb.types.Post;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.concurrent.CompletableFuture;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.beans.factory.annotation.Qualifier;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.social.twitter.api.Twitter;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service("FacebookProcessPost")
/*     */ public class FacebookProcessPostServiceImpl
/*     */   implements SourceProcessPostService
/*     */ {
/*  40 */   private static final Logger logger = Logger.getLogger(TwitterProcessPostServiceImpl.class);
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   PreproccesingService scs;
/*     */   @Autowired
/*     */   SentimentService ss;
/*     */   @Autowired
/*     */   @Qualifier("TwitterPoolConectionManager")
/*     */   SocialPoolConectionManager tpcm;
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   
/*     */   public void process(CommandProcess cp, ComandDTO cdto) throws Exception
/*     */   {
/*  55 */     String searchCriteria = (String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
/*  56 */     List<Comment> currentlt = new ArrayList();
/*     */     
/*  58 */     int pagesize = new Integer(this.pr.findByName(PropertyEnum.TWITTERPAGESIZE.getProperty())).intValue();
/*     */     
/*     */ 
/*  61 */     FacebookClient facebookClient = (FacebookClient)getAvailableConecction();
/*  62 */     Connection<Post> postConnection = 
/*  63 */       facebookClient.fetchConnection(searchCriteria + "/posts", 
/*  64 */       Post.class, new Parameter[0]);
/*     */     
/*  66 */     for (List<Post> myFeedPage : postConnection)
/*     */     {
/*  68 */       if (!myFeedPage.isEmpty()) {
/*  69 */         FacebookClient facebookClient1 = (FacebookClient)getAvailableConecction();
/*  70 */         Iterator localIterator3; for (Iterator localIterator2 = myFeedPage.iterator(); localIterator2.hasNext(); 
/*     */             
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  76 */             localIterator3.hasNext())
/*     */         {
/*  70 */           Post post = (Post)localIterator2.next();
/*  71 */           facebookClient1 = (FacebookClient)getAvailableConecction();
/*  72 */           String postId = post.getId();
/*  73 */           Connection<Comment> commentConnection = 
/*  74 */             facebookClient.fetchConnection(postId + "/comments", 
/*  75 */             Comment.class, new Parameter[] { Parameter.with("limit", Integer.valueOf(100)) });
/*  76 */           localIterator3 = commentConnection.iterator(); continue;List<Comment> commentPage = (List)localIterator3.next();
/*  77 */           facebookClient1 = (FacebookClient)getAvailableConecction();
/*  78 */           List<List<Comment>> smallerLists = Lists.partition(commentPage, 25);
/*     */           
/*     */ 
/*  81 */           CompletableFuture[] fs = 
/*  82 */             new CompletableFuture[smallerLists.size()];
/*     */           
/*  84 */           int i = 0;
/*  85 */           for (List<Comment> lt : smallerLists) {
/*  86 */             page = this.tpt.executeUnit(lt, cp, cdto);
/*  87 */             fs[i] = page;
/*  88 */             i++;
/*     */           }
/*     */           
/*  91 */           CompletableFuture.allOf(fs);
/*     */           CompletableFuture[] arrayOfCompletableFuture1;
/*  93 */           CompletableFuture<String> page = (arrayOfCompletableFuture1 = fs).length; for (CompletableFuture<String> localCompletableFuture1 = 0; localCompletableFuture1 < page; localCompletableFuture1++) { CompletableFuture<String> page = arrayOfCompletableFuture1[localCompletableFuture1];
/*  94 */             if ("stop".equals(page.get())) {
/*  95 */               return;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 102 */         return;
/*     */       } }
/*     */   }
/*     */   
/*     */   @Autowired
/*     */   @Qualifier("FacebookProcessUnit")
/*     */   SourceProcessUnit tpt;
/*     */   
/* 110 */   private Object getAvailableConecction() throws Exception { boolean success = false;
/* 111 */     while (!success) {
/*     */       try {
/* 113 */         success = true;
/* 114 */         return (Twitter)this.tpcm.getAvailableConecction(new String[0]);
/*     */       } catch (SocialPoolConectionManagerNotFoundAvailableException e) {
/* 116 */         success = false;
/* 117 */         Thread.currentThread();
/* 118 */         Thread.sleep(this.tpcm.getMinimalSecondResetConection());
/*     */       }
/*     */     }
/* 121 */     return null;
/*     */   }
/*     */   
/*     */   @Autowired
/*     */   @Qualifier("EmailResponseBot")
/*     */   ResponseBotService erbs;
/*     */   @Autowired
/*     */   @Qualifier("TelegramResponseBot")
/*     */   ResponseBotService trbs;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   @Autowired
/*     */   CommandProcessRepository cpr;
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\FacebookProcessPostServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */