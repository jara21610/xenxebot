/*     */ package co.com.zien.xenxerobot.backend.services.dataset;
/*     */ 
/*     */ import co.com.zien.xenxerobot.persistence.entity.Concepts;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Packages;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Post;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Worker;
/*     */ import co.com.zien.xenxerobot.persistence.repository.ConceptsRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PackagesRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import java.math.BigDecimal;
/*     */ import java.util.List;
/*     */ import org.apache.commons.lang3.StringUtils;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.data.domain.PageRequest;
/*     */ import org.springframework.data.domain.Pageable;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service("packagesService")
/*     */ public class PackagesServiceImpl
/*     */   implements PackagesService
/*     */ {
/*     */   @Autowired
/*     */   PackagesRepository pr;
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   ConceptsRepository cr;
/*     */   
/*     */   public List<Packages> findByWorker(Worker worker)
/*     */   {
/*  33 */     return this.pr.findByWorker(worker);
/*     */   }
/*     */   
/*     */   public List<Packages> findByState(String state)
/*     */   {
/*  38 */     return this.pr.findByState(state);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<Post> nextworkitembypackage(BigDecimal id)
/*     */   {
/*  44 */     return this.postr.nextworkitembypackage(id);
/*     */   }
/*     */   
/*     */ 
/*     */   public void savehumantask(Post p)
/*     */   {
/*  50 */     for (Concepts c : p.getConceptsCollection()) {
/*  51 */       this.cr.saveAndFlush(c);
/*     */     }
/*  53 */     this.postr.saveAndFlush(p);
/*     */   }
/*     */   
/*     */   public void savePackage(Packages p)
/*     */   {
/*  58 */     this.pr.saveAndFlush(p);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Packages findPackage(BigDecimal id)
/*     */   {
/*  65 */     return (Packages)this.pr.findOne(id);
/*     */   }
/*     */   
/*     */   public Integer packageSize(BigDecimal id)
/*     */   {
/*  70 */     return this.postr.packageSize(id);
/*     */   }
/*     */   
/*     */   public Post change(Post current)
/*     */   {
/*  75 */     int i = 0;
/*  76 */     Post newpost = null;
/*     */     
/*  78 */     Pageable pageable = new PageRequest(i, 1);
/*  79 */     List<Post> changelist = this.postr.posttochange(current.getIdPackage().getId(), pageable);
/*  80 */     if (changelist != null) {
/*  81 */       newpost = (Post)changelist.get(0);
/*     */     } else {
/*  83 */       boolean similartoany = true;
/*  84 */       List<Post> packagepost = this.postr.postbypackage(current.getIdPackage().getId());
/*  85 */       while (similartoany) {
/*  86 */         pageable = new PageRequest(i, 1);
/*  87 */         newpost = (Post)this.postr.findOne((String)((Object[])this.postr.posttopackage(current.getCommandprocess().getId(), pageable).get(0))[0]);
/*  88 */         similartoany = false;
/*  89 */         for (Post p : packagepost) {
/*  90 */           int distance = StringUtils.getLevenshteinDistance(p.getOriginalmessage(), newpost.getOriginalmessage());
/*  91 */           double similarity = 100.0D - distance / Math.max(p.getOriginalmessage().length(), current.getOriginalmessage().length()) * 100.0D;
/*  92 */           if (similarity > 80.0D) {
/*  93 */             similartoany = true;
/*  94 */             newpost.setDiscartedtopack("Y");
/*  95 */             this.postr.saveAndFlush(newpost);
/*  96 */             break;
/*     */           }
/*     */         }
/*     */         
/* 100 */         if (!similartoany) {
/*     */           break;
/*     */         }
/* 103 */         i++;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 109 */     newpost.setIdPackage(current.getIdPackage());
/* 110 */     newpost.setOptiontochange(null);
/* 111 */     current.setDiscartedtopack("Y");
/* 112 */     current.setIdPackage(null);
/* 113 */     this.postr.saveAndFlush(newpost);
/* 114 */     this.postr.saveAndFlush(current);
/* 115 */     return newpost;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\dataset\PackagesServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */