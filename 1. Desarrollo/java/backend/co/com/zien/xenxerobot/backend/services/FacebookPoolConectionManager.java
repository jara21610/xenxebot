/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.repository.SocialConnectionRepository;
/*    */ import com.restfb.Connection;
/*    */ import com.restfb.DebugHeaderInfo;
/*    */ import com.restfb.DebugHeaderInfo.HeaderUsage;
/*    */ import com.restfb.DefaultFacebookClient;
/*    */ import com.restfb.FacebookClient;
/*    */ import com.restfb.Parameter;
/*    */ import com.restfb.Version;
/*    */ import com.restfb.WebRequestor;
/*    */ import com.restfb.types.User;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.MessageSource;
/*    */ import org.springframework.stereotype.Service;
/*    */ import org.springframework.transaction.annotation.Transactional;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service("FacebookPoolConectionManager")
/*    */ @Transactional
/*    */ public class FacebookPoolConectionManager
/*    */   implements SocialPoolConectionManager
/*    */ {
/* 33 */   private static final Logger logger = Logger.getLogger(FacebookPoolConectionManager.class);
/*    */   
/* 35 */   List<SocialConnectionDTO> scdtolist = null;
/* 36 */   int windowsizeminutes = 0;
/* 37 */   int limitcallsallowed = 0;
/*    */   @Autowired
/*    */   SocialConnectionRepository scr;
/*    */   @Autowired
/*    */   MessageSource messageSource;
/*    */   
/*    */   public void init()
/*    */     throws SocialPoolConectionManagerNotConfigured
/*    */   {}
/*    */   
/*    */   public Object getAvailableConecction(String... parameter) throws SocialPoolConectionManagerNotConfigured, SocialPoolConectionManagerNotFoundAvailableException
/*    */   {
/* 49 */     FacebookClient facebookClient = new DefaultFacebookClient(parameter[0], Version.LATEST);
/* 50 */     Connection<User> postConnection = 
/* 51 */       facebookClient.fetchConnection("me", User.class, new Parameter[0]);
/* 52 */     if ((facebookClient.getWebRequestor().getDebugHeaderInfo().getAppUsage().getCallCount().intValue() > 80) && 
/* 53 */       (facebookClient.getWebRequestor().getDebugHeaderInfo().getAppUsage().getTotalCputime().intValue() > 80) && 
/* 54 */       (facebookClient.getWebRequestor().getDebugHeaderInfo().getAppUsage().getTotalTime().intValue() > 80)) {
/* 55 */       return facebookClient;
/*    */     }
/*    */     
/* 58 */     throw new SocialPoolConectionManagerNotFoundAvailableException("");
/*    */   }
/*    */   
/*    */ 
/*    */   public List<SocialConnectionDTO> getCurrentState()
/*    */   {
/* 64 */     return this.scdtolist;
/*    */   }
/*    */   
/*    */   public Object getSocialServerLimitStatus()
/*    */   {
/* 69 */     return null;
/*    */   }
/*    */   
/*    */   public int getMinimalSecondResetConection()
/*    */   {
/* 74 */     return 216000;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\FacebookPoolConectionManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */