/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum CommandArgumentsEnum
/*    */ {
/*  6 */   SOCIALMEDIASOURCE("SOCIALMEDIASOURCE"),  SEARCHTYPE("SEARCHTYPE"),  SEARCHCRITERIA("SEARCHCRITERIA"),  SEARCHSINCE("SEARCHSINCE"), 
/*  7 */   SEARCHUNTIL("SEARCHUNTIL"),  POSTLIST("POSTLIST"),  COMMANDPROCESSID("COMMANDPROCESSID"),  SERVICETYPE("SERVICETYPE"),  EMAIL("EMAIL");
/*    */   
/*    */   private String commandargument;
/*    */   
/*    */   public String getCommandargument() {
/* 12 */     return this.commandargument;
/*    */   }
/*    */   
/*    */   private CommandArgumentsEnum(String commandargument) {
/* 16 */     this.commandargument = commandargument;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\CommandArgumentsEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */