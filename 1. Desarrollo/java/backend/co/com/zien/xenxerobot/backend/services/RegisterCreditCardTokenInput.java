/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class RegisterCreditCardTokenInput
/*    */ {
/*    */   private String language;
/*    */   private String command;
/*    */   private MerchantInput merchant;
/*    */   private CreditCardToken creditCardToken;
/*    */   
/*    */   public String getLanguage() {
/* 11 */     return this.language;
/*    */   }
/*    */   
/* 14 */   public void setLanguage(String language) { this.language = language; }
/*    */   
/*    */   public String getCommand() {
/* 17 */     return this.command;
/*    */   }
/*    */   
/* 20 */   public void setCommand(String command) { this.command = command; }
/*    */   
/*    */   public MerchantInput getMerchant() {
/* 23 */     return this.merchant;
/*    */   }
/*    */   
/* 26 */   public void setMerchant(MerchantInput merchant) { this.merchant = merchant; }
/*    */   
/*    */   public CreditCardToken getCreditCardToken() {
/* 29 */     return this.creditCardToken;
/*    */   }
/*    */   
/* 32 */   public void setCreditCardToken(CreditCardToken creditCardToken) { this.creditCardToken = creditCardToken; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\RegisterCreditCardTokenInput.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */