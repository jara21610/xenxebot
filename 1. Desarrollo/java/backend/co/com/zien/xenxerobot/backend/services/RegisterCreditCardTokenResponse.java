/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class RegisterCreditCardTokenResponse
/*    */ {
/*    */   private String code;
/*    */   private String error;
/*    */   private CreditCardToken creditCardToken;
/*    */   
/*    */   public String getCode()
/*    */   {
/* 11 */     return this.code;
/*    */   }
/*    */   
/* 14 */   public void setCode(String code) { this.code = code; }
/*    */   
/*    */   public CreditCardToken getCreditCardToken() {
/* 17 */     return this.creditCardToken;
/*    */   }
/*    */   
/* 20 */   public void setCreditCardToken(CreditCardToken creditCardToken) { this.creditCardToken = creditCardToken; }
/*    */   
/*    */   public String getError() {
/* 23 */     return this.error;
/*    */   }
/*    */   
/* 26 */   public void setError(String error) { this.error = error; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\RegisterCreditCardTokenResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */