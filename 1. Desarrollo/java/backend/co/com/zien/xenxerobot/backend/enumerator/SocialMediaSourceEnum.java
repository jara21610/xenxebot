/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum SocialMediaSourceEnum
/*    */ {
/*  6 */   TWITTER("TWITTER"),  FACEBOOK("FACEBOOK");
/*    */   
/*    */   private String socialmediasource;
/*    */   
/*    */   public String getSocialmediasource() {
/* 11 */     return this.socialmediasource;
/*    */   }
/*    */   
/*    */   private SocialMediaSourceEnum(String socialmediasource) {
/* 15 */     this.socialmediasource = socialmediasource;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\SocialMediaSourceEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */