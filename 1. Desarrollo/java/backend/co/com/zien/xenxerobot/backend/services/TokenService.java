package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.Users;

public abstract interface TokenService
{
  public abstract String generateUserToken(Users paramUsers);
  
  public abstract Users validateUserToken(String paramString)
    throws TokenNotExistException, TokenExpiredException;
  
  public abstract Users getUserOwnerToken(String paramString)
    throws TokenNotExistException;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TokenService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */