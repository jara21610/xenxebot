/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.jit.backend.util.Utils;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.SentimentEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import com.thoughtworks.xstream.XStream;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.text.DateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import org.apache.poi.hssf.usermodel.HSSFWorkbook;
/*     */ import org.apache.poi.ss.usermodel.Cell;
/*     */ import org.apache.poi.ss.usermodel.Row;
/*     */ import org.apache.poi.ss.usermodel.Sheet;
/*     */ import org.apache.poi.ss.usermodel.Workbook;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.context.i18n.LocaleContextHolder;
/*     */ import org.springframework.core.env.Environment;
/*     */ import org.springframework.scheduling.annotation.Async;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service("XenxeReportDetailManager")
/*     */ public class ReportDetailManagerXenxeBot
/*     */   implements ReportDetailManager
/*     */ {
/*     */   @Autowired
/*     */   CommandProcessRepository cpr;
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   @Autowired
/*     */   ResponseBotServiceFactory rbsf;
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   Environment env;
/*     */   
/*     */   public void saveStatus(CommandProcess cp, ReportDetailDTO rrdto)
/*     */   {
/*  72 */     XStream xs = new XStream();
/*  73 */     String reportdetail = xs.toXML(rrdto);
/*  74 */     cp.setReportdetail(reportdetail);
/*  75 */     this.cpr.saveAndFlush(cp);
/*  76 */     System.out.println("PASE");
/*     */   }
/*     */   
/*     */   public String generateSentimentAnalysisReport(String cpId)
/*     */     throws Exception
/*     */   {
/*  82 */     String path = "";
/*  83 */     String pathpdf = "";
/*  84 */     CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
/*     */     
/*  86 */     int total = this.postr.totalPost(cp.getId()).intValue();
/*  87 */     if (total > 0) {
/*  88 */       XStream xs = new XStream();
/*  89 */       ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/*     */       
/*  91 */       String name = this.pr.findByName(PropertyEnum.XENXETEMPLATE.getProperty());
/*  92 */       Workbook wb = new HSSFWorkbook(new FileInputStream(name));
/*     */       
/*  94 */       Sheet s1 = wb.getSheetAt(0);
/*  95 */       s1.getRow(4).getCell(2).setCellValue(cp.getUser().getName() + " " + cp.getUser().getLastname());
/*     */       
/*     */ 
/*     */ 
/*  99 */       s1.getRow(5).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()));
/* 100 */       s1.getRow(6).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()));
/* 101 */       s1.getRow(7).getCell(2).setCellValue(total);
/*     */       
/* 103 */       Date date = new Date();
/* 104 */       DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
/*     */       
/* 106 */       s1.getRow(4).getCell(5).setCellValue(df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())));
/*     */       
/* 108 */       s1.getRow(5).getCell(5).setCellValue(df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())));
/*     */       
/* 110 */       s1.getRow(6).getCell(5).setCellValue(df.format(date));
/*     */       
/* 112 */       int positive = this.postr.totalXSentiment(cp.getId(), SentimentEnum.POSITIVE.getSentiment()).intValue();
/* 113 */       int negative = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEGATIVE.getSentiment()).intValue();
/* 114 */       int neutral = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEUTRAL.getSentiment()).intValue();
/*     */       
/*     */ 
/* 117 */       s1.getRow(11).getCell(2).setCellValue(positive / total);
/* 118 */       s1.getRow(12).getCell(2).setCellValue(negative / total);
/* 119 */       s1.getRow(13).getCell(2).setCellValue(neutral / total);
/*     */       
/* 121 */       String ruta = this.env.getRequiredProperty("outputpath");
/* 122 */       String filename = UUID.randomUUID().toString() + "_" + 
/* 123 */         cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) + "_" + df.format(date);
/* 124 */       path = ruta + filename + ".xls";
/*     */       
/* 126 */       FileOutputStream fileOut = new FileOutputStream(path);
/* 127 */       wb.write(fileOut);
/* 128 */       fileOut.close();
/*     */       
/* 130 */       pathpdf = ruta + filename + ".pdf";
/*     */       
/* 132 */       Utils.convertDocuments(path, pathpdf);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 137 */     return pathpdf;
/*     */   }
/*     */   
/*     */   @Async
/*     */   public void reportdetailgenerateSentimentAnalysisReport(String cpId) throws Exception {
/* 142 */     String path = generateSentimentAnalysisReport(cpId);
/* 143 */     ResponseBotService rbs = this.rbsf.getResponseBotService(ChannelEnum.TELEGRAM.getChannel());
/* 144 */     CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
/* 145 */     XStream xs = new XStream();
/* 146 */     ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/* 147 */     rbs.okPartialReportResponseBot(path, cdto);
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ReportDetailManagerXenxeBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */