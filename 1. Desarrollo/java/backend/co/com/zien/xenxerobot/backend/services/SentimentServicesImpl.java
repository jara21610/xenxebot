/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.sun.jersey.api.client.Client;
/*    */ import com.sun.jersey.api.client.ClientResponse;
/*    */ import com.sun.jersey.api.client.WebResource;
/*    */ import com.sun.jersey.api.client.WebResource.Builder;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.core.env.Environment;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ @Service
/*    */ public class SentimentServicesImpl
/*    */   implements SentimentService
/*    */ {
/*    */   @Autowired
/*    */   Environment env;
/*    */   
/*    */   public SentimentAnalysisOutputDTO getSentimentAnalysis(String texttoanalize, String servicetype)
/*    */   {
/* 21 */     String uri = this.env.getRequiredProperty("mlservice.url");
/*    */     
/* 23 */     SentimentAnalysisInputDTO sadto = new SentimentAnalysisInputDTO();
/* 24 */     sadto.setTweet(texttoanalize);
/* 25 */     sadto.setServicetype(servicetype);
/* 26 */     Client client = Client.create();
/*    */     
/* 28 */     WebResource webResource = client
/* 29 */       .resource(uri);
/*    */     
/* 31 */     Gson gson = new Gson();
/* 32 */     String input = gson.toJson(sadto);
/*    */     
/* 34 */     ClientResponse response = 
/* 35 */       (ClientResponse)webResource.type("application/json").post(ClientResponse.class, input);
/*    */     
/* 37 */     if (response.getStatus() != 200) {
/* 38 */       throw new RuntimeException("Failed : HTTP error code : " + 
/* 39 */         response.getStatus());
/*    */     }
/*    */     
/* 42 */     return (SentimentAnalysisOutputDTO)gson.fromJson((String)response.getEntity(String.class), SentimentAnalysisOutputDTO.class);
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SentimentServicesImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */