/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.enumerator.FacebookSearchTypeEnum;
/*     */ import com.restfb.Connection;
/*     */ import com.restfb.ConnectionIterator;
/*     */ import com.restfb.FacebookClient;
/*     */ import com.restfb.Parameter;
/*     */ import com.restfb.types.Comment;
/*     */ import com.restfb.types.Post;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.beans.factory.annotation.Qualifier;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.springframework.transaction.annotation.Transactional;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service
/*     */ @Transactional
/*     */ public class FacebookServiceImpl
/*     */   implements FacebookService
/*     */ {
/*     */   @Autowired
/*     */   @Qualifier("FacebookPoolConectionManager")
/*     */   SocialPoolConectionManager fpcm;
/*     */   
/*     */   private List<Comment> searchCommentsfromPage(String pageid, String since)
/*     */     throws Exception
/*     */   {
/*  35 */     List<Comment> allcoments = new ArrayList();
/*  36 */     FacebookClient facebookClient = (FacebookClient)getAvailableConecction();
/*  37 */     Connection<Post> postConnection = 
/*  38 */       facebookClient.fetchConnection(pageid + "/posts", 
/*  39 */       Post.class, new Parameter[] { Parameter.with("since", since) });
/*     */     Iterator localIterator2;
/*  41 */     for (Iterator localIterator1 = postConnection.iterator(); localIterator1.hasNext(); 
/*  42 */         localIterator2.hasNext())
/*     */     {
/*  41 */       List<Post> myFeedPage = (List)localIterator1.next();
/*  42 */       localIterator2 = myFeedPage.iterator(); continue;Post post = (Post)localIterator2.next();
/*  43 */       String postId = post.getId();
/*  44 */       Connection<Comment> commentConnection = 
/*  45 */         facebookClient.fetchConnection(postId + "/comments", 
/*  46 */         Comment.class, new Parameter[0]);
/*  47 */       for (List<Comment> commentPage : commentConnection) {
/*  48 */         allcoments.addAll(commentPage);
/*     */       }
/*     */     }
/*     */     
/*  52 */     return allcoments;
/*     */   }
/*     */   
/*     */   public List<Post> searchlastPosts(String pageid, int numberPost) throws Exception
/*     */   {
/*  57 */     FacebookClient facebookClient = (FacebookClient)getAvailableConecction();
/*  58 */     Connection<Post> postConnection = 
/*  59 */       facebookClient.fetchConnection(pageid + "/posts", 
/*  60 */       Post.class, new Parameter[] { Parameter.with("limit", Integer.valueOf(numberPost)) });
/*  61 */     if (postConnection.iterator().hasNext()) {
/*  62 */       return (List)postConnection.iterator().next();
/*     */     }
/*  64 */     return null;
/*     */   }
/*     */   
/*     */   private List<Comment> searchCommentsfromPosts(String postid, String since) throws Exception
/*     */   {
/*  69 */     List<Comment> allcoments = new ArrayList();
/*  70 */     FacebookClient facebookClient = (FacebookClient)getAvailableConecction();
/*     */     
/*  72 */     Connection<Comment> commentConnection = 
/*  73 */       facebookClient.fetchConnection(postid + "/comments", 
/*  74 */       Comment.class, new Parameter[0]);
/*  75 */     for (List<Comment> commentPage : commentConnection) {
/*  76 */       allcoments.addAll(commentPage);
/*     */     }
/*     */     
/*  79 */     return allcoments;
/*     */   }
/*     */   
/*     */   private Object getAvailableConecction() throws Exception {
/*  83 */     boolean success = false;
/*  84 */     while (!success) {
/*     */       try {
/*  86 */         success = true;
/*  87 */         return (FacebookClient)this.fpcm.getAvailableConecction(new String[0]);
/*     */       } catch (SocialPoolConectionManagerNotFoundAvailableException e) {
/*  89 */         Thread.currentThread();
/*  90 */         Thread.sleep(this.fpcm.getMinimalSecondResetConection() * 60000);
/*     */       }
/*     */     }
/*  93 */     return null;
/*     */   }
/*     */   
/*     */   public List<Comment> searchComments(String searchType, String searchCriteria, String since)
/*     */     throws Exception
/*     */   {
/*  99 */     List<Comment> lc = null;
/* 100 */     if (searchType.equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
/* 101 */       lc = searchCommentsfromPage(searchCriteria, since);
/* 102 */     } else if (searchType.equals(FacebookSearchTypeEnum.POST.getFacebooksearchtype())) {
/* 103 */       lc = searchCommentsfromPosts(searchCriteria, since);
/*     */     }
/* 105 */     return lc;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\FacebookServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */