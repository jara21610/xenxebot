/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum PlanTypeEnum
/*    */ {
/*  6 */   FREEPLAN("FREEPLAN");
/*    */   
/*    */   private String type;
/*    */   
/*    */   public String getType() {
/* 11 */     return this.type;
/*    */   }
/*    */   
/*    */   private PlanTypeEnum(String type) {
/* 15 */     this.type = type;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\PlanTypeEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */