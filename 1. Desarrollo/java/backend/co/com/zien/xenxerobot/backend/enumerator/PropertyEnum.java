/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ public enum PropertyEnum
/*    */ {
/*  5 */   GOOGLESUGGESTURL("GOOGLESUGGESTURL"),  DICTIONARYPATH("DICTIONARYPATH"),  TWITTERPAGESIZE("TWITTERPAGESIZE"), 
/*  6 */   XENXETEMPLATE("XENXETEMPLATE"),  OUTPUTPTAH("OUTPUTPTAH"),  XENXEBOTTOKEN("XENXEBOTTOKEN"),  PREXTIGEBOTTOKEN("PREXTIGEBOTTOKEN"),  STRESSLOADTESTTOKEN("STRESSLOADTESTTOKEN"), 
/*  7 */   PARTIALPUSH("PARTIALPUSH"),  DATASETEXPORTTEMPLATE("DATASETEXPORTTEMPLATE"),  PACKAGESIZE("PACKAGESIZE"), 
/*  8 */   INFLUENCEENROLLLICENCE("INFLUENCEENROLLLICENCE"),  PREXTIGETEMPLATE("PREXTIGETEMPLATE"),  LOADSTRESSTESTACTIVE("LOADSTRESSTESTACTIVE"),  USERLICENSE("USERLICENSE"), 
/*  9 */   WELCOMEEMAIL("WELCOMEEMAIL");
/*    */   
/*    */   private String property;
/*    */   
/*    */   public String getProperty() {
/* 14 */     return this.property;
/*    */   }
/*    */   
/*    */   private PropertyEnum(String property) {
/* 18 */     this.property = property;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\PropertyEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */