/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum PlanStateEnum
/*    */ {
/*  6 */   ACTIVO("ACTIVO"),  FINALIZADO("FINALIZADO"),  NOTPAID("NOTPAID");
/*    */   
/*    */   private String state;
/*    */   
/*    */   public String getState() {
/* 11 */     return this.state;
/*    */   }
/*    */   
/*    */   private PlanStateEnum(String state) {
/* 15 */     this.state = state;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\PlanStateEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */