package co.com.zien.xenxerobot.backend.services;

public abstract interface TransactionCounterService
{
  public abstract int addUsedTransactionPlan(String paramString, int paramInt)
    throws TxLimitException;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TransactionCounterService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */