/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum ReportDetailEnum
/*    */ {
/*  6 */   TOTALPOST("TOTALPOST"),  TOTALPOSITIVE("TOTALPOSITIVE"),  TOTALNEGATIVE("TOTALNEGATIVE"), 
/*  7 */   TOTALNEUTRAL("TOTALNEUTRAL"),  USUARIO("USUARIO"),  COMMANDINFO("COMMANDINFO");
/*    */   
/*    */   private String sentiment;
/*    */   
/*    */   public String getReportDetail() {
/* 12 */     return this.sentiment;
/*    */   }
/*    */   
/*    */   private ReportDetailEnum(String sentiment) {
/* 16 */     this.sentiment = sentiment;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\ReportDetailEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */