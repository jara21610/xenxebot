package co.com.zien.xenxerobot.backend.services;

public abstract interface LemmatizationService
{
  public abstract String getWordLemma(String paramString);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\LemmatizationService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */