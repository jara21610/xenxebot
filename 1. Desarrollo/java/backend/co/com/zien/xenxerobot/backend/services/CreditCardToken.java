/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class CreditCardToken
/*    */ {
/*    */   public String payerId;
/*    */   public String name;
/*    */   public String identificationNumber;
/*    */   public String paymentMethod;
/*    */   public String number;
/*    */   public String expirationDate;
/*    */   public String maskedNumber;
/*    */   public String creditCardTokenId;
/*    */   public String errorDescription;
/*    */   
/*    */   public String getPayerId() {
/* 16 */     return this.payerId;
/*    */   }
/*    */   
/* 19 */   public void setPayerId(String payerId) { this.payerId = payerId; }
/*    */   
/*    */   public String getName() {
/* 22 */     return this.name;
/*    */   }
/*    */   
/* 25 */   public void setName(String name) { this.name = name; }
/*    */   
/*    */   public String getIdentificationNumber() {
/* 28 */     return this.identificationNumber;
/*    */   }
/*    */   
/* 31 */   public void setIdentificationNumber(String identificationNumber) { this.identificationNumber = identificationNumber; }
/*    */   
/*    */   public String getPaymentMethod() {
/* 34 */     return this.paymentMethod;
/*    */   }
/*    */   
/* 37 */   public void setPaymentMethod(String paymentMethod) { this.paymentMethod = paymentMethod; }
/*    */   
/*    */   public String getNumber() {
/* 40 */     return this.number;
/*    */   }
/*    */   
/* 43 */   public void setNumber(String number) { this.number = number; }
/*    */   
/*    */   public String getExpirationDate() {
/* 46 */     return this.expirationDate;
/*    */   }
/*    */   
/* 49 */   public void setExpirationDate(String expirationDate) { this.expirationDate = expirationDate; }
/*    */   
/*    */   public String getMaskedNumber() {
/* 52 */     return this.maskedNumber;
/*    */   }
/*    */   
/* 55 */   public void setMaskedNumber(String maskedNumber) { this.maskedNumber = maskedNumber; }
/*    */   
/*    */   public String getCreditCardTokenId() {
/* 58 */     return this.creditCardTokenId;
/*    */   }
/*    */   
/* 61 */   public void setCreditCardTokenId(String creditCardTokenId) { this.creditCardTokenId = creditCardTokenId; }
/*    */   
/*    */   public String getErrorDescription() {
/* 64 */     return this.errorDescription;
/*    */   }
/*    */   
/* 67 */   public void setErrorDescription(String errorDescription) { this.errorDescription = errorDescription; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\CreditCardToken.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */