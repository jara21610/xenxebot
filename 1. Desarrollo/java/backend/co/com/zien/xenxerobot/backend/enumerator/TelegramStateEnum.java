/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum TelegramStateEnum
/*    */ {
/*  6 */   MAINMENU("MAINMENU"),  SENTIMENT("SENTIMENT"),  SENTIMENTSOCIALSELECT("SENTIMENTSOCIALSELECT"), 
/*  7 */   SENTIMENTSEARCHTYPE("SENTIMENTSEARCHTYPE"),  SENTIMENTSEARCHOBJECT("SENTIMENTSEARCHOBJECT"), 
/*  8 */   SENTIMENTSINCE("SENTIMENTSINCE"),  SENTIMENTUNTIL("SENTIMENTUNTIL"),  SENTIMENTCHOOSESEARCHOBJECT("SENTIMENTCHOOSESEARCHOBJECT"), 
/*  9 */   DETAILREPORT("DETAILREPORT"),  EMAILENRROLL("EMAILENRROLL");
/*    */   
/*    */   private String state;
/*    */   
/*    */   public String getState() {
/* 14 */     return this.state;
/*    */   }
/*    */   
/*    */   private TelegramStateEnum(String state) {
/* 18 */     this.state = state;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\TelegramStateEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */