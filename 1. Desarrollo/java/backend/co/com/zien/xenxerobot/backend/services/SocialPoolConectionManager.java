package co.com.zien.xenxerobot.backend.services;

import java.util.List;

public abstract interface SocialPoolConectionManager
{
  public abstract void init()
    throws SocialPoolConectionManagerNotConfigured;
  
  public abstract Object getAvailableConecction(String... paramVarArgs)
    throws SocialPoolConectionManagerNotConfigured, SocialPoolConectionManagerNotFoundAvailableException;
  
  public abstract List<SocialConnectionDTO> getCurrentState();
  
  public abstract Object getSocialServerLimitStatus();
  
  public abstract int getMinimalSecondResetConection();
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SocialPoolConectionManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */