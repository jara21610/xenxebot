package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.Userplan;

public abstract interface PlanService
{
  public abstract Userplan getLastUserPlan(String paramString);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PlanService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */