/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ 
/*     */ public class Transaction
/*     */ {
/*     */   private Order order;
/*     */   
/*     */   private Payer payer;
/*     */   private String creditCardTokenId;
/*     */   private ExtraParameters extraParameters;
/*     */   private String type;
/*     */   private String paymentMethod;
/*     */   private String paymentCountry;
/*     */   private String deviceSessionId;
/*     */   private String ipAddress;
/*     */   private String cookie;
/*     */   private String userAgent;
/*     */   
/*     */   public Order getOrder()
/*     */   {
/*  21 */     return this.order;
/*     */   }
/*     */   
/*     */   public void setOrder(Order order) {
/*  25 */     this.order = order;
/*     */   }
/*     */   
/*     */   public Payer getPayer() {
/*  29 */     return this.payer;
/*     */   }
/*     */   
/*     */   public void setPayer(Payer payer) {
/*  33 */     this.payer = payer;
/*     */   }
/*     */   
/*     */   public String getCreditCardTokenId() {
/*  37 */     return this.creditCardTokenId;
/*     */   }
/*     */   
/*     */   public void setCreditCardTokenId(String creditCardTokenId) {
/*  41 */     this.creditCardTokenId = creditCardTokenId;
/*     */   }
/*     */   
/*     */   public ExtraParameters getExtraParameters() {
/*  45 */     return this.extraParameters;
/*     */   }
/*     */   
/*     */   public void setExtraParameters(ExtraParameters extraParameters) {
/*  49 */     this.extraParameters = extraParameters;
/*     */   }
/*     */   
/*     */   public String getType() {
/*  53 */     return this.type;
/*     */   }
/*     */   
/*     */   public void setType(String type) {
/*  57 */     this.type = type;
/*     */   }
/*     */   
/*     */   public String getPaymentMethod() {
/*  61 */     return this.paymentMethod;
/*     */   }
/*     */   
/*     */   public void setPaymentMethod(String paymentMethod) {
/*  65 */     this.paymentMethod = paymentMethod;
/*     */   }
/*     */   
/*     */   public String getPaymentCountry() {
/*  69 */     return this.paymentCountry;
/*     */   }
/*     */   
/*     */   public void setPaymentCountry(String paymentCountry) {
/*  73 */     this.paymentCountry = paymentCountry;
/*     */   }
/*     */   
/*     */   public String getDeviceSessionId() {
/*  77 */     return this.deviceSessionId;
/*     */   }
/*     */   
/*     */   public void setDeviceSessionId(String deviceSessionId) {
/*  81 */     this.deviceSessionId = deviceSessionId;
/*     */   }
/*     */   
/*     */   public String getIpAddress() {
/*  85 */     return this.ipAddress;
/*     */   }
/*     */   
/*     */   public void setIpAddress(String ipAddress) {
/*  89 */     this.ipAddress = ipAddress;
/*     */   }
/*     */   
/*     */   public String getCookie() {
/*  93 */     return this.cookie;
/*     */   }
/*     */   
/*     */   public void setCookie(String cookie) {
/*  97 */     this.cookie = cookie;
/*     */   }
/*     */   
/*     */   public String getUserAgent() {
/* 101 */     return this.userAgent;
/*     */   }
/*     */   
/*     */   public void setUserAgent(String userAgent) {
/* 105 */     this.userAgent = userAgent;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\Transaction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */