/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum SentimentEnum
/*    */ {
/*  6 */   POSITIVE("P"),  NEGATIVE("N"),  NEUTRAL("NEU");
/*    */   
/*    */   private String sentiment;
/*    */   
/*    */   public String getSentiment() {
/* 11 */     return this.sentiment;
/*    */   }
/*    */   
/*    */   private SentimentEnum(String sentiment) {
/* 15 */     this.sentiment = sentiment;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\SentimentEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */