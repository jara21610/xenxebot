/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ 
/*    */ public class PayInput
/*    */ {
/*    */   private String language;
/*    */   
/*    */   private String command;
/*    */   private MerchantInput merchant;
/*    */   private Transaction transaction;
/*    */   private boolean test;
/*    */   
/*    */   public String getLanguage()
/*    */   {
/* 15 */     return this.language;
/*    */   }
/*    */   
/*    */   public void setLanguage(String language) {
/* 19 */     this.language = language;
/*    */   }
/*    */   
/*    */   public String getCommand() {
/* 23 */     return this.command;
/*    */   }
/*    */   
/*    */   public void setCommand(String command) {
/* 27 */     this.command = command;
/*    */   }
/*    */   
/*    */   public MerchantInput getMerchant() {
/* 31 */     return this.merchant;
/*    */   }
/*    */   
/*    */   public void setMerchant(MerchantInput merchant) {
/* 35 */     this.merchant = merchant;
/*    */   }
/*    */   
/*    */   public Transaction getTransaction() {
/* 39 */     return this.transaction;
/*    */   }
/*    */   
/*    */   public void setTransaction(Transaction transaction) {
/* 43 */     this.transaction = transaction;
/*    */   }
/*    */   
/*    */   public boolean isTest() {
/* 47 */     return this.test;
/*    */   }
/*    */   
/*    */   public void setTest(boolean test) {
/* 51 */     this.test = test;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PayInput.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */