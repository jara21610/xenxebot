package co.com.zien.xenxerobot.backend.services;

public abstract interface ResponseBotService
{
  public abstract void okResponseBot(String paramString, ComandDTO paramComandDTO);
  
  public abstract void isprocessingResponseBot(ComandDTO paramComandDTO);
  
  public abstract void okPartialReportResponseBot(String paramString, ComandDTO paramComandDTO);
  
  public abstract void exceptionResponseBot(ComandDTO paramComandDTO, Exception paramException);
  
  public abstract void messageResponseBot(String paramString, ComandDTO paramComandDTO);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ResponseBotService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */