package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.backend.dictionary.InflectionDictionary;

public abstract interface InflectionDictionaryService
{
  public abstract InflectionDictionary getDictionary();
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\InflectionDictionaryService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */