/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.entity.Userplan;
/*    */ import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;
/*    */ import java.util.List;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.data.domain.PageRequest;
/*    */ import org.springframework.data.domain.Pageable;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ public class PlanServiceImpl
/*    */   implements PlanService
/*    */ {
/*    */   @Autowired
/*    */   UserplanRepository upr;
/*    */   
/*    */   public Userplan getLastUserPlan(String idUser)
/*    */   {
/* 22 */     Pageable pageable = new PageRequest(0, 1);
/*    */     
/* 24 */     List<Userplan> userplanlist = this.upr.historicByUserid(idUser, pageable);
/* 25 */     if (userplanlist.size() > 0) {
/* 26 */       return (Userplan)userplanlist.get(0);
/*    */     }
/* 28 */     return null;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PlanServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */