package co.com.zien.xenxerobot.backend.services;

import java.util.List;
import java.util.Map;

public abstract interface EmailService
{
  public abstract void sendEmail(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void sendHtmlEmail(String paramString1, String paramString2, String paramString3, String paramString4, Map<String, String> paramMap);
  
  public abstract void sendEmail(String paramString1, String paramString2, String paramString3, String paramString4, String[] paramArrayOfString);
  
  public abstract List<EmailMessageDTO> getNewEmailsImap();
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\EmailService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */