/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class CommandMalformedException
/*    */   extends Exception
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   
/*    */   public CommandMalformedException() {}
/*    */   
/* 10 */   public CommandMalformedException(String message) { super(message); }
/* 11 */   public CommandMalformedException(String message, Throwable cause) { super(message, cause); }
/* 12 */   public CommandMalformedException(Throwable cause) { super(cause); }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\CommandMalformedException.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */