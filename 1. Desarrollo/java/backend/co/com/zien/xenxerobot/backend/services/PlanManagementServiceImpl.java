/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.config.Config;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*     */ import co.com.zien.xenxerobot.backend.telegram.bot.ServiceBot;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Paymenthistory;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Paymentmethod;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Plan;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Userplan;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PaymenthistoryRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PaymentmethodRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PlanRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;
/*     */ import com.google.gson.Gson;
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.UUID;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.context.ApplicationContext;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.scheduling.annotation.Async;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.springframework.web.bind.annotation.RequestBody;
/*     */ 
/*     */ @Service
/*     */ public class PlanManagementServiceImpl
/*     */   implements PlanManagementService
/*     */ {
/*     */   @Autowired
/*     */   PaymentServices ps;
/*     */   @Autowired
/*     */   UserplanRepository upr;
/*     */   @Autowired
/*     */   PlanRepository planr;
/*     */   @Autowired
/*     */   UserRepository ur;
/*     */   @Autowired
/*     */   PaymentmethodRepository pmr;
/*     */   @Autowired
/*     */   PaymenthistoryRepository phr;
/*     */   @Autowired
/*     */   TokenService ts;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   @Autowired
/*     */   PlanService plans;
/*     */   
/*     */   public Userplan upgradeplan(Users u, String plan) throws Exception
/*     */   {
/*  52 */     Plan templateplan = (Plan)this.planr.findOne(plan);
/*     */     
/*  54 */     Userplan up = new Userplan();
/*  55 */     up.setId(UUID.randomUUID().toString());
/*  56 */     up.setIdplan(templateplan);
/*  57 */     up.setIduser(u);
/*  58 */     up.setTxplancapacity(Integer.valueOf(templateplan.getTxplancapacity()));
/*  59 */     up.setTxpalused(Integer.valueOf(0));
/*  60 */     up.setStartdate(new Date());
/*  61 */     up.setState("NOTPAID");
/*  62 */     up.setPrice(templateplan.getPrice());
/*  63 */     Calendar cal = Calendar.getInstance();
/*     */     
/*  65 */     cal.add(2, new Integer(templateplan.getTimequantity()).intValue());
/*  66 */     up.setFinishdate(cal.getTime());
/*     */     
/*  68 */     this.upr.saveAndFlush(up);
/*  69 */     this.ur.saveAndFlush(u);
/*     */     
/*  71 */     return up;
/*     */   }
/*     */   
/*     */   public Paymentmethod storePaymentMethod(Users u, String name, String identificationNumber, String number, String expirationDate, String ccv, String idType)
/*     */     throws StorePaymentmethodException
/*     */   {
/*  77 */     RegisterCreditCardTokenResponse tokenresponse = 
/*  78 */       this.ps.createCreditCardToken(u.getId(), name, identificationNumber, number, expirationDate);
/*     */     
/*  80 */     if (tokenresponse.getCode().equals("SUCCESS"))
/*     */     {
/*  82 */       Paymentmethod pm = new Paymentmethod();
/*  83 */       pm.setUserid(u);
/*  84 */       pm.setId(UUID.randomUUID().toString());
/*  85 */       pm.setCreditcardtoken(tokenresponse.getCreditCardToken().getCreditCardTokenId());
/*  86 */       pm.setPaymentmethod(tokenresponse.getCreditCardToken().getPaymentMethod());
/*  87 */       pm.setMaskednumber(tokenresponse.getCreditCardToken().getMaskedNumber());
/*  88 */       pm.setCardname(name);
/*  89 */       pm.setDninumber(identificationNumber);
/*  90 */       pm.setPredeterminate("Y");
/*  91 */       this.pmr.saveAndFlush(pm);
/*     */       
/*  93 */       return pm;
/*     */     }
/*  95 */     throw new StorePaymentmethodException();
/*     */   }
/*     */   
/*     */   public void makePlanPayment(Users u, Userplan up, Paymentmethod pm)
/*     */     throws PaymentException
/*     */   {
/* 101 */     Gson gson = new Gson();
/*     */     
/* 103 */     PayOutput payresponse = 
/* 104 */       this.ps.payWithCreditCardToken(up.getId(), pm.getPaymentmethod(), pm.getCreditcardtoken(), "COP", up.getPrice(), u.getId(), pm.getCardname(), pm.getDninumber());
/*     */     
/* 106 */     Paymenthistory ph = new Paymenthistory();
/* 107 */     ph.setId(UUID.randomUUID().toString());
/* 108 */     ph.setDetail(gson.toJson(payresponse.getTransactionResponse()));
/* 109 */     ph.setPaymentdate(new Date());
/* 110 */     ph.setPaymentmethodid(pm);
/* 111 */     ph.setUserplanid(up);
/* 112 */     if (payresponse.getTransactionResponse().getState().equals("APPROVED")) {
/* 113 */       ph.setStatus("SUCCESS");
/*     */     }
/*     */     else {
/* 116 */       ph.setStatus("REJECTED");
/* 117 */       this.phr.saveAndFlush(ph);
/* 118 */       pm.setPredeterminate("N");
/* 119 */       this.pmr.saveAndFlush(pm);
/* 120 */       throw new PaymentException();
/*     */     }
/*     */     
/* 123 */     this.phr.saveAndFlush(ph);
/*     */   }
/*     */   
/*     */   @Async
/*     */   public void upgradePlanWithPaymentMethod(@RequestBody UpgradePlanDTO updto) throws Exception
/*     */   {
/* 129 */     Userplan up = null;
/* 130 */     Paymentmethod pm = null;
/* 131 */     Users u = null;
/* 132 */     ServiceBot xb = (ServiceBot)Config.getContext().getBean(ServiceTypeEnum.XENXE.getServicetype());
/*     */     try {
/* 134 */       u = this.ts.validateUserToken(updto.getUsertoken());
/*     */       
/* 136 */       if (updto.getPlan() != null) {
/* 137 */         up = upgradeplan(u, updto.getPlan());
/*     */       } else {
/* 139 */         up = this.upr.activePlanByUseridandstate(u.getId(), "NOTPAID");
/*     */       }
/* 141 */       pm = storePaymentMethod(u, updto.getName(), updto.getIdentificationNumber(), updto.getNumber().replace(" ", ""), updto.getExpirationDate(), updto.getCcv(), updto.getIdType());
/* 142 */       makePlanPayment(u, up, pm);
/*     */       
/* 144 */       up.setState("ACTIVO");
/*     */       
/* 146 */       this.upr.saveAndFlush(up);
/*     */       
/* 148 */       xb.handleUpgradePlanOK(u.getChatid());
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     catch (TokenNotExistException localTokenNotExistException) {}catch (TokenExpiredException e)
/*     */     {
/*     */ 
/*     */ 
/* 157 */       u = this.ts.getUserOwnerToken(updto.getUsertoken());
/*     */       
/* 159 */       xb.handleUserTokenException(u.getChatid());
/* 160 */       xb.handleFreePlanFinished(u.getChatid(), u);
/*     */     }
/*     */     catch (StorePaymentmethodException e)
/*     */     {
/* 164 */       up.setState("NOTPAID");
/*     */       
/* 166 */       this.upr.saveAndFlush(up);
/* 167 */       this.ur.saveAndFlush(u);
/* 168 */       xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
/*     */     }
/*     */     catch (PaymentException e) {
/* 171 */       up.setState("NOTPAID");
/*     */       
/* 173 */       this.upr.saveAndFlush(up);
/* 174 */       this.ur.saveAndFlush(u);
/* 175 */       xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
/*     */     }
/*     */     catch (Exception localException) {}
/*     */   }
/*     */   
/*     */ 
/*     */   public void upgradePlanPaymentMethodPredeterminated(Users u)
/*     */     throws Exception
/*     */   {
/* 184 */     Userplan up = null;
/* 185 */     Userplan up1 = null;
/* 186 */     Paymentmethod pm = null;
/* 187 */     ServiceBot xb = (ServiceBot)Config.getContext().getBean(ServiceTypeEnum.XENXE.getServicetype());
/*     */     try
/*     */     {
/* 190 */       up = this.plans.getLastUserPlan(u.getId());
/*     */       
/* 192 */       up1 = upgradeplan(u, up.getIdplan().getPlanid());
/*     */       
/* 194 */       pm = this.pmr.findPrederminateMethod(u.getId());
/* 195 */       makePlanPayment(u, up1, pm);
/*     */       
/* 197 */       up1.setState("ACTIVO");
/*     */       
/* 199 */       this.upr.saveAndFlush(up1);
/*     */       
/* 201 */       xb.handleUpgradePlanOK(u.getChatid());
/*     */     }
/*     */     catch (StorePaymentmethodException e)
/*     */     {
/* 205 */       up.setState("NOTPAID");
/*     */       
/* 207 */       this.upr.saveAndFlush(up);
/* 208 */       this.ur.saveAndFlush(u);
/* 209 */       xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
/*     */     }
/*     */     catch (PaymentException e) {
/* 212 */       up.setState("NOTPAID");
/*     */       
/* 214 */       this.upr.saveAndFlush(up);
/* 215 */       this.ur.saveAndFlush(u);
/* 216 */       xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
/*     */     }
/*     */     catch (Exception localException) {}
/*     */   }
/*     */   
/*     */   public void planManagementDailyVerifications()
/*     */     throws Exception
/*     */   {}
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PlanManagementServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */