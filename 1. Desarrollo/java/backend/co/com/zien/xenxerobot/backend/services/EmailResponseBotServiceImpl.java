/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*    */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*    */ import java.util.Map;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.MessageSource;
/*    */ import org.springframework.context.i18n.LocaleContextHolder;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service("EmailResponseBot")
/*    */ public class EmailResponseBotServiceImpl
/*    */   implements ResponseBotService
/*    */ {
/*    */   @Autowired
/*    */   PropertiesRepository pr;
/*    */   @Autowired
/*    */   MessageSource messageSource;
/*    */   @Autowired
/*    */   EmailService es;
/*    */   
/*    */   public void okResponseBot(String reportpath, ComandDTO cdto)
/*    */   {
/* 30 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), 
/* 31 */       this.messageSource.getMessage("message.finishsentimentanalysis.subject", new Object[] {
/* 32 */       cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), 
/* 33 */       cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), 
/* 34 */       this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), 
/* 35 */       cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), new String[] { reportpath });
/*    */   }
/*    */   
/*    */   public void exceptionResponseBot(ComandDTO cdto, Exception e)
/*    */   {
/* 40 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), 
/* 41 */       this.messageSource.getMessage("message.exception.runtimeexception.subject", null, LocaleContextHolder.getLocale()), 
/* 42 */       e.getMessage());
/*    */   }
/*    */   
/*    */   public void messageResponseBot(String message, ComandDTO cdto)
/*    */   {
/* 47 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), 
/* 48 */       this.messageSource.getMessage("message.postnotfound.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()), 
/* 49 */       cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), 
/* 50 */       message);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void okPartialReportResponseBot(String reportpath, ComandDTO cdto) {}
/*    */   
/*    */ 
/*    */   public void isprocessingResponseBot(ComandDTO cdto)
/*    */   {
/* 60 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), 
/* 61 */       this.messageSource.getMessage("message.isprocesing.subject", new Object[] { cdto.getArguments().get(Integer.valueOf(2)) }, LocaleContextHolder.getLocale()), 
/* 62 */       this.messageSource.getMessage("message.isprocesing.text", null, LocaleContextHolder.getLocale()));
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\EmailResponseBotServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */