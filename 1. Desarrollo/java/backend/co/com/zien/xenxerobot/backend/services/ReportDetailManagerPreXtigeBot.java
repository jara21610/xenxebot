/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.jit.backend.util.Utils;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ConceptsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.SentimentEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*     */ import co.com.zien.xenxerobot.persistence.entity.ElementSentimentDTO;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import com.thoughtworks.xstream.XStream;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.nio.file.Files;
/*     */ import java.text.DateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
/*     */ import org.apache.poi.hssf.usermodel.HSSFPatriarch;
/*     */ import org.apache.poi.hssf.usermodel.HSSFPicture;
/*     */ import org.apache.poi.hssf.usermodel.HSSFWorkbook;
/*     */ import org.apache.poi.ss.usermodel.Cell;
/*     */ import org.apache.poi.ss.usermodel.ClientAnchor;
/*     */ import org.apache.poi.ss.usermodel.Row;
/*     */ import org.apache.poi.ss.usermodel.Sheet;
/*     */ import org.apache.poi.ss.usermodel.Workbook;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.context.i18n.LocaleContextHolder;
/*     */ import org.springframework.core.env.Environment;
/*     */ import org.springframework.data.domain.PageRequest;
/*     */ import org.springframework.data.domain.Pageable;
/*     */ import org.springframework.scheduling.annotation.Async;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service("PreXtigeReportDetailManager")
/*     */ public class ReportDetailManagerPreXtigeBot
/*     */   implements ReportDetailManager
/*     */ {
/*     */   @Autowired
/*     */   CommandProcessRepository cpr;
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   ResponseBotServiceFactory rbsf;
/*     */   @Autowired
/*     */   Environment env;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   
/*     */   public void saveStatus(CommandProcess cp, ReportDetailDTO rrdto)
/*     */   {
/*  80 */     cp = (CommandProcess)this.cpr.findOne(cp.getId());
/*  81 */     XStream xs = new XStream();
/*  82 */     String reportdetail = xs.toXML(rrdto);
/*  83 */     cp.setReportdetail(reportdetail);
/*  84 */     this.cpr.saveAndFlush(cp);
/*  85 */     System.out.println("PASE");
/*     */   }
/*     */   
/*     */   public String generateSentimentAnalysisReport(String cpId)
/*     */     throws Exception
/*     */   {
/*  91 */     String pathpdf = "";
/*  92 */     String path = "";
/*  93 */     CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
/*     */     
/*  95 */     int total = this.postr.totalPost(cp.getId()).intValue();
/*  96 */     if (total > 0) {
/*  97 */       String ruta = this.env.getRequiredProperty("outputpath");
/*  98 */       XStream xs = new XStream();
/*  99 */       ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/*     */       
/* 101 */       String name = this.pr.findByName(PropertyEnum.PREXTIGETEMPLATE.getProperty());
/* 102 */       Workbook wb = new HSSFWorkbook(new FileInputStream(name));
/*     */       
/* 104 */       Sheet s1 = wb.getSheetAt(0);
/* 105 */       s1.getRow(4).getCell(2).setCellValue(cp.getUser().getName() + " " + cp.getUser().getLastname());
/*     */       
/*     */ 
/*     */ 
/* 109 */       s1.getRow(5).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()));
/* 110 */       s1.getRow(6).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()));
/* 111 */       s1.getRow(7).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()));
/*     */       
/* 113 */       Date date = new Date();
/* 114 */       DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
/*     */       
/* 116 */       s1.getRow(4).getCell(5).setCellValue(df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())));
/*     */       
/* 118 */       s1.getRow(5).getCell(5).setCellValue(df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())));
/*     */       
/* 120 */       s1.getRow(6).getCell(5).setCellValue(df.format(date));
/*     */       
/* 122 */       s1.getRow(7).getCell(5).setCellValue(total);
/*     */       
/*     */ 
/* 125 */       int positive = this.postr.totalXSentiment(cp.getId(), SentimentEnum.POSITIVE.getSentiment()).intValue();
/* 126 */       int negative = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEGATIVE.getSentiment()).intValue();
/* 127 */       int neutral = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEUTRAL.getSentiment()).intValue();
/*     */       
/*     */ 
/* 130 */       HashMap<String, Object> sentimentpiecontext = new HashMap();
/* 131 */       sentimentpiecontext.put("positivo", new Integer(positive).toString());
/* 132 */       sentimentpiecontext.put("negativo", new Integer(negative).toString());
/* 133 */       sentimentpiecontext.put("neutral", new Integer(neutral).toString());
/*     */       
/* 135 */       String filenamejpg = UUID.randomUUID().toString() + ".jpeg";
/* 136 */       String filenamehtml = UUID.randomUUID().toString() + ".html";
/*     */       
/* 138 */       Utils.trasformTemplate(this.env.getRequiredProperty("sentimentpie.template.path"), ruta + filenamehtml, sentimentpiecontext);
/*     */       
/* 140 */       Utils.coverthtmltojpeg(ruta + filenamehtml, ruta + filenamejpg, 600, 310);
/*     */       
/* 142 */       Thread.currentThread();
/* 143 */       Thread.sleep(10000L);
/*     */       
/* 145 */       int my_picture_id = wb.addPicture(Files.readAllBytes(new File(ruta + filenamejpg).toPath()), 5);
/* 146 */       HSSFPatriarch drawing = (HSSFPatriarch)s1.createDrawingPatriarch();
/*     */       
/* 148 */       ClientAnchor my_anchor = new HSSFClientAnchor();
/* 149 */       my_anchor.setCol1(2);
/* 150 */       my_anchor.setRow1(11);
/* 151 */       HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
/* 152 */       my_picture.resize();
/*     */       
/* 154 */       List<ElementSentimentDTO> sentimentXCategory = this.postr.sentimentXCategory(cp.getId());
/*     */       
/* 156 */       HashMap<String, Object> categorybarchartcontext = new HashMap();
/*     */       
/* 158 */       categorybarchartcontext.put("elementsentimentlist", sentimentXCategory);
/* 159 */       categorybarchartcontext.put("title", "Sentimiento por Categoria");
/*     */       
/* 161 */       filenamejpg = UUID.randomUUID().toString() + ".jpeg";
/* 162 */       filenamehtml = UUID.randomUUID().toString() + ".html";
/*     */       
/* 164 */       Utils.trasformTemplate(this.env.getRequiredProperty("categorybarchart.template.path"), ruta + filenamehtml, categorybarchartcontext);
/*     */       
/* 166 */       Utils.coverthtmltojpeg(ruta + filenamehtml, ruta + filenamejpg, 600, 310);
/*     */       
/* 168 */       Thread.currentThread();
/* 169 */       Thread.sleep(10000L);
/*     */       
/* 171 */       int my_picture_id_1 = wb.addPicture(Files.readAllBytes(new File(ruta + filenamejpg).toPath()), 5);
/*     */       
/* 173 */       ClientAnchor my_anchor_1 = new HSSFClientAnchor();
/* 174 */       my_anchor_1.setCol1(2);
/* 175 */       my_anchor_1.setRow1(68);
/* 176 */       HSSFPicture my_picture_1 = drawing.createPicture(my_anchor_1, my_picture_id_1);
/* 177 */       my_picture_1.resize();
/*     */       
/*     */ 
/* 180 */       Pageable pag = new PageRequest(0, 10);
/*     */       
/* 182 */       List<Object[]> topfrecuencyXconceptTypeE = this.postr.topfrecuencyXconceptType(cp.getId(), ConceptsEnum.ENTIDAD.getConcept(), pag);
/*     */       
/* 184 */       List<ElementSentimentDTO> sentimentXTopic = new ArrayList();
/*     */       
/* 186 */       String positiveentitiyanalisis = this.messageSource.getMessage("message.report.prextige.entity.text", new Object[] { Long.valueOf(Math.round(new Double(positive).doubleValue() / new Double(total).doubleValue() * 100.0D)), "Positivo", "Positiva" }, LocaleContextHolder.getLocale());
/* 187 */       String negativeentitiyanalisis = this.messageSource.getMessage("message.report.prextige.entity.text", new Object[] { Long.valueOf(Math.round(new Double(negative).doubleValue() / new Double(total).doubleValue() * 100.0D)), "Negativo", "Negativa" }, LocaleContextHolder.getLocale());
/* 188 */       int positiveentity = 0;
/* 189 */       int negativeentity = 0;
/* 190 */       for (Object[] obj : topfrecuencyXconceptTypeE) {
/* 191 */         long total1 = ((Long)obj[1]).longValue();
/* 192 */         if (total1 > 10L)
/*     */         {
/* 194 */           ElementSentimentDTO sentimentXbyConceptName = this.postr.sentimentXbyConceptName(cp.getId(), ConceptsEnum.ENTIDAD.getConcept(), (String)obj[0]);
/* 195 */           if (sentimentXbyConceptName != null) {
/* 196 */             sentimentXTopic.add(sentimentXbyConceptName);
/* 197 */             if (sentimentXbyConceptName.getPositivo() > 0.5D)
/*     */             {
/* 199 */               if (positiveentity <= 4) {
/* 200 */                 positiveentitiyanalisis = positiveentitiyanalisis + sentimentXbyConceptName.getName() + " ,";
/* 201 */                 positiveentity++;
/*     */               }
/* 203 */             } else if ((sentimentXbyConceptName.getNegativo() > 0.5D) && 
/* 204 */               (negativeentity <= 4)) {
/* 205 */               negativeentitiyanalisis = negativeentitiyanalisis + sentimentXbyConceptName.getName() + " ,";
/* 206 */               negativeentity++;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 214 */       if (positiveentity > 0) {
/* 215 */         positiveentitiyanalisis = positiveentitiyanalisis.substring(0, positiveentitiyanalisis.length() - 1);
/* 216 */         s1.getRow(31).getCell(2).setCellValue(positiveentitiyanalisis.toUpperCase());
/*     */       } else {
/* 218 */         s1.getRow(31).getCell(2).setCellValue(this.messageSource.getMessage("message.report.prextige.conceptnotfound.text", new Object[] { "PERSONAS, EMPRESAS O LUGARES", "POSITIVO" }, LocaleContextHolder.getLocale()).toUpperCase());
/*     */       }
/* 220 */       if (negativeentity > 0) {
/* 221 */         negativeentitiyanalisis = negativeentitiyanalisis.substring(0, negativeentitiyanalisis.length() - 1);
/* 222 */         s1.getRow(31).getCell(6).setCellValue(negativeentitiyanalisis.toUpperCase());
/*     */       } else {
/* 224 */         s1.getRow(31).getCell(6).setCellValue(this.messageSource.getMessage("message.report.prextige.conceptnotfound.text", new Object[] { "PERSONAS, EMPRESAS O LUGARES", "NEGATIVO" }, LocaleContextHolder.getLocale()).toUpperCase());
/*     */       }
/*     */       
/* 227 */       if (sentimentXTopic.size() > 0) {
/* 228 */         HashMap<String, Object> entitybarchartcontext = new HashMap();
/*     */         
/* 230 */         entitybarchartcontext.put("elementsentimentlist", sentimentXTopic);
/* 231 */         entitybarchartcontext.put("title", "Sentimiento por Topico");
/*     */         
/* 233 */         filenamejpg = UUID.randomUUID().toString() + ".jpeg";
/* 234 */         filenamehtml = UUID.randomUUID().toString() + ".html";
/*     */         
/* 236 */         Utils.trasformTemplate(this.env.getRequiredProperty("categorybarchart.template.path"), ruta + filenamehtml, entitybarchartcontext);
/*     */         
/* 238 */         Utils.coverthtmltojpeg(ruta + filenamehtml, ruta + filenamejpg, 600, 310);
/*     */         
/* 240 */         Thread.currentThread();
/* 241 */         Thread.sleep(10000L);
/*     */         
/* 243 */         int my_picture_id_2 = wb.addPicture(Files.readAllBytes(new File(ruta + filenamejpg).toPath()), 5);
/*     */         
/* 245 */         ClientAnchor my_anchor_2 = new HSSFClientAnchor();
/* 246 */         my_anchor_2.setCol1(2);
/* 247 */         my_anchor_2.setRow1(87);
/* 248 */         HSSFPicture my_picture_2 = drawing.createPicture(my_anchor_2, my_picture_id_2);
/* 249 */         my_picture_2.resize();
/*     */       } else {
/* 251 */         s1.getRow(50).getCell(2).setCellValue("NO HAY INFORMACION PARA MOSTRAR");
/*     */       }
/*     */       
/*     */ 
/* 255 */       List<Object[]> topfrecuencyXconceptType = this.postr.topfrecuencyXconceptType(cp.getId(), ConceptsEnum.TOPICO.getConcept(), pag);
/*     */       
/*     */ 
/* 258 */       Object sentimentXEntity = new ArrayList();
/*     */       
/* 260 */       String positivetopicanalisis = this.messageSource.getMessage("message.report.prextige.topic.text", new Object[] { Long.valueOf(Math.round(new Double(positive).doubleValue() / new Double(total).doubleValue() * 100.0D)), "Positivo", "Positiva" }, LocaleContextHolder.getLocale());
/* 261 */       String negativetopicanalisis = this.messageSource.getMessage("message.report.prextige.topic.text", new Object[] { Long.valueOf(Math.round(new Double(negative).doubleValue() / new Double(total).doubleValue() * 100.0D)), "Negativo", "Negativa" }, LocaleContextHolder.getLocale());
/* 262 */       int positivetopic = 0;
/* 263 */       int negativetopic = 0;
/*     */       
/* 265 */       for (Object[] obj : topfrecuencyXconceptType) {
/* 266 */         long total1 = ((Long)obj[1]).longValue();
/* 267 */         if (total1 > 10L)
/*     */         {
/* 269 */           ElementSentimentDTO sentimentXbyConceptName = this.postr.sentimentXbyConceptName(cp.getId(), ConceptsEnum.TOPICO.getConcept(), (String)obj[0]);
/* 270 */           if (sentimentXbyConceptName != null) {
/* 271 */             ((List)sentimentXEntity).add(sentimentXbyConceptName);
/*     */             
/* 273 */             if (sentimentXbyConceptName.getPositivo() > 0.5D)
/*     */             {
/* 275 */               if (positivetopic <= 4) {
/* 276 */                 positivetopicanalisis = positivetopicanalisis + sentimentXbyConceptName.getName() + " ,";
/* 277 */                 positivetopic++;
/*     */               }
/* 279 */             } else if ((sentimentXbyConceptName.getNegativo() > 0.5D) && 
/* 280 */               (negativetopic <= 4)) {
/* 281 */               negativetopicanalisis = negativetopicanalisis + sentimentXbyConceptName.getName() + " ,";
/* 282 */               negativetopic++;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 289 */       if (positivetopic > 0) {
/* 290 */         positivetopicanalisis = positivetopicanalisis.substring(0, positivetopicanalisis.length() - 1);
/* 291 */         s1.getRow(38).getCell(2).setCellValue(positivetopicanalisis.toUpperCase());
/*     */       } else {
/* 293 */         s1.getRow(38).getCell(2).setCellValue(this.messageSource.getMessage("message.report.prextige.conceptnotfound.text", new Object[] { "TEMAS", "POSITIVO" }, LocaleContextHolder.getLocale()).toUpperCase());
/*     */       }
/* 295 */       if (negativetopic > 0) {
/* 296 */         negativetopicanalisis = negativetopicanalisis.substring(0, negativetopicanalisis.length() - 1);
/* 297 */         s1.getRow(38).getCell(6).setCellValue(negativetopicanalisis.toUpperCase());
/*     */       } else {
/* 299 */         s1.getRow(38).getCell(6).setCellValue(this.messageSource.getMessage("message.report.prextige.conceptnotfound.text", new Object[] { "TEMAS", "NEGATIVO" }, LocaleContextHolder.getLocale()).toUpperCase());
/*     */       }
/*     */       
/* 302 */       if (((List)sentimentXEntity).size() > 0) {
/* 303 */         HashMap<String, Object> topicbarchartcontext = new HashMap();
/*     */         
/* 305 */         topicbarchartcontext.put("elementsentimentlist", sentimentXEntity);
/* 306 */         topicbarchartcontext.put("title", "Sentimiento por Entidad");
/*     */         
/* 308 */         filenamejpg = UUID.randomUUID().toString() + ".jpeg";
/* 309 */         filenamehtml = UUID.randomUUID().toString() + ".html";
/*     */         
/* 311 */         Utils.trasformTemplate(this.env.getRequiredProperty("categorybarchart.template.path"), ruta + filenamehtml, topicbarchartcontext);
/*     */         
/* 313 */         Utils.coverthtmltojpeg(ruta + filenamehtml, ruta + filenamejpg, 600, 310);
/*     */         
/* 315 */         Thread.currentThread();
/* 316 */         Thread.sleep(10000L);
/*     */         
/* 318 */         int my_picture_id_3 = wb.addPicture(Files.readAllBytes(new File(ruta + filenamejpg).toPath()), 5);
/*     */         
/*     */ 
/* 321 */         ClientAnchor my_anchor_3 = new HSSFClientAnchor();
/*     */         
/* 323 */         my_anchor_3.setCol1(2);
/* 324 */         my_anchor_3.setRow1(106);
/*     */         
/* 326 */         HSSFPicture my_picture_3 = drawing.createPicture(my_anchor_3, my_picture_id_3);
/*     */         
/* 328 */         my_picture_3.resize();
/*     */       } else {
/* 330 */         s1.getRow(69).getCell(2).setCellValue("NO HAY INFORMACION PARA MOSTRAR");
/*     */       }
/*     */       
/*     */ 
/* 334 */       String filename = UUID.randomUUID().toString() + "_" + 
/* 335 */         cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) + "_" + df.format(date);
/* 336 */       path = ruta + filename + ".xls";
/*     */       
/* 338 */       FileOutputStream fileOut = new FileOutputStream(path);
/* 339 */       wb.write(fileOut);
/* 340 */       fileOut.close();
/*     */       
/*     */ 
/* 343 */       pathpdf = ruta + filename + ".pdf";
/*     */       
/* 345 */       Utils.convertDocuments(path, pathpdf);
/*     */     } else {
/* 347 */       throw new NotDataToPartialReportException();
/*     */     }
/*     */     
/* 350 */     return pathpdf;
/*     */   }
/*     */   
/*     */   @Async
/*     */   public void reportdetailgenerateSentimentAnalysisReport(String cpId) throws Exception
/*     */   {
/* 356 */     CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
/* 357 */     String path = "";
/* 358 */     ResponseBotService rbs = this.rbsf.getResponseBotService(ChannelEnum.TELEGRAM.getChannel());
/*     */     try {
/* 360 */       path = generateSentimentAnalysisReport(cpId);
/*     */       
/*     */ 
/* 363 */       XStream xs = new XStream();
/* 364 */       ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/* 365 */       rbs.okPartialReportResponseBot(path, cdto);
/*     */     } catch (NotDataToPartialReportException e) {
/* 367 */       XStream xs = new XStream();
/* 368 */       ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/* 369 */       rbs.messageResponseBot(this.messageSource.getMessage("messsage.telegram.notdatatopartialreport.text", null, LocaleContextHolder.getLocale()), cdto);
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ReportDetailManagerPreXtigeBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */