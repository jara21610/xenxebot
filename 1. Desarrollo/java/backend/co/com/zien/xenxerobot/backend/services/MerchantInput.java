/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class MerchantInput
/*    */ {
/*    */   private String apiLogin;
/*    */   private String apiKey;
/*    */   
/*    */   public String getApiLogin() {
/*  9 */     return this.apiLogin;
/*    */   }
/*    */   
/* 12 */   public void setApiLogin(String apiLogin) { this.apiLogin = apiLogin; }
/*    */   
/*    */   public String getApiKey() {
/* 15 */     return this.apiKey;
/*    */   }
/*    */   
/* 18 */   public void setApiKey(String apiKey) { this.apiKey = apiKey; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\MerchantInput.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */