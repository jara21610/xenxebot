/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum CommandEnum
/*    */ {
/*  6 */   APPLICATIONSENTIMENT("/sentiment"),  START("/start"),  TELEGRAMSENTIMENT("/AnalisisSentimiento"),  TWITTER("/Twitter"), 
/*  7 */   FACEBOOK("/Facebook"),  USUARIO("/Usuario"),  HASHTAG("/Hashtag"),  PAGE("/Pagina"),  POST("/Post"), 
/*  8 */   PPALMENU("/MenuPrincipal"),  HELP("/Help"),  PARTIALREPORT("/ReporteParcial"),  COMANDSTATUS("/MisAnalisis"), 
/*  9 */   INFLUENCERENNROLL("/RegistroInfluenciador"),  CANCEL("/Cancelar"),  FINALIZEREGISTER("/FinalizarRegistro"),  DELETEREGISTER("/BorrarRegistro");
/*    */   
/*    */   private String command;
/*    */   
/*    */   public String getCommand() {
/* 14 */     return this.command;
/*    */   }
/*    */   
/*    */   private CommandEnum(String command) {
/* 18 */     this.command = command;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\CommandEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */