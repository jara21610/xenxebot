/*      */ package co.com.zien.xenxerobot.backend.telegram.bot;
/*      */ 
/*      */ import co.com.zien.xenxerobot.backend.config.Config;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandProcessStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.FacebookSearchTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PlanStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PlanTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.TelegramStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.TwitterSearchTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.services.ComandDTO;
/*      */ import co.com.zien.xenxerobot.backend.services.FacebookService;
/*      */ import co.com.zien.xenxerobot.backend.services.PlanManagementService;
/*      */ import co.com.zien.xenxerobot.backend.services.PlanService;
/*      */ import co.com.zien.xenxerobot.backend.services.PostProcessingService;
/*      */ import co.com.zien.xenxerobot.backend.services.ReportDetailManager;
/*      */ import co.com.zien.xenxerobot.backend.services.ReportDetailServiceFactory;
/*      */ import co.com.zien.xenxerobot.backend.services.TokenService;
/*      */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Plan;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Userplan;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*      */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*      */ import com.restfb.types.Post;
/*      */ import com.thoughtworks.xstream.XStream;
/*      */ import java.text.DateFormat;
/*      */ import java.text.ParseException;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Arrays;
/*      */ import java.util.Calendar;
/*      */ import java.util.Date;
/*      */ import java.util.HashMap;
/*      */ import java.util.List;
/*      */ import java.util.Map;
/*      */ import java.util.UUID;
/*      */ import org.springframework.context.ApplicationContext;
/*      */ import org.springframework.context.MessageSource;
/*      */ import org.springframework.context.i18n.LocaleContextHolder;
/*      */ import org.springframework.core.env.Environment;
/*      */ import org.telegram.telegrambots.api.methods.send.SendMessage;
/*      */ import org.telegram.telegrambots.api.objects.CallbackQuery;
/*      */ import org.telegram.telegrambots.api.objects.Contact;
/*      */ import org.telegram.telegrambots.api.objects.Message;
/*      */ import org.telegram.telegrambots.api.objects.Update;
/*      */ import org.telegram.telegrambots.api.objects.User;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
/*      */ import org.telegram.telegrambots.bots.TelegramLongPollingBot;
/*      */ import org.telegram.telegrambots.exceptions.TelegramApiException;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class PreXtigeBot
/*      */   extends TelegramLongPollingBot
/*      */   implements ServiceBot
/*      */ {
/*   84 */   private static Map<String, ComandDTO> usercommandstate = null;
/*      */   
/*      */   static
/*      */   {
/*   88 */     pr = (PropertiesRepository)Config.getContext().getBean(PropertiesRepository.class);
/*   89 */     messageSource = (MessageSource)Config.getContext().getBean(MessageSource.class);
/*   90 */     pps = (PostProcessingService)Config.getContext().getBean(PostProcessingService.class);
/*   91 */     ur = (UserRepository)Config.getContext().getBean(UserRepository.class);
/*   92 */     fs = (FacebookService)Config.getContext().getBean(FacebookService.class); }
/*   93 */   private static CommandProcessRepository cpr = (CommandProcessRepository)Config.getContext().getBean(CommandProcessRepository.class);
/*   94 */   private static PostRepository postr = (PostRepository)Config.getContext().getBean(PostRepository.class);
/*   95 */   private static ReportDetailServiceFactory rdmf = (ReportDetailServiceFactory)Config.getContext().getBean(ReportDetailServiceFactory.class);
/*   96 */   private static Environment env = (Environment)Config.getContext().getBean(Environment.class);
/*   97 */   private static PlanService ps = (PlanService)Config.getContext().getBean(PlanService.class);
/*   98 */   private static TokenService ts = (TokenService)Config.getContext().getBean(TokenService.class);
/*   99 */   private static PlanManagementService pm = (PlanManagementService)Config.getContext().getBean(PlanManagementService.class);
/*      */   private static PropertiesRepository pr;
/*      */   private static MessageSource messageSource;
/*      */   private static PostProcessingService pps;
/*      */   
/*      */   public void onUpdateReceived(Update update)
/*      */   {
/*  106 */     if (usercommandstate == null) {
/*  107 */       usercommandstate = new HashMap();
/*      */     }
/*      */     
/*  110 */     if ((update.hasMessage()) && (update.getMessage().hasText())) {
/*  111 */       String chatid = update.getMessage().getChatId().toString();
/*      */       
/*  113 */       Users u = ur.isUserActivebyChatid(chatid);
/*      */       
/*  115 */       if (chatid != null)
/*      */       {
/*  117 */         if (u != null)
/*      */         {
/*      */ 
/*  120 */           Userplan up = ps.getLastUserPlan(u.getId());
/*      */           
/*  122 */           if (up != null)
/*      */           {
/*  124 */             if (up.getState().equals(PlanStateEnum.ACTIVO.getState())) {
/*  125 */               String receivedmessage = update.getMessage().getText();
/*  126 */               List<String> receivedmessagelist = new ArrayList(Arrays.asList(receivedmessage.split(" ")));
/*  127 */               String command = (String)receivedmessagelist.get(0);
/*  128 */               if (CommandEnum.START.getCommand().equals(command)) {
/*  129 */                 handleStartCommand(chatid);
/*  130 */               } else if (CommandEnum.DELETEREGISTER.getCommand().equals(command)) {
/*  131 */                 u.setState("");
/*  132 */                 u.setMobilphone("");
/*  133 */                 u.setEmail("");
/*  134 */                 ur.saveAndFlush(u);
/*  135 */               } else if (CommandEnum.TELEGRAMSENTIMENT.getCommand().equals(command)) {
/*  136 */                 handleSentimentCommand(chatid);
/*  137 */               } else if (CommandEnum.TWITTER.getCommand().equals(command)) {
/*  138 */                 handleTwitterSentimentCommand(chatid);
/*  139 */               } else if (CommandEnum.FACEBOOK.getCommand().equals(command)) {
/*  140 */                 handleFacebookSentimentCommand(chatid);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               }
/*  149 */               else if (CommandEnum.PPALMENU.getCommand().equals(command)) {
/*  150 */                 handlePpalMenuCommand(chatid);
/*  151 */               } else if (CommandEnum.PARTIALREPORT.getCommand().equals(command)) {
/*  152 */                 handleReportDetailCommand(chatid);
/*  153 */               } else if (CommandEnum.CANCEL.getCommand().equals(command)) {
/*  154 */                 handleCancelCommand(chatid);
/*  155 */               } else if (CommandEnum.COMANDSTATUS.getCommand().equals(command)) {
/*  156 */                 handleStatusCommand(chatid);
/*  157 */               } else if (CommandEnum.HELP.getCommand().equals(command)) {
/*  158 */                 handleHelpCommand(chatid);
/*      */               } else {
/*  160 */                 ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  178 */                 if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()))
/*      */                 {
/*  180 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.TWITTER.getSocialmediasource())) {
/*  181 */                     handleTwitterSentimentSearchObjectCommand(chatid, receivedmessage);
/*  182 */                     return;
/*      */                   }
/*      */                 }
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  191 */                 if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()))
/*      */                 {
/*  193 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  194 */                     if (cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()).equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
/*  195 */                       handleFacebookSentimentSearchObjectCommand(chatid, receivedmessage);
/*  196 */                       return; }
/*  197 */                     handleFacebookSentimentSearchObjectPostCommand(chatid, receivedmessage);
/*      */                     
/*  199 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSINCE.getState()))
/*      */                 {
/*  201 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  202 */                     handleFacebookSentimentSinceCommand(chatid, receivedmessage);
/*  203 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTUNTIL.getState()))
/*      */                 {
/*  205 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  206 */                     handleFacebookSentimentUntilCommand(chatid, receivedmessage);
/*  207 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState()))
/*      */                 {
/*  209 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  210 */                     handleFacebookSentimentChoosePostCommand(chatid, receivedmessage);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */                   }
/*      */                   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */                 }
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               }
/*      */               
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             }
/*  245 */             else if (up.getState().equals(PlanStateEnum.FINALIZADO.getState())) {
/*  246 */               if (up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType()))
/*      */               {
/*  248 */                 handleFreePlanFinished(chatid, u);
/*  249 */               } else if ((!up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType())) && 
/*  250 */                 (up.getTxpalused().intValue() >= up.getTxplancapacity().intValue())) {
/*  251 */                 handlePlanTxFinished(chatid, u);
/*      */               }
/*      */               
/*      */             }
/*      */             
/*      */           }
/*      */           
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  263 */           SendMessage message = new SendMessage()
/*  264 */             .setChatId(update.getMessage().getChatId())
/*  265 */             .setReplyMarkup(getRegistryboard())
/*  266 */             .setText(messageSource.getMessage("messsage.telegram.finalizarregistro.text", null, LocaleContextHolder.getLocale()));
/*      */           try {
/*  268 */             sendMessage(message);
/*      */           } catch (TelegramApiException e1) {
/*  270 */             e1.printStackTrace();
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */           }
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  293 */     else if (update.hasCallbackQuery()) {
/*  294 */       CallbackQuery callbackquery = update.getCallbackQuery();
/*  295 */       String[] data = callbackquery.getData().split(":");
/*      */       
/*  297 */       String chatid = callbackquery.getMessage().getChatId().toString();
/*      */       
/*      */ 
/*      */ 
/*  301 */       if (data[0].equals("detailreport")) {
/*  302 */         handleReportDetailCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
/*  303 */       } else if (data[0].equals("cancelreport")) {
/*  304 */         handleCancelCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
/*  305 */       } else if (data[0].equals("licenseinfluenceenroll")) {
/*  306 */         handleInfluencerEnrollAcceptance(callbackquery.getMessage().getChatId().toString(), data[1], callbackquery.getFrom());
/*  307 */       } else if (data[0].equals("renewplantoday")) {
/*  308 */         Users u = ur.isUserActivebyChatid(chatid);
/*  309 */         handleRenewPlanToday(chatid, u);
/*      */       }
/*      */     }
/*  312 */     else if ((update.hasMessage()) && (update.getMessage().getContact() != null)) {
/*  313 */       String chatid = update.getMessage().getChatId().toString();
/*  314 */       handleFinalizeRegisterCommand(update, chatid);
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleRenewPlanToday(String chatid, Users u) {
/*      */     try {
/*  320 */       pm.upgradePlanPaymentMethodPredeterminated(u);
/*      */     } catch (Exception e) {
/*  322 */       SendMessage message = new SendMessage()
/*  323 */         .setChatId(chatid)
/*  324 */         .setReplyMarkup(getMainMenuKeyboard())
/*  325 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  327 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  329 */         e1.printStackTrace();
/*      */       }
/*  331 */       e.printStackTrace();
/*      */       try {
/*  333 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  335 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handlePlanTxFinished(String chatid, Users u)
/*      */   {
/*      */     try
/*      */     {
/*  344 */       SendMessage message = new SendMessage()
/*  345 */         .setChatId(chatid)
/*  346 */         .setReplyMarkup(getPlanTxFinishedInlineMenuKeyboard(u))
/*  347 */         .setText(messageSource.getMessage("message.telegram.planfinish.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/*  350 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  352 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  372 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  355 */       message = 
/*      */       
/*      */ 
/*  358 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  360 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  362 */         e1.printStackTrace();
/*      */       }
/*  364 */       e.printStackTrace();
/*      */       try {
/*  366 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  368 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getPlanTxFinishedInlineMenuKeyboard(Users u)
/*      */   {
/*  375 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/*  377 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/*  379 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/*  380 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.planfinish.button.text", null, LocaleContextHolder.getLocale())).setCallbackData("renewplantoday"));
/*      */     
/*  382 */     rowsInline.add(rowInline);
/*  383 */     markupInline.setKeyboard(rowsInline);
/*      */     
/*  385 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleCancelCommandfromState(String chatid, String cpId)
/*      */   {
/*  390 */     pps.cancel(cpId);
/*      */     
/*  392 */     SendMessage message = new SendMessage()
/*  393 */       .setChatId(chatid)
/*  394 */       .setReplyMarkup(getMainMenuKeyboard())
/*  395 */       .setText(messageSource.getMessage("messsage.telegram.aftercancel.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  397 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  399 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleCancelCommand(String chatid)
/*      */   {
/*  405 */     String cpid = "";
/*      */     
/*  407 */     List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*  408 */     if (lcp.size() > 0) {
/*  409 */       cpid = ((CommandProcess)lcp.get(0)).getId();
/*  410 */       pps.cancel(cpid);
/*      */       
/*  412 */       SendMessage message = new SendMessage()
/*  413 */         .setChatId(chatid)
/*  414 */         .setReplyMarkup(getMainMenuKeyboard())
/*  415 */         .setText(messageSource.getMessage("messsage.telegram.aftercancel.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  417 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  419 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  424 */       SendMessage message = new SendMessage()
/*  425 */         .setChatId(chatid)
/*  426 */         .setReplyMarkup(getMainMenuKeyboard())
/*  427 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  429 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  431 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private static ReplyKeyboardMarkup getMainMenuKeyboard()
/*      */   {
/*  439 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  440 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  441 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  442 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  444 */     List<KeyboardRow> keyboard = new ArrayList();
/*  445 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  446 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.sentiment.text", null, LocaleContextHolder.getLocale()));
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  451 */     keyboard.add(keyboardFirstRow);
/*      */     
/*      */ 
/*  454 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  456 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getRegistryboard() {
/*  460 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  461 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  462 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  463 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  465 */     List<KeyboardRow> keyboard = new ArrayList();
/*  466 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*      */     
/*  468 */     KeyboardButton button = new KeyboardButton(messageSource.getMessage("messsage.telegram.command.finalizarregistro.text", null, LocaleContextHolder.getLocale()));
/*  469 */     button.setRequestContact(Boolean.valueOf(true));
/*  470 */     keyboardFirstRow.add(button);
/*  471 */     keyboard.add(keyboardFirstRow);
/*  472 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  474 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getSentimentMenuKeyboard() {
/*  478 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  479 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  480 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  481 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  483 */     List<KeyboardRow> keyboard = new ArrayList();
/*  484 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*      */     
/*  486 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.text", null, LocaleContextHolder.getLocale()));
/*      */     
/*      */ 
/*  489 */     keyboard.add(keyboardFirstRow);
/*      */     
/*  491 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  493 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getPpalMenuKeyboard() {
/*  497 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  498 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  499 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  500 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*  501 */     List<KeyboardRow> keyboard = new ArrayList();
/*  502 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  503 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.ppalmenu.text", null, LocaleContextHolder.getLocale()));
/*  504 */     keyboard.add(keyboardSecondRow);
/*  505 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  507 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getTwitterSearchTypeMenuKeyboard() {
/*  511 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  512 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  513 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  514 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  516 */     List<KeyboardRow> keyboard = new ArrayList();
/*  517 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  518 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.searchbyusertext", null, LocaleContextHolder.getLocale()));
/*  519 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.searchbyhastagtext", null, LocaleContextHolder.getLocale()));
/*  520 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  521 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/*  522 */     keyboard.add(keyboardFirstRow);
/*  523 */     keyboard.add(keyboardSecondRow);
/*  524 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  526 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getFacebookSearchTypeMenuKeyboard() {
/*  530 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  531 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  532 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  533 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  535 */     List<KeyboardRow> keyboard = new ArrayList();
/*  536 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  537 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.facebook.searchbypagetext", null, LocaleContextHolder.getLocale()));
/*  538 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.facebook.searchbyposttext", null, LocaleContextHolder.getLocale()));
/*  539 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  540 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/*  541 */     keyboard.add(keyboardFirstRow);
/*  542 */     keyboard.add(keyboardSecondRow);
/*  543 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  545 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private void handleFinalizeRegisterCommand(Update update, String chatid)
/*      */   {
/*  550 */     Users u = ur.isUserActivebyMobilPhone(update.getMessage().getContact().getPhoneNumber().replace("+", ""));
/*      */     
/*  552 */     if (u != null) {
/*  553 */       u.setChatid(update.getMessage().getChatId().toString());
/*  554 */       ur.saveAndFlush(u);
/*  555 */       handleStartCommand(update.getMessage().getChatId().toString());
/*      */     }
/*      */     else {
/*      */       try {
/*  559 */         SendMessage message = new SendMessage()
/*  560 */           .setChatId(chatid)
/*  561 */           .setReplyMarkup(getRegisterUserInlineMenuKeyboard())
/*  562 */           .setText(messageSource.getMessage("message.telegram.registrarusuario.text", null, LocaleContextHolder.getLocale()));
/*      */         try
/*      */         {
/*  565 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  567 */           e1.printStackTrace();
/*      */         }
/*      */         SendMessage message;
/*      */         return; } catch (Exception e) { message = 
/*      */         
/*      */ 
/*  573 */           new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */         try {
/*  575 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  577 */           e1.printStackTrace();
/*      */         }
/*  579 */         e.printStackTrace();
/*      */         try {
/*  581 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  583 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleStartCommand(String chatid)
/*      */   {
/*  591 */     List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*      */     
/*  593 */     if (lcp.size() == 0) {
/*  594 */       ComandDTO cdto = new ComandDTO();
/*  595 */       cdto.setState(TelegramStateEnum.MAINMENU.getState());
/*  596 */       cdto.setArguments(new HashMap());
/*  597 */       cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), 
/*  598 */         ServiceTypeEnum.PREXTIGE.getServicetype());
/*  599 */       usercommandstate.put(chatid, cdto);
/*  600 */       SendMessage message = new SendMessage()
/*  601 */         .setChatId(chatid)
/*  602 */         .setReplyMarkup(getMainMenuKeyboard())
/*  603 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()));
/*      */       try {
/*  605 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  607 */         e1.printStackTrace();
/*      */       }
/*      */     } else {
/*  610 */       SendMessage message = new SendMessage()
/*  611 */         .setChatId(chatid)
/*  612 */         .setReplyMarkup(getPushPartialMenuKeyboard())
/*  613 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.currentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  615 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  617 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handlePpalMenuCommand(String chatid) {
/*  623 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  624 */     cdto.setState(TelegramStateEnum.MAINMENU.getState());
/*  625 */     SendMessage message = new SendMessage()
/*  626 */       .setChatId(chatid)
/*  627 */       .setReplyMarkup(getMainMenuKeyboard())
/*  628 */       .setText(messageSource.getMessage("messsage.telegram.mainmenu.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  630 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  632 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleHelpCommand(String chatid) {
/*  637 */     SendMessage message = new SendMessage()
/*  638 */       .setChatId(chatid)
/*  639 */       .setReplyMarkup(getMainMenuKeyboard())
/*  640 */       .setText(messageSource.getMessage("messsage.telegram.help.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  642 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  644 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleSentimentCommand(String chatid) {
/*  649 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  650 */     cdto.setState(TelegramStateEnum.SENTIMENT.getState());
/*  651 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  652 */     SendMessage message = new SendMessage()
/*  653 */       .setChatId(chatid)
/*  654 */       .setReplyMarkup(getSentimentMenuKeyboard())
/*  655 */       .setText(messageSource.getMessage("messsage.telegram.sentiment.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  657 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  659 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentCommand(String chatid) {
/*  664 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  665 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  666 */     cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), 
/*  667 */       SocialMediaSourceEnum.TWITTER.getSocialmediasource());
/*  668 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  669 */       TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/*  670 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  671 */     SendMessage message = new SendMessage()
/*  672 */       .setChatId(chatid)
/*  673 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  674 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  676 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  678 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSearchbyUserCommand(String chatid) {
/*  683 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  684 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  685 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  686 */       TwitterSearchTypeEnum.USUARIO.getTwittersearchtype());
/*  687 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  688 */     SendMessage message = new SendMessage()
/*  689 */       .setChatId(chatid)
/*  690 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  692 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  694 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSearchbyHashtagCommand(String chatid) {
/*  699 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  700 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  701 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  702 */       TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/*  703 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  704 */     SendMessage message = new SendMessage()
/*  705 */       .setChatId(chatid)
/*  706 */       .setReplyMarkup(getPpalMenuKeyboard())
/*  707 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  709 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  711 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentCommand(String chatid) {
/*  716 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  717 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHTYPE.getState());
/*  718 */     cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), 
/*  719 */       SocialMediaSourceEnum.FACEBOOK.getSocialmediasource());
/*  720 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  721 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
/*  722 */       cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  723 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  724 */         FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
/*      */     }
/*  726 */     SendMessage message = new SendMessage()
/*  727 */       .setChatId(chatid)
/*  728 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  729 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  731 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  733 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchbyPageCommand(String chatid)
/*      */   {
/*  739 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  740 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
/*  741 */       cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  742 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  743 */         FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
/*      */     }
/*  745 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  746 */     SendMessage message = new SendMessage()
/*  747 */       .setChatId(chatid)
/*  748 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  749 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  751 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  753 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchbyPostCommand(String chatid) {
/*  758 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  759 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState())) {
/*  760 */       cdto.setState(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState());
/*  761 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  762 */         FacebookSearchTypeEnum.POST.getFacebooksearchtype());
/*      */     }
/*  764 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  765 */     SendMessage message = new SendMessage()
/*  766 */       .setChatId(chatid)
/*  767 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  768 */       .setText(messageSource.getMessage("messsage.telegram.facebook.choosepage.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  770 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  772 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchObjectCommand(String chatid, String searchobject) {
/*  777 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  778 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  779 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  780 */       searchobject);
/*  781 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  782 */     Calendar cal = Calendar.getInstance();
/*  783 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  784 */       cal.getTime());
/*      */     
/*  786 */     cal.add(5, -8);
/*      */     
/*  788 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  789 */       cal.getTime());
/*  790 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  791 */     cdto.setCommunicationKey(chatid);
/*  792 */     ComandDTO cdtocopy = new ComandDTO();
/*  793 */     cdtocopy.setArguments(cdto.getArguments());
/*  794 */     cdtocopy.setChannel(cdto.getChannel());
/*  795 */     cdtocopy.setCommand(cdto.getCommand());
/*  796 */     cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  797 */     cdtocopy.setState(cdto.getState());
/*  798 */     cdtocopy.setCratedAt(new Date());
/*  799 */     pps.proccess(cdtocopy);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private static UserRepository ur;
/*      */   
/*      */ 
/*      */   private static FacebookService fs;
/*      */   
/*      */   private static ReportDetailManager rdm;
/*      */   
/*      */   private void handleTwitterSentimentSearchObjectCommand(String chatid, String searchobject)
/*      */   {
/*  813 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  814 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  815 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  816 */       searchobject);
/*  817 */     Calendar cal = Calendar.getInstance();
/*  818 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  819 */       cal.getTime());
/*      */     
/*  821 */     cal.add(11, -24);
/*      */     
/*  823 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  824 */       cal.getTime());
/*  825 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  826 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  827 */     cdto.setCommunicationKey(chatid);
/*  828 */     ComandDTO cdtocopy = new ComandDTO();
/*  829 */     cdtocopy.setArguments(cdto.getArguments());
/*  830 */     cdtocopy.setChannel(cdto.getChannel());
/*  831 */     cdtocopy.setCommand(cdto.getCommand());
/*  832 */     cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  833 */     cdtocopy.setState(cdto.getState());
/*  834 */     cdtocopy.setCratedAt(new Date());
/*  835 */     pps.proccess(cdtocopy);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void handleFacebookSentimentSearchObjectPostCommand(String chatid, String postnumber)
/*      */   {
/*  849 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     
/*  851 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  852 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  853 */       ((Post)((List)cdto.getArguments().get(CommandArgumentsEnum.POSTLIST.getCommandargument())).get(new Integer(postnumber).intValue())).getId());
/*  854 */     cdto.getArguments().remove(CommandArgumentsEnum.POSTLIST.getCommandargument());
/*  855 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  856 */     SendMessage message = new SendMessage()
/*  857 */       .setChatId(chatid)
/*  858 */       .setText(messageSource.getMessage("messsage.telegram.since.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  860 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  862 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentChoosePostCommand(String chatid, String searchobject) {
/*  867 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  868 */     List<Post> postlist = null;
/*      */     try {
/*  870 */       postlist = fs.searchlastPosts(searchobject, 10);
/*      */     } catch (Exception e) {
/*  872 */       SendMessage message = new SendMessage()
/*  873 */         .setChatId(chatid)
/*  874 */         .setReplyMarkup(getMainMenuKeyboard())
/*  875 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  877 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  879 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */     
/*  883 */     int i = 0;
/*  884 */     for (Post p : postlist) {
/*  885 */       SendMessage message = new SendMessage()
/*  886 */         .setChatId(chatid)
/*  887 */         .setReplyMarkup(new ReplyKeyboardRemove())
/*  888 */         .setText("Post " + i + " :" + p.getMessage());
/*      */       try {
/*  890 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  892 */         e1.printStackTrace();
/*      */       }
/*  894 */       i++;
/*      */     }
/*      */     
/*  897 */     SendMessage message = new SendMessage()
/*  898 */       .setChatId(chatid)
/*  899 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  900 */       .setText(messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  902 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  904 */       e1.printStackTrace();
/*      */     }
/*  906 */     cdto.getArguments().put(CommandArgumentsEnum.POSTLIST.getCommandargument(), 
/*  907 */       postlist);
/*  908 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSinceCommand(String chatid, String since)
/*      */   {
/*  913 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  914 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  916 */       cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
/*  917 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  918 */         dateFormat.parse(since));
/*  919 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  920 */       cdto.setCommunicationKey(chatid);
/*  921 */       SendMessage message = new SendMessage()
/*  922 */         .setChatId(chatid)
/*  923 */         .setReplyMarkup(getMainMenuKeyboard())
/*  924 */         .setText(messageSource.getMessage("messsage.telegram.until.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  926 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  928 */         e1.printStackTrace();
/*      */       }
/*      */       SendMessage message;
/*      */       return; } catch (ParseException e) { message = 
/*      */       
/*      */ 
/*  934 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  936 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  938 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentUntilCommand(String chatid, String since) {
/*  944 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  945 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  947 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  948 */         dateFormat.parse(since));
/*  949 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  950 */       cdto.setCommunicationKey(chatid);
/*  951 */       ComandDTO cdtocopy = new ComandDTO();
/*  952 */       cdtocopy.setArguments(cdto.getArguments());
/*  953 */       cdtocopy.setChannel(cdto.getChannel());
/*  954 */       cdtocopy.setCommand(cdto.getCommand());
/*  955 */       cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  956 */       cdtocopy.setState(cdto.getState());
/*  957 */       cdtocopy.setCratedAt(new Date());
/*  958 */       pps.proccess(cdtocopy);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (ParseException e)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  970 */       SendMessage message = new SendMessage()
/*  971 */         .setChatId(chatid)
/*  972 */         .setReplyMarkup(getMainMenuKeyboard())
/*  973 */         .setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  975 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  977 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentUntilCommand(String chatid, String since)
/*      */   {
/*  984 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  985 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  987 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  988 */         dateFormat.parse(since));
/*  989 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  990 */       cdto.setCommunicationKey(chatid);
/*  991 */       ComandDTO cdtocopy = new ComandDTO();
/*  992 */       cdtocopy.setArguments(cdto.getArguments());
/*  993 */       cdtocopy.setChannel(cdto.getChannel());
/*  994 */       cdtocopy.setCommand(cdto.getCommand());
/*  995 */       cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  996 */       cdtocopy.setState(cdto.getState());
/*  997 */       cdtocopy.setCratedAt(new Date());
/*  998 */       pps.proccess(cdtocopy);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (ParseException e)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1010 */       SendMessage message = new SendMessage()
/* 1011 */         .setChatId(chatid)
/* 1012 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1013 */         .setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1015 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1017 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSinceCommand(String chatid, String since)
/*      */   {
/* 1024 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/* 1025 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/* 1027 */       cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
/* 1028 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/* 1029 */         dateFormat.parse(since));
/* 1030 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 1031 */       cdto.setCommunicationKey(chatid);
/* 1032 */       SendMessage message = new SendMessage()
/* 1033 */         .setChatId(chatid)
/* 1034 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1035 */         .setText(messageSource.getMessage("messsage.telegram.until.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1037 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1039 */         e1.printStackTrace();
/*      */       }
/*      */       SendMessage message;
/*      */       return; } catch (ParseException e) { message = 
/*      */       
/*      */ 
/* 1045 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1047 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1049 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void handlePushPartialReport(String chatid, int totalpost, String searchcriteria, String cpid)
/*      */   {
/* 1057 */     ComandDTO cdto = new ComandDTO();
/* 1058 */     cdto.setState(TelegramStateEnum.DETAILREPORT.getState());
/* 1059 */     cdto.setArguments(new HashMap());
/* 1060 */     cdto.getArguments().put(CommandArgumentsEnum.COMMANDPROCESSID.getCommandargument(), 
/* 1061 */       cpid);
/* 1062 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 1063 */     cdto.setCommunicationKey(chatid);
/*      */     
/* 1065 */     usercommandstate.replace(chatid, cdto);
/*      */     
/* 1067 */     SendMessage message = new SendMessage()
/* 1068 */       .setChatId(chatid)
/* 1069 */       .setReplyMarkup(getPushPartialMenuKeyboard())
/* 1070 */       .setText(messageSource.getMessage("messsage.telegram.pushnotification1.text", new Object[] { Integer.valueOf(totalpost), searchcriteria }, LocaleContextHolder.getLocale()));
/*      */     try {
/* 1072 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/* 1074 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private ReplyKeyboard getPushPartialMenuKeyboard()
/*      */   {
/* 1081 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/* 1082 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/* 1083 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/* 1084 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/* 1086 */     List<KeyboardRow> keyboard = new ArrayList();
/* 1087 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/* 1088 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", null, LocaleContextHolder.getLocale()));
/* 1089 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/* 1090 */     keyboard.add(keyboardFirstRow);
/* 1091 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/* 1093 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private void handleReportDetailCommand(String chatid) {
/*      */     try {
/* 1098 */       String cpid = "";
/*      */       
/* 1100 */       List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/* 1101 */       if (lcp.size() > 0) {
/* 1102 */         cpid = ((CommandProcess)lcp.get(0)).getId();
/* 1103 */         rdm = rdmf.getResponseBotService(ServiceTypeEnum.PREXTIGE.getServicetype());
/* 1104 */         rdm.reportdetailgenerateSentimentAnalysisReport(
/* 1105 */           cpid);
/*      */       } else {
/* 1107 */         SendMessage message = new SendMessage()
/* 1108 */           .setChatId(chatid)
/* 1109 */           .setReplyMarkup(getMainMenuKeyboard())
/* 1110 */           .setText(messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1112 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1114 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1132 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1122 */       message = 
/*      */       
/*      */ 
/* 1125 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1127 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1129 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleStatusCommand(String chatid)
/*      */   {
/*      */     try {
/* 1137 */       List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*      */       
/* 1139 */       if (lcp.size() > 0) {
/* 1140 */         for (CommandProcess cp : lcp)
/*      */         {
/* 1142 */           XStream xs = new XStream();
/* 1143 */           ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/*      */           
/* 1145 */           int total = postr.totalPost(cp.getId()).intValue();
/*      */           
/* 1147 */           Date date = new Date();
/* 1148 */           DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
/* 1149 */           SendMessage message = new SendMessage()
/* 1150 */             .setChatId(chatid)
/* 1151 */             .setReplyMarkup(getProcessStateInlineMenuKeyboard(cp))
/* 1152 */             .setText(messageSource.getMessage("messsage.telegram.procesrunning.text", new Object[] {
/* 1153 */             cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()), 
/* 1154 */             cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), 
/* 1155 */             df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())), 
/* 1156 */             df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())), 
/* 1157 */             Integer.valueOf(total) }, LocaleContextHolder.getLocale()));
/*      */           try {
/* 1159 */             sendMessage(message);
/*      */           } catch (TelegramApiException e1) {
/* 1161 */             e1.printStackTrace();
/*      */           }
/*      */         }
/*      */       } else {
/* 1165 */         SendMessage message = new SendMessage()
/* 1166 */           .setChatId(chatid)
/* 1167 */           .setReplyMarkup(getMainMenuKeyboard())
/* 1168 */           .setText(messageSource.getMessage("messsage.telegram.notprocesrunning.text", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1170 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1172 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1187 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1176 */       message = 
/*      */       
/*      */ 
/* 1179 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1181 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1183 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private ReplyKeyboard getProcessStateInlineMenuKeyboard(CommandProcess cp)
/*      */   {
/* 1191 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1193 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1195 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1196 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", null, LocaleContextHolder.getLocale())).setCallbackData("detailreport:cpid:" + cp.getId()));
/* 1197 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale())).setCallbackData("cancelreport:cpid:" + cp.getId()));
/*      */     
/* 1199 */     rowsInline.add(rowInline);
/* 1200 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1202 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleReportDetailCommandfromState(String chatid, String cpid) {
/*      */     try {
/* 1207 */       rdm = rdmf.getResponseBotService(ServiceTypeEnum.PREXTIGE.getServicetype());
/* 1208 */       rdm.reportdetailgenerateSentimentAnalysisReport(
/* 1209 */         cpid);
/*      */       
/*      */ 
/* 1212 */       usercommandstate.replace(chatid, new ComandDTO());
/*      */     } catch (Exception e) {
/* 1214 */       SendMessage message = new SendMessage()
/* 1215 */         .setChatId(chatid)
/* 1216 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1217 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1219 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1221 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnroll(String chatid)
/*      */   {
/*      */     try {
/* 1229 */       ComandDTO cdto = new ComandDTO();
/* 1230 */       cdto.setState(TelegramStateEnum.EMAILENRROLL.getState());
/* 1231 */       cdto.setCommand(CommandEnum.INFLUENCERENNROLL.getCommand());
/*      */       
/* 1233 */       usercommandstate.put(chatid, cdto);
/*      */       
/* 1235 */       SendMessage message = new SendMessage()
/* 1236 */         .setChatId(chatid)
/* 1237 */         .setText(messageSource.getMessage("messsage.telegram.influencerenroll.email.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1240 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1242 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1261 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1245 */       message = 
/*      */       
/*      */ 
/* 1248 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1250 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1252 */         e1.printStackTrace();
/*      */       }
/* 1254 */       e.printStackTrace();
/*      */       try {
/* 1256 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1258 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleFreePlanFinished(String chatid, Users u)
/*      */   {
/*      */     try {
/* 1266 */       SendMessage message = new SendMessage()
/* 1267 */         .setChatId(chatid)
/* 1268 */         .setReplyMarkup(getSelectPlanInlineMenuKeyboard(u))
/* 1269 */         .setText(messageSource.getMessage("message.telegram.freeplanfinish.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1272 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1274 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1293 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1277 */       message = 
/*      */       
/*      */ 
/* 1280 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1282 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1284 */         e1.printStackTrace();
/*      */       }
/* 1286 */       e.printStackTrace();
/*      */       try {
/* 1288 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1290 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnrollLicence(String chatid, String email)
/*      */   {
/*      */     try {
/* 1298 */       ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */       
/* 1300 */       cdto.setArguments(new HashMap());
/* 1301 */       cdto.getArguments().put(CommandArgumentsEnum.EMAIL.getCommandargument(), 
/* 1302 */         email);
/*      */       
/* 1304 */       String licence = pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
/*      */       
/* 1306 */       SendMessage message = new SendMessage()
/* 1307 */         .setChatId(chatid)
/* 1308 */         .setReplyMarkup(getlicenseinfluencerInlineMenuKeyboard())
/* 1309 */         .setText(licence);
/*      */       try {
/* 1311 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1313 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1332 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1316 */       message = 
/*      */       
/*      */ 
/* 1319 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1321 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1323 */         e1.printStackTrace();
/*      */       }
/* 1325 */       e.printStackTrace();
/*      */       try {
/* 1327 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1329 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getlicenseinfluencerInlineMenuKeyboard()
/*      */   {
/* 1336 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1338 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1340 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1341 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.influencerenroll.acept.text", null, LocaleContextHolder.getLocale())).setCallbackData("licenseinfluenceenroll:YES"));
/* 1342 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.influencerenroll.reject.text", null, LocaleContextHolder.getLocale())).setCallbackData("licenseinfluenceenroll:NO"));
/*      */     
/* 1344 */     rowsInline.add(rowInline);
/* 1345 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1347 */     return markupInline;
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getSelectPlanInlineMenuKeyboard(Users u)
/*      */   {
/* 1352 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1354 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1356 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1357 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.freeplanfinish.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("selectplan.url") + "?usertoken=" + ts.generateUserToken(u)));
/*      */     
/* 1359 */     rowsInline.add(rowInline);
/* 1360 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1362 */     return markupInline;
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getRegisterUserInlineMenuKeyboard()
/*      */   {
/* 1367 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1369 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1371 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1372 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.registrarusuario.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("register.url")));
/*      */     
/* 1374 */     rowsInline.add(rowInline);
/* 1375 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1377 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnrollAcceptance(String chatid, String acceptance, User u)
/*      */   {
/*      */     try {
/* 1383 */       if (acceptance.equals("YES"))
/*      */       {
/* 1385 */         String licence = pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
/*      */         
/* 1387 */         Users user = new Users();
/* 1388 */         user.setChatid(chatid);
/* 1389 */         user.setId(UUID.randomUUID().toString());
/* 1390 */         user.setName(u.getFirstName());
/* 1391 */         user.setLastname(u.getLastName());
/* 1392 */         user.setState("activo");
/* 1393 */         user.setTestlicenceacepted("Y");
/* 1394 */         user.setTexttestlicence(licence);
/*      */         
/* 1396 */         ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */         
/* 1398 */         user.setEmail((String)cdto.getArguments().get(CommandArgumentsEnum.EMAIL.getCommandargument()));
/* 1399 */         ur.saveAndFlush(user);
/*      */         
/*      */ 
/* 1402 */         handleStartCommand(chatid);
/*      */       }
/*      */       else {
/* 1405 */         SendMessage message = new SendMessage()
/* 1406 */           .setChatId(chatid)
/* 1407 */           .setText(messageSource.getMessage("messsage.telegram.influencerenroll.reject.text1", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1409 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1411 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1431 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1415 */       message = 
/*      */       
/*      */ 
/* 1418 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1420 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1422 */         e1.printStackTrace();
/*      */       }
/* 1424 */       e.printStackTrace();
/*      */       try {
/* 1426 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1428 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleUserTokenException(String chatid)
/*      */   {
/*      */     try {
/* 1436 */       SendMessage message = new SendMessage()
/* 1437 */         .setChatId(chatid)
/* 1438 */         .setText(messageSource.getMessage("message.telegram.usertokenexception.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1441 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1443 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1462 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1446 */       message = 
/*      */       
/*      */ 
/* 1449 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1451 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1453 */         e1.printStackTrace();
/*      */       }
/* 1455 */       e.printStackTrace();
/*      */       try {
/* 1457 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1459 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleUpgradePlanOK(String chatid)
/*      */   {
/*      */     try {
/* 1467 */       SendMessage message = new SendMessage()
/* 1468 */         .setChatId(chatid)
/* 1469 */         .setText(messageSource.getMessage("message.telegram.upgradeplanok.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1472 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1474 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1493 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1477 */       message = 
/*      */       
/*      */ 
/* 1480 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1482 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1484 */         e1.printStackTrace();
/*      */       }
/* 1486 */       e.printStackTrace();
/*      */       try {
/* 1488 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1490 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handlePaymentExceptionUpgradePlan(String chatid, Users u)
/*      */   {
/*      */     try {
/* 1498 */       SendMessage message = new SendMessage()
/* 1499 */         .setChatId(chatid)
/* 1500 */         .setReplyMarkup(gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(u))
/* 1501 */         .setText(messageSource.getMessage("message.telegram.paymentexceptionupgradeplan.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1504 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1506 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1525 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1509 */       message = 
/*      */       
/*      */ 
/* 1512 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1514 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1516 */         e1.printStackTrace();
/*      */       }
/* 1518 */       e.printStackTrace();
/*      */       try {
/* 1520 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1522 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(Users u)
/*      */   {
/* 1529 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1531 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1533 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1534 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.freeplanfinish.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("paymentmethod.url") + "?usertoken=" + ts.generateUserToken(u)));
/*      */     
/* 1536 */     rowsInline.add(rowInline);
/* 1537 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1539 */     return markupInline;
/*      */   }
/*      */   
/*      */ 
/*      */   public String getBotUsername()
/*      */   {
/* 1545 */     return "prextigebot";
/*      */   }
/*      */   
/*      */   public String getBotToken()
/*      */   {
/* 1550 */     return env.getRequiredProperty("prextigebot.token");
/*      */   }
/*      */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\telegram\bot\PreXtigeBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */