/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class RegisterUserDTO
/*    */ {
/*    */   private String name;
/*    */   private String lastname;
/*    */   private String email;
/*    */   private String mobilphone;
/*    */   
/*    */   public String getName() {
/* 11 */     return this.name;
/*    */   }
/*    */   
/* 14 */   public void setName(String name) { this.name = name; }
/*    */   
/*    */   public String getLastname() {
/* 17 */     return this.lastname;
/*    */   }
/*    */   
/* 20 */   public void setLastname(String lastname) { this.lastname = lastname; }
/*    */   
/*    */   public String getEmail() {
/* 23 */     return this.email;
/*    */   }
/*    */   
/* 26 */   public void setEmail(String email) { this.email = email; }
/*    */   
/*    */   public String getMobilphone() {
/* 29 */     return this.mobilphone;
/*    */   }
/*    */   
/* 32 */   public void setMobilphone(String mobilphone) { this.mobilphone = mobilphone; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\RegisterUserDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */