/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.Date;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class ComandDTO
/*    */ {
/*    */   private String communicationKey;
/*    */   private String channel;
/*    */   private String state;
/*    */   private String command;
/*    */   private Date cratedAt;
/*    */   private Map<String, Object> arguments;
/*    */   
/*    */   public String getCommand()
/*    */   {
/* 17 */     return this.command;
/*    */   }
/*    */   
/* 20 */   public void setCommand(String command) { this.command = command; }
/*    */   
/*    */   public Map<String, Object> getArguments() {
/* 23 */     return this.arguments;
/*    */   }
/*    */   
/* 26 */   public void setArguments(Map<String, Object> arguments) { this.arguments = arguments; }
/*    */   
/*    */   public String getState() {
/* 29 */     return this.state;
/*    */   }
/*    */   
/* 32 */   public void setState(String state) { this.state = state; }
/*    */   
/*    */   public String getCommunicationKey() {
/* 35 */     return this.communicationKey;
/*    */   }
/*    */   
/* 38 */   public void setCommunicationKey(String communicationKey) { this.communicationKey = communicationKey; }
/*    */   
/*    */   public String getChannel() {
/* 41 */     return this.channel;
/*    */   }
/*    */   
/* 44 */   public void setChannel(String channel) { this.channel = channel; }
/*    */   
/*    */   public Date getCratedAt() {
/* 47 */     return this.cratedAt;
/*    */   }
/*    */   
/* 50 */   public void setCratedAt(Date cratedAt) { this.cratedAt = cratedAt; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ComandDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */