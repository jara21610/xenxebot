/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*    */ import java.io.BufferedWriter;
/*    */ import java.io.File;
/*    */ import java.io.FileWriter;
/*    */ import java.text.DateFormat;
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.Date;
/*    */ import java.util.UUID;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.context.i18n.LocaleContextHolder;
/*    */ import org.springframework.core.env.Environment;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ public class LoadStressTestServiceImpl
/*    */   implements LoadStressTestService
/*    */ {
/* 23 */   int total = 0;
/* 24 */   double totalservicetime = 0.0D;
/* 25 */   double totalpreprocessingtime = 0.0D;
/* 26 */   double totaltime = 0.0D;
/*    */   
/* 28 */   ResumeSnapshotProcessingPostDTO rsppdto = null;
/* 29 */   String filename = "";
/*    */   @Autowired
/*    */   PropertiesRepository pr;
/*    */   @Autowired
/*    */   Environment env;
/*    */   
/*    */   public synchronized void putDataProcessingPost(LoadStressProcessingPost lstdto) throws Exception {
/* 36 */     BufferedWriter bw = null;
/* 37 */     FileWriter fw = null;
/* 38 */     DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
/* 39 */     this.total += 1;
/* 40 */     File file = null;
/* 41 */     String ruta = this.env.getRequiredProperty("outputpath");
/* 42 */     if (this.rsppdto == null) {
/* 43 */       this.rsppdto = new ResumeSnapshotProcessingPostDTO();
/* 44 */       this.filename = (UUID.randomUUID().toString() + "_" + df.format(new Date()));
/* 45 */       file = new File(ruta, this.filename);
/* 46 */       fw = new FileWriter(file);
/* 47 */       bw = new BufferedWriter(fw);
/*    */     } else {
/* 49 */       file = new File(ruta, this.filename);
/* 50 */       fw = new FileWriter(file, true);
/* 51 */       bw = new BufferedWriter(fw);
/* 52 */       bw.newLine();
/*    */     }
/*    */     
/* 55 */     this.totalservicetime += lstdto.getTotalservice();
/* 56 */     this.totalpreprocessingtime += lstdto.getTotaltimepreprocesing();
/* 57 */     this.totaltime += lstdto.getTotaltime();
/* 58 */     this.rsppdto.setTotal(this.total);
/* 59 */     this.rsppdto.setTotalservice(this.totalservicetime / this.total);
/* 60 */     this.rsppdto.setTotaltime(this.totaltime / this.total);
/* 61 */     this.rsppdto.setTotaltimepreprocesing(this.totalpreprocessingtime / this.total);
/* 62 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
/*    */     
/*    */ 
/* 65 */     bw.write(dateFormat.format(new Date()) + "," + lstdto.getTotalservice() + "," + lstdto.getTotaltimepreprocesing() + "," + lstdto.getTotaltime());
/*    */     
/* 67 */     bw.close();
/*    */   }
/*    */   
/*    */   public ResumeSnapshotProcessingPostDTO getResumeSnapshotProcessingPost()
/*    */     throws Exception
/*    */   {
/* 73 */     return this.rsppdto;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\LoadStressTestServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */