/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.math.BigInteger;
/*    */ 
/*    */ 
/*    */ public class TXVALUE
/*    */ {
/*    */   private BigInteger value;
/*    */   private String currency;
/*    */   
/*    */   public BigInteger getValue()
/*    */   {
/* 13 */     return this.value;
/*    */   }
/*    */   
/*    */   public void setValue(BigInteger value) {
/* 17 */     this.value = value;
/*    */   }
/*    */   
/*    */   public String getCurrency() {
/* 21 */     return this.currency;
/*    */   }
/*    */   
/*    */   public void setCurrency(String currency) {
/* 25 */     this.currency = currency;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TXVALUE.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */