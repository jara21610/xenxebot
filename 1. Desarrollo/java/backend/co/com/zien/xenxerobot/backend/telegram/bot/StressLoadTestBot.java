/*     */ package co.com.zien.xenxerobot.backend.telegram.bot;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.config.Config;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.TwitterSearchTypeEnum;
/*     */ import co.com.zien.xenxerobot.backend.services.ComandDTO;
/*     */ import co.com.zien.xenxerobot.backend.services.FacebookService;
/*     */ import co.com.zien.xenxerobot.backend.services.LoadStressTestService;
/*     */ import co.com.zien.xenxerobot.backend.services.PostProcessingService;
/*     */ import co.com.zien.xenxerobot.backend.services.ReportDetailManager;
/*     */ import co.com.zien.xenxerobot.backend.services.ReportDetailServiceFactory;
/*     */ import co.com.zien.xenxerobot.backend.services.ResumeSnapshotProcessingPostDTO;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.Calendar;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import org.springframework.context.ApplicationContext;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.core.env.Environment;
/*     */ import org.telegram.telegrambots.api.methods.send.SendMessage;
/*     */ import org.telegram.telegrambots.api.objects.Message;
/*     */ import org.telegram.telegrambots.api.objects.Update;
/*     */ import org.telegram.telegrambots.bots.TelegramLongPollingBot;
/*     */ import org.telegram.telegrambots.exceptions.TelegramApiException;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class StressLoadTestBot
/*     */   extends TelegramLongPollingBot
/*     */ {
/*  76 */   private static Map<String, ComandDTO> usercommandstate = null;
/*     */   
/*     */ 
/*     */   static
/*     */   {
/*  81 */     pr = (PropertiesRepository)Config.getContext().getBean(PropertiesRepository.class);
/*  82 */     messageSource = (MessageSource)Config.getContext().getBean(MessageSource.class);
/*  83 */     pps = (PostProcessingService)Config.getContext().getBean(PostProcessingService.class);
/*  84 */     ur = (UserRepository)Config.getContext().getBean(UserRepository.class);
/*  85 */     fs = (FacebookService)Config.getContext().getBean(FacebookService.class); }
/*  86 */   private static CommandProcessRepository cpr = (CommandProcessRepository)Config.getContext().getBean(CommandProcessRepository.class);
/*  87 */   private static PostRepository postr = (PostRepository)Config.getContext().getBean(PostRepository.class);
/*  88 */   private static ReportDetailServiceFactory rdmf = (ReportDetailServiceFactory)Config.getContext().getBean(ReportDetailServiceFactory.class);
/*  89 */   private static LoadStressTestService lsts = (LoadStressTestService)Config.getContext().getBean(LoadStressTestService.class);
/*  90 */   private static Environment env = (Environment)Config.getContext().getBean(Environment.class);
/*     */   private static PropertiesRepository pr;
/*     */   private static MessageSource messageSource;
/*     */   private static PostProcessingService pps;
/*     */   
/*     */   public void onUpdateReceived(Update update) {
/*  96 */     String chatid = update.getMessage().getChatId().toString();
/*     */     
/*  98 */     if ((update.hasMessage()) && (update.getMessage().hasText()))
/*     */     {
/* 100 */       String receivedmessage = update.getMessage().getText();
/* 101 */       List<String> receivedmessagelist = new ArrayList(Arrays.asList(receivedmessage.split(" ")));
/* 102 */       String command = (String)receivedmessagelist.get(0);
/*     */       
/* 104 */       if (CommandEnum.START.getCommand().equals(command)) {
/* 105 */         int threads = new Integer((String)receivedmessagelist.get(1)).intValue();
/* 106 */         stressloadtestlaucher(threads, chatid);
/* 107 */       } else if (CommandEnum.PARTIALREPORT.getCommand().equals(command)) {
/* 108 */         partialreport(chatid);
/* 109 */       } else if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(command)) {
/* 110 */         String search = (String)receivedmessagelist.get(1);
/* 111 */         aplicationsentiment(chatid, search);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void aplicationsentiment(String chatid, String search) {
/* 117 */     ComandDTO cdto1 = new ComandDTO();
/*     */     
/* 119 */     cdto1.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 120 */     cdto1.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/* 121 */     cdto1.setCommunicationKey(chatid);
/* 122 */     cdto1.setArguments(new HashMap());
/* 123 */     cdto1.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), search);
/* 124 */     cdto1.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/* 125 */     cdto1.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
/* 126 */     cdto1.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
/* 127 */     Calendar cal = Calendar.getInstance();
/* 128 */     cdto1.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/* 129 */       cal.getTime());
/* 130 */     cal.add(11, -24);
/* 131 */     cdto1.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/* 132 */       cal.getTime());
/*     */     
/* 134 */     pps.proccess(cdto1);
/*     */     
/* 136 */     SendMessage message = new SendMessage()
/* 137 */       .setChatId(chatid)
/* 138 */       .setText("Consulta Lanzada");
/*     */     try {
/* 140 */       sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 142 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   private void stressloadtestlaucher(int threads, String chatid)
/*     */   {
/* 148 */     for (int i = 0; i < threads; i++) {
/* 149 */       ComandDTO cdto = new ComandDTO();
/*     */       
/* 151 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 152 */       cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/* 153 */       cdto.setArguments(new HashMap());
/* 154 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), "@petrogustavo");
/* 155 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/* 156 */       cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
/* 157 */       cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
/* 158 */       Calendar cal = Calendar.getInstance();
/* 159 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/* 160 */         cal.getTime());
/* 161 */       cal.add(5, -8);
/* 162 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/* 163 */         cal.getTime());
/*     */       
/*     */ 
/* 166 */       pps.proccess(cdto);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 171 */     SendMessage message = new SendMessage()
/* 172 */       .setChatId(chatid)
/* 173 */       .setText("La carga ha sido aplicada");
/*     */     try {
/* 175 */       sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 177 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   private static UserRepository ur;
/*     */   private static FacebookService fs;
/*     */   private static ReportDetailManager rdm;
/*     */   private void partialreport(String chatid)
/*     */   {
/* 186 */     ResumeSnapshotProcessingPostDTO rspp = null;
/*     */     try {
/* 188 */       rspp = lsts.getResumeSnapshotProcessingPost();
/*     */     }
/*     */     catch (Exception e) {
/* 191 */       e.printStackTrace();
/*     */     }
/* 193 */     SendMessage message = new SendMessage()
/* 194 */       .setChatId(chatid)
/* 195 */       .setText("Tiempos promedio Total Post Analizados= " + rspp.getTotal() + " Promedio Servicio = " + rspp.getTotalservice() + 
/* 196 */       " Promedio Preprocesamiento = " + rspp.getTotaltimepreprocesing() + " Promedio Total = " + rspp.getTotaltime());
/*     */     try {
/* 198 */       sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 200 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public String getBotUsername()
/*     */   {
/* 206 */     return "xenxeloadstresstestbot";
/*     */   }
/*     */   
/*     */   public String getBotToken()
/*     */   {
/* 211 */     return env.getRequiredProperty("loadstressbot.token");
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\telegram\bot\StressLoadTestBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */