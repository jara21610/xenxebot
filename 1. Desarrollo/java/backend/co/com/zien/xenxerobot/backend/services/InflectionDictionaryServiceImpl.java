/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.dictionary.InflectionDictionary;
/*    */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*    */ import java.io.File;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.core.env.Environment;
/*    */ import org.springframework.stereotype.Service;
/*    */ import org.springframework.transaction.annotation.Transactional;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ @Transactional
/*    */ public class InflectionDictionaryServiceImpl
/*    */   implements InflectionDictionaryService
/*    */ {
/* 19 */   private InflectionDictionary dictionary = null;
/*    */   @Autowired
/*    */   PropertiesRepository pr;
/*    */   
/*    */   public InflectionDictionary getDictionary() {
/* 24 */     if (this.dictionary == null) {
/* 25 */       this.dictionary = new InflectionDictionary();
/* 26 */       this.dictionary.setPlaintxtDictionary(new File(this.env.getRequiredProperty("lemma.path")));
/* 27 */       this.dictionary.loadPlainTextDictionary();
/*    */     }
/* 29 */     return this.dictionary;
/*    */   }
/*    */   
/*    */   @Autowired
/*    */   Environment env;
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\InflectionDictionaryServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */