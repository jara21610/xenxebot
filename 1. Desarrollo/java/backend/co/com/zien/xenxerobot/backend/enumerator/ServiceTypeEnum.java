/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum ServiceTypeEnum
/*    */ {
/*  6 */   PREXTIGE("PreXtige"),  XENXE("Xenxe");
/*    */   
/*    */   private String servicetype;
/*    */   
/*    */   public String getServicetype() {
/* 11 */     return this.servicetype;
/*    */   }
/*    */   
/*    */   private ServiceTypeEnum(String servicetype) {
/* 15 */     this.servicetype = servicetype;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\ServiceTypeEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */