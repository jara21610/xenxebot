/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class ResumeSnapshotProcessingPostDTO
/*    */ {
/*    */   private double totaltime;
/*    */   private double totaltimepreprocesing;
/*    */   private double totalservice;
/*    */   private int total;
/*    */   
/*    */   public double getTotaltime() {
/* 11 */     return this.totaltime;
/*    */   }
/*    */   
/* 14 */   public void setTotaltime(double totaltime) { this.totaltime = totaltime; }
/*    */   
/*    */   public double getTotaltimepreprocesing() {
/* 17 */     return this.totaltimepreprocesing;
/*    */   }
/*    */   
/* 20 */   public void setTotaltimepreprocesing(double totaltimepreprocesing) { this.totaltimepreprocesing = totaltimepreprocesing; }
/*    */   
/*    */   public double getTotalservice() {
/* 23 */     return this.totalservice;
/*    */   }
/*    */   
/* 26 */   public void setTotalservice(double totalservice) { this.totalservice = totalservice; }
/*    */   
/*    */   public int getTotal() {
/* 29 */     return this.total;
/*    */   }
/*    */   
/* 32 */   public void setTotal(int total) { this.total = total; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ResumeSnapshotProcessingPostDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */