/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class Payer
/*    */ {
/*    */   private String merchantPayerId;
/*    */   private String fullName;
/*    */   private String emailAddress;
/*    */   private String contactPhone;
/*    */   private String dniNumber;
/*    */   private BillingAddress billingAddress;
/*    */   
/*    */   public String getMerchantPayerId() {
/* 13 */     return this.merchantPayerId;
/*    */   }
/*    */   
/*    */   public void setMerchantPayerId(String merchantPayerId) {
/* 17 */     this.merchantPayerId = merchantPayerId;
/*    */   }
/*    */   
/*    */   public String getFullName() {
/* 21 */     return this.fullName;
/*    */   }
/*    */   
/*    */   public void setFullName(String fullName) {
/* 25 */     this.fullName = fullName;
/*    */   }
/*    */   
/*    */   public String getEmailAddress() {
/* 29 */     return this.emailAddress;
/*    */   }
/*    */   
/*    */   public void setEmailAddress(String emailAddress) {
/* 33 */     this.emailAddress = emailAddress;
/*    */   }
/*    */   
/*    */   public String getContactPhone() {
/* 37 */     return this.contactPhone;
/*    */   }
/*    */   
/*    */   public void setContactPhone(String contactPhone) {
/* 41 */     this.contactPhone = contactPhone;
/*    */   }
/*    */   
/*    */   public String getDniNumber() {
/* 45 */     return this.dniNumber;
/*    */   }
/*    */   
/*    */   public void setDniNumber(String dniNumber) {
/* 49 */     this.dniNumber = dniNumber;
/*    */   }
/*    */   
/*    */   public BillingAddress getBillingAddress() {
/* 53 */     return this.billingAddress;
/*    */   }
/*    */   
/*    */   public void setBillingAddress(BillingAddress billingAddress) {
/* 57 */     this.billingAddress = billingAddress;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\Payer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */