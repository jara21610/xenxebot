/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class SentimentAnalysisConceptOutputDTO
/*    */ {
/*    */   private String type;
/*    */   private String name;
/*    */   private String sentiment;
/*    */   
/*    */   public String getType() {
/* 10 */     return this.type;
/*    */   }
/*    */   
/* 13 */   public void setType(String type) { this.type = type; }
/*    */   
/*    */   public String getName() {
/* 16 */     return this.name;
/*    */   }
/*    */   
/* 19 */   public void setName(String name) { this.name = name; }
/*    */   
/*    */   public String getSentiment() {
/* 22 */     return this.sentiment;
/*    */   }
/*    */   
/* 25 */   public void setSentiment(String sentiment) { this.sentiment = sentiment; }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SentimentAnalysisConceptOutputDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */