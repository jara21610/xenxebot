package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.CommandProcess;

public abstract interface SourceProcessPostService
{
  public abstract void process(CommandProcess paramCommandProcess, ComandDTO paramComandDTO)
    throws Exception;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SourceProcessPostService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */