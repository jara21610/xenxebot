/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum CategoriesEnum
/*    */ {
/*  6 */   ENTRETENIMIENTO("ENTRETENIMIENTO"),  OTROS("OTROS"),  CINE("CINE"),  FUTBOL("FUTBOL"),  TECNOLOGIA("TECNOLOGIA"), 
/*  7 */   DEPORTES("DEPORTES"),  MUSICA("MUSICA"),  LITERATURA("LITERATURA"),  POLITICA("POLITICA"),  ECONOMIA("ECONOMIA");
/*    */   
/*    */   private String category;
/*    */   
/*    */   public String getCategory() {
/* 12 */     return this.category;
/*    */   }
/*    */   
/*    */   private CategoriesEnum(String category) {
/* 16 */     this.category = category;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\CategoriesEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */