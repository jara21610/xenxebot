/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.config.Config;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.context.ApplicationContext;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.context.i18n.LocaleContextHolder;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.telegram.telegrambots.api.methods.send.SendDocument;
/*     */ import org.telegram.telegrambots.api.methods.send.SendMessage;
/*     */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
/*     */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
/*     */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
/*     */ import org.telegram.telegrambots.bots.TelegramLongPollingBot;
/*     */ import org.telegram.telegrambots.exceptions.TelegramApiException;
/*     */ 
/*     */ @Service("TelegramResponseBot")
/*     */ public class TelegramResponseBotServiceImpl implements ResponseBotService
/*     */ {
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   @Autowired
/*     */   EmailService es;
/*     */   @Autowired
/*     */   UserRepository ur;
/*     */   
/*     */   public void okResponseBot(String reportpath, ComandDTO cdto)
/*     */   {
/*  39 */     SendMessage message = new SendMessage()
/*  40 */       .setChatId(cdto.getCommunicationKey())
/*  41 */       .setReplyMarkup(getMainMenuKeyboard())
/*  42 */       .setText(this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] {
/*  43 */       cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), 
/*  44 */       cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()) }, LocaleContextHolder.getLocale()));
/*     */     
/*  46 */     SendDocument document = new SendDocument().setChatId(cdto.getCommunicationKey()).setNewDocument(new File(reportpath));
/*     */     try {
/*  48 */       TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean(
/*  49 */         (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/*  50 */       xb.sendMessage(message);
/*  51 */       xb.sendDocument(document);
/*     */     } catch (TelegramApiException e1) {
/*  53 */       e1.printStackTrace();
/*     */     }
/*  55 */     Users u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
/*  56 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), u.getEmail(), 
/*  57 */       this.messageSource.getMessage("message.finishsentimentanalysis.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), 
/*  58 */       cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()), 
/*  59 */       this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), 
/*  60 */       cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), new String[] { reportpath });
/*     */   }
/*     */   
/*     */ 
/*     */   public void okPartialReportResponseBot(String reportpath, ComandDTO cdto)
/*     */   {
/*  66 */     SendMessage message = new SendMessage()
/*  67 */       .setChatId(cdto.getCommunicationKey())
/*  68 */       .setText(this.messageSource.getMessage("message.reportdetailsentimentanalysis.text", null, LocaleContextHolder.getLocale()));
/*     */     
/*  70 */     SendDocument document = new SendDocument().setChatId(cdto.getCommunicationKey()).setNewDocument(new File(reportpath));
/*     */     try
/*     */     {
/*  73 */       TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean(
/*  74 */         (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/*  75 */       xb.sendMessage(message);
/*  76 */       xb.sendDocument(document);
/*     */     } catch (TelegramApiException e1) {
/*  78 */       e1.printStackTrace();
/*     */     }
/*     */     
/*  81 */     Users u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
/*  82 */     this.es.sendEmail(this.pr.findByName("mail.smtp.email"), u.getEmail(), 
/*  83 */       this.messageSource.getMessage("message.reportdetailsentimentanalysis.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()), 
/*  84 */       this.messageSource.getMessage("message.reportdetailsentimentanalysis.text", null, LocaleContextHolder.getLocale()), new String[] { reportpath });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void exceptionResponseBot(ComandDTO cdto, Exception e)
/*     */   {
/*  92 */     SendMessage message = new SendMessage()
/*  93 */       .setChatId(cdto.getCommunicationKey())
/*  94 */       .setReplyMarkup(getMainMenuKeyboard())
/*  95 */       .setText(this.messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*     */     try {
/*  97 */       TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean(
/*  98 */         (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/*  99 */       xb.sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 101 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void messageResponseBot(String text, ComandDTO cdto)
/*     */   {
/* 107 */     SendMessage message = new SendMessage()
/* 108 */       .setChatId(cdto.getCommunicationKey())
/* 109 */       .setReplyMarkup(getMainMenuKeyboard())
/* 110 */       .setText(text);
/*     */     try {
/* 112 */       TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean(
/* 113 */         (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/* 114 */       xb.sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 116 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void isprocessingResponseBot(ComandDTO cdto)
/*     */   {
/* 122 */     SendMessage message = new SendMessage()
/* 123 */       .setChatId(cdto.getCommunicationKey())
/* 124 */       .setReplyMarkup(getPushPartialMenuKeyboard())
/* 125 */       .setText(this.messageSource.getMessage("message.isprocesing.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()));
/*     */     try {
/* 127 */       TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean(
/* 128 */         (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/* 129 */       xb.sendMessage(message);
/*     */     } catch (TelegramApiException e1) {
/* 131 */       e1.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private ReplyKeyboard getPushPartialMenuKeyboard()
/*     */   {
/* 138 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/* 139 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/* 140 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/* 141 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*     */     
/* 143 */     List<KeyboardRow> keyboard = new ArrayList();
/* 144 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/* 145 */     keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", null, LocaleContextHolder.getLocale()));
/* 146 */     keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/* 147 */     keyboard.add(keyboardFirstRow);
/* 148 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*     */     
/* 150 */     return replyKeyboardMarkup;
/*     */   }
/*     */   
/*     */   private ReplyKeyboardMarkup getMainMenuKeyboard() {
/* 154 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/* 155 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/* 156 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/* 157 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*     */     
/* 159 */     List<KeyboardRow> keyboard = new ArrayList();
/* 160 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/* 161 */     keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.sentiment.text", null, LocaleContextHolder.getLocale()));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 166 */     keyboard.add(keyboardFirstRow);
/*     */     
/*     */ 
/* 169 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*     */     
/* 171 */     return replyKeyboardMarkup;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TelegramResponseBotServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */