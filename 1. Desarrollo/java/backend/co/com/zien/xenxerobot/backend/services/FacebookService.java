package co.com.zien.xenxerobot.backend.services;

import com.restfb.types.Comment;
import com.restfb.types.Post;
import java.util.List;

public abstract interface FacebookService
{
  public abstract List<Comment> searchComments(String paramString1, String paramString2, String paramString3)
    throws Exception;
  
  public abstract List<Post> searchlastPosts(String paramString, int paramInt)
    throws Exception;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\FacebookService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */