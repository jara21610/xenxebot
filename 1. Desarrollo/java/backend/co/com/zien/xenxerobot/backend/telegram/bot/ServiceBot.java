package co.com.zien.xenxerobot.backend.telegram.bot;

import co.com.zien.xenxerobot.persistence.entity.Users;

public abstract interface ServiceBot
{
  public abstract void handleUserTokenException(String paramString);
  
  public abstract void handleUpgradePlanOK(String paramString);
  
  public abstract void handlePaymentExceptionUpgradePlan(String paramString, Users paramUsers);
  
  public abstract void handleFreePlanFinished(String paramString, Users paramUsers);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\telegram\bot\ServiceBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */