/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum TwitterSearchTypeEnum
/*    */ {
/*  6 */   HASHTAG("HASHTAG"),  USUARIO("USUARIO");
/*    */   
/*    */   private String twittersearchtype;
/*    */   
/*    */   public String getTwittersearchtype() {
/* 11 */     return this.twittersearchtype;
/*    */   }
/*    */   
/*    */   private TwitterSearchTypeEnum(String twittersearchtype) {
/* 15 */     this.twittersearchtype = twittersearchtype;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\TwitterSearchTypeEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */