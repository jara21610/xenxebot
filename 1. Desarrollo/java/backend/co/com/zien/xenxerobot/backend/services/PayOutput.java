/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class PayOutput
/*    */ {
/*    */   private String code;
/*    */   private Object error;
/*    */   private TransactionResponse transactionResponse;
/* 11 */   private Map<String, Object> additionalProperties = new HashMap();
/*    */   
/*    */   public String getCode() {
/* 14 */     return this.code;
/*    */   }
/*    */   
/*    */   public void setCode(String code) {
/* 18 */     this.code = code;
/*    */   }
/*    */   
/*    */   public Object getError() {
/* 22 */     return this.error;
/*    */   }
/*    */   
/*    */   public void setError(Object error) {
/* 26 */     this.error = error;
/*    */   }
/*    */   
/*    */   public TransactionResponse getTransactionResponse() {
/* 30 */     return this.transactionResponse;
/*    */   }
/*    */   
/*    */   public void setTransactionResponse(TransactionResponse transactionResponse) {
/* 34 */     this.transactionResponse = transactionResponse;
/*    */   }
/*    */   
/*    */   public Map<String, Object> getAdditionalProperties() {
/* 38 */     return this.additionalProperties;
/*    */   }
/*    */   
/*    */   public void setAdditionalProperty(String name, Object value) {
/* 42 */     this.additionalProperties.put(name, value);
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PayOutput.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */