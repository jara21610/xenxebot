/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandProcessStateEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import com.google.common.collect.Lists;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.concurrent.CompletableFuture;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.beans.factory.annotation.Qualifier;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.social.twitter.api.SearchOperations;
/*     */ import org.springframework.social.twitter.api.SearchParameters;
/*     */ import org.springframework.social.twitter.api.SearchResults;
/*     */ import org.springframework.social.twitter.api.Tweet;
/*     */ import org.springframework.social.twitter.api.Twitter;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service("TwitterProcessPost")
/*     */ public class TwitterProcessPostServiceImpl
/*     */   implements SourceProcessPostService
/*     */ {
/*  48 */   private static final Logger logger = Logger.getLogger(TwitterProcessPostServiceImpl.class);
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   PreproccesingService scs;
/*     */   @Autowired
/*     */   SentimentService ss;
/*     */   @Autowired
/*     */   @Qualifier("TwitterPoolConectionManager")
/*     */   SocialPoolConectionManager tpcm;
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   
/*     */   public void process(CommandProcess cp, ComandDTO cdto)
/*     */     throws Exception
/*     */   {
/*  64 */     String searchCriteria = (String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
/*  65 */     List<Tweet> currentlt = new ArrayList();
/*     */     
/*  67 */     int pagesize = new Integer(this.pr.findByName(PropertyEnum.TWITTERPAGESIZE.getProperty())).intValue();
/*     */     
/*  69 */     Twitter twitter = (Twitter)getAvailableConecction();
/*     */     
/*  71 */     boolean twitteroverload = true;
/*     */     
/*  73 */     SearchResults sr = null;
/*     */     
/*  75 */     while (twitteroverload) {
/*     */       try {
/*  77 */         sr = twitter.searchOperations().search(new SearchParameters(searchCriteria).count(pagesize).lang("es"));
/*     */       } catch (Exception e) {
/*  79 */         twitteroverload = true;
/*     */       }
/*  81 */       twitteroverload = false;
/*     */     }
/*     */     
/*  84 */     if (sr.getTweets().size() > 0) {
/*  85 */       CompletableFuture<String> page = this.tpt.executeUnit(sr.getTweets(), cp, cdto);
/*  86 */       page.join();
/*  87 */       if (!"stop".equals(page.get())) {}
/*     */     }
/*     */     else
/*     */     {
/*     */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     for (;;)
/*     */     {
/* 101 */       cp = (CommandProcess)this.cpr.findOne(cp.getId());
/* 102 */       if ((cp.getState().equals(CommandProcessStateEnum.CANCELADOXUSUARIO.getState())) || (cp.getState().equals(CommandProcessStateEnum.ERROR.getState()))) return;
/* 103 */       twitter = (Twitter)getAvailableConecction();
/* 104 */       twitteroverload = true;
/* 105 */       while (twitteroverload) {
/*     */         try {
/* 107 */           sr = twitter.searchOperations().search(new SearchParameters(searchCriteria).includeEntities(true).count(pagesize).lang("es")
/* 108 */             .maxId(((Tweet)sr.getTweets().get(sr.getTweets().size() - 1)).getId() - 1L));
/*     */         } catch (Exception e) {
/* 110 */           twitteroverload = true;
/*     */         }
/* 112 */         twitteroverload = false;
/*     */       }
/*     */       
/* 115 */       currentlt = sr.getTweets();
/*     */       
/* 117 */       if (currentlt.size() <= 0)
/*     */         break;
/* 119 */       List<List<Tweet>> smallerLists = Lists.partition(currentlt, 25);
/*     */       
/*     */ 
/*     */ 
/* 123 */       CompletableFuture[] fs = 
/* 124 */         new CompletableFuture[smallerLists.size()];
/*     */       
/* 126 */       int i = 0;
/* 127 */       for (List<Tweet> lt : smallerLists) {
/* 128 */         page = this.tpt.executeUnit(lt, cp, cdto);
/* 129 */         fs[i] = page;
/* 130 */         i++;
/*     */       }
/*     */       
/* 133 */       CompletableFuture.allOf(fs);
/*     */       CompletableFuture[] arrayOfCompletableFuture1;
/* 135 */       CompletableFuture<String> page = (arrayOfCompletableFuture1 = fs).length; for (CompletableFuture<String> localCompletableFuture1 = 0; localCompletableFuture1 < page; localCompletableFuture1++) { CompletableFuture<String> page = arrayOfCompletableFuture1[localCompletableFuture1];
/* 136 */         if ("stop".equals(page.get())) {
/* 137 */           return;
/*     */         }
/*     */       }
/*     */     }
/* 141 */     return; }
/*     */   
/*     */   @Autowired
/*     */   @Qualifier("TwitterProcessUnit")
/*     */   SourceProcessUnit tpt;
/*     */   @Autowired
/*     */   @Qualifier("EmailResponseBot")
/*     */   ResponseBotService erbs;
/*     */   
/* 150 */   private Object getAvailableConecction() throws Exception { boolean success = false;
/* 151 */     while (!success) {
/*     */       try {
/* 153 */         success = true;
/* 154 */         return (Twitter)this.tpcm.getAvailableConecction(new String[0]);
/*     */       } catch (SocialPoolConectionManagerNotFoundAvailableException e) {
/* 156 */         success = false;
/* 157 */         Thread.currentThread();
/* 158 */         Thread.sleep(this.tpcm.getMinimalSecondResetConection());
/*     */       }
/*     */     }
/* 161 */     return null;
/*     */   }
/*     */   
/*     */   @Autowired
/*     */   @Qualifier("TelegramResponseBot")
/*     */   ResponseBotService trbs;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   @Autowired
/*     */   CommandProcessRepository cpr;
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TwitterProcessPostServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */