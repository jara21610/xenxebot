package co.com.zien.xenxerobot.backend.services.dataset;

import co.com.zien.xenxerobot.persistence.entity.Worker;
import java.util.List;

public abstract interface WorkerService
{
  public abstract List<Worker> getavailableWorkers();
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\dataset\WorkerService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */