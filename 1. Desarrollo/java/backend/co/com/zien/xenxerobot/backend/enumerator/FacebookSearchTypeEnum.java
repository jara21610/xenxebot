/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum FacebookSearchTypeEnum
/*    */ {
/*  6 */   PAGE("PAGE"),  POST("POST");
/*    */   
/*    */   private String facebooksearchtype;
/*    */   
/*    */   public String getFacebooksearchtype() {
/* 11 */     return this.facebooksearchtype;
/*    */   }
/*    */   
/*    */   private FacebookSearchTypeEnum(String facebooksearchtype) {
/* 15 */     this.facebooksearchtype = facebooksearchtype;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\FacebookSearchTypeEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */