/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlRootElement;
/*    */ 
/*    */ @XmlRootElement
/*    */ public class SentimentAnalysisInputDTO
/*    */ {
/*    */   private String tweet;
/*    */   private String servicetype;
/*    */   
/*    */   public String getTweet() {
/* 12 */     return this.tweet;
/*    */   }
/*    */   
/*    */   public void setTweet(String tweet) {
/* 16 */     this.tweet = tweet;
/*    */   }
/*    */   
/*    */   public String getServicetype() {
/* 20 */     return this.servicetype;
/*    */   }
/*    */   
/*    */   public void setServicetype(String servicetype) {
/* 24 */     this.servicetype = servicetype;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SentimentAnalysisInputDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */