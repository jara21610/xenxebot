package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.CommandProcess;

public abstract interface ReportDetailManager
{
  public abstract void saveStatus(CommandProcess paramCommandProcess, ReportDetailDTO paramReportDetailDTO);
  
  public abstract String generateSentimentAnalysisReport(String paramString)
    throws Exception;
  
  public abstract void reportdetailgenerateSentimentAnalysisReport(String paramString)
    throws Exception;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ReportDetailManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */