/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import java.math.BigInteger;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ 
/*     */ public class TransactionResponse
/*     */ {
/*     */   private int orderId;
/*     */   private String transactionId;
/*     */   private String state;
/*     */   private String paymentNetworkResponseCode;
/*     */   private Object paymentNetworkResponseErrorMessage;
/*     */   private String trazabilityCode;
/*     */   private String authorizationCode;
/*     */   private Object pendingReason;
/*     */   private String responseCode;
/*     */   private Object errorCode;
/*     */   private Object responseMessage;
/*     */   private String transactionDate;
/*     */   private String transactionTime;
/*     */   private BigInteger operationDate;
/*     */   private Object referenceQuestionnaire;
/*     */   private Object extraParameters;
/*     */   private AdditionalInfo additionalInfo;
/*  26 */   private Map<String, Object> additionalProperties = new HashMap();
/*     */   
/*     */   public int getOrderId() {
/*  29 */     return this.orderId;
/*     */   }
/*     */   
/*     */   public void setOrderId(int orderId) {
/*  33 */     this.orderId = orderId;
/*     */   }
/*     */   
/*     */   public String getTransactionId() {
/*  37 */     return this.transactionId;
/*     */   }
/*     */   
/*     */   public void setTransactionId(String transactionId) {
/*  41 */     this.transactionId = transactionId;
/*     */   }
/*     */   
/*     */   public String getState() {
/*  45 */     return this.state;
/*     */   }
/*     */   
/*     */   public void setState(String state) {
/*  49 */     this.state = state;
/*     */   }
/*     */   
/*     */   public String getPaymentNetworkResponseCode() {
/*  53 */     return this.paymentNetworkResponseCode;
/*     */   }
/*     */   
/*     */   public void setPaymentNetworkResponseCode(String paymentNetworkResponseCode) {
/*  57 */     this.paymentNetworkResponseCode = paymentNetworkResponseCode;
/*     */   }
/*     */   
/*     */   public Object getPaymentNetworkResponseErrorMessage() {
/*  61 */     return this.paymentNetworkResponseErrorMessage;
/*     */   }
/*     */   
/*     */   public void setPaymentNetworkResponseErrorMessage(Object paymentNetworkResponseErrorMessage) {
/*  65 */     this.paymentNetworkResponseErrorMessage = paymentNetworkResponseErrorMessage;
/*     */   }
/*     */   
/*     */   public String getTrazabilityCode() {
/*  69 */     return this.trazabilityCode;
/*     */   }
/*     */   
/*     */   public void setTrazabilityCode(String trazabilityCode) {
/*  73 */     this.trazabilityCode = trazabilityCode;
/*     */   }
/*     */   
/*     */   public String getAuthorizationCode() {
/*  77 */     return this.authorizationCode;
/*     */   }
/*     */   
/*     */   public void setAuthorizationCode(String authorizationCode) {
/*  81 */     this.authorizationCode = authorizationCode;
/*     */   }
/*     */   
/*     */   public Object getPendingReason() {
/*  85 */     return this.pendingReason;
/*     */   }
/*     */   
/*     */   public void setPendingReason(Object pendingReason) {
/*  89 */     this.pendingReason = pendingReason;
/*     */   }
/*     */   
/*     */   public String getResponseCode() {
/*  93 */     return this.responseCode;
/*     */   }
/*     */   
/*     */   public void setResponseCode(String responseCode) {
/*  97 */     this.responseCode = responseCode;
/*     */   }
/*     */   
/*     */   public Object getErrorCode() {
/* 101 */     return this.errorCode;
/*     */   }
/*     */   
/*     */   public void setErrorCode(Object errorCode) {
/* 105 */     this.errorCode = errorCode;
/*     */   }
/*     */   
/*     */   public Object getResponseMessage() {
/* 109 */     return this.responseMessage;
/*     */   }
/*     */   
/*     */   public void setResponseMessage(Object responseMessage) {
/* 113 */     this.responseMessage = responseMessage;
/*     */   }
/*     */   
/*     */   public String getTransactionDate() {
/* 117 */     return this.transactionDate;
/*     */   }
/*     */   
/*     */   public void setTransactionDate(String transactionDate) {
/* 121 */     this.transactionDate = transactionDate;
/*     */   }
/*     */   
/*     */   public String getTransactionTime() {
/* 125 */     return this.transactionTime;
/*     */   }
/*     */   
/*     */   public void setTransactionTime(String transactionTime) {
/* 129 */     this.transactionTime = transactionTime;
/*     */   }
/*     */   
/*     */   public BigInteger getOperationDate() {
/* 133 */     return this.operationDate;
/*     */   }
/*     */   
/*     */   public void setOperationDate(BigInteger operationDate) {
/* 137 */     this.operationDate = operationDate;
/*     */   }
/*     */   
/*     */   public Object getReferenceQuestionnaire() {
/* 141 */     return this.referenceQuestionnaire;
/*     */   }
/*     */   
/*     */   public void setReferenceQuestionnaire(Object referenceQuestionnaire) {
/* 145 */     this.referenceQuestionnaire = referenceQuestionnaire;
/*     */   }
/*     */   
/*     */   public Object getExtraParameters() {
/* 149 */     return this.extraParameters;
/*     */   }
/*     */   
/*     */   public void setExtraParameters(Object extraParameters) {
/* 153 */     this.extraParameters = extraParameters;
/*     */   }
/*     */   
/*     */   public AdditionalInfo getAdditionalInfo() {
/* 157 */     return this.additionalInfo;
/*     */   }
/*     */   
/*     */   public void setAdditionalInfo(AdditionalInfo additionalInfo) {
/* 161 */     this.additionalInfo = additionalInfo;
/*     */   }
/*     */   
/*     */   public Map<String, Object> getAdditionalProperties() {
/* 165 */     return this.additionalProperties;
/*     */   }
/*     */   
/*     */   public void setAdditionalProperty(String name, Object value) {
/* 169 */     this.additionalProperties.put(name, value);
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TransactionResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */