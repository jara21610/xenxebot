/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class AdditionalInfo
/*    */ {
/*    */   private String paymentNetwork;
/*    */   private String rejectionType;
/*    */   private Object responseNetworkMessage;
/*    */   private Object travelAgencyAuthorizationCode;
/*    */   private Object cardType;
/* 13 */   private Map<String, Object> additionalProperties = new HashMap();
/*    */   
/*    */   public String getPaymentNetwork() {
/* 16 */     return this.paymentNetwork;
/*    */   }
/*    */   
/*    */   public void setPaymentNetwork(String paymentNetwork) {
/* 20 */     this.paymentNetwork = paymentNetwork;
/*    */   }
/*    */   
/*    */   public String getRejectionType() {
/* 24 */     return this.rejectionType;
/*    */   }
/*    */   
/*    */   public void setRejectionType(String rejectionType) {
/* 28 */     this.rejectionType = rejectionType;
/*    */   }
/*    */   
/*    */   public Object getResponseNetworkMessage() {
/* 32 */     return this.responseNetworkMessage;
/*    */   }
/*    */   
/*    */   public void setResponseNetworkMessage(Object responseNetworkMessage) {
/* 36 */     this.responseNetworkMessage = responseNetworkMessage;
/*    */   }
/*    */   
/*    */   public Object getTravelAgencyAuthorizationCode() {
/* 40 */     return this.travelAgencyAuthorizationCode;
/*    */   }
/*    */   
/*    */   public void setTravelAgencyAuthorizationCode(Object travelAgencyAuthorizationCode) {
/* 44 */     this.travelAgencyAuthorizationCode = travelAgencyAuthorizationCode;
/*    */   }
/*    */   
/*    */   public Object getCardType() {
/* 48 */     return this.cardType;
/*    */   }
/*    */   
/*    */   public void setCardType(Object cardType) {
/* 52 */     this.cardType = cardType;
/*    */   }
/*    */   
/*    */   public Map<String, Object> getAdditionalProperties() {
/* 56 */     return this.additionalProperties;
/*    */   }
/*    */   
/*    */   public void setAdditionalProperty(String name, Object value) {
/* 60 */     this.additionalProperties.put(name, value);
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\AdditionalInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */