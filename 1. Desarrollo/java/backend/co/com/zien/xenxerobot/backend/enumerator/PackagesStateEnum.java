/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum PackagesStateEnum
/*    */ {
/*  6 */   AVAILABLE("AVAILABLE"),  PROCCESS("PROCCESS"),  FINISHED("FINISHED");
/*    */   
/*    */   private String state;
/*    */   
/*    */   public String getState() {
/* 11 */     return this.state;
/*    */   }
/*    */   
/*    */   private PackagesStateEnum(String state) {
/* 15 */     this.state = state;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\PackagesStateEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */