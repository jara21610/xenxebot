/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.jit.backend.util.Utils;
/*     */ import co.com.zien.xenxerobot.backend.dictionary.InflectionDictionary;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.MasterListEnum;
/*     */ import co.com.zien.xenxerobot.persistence.repository.MastervalueRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import java.util.StringTokenizer;
/*     */ import org.apache.commons.lang.NumberUtils;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.springframework.transaction.annotation.Transactional;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service
/*     */ @Transactional
/*     */ public class PreproccesingServiceImpl
/*     */   implements PreproccesingService
/*     */ {
/*  24 */   private static final Logger logger = Logger.getLogger(PreproccesingServiceImpl.class);
/*     */   
/*     */   @Autowired
/*     */   InflectionDictionaryService ids;
/*     */   @Autowired
/*     */   DictionaryService ds;
/*     */   
/*     */   public String preproccesingText(String tocheck)
/*     */   {
/*  33 */     String spellcheckedtext = "";
/*     */     try {
/*  35 */       if (tocheck.contains("on Twitter: ")) {
/*  36 */         tocheck = tocheck.split("on Twitter: ")[1];
/*     */       }
/*     */       
/*     */ 
/*  40 */       tocheck = tocheck.toLowerCase();
/*     */       
/*  42 */       tocheck = Utils.deleteaccentmarkdieresis(tocheck);
/*     */       
/*  44 */       tocheck = Utils.cleanString(this.mvr.findBymastertypename(
/*  45 */         MasterListEnum.REGEXTOCLEANPUNTUATIONMARKS.getMasterlist()), tocheck);
/*     */       
/*     */ 
/*  48 */       tocheck = Utils.replacePunctuationMarks(tocheck);
/*  49 */       tocheck = Utils.cleanRepeatedChars(tocheck);
/*     */       
/*     */ 
/*  52 */       boolean begintocheck = true;
/*     */       
/*  54 */       StringTokenizer st = new StringTokenizer(tocheck);
/*  55 */       while (st.hasMoreElements())
/*     */       {
/*  57 */         String token = st.nextToken();
/*     */         
/*     */ 
/*  60 */         if (token.length() > 1) {
/*  61 */           if (!NumberUtils.isNumber(token)) {
/*  62 */             if ((!Utils.isHashtag(token)) && (!Utils.isUserMention(token))) {
/*  63 */               begintocheck = false;
/*     */               
/*     */ 
/*     */ 
/*     */ 
/*  68 */               String lemma = this.ids.getDictionary().getLemma(token);
/*  69 */               if (lemma != null)
/*     */               {
/*  71 */                 spellcheckedtext = spellcheckedtext + " " + lemma;
/*     */               }
/*     */               else {
/*  74 */                 spellcheckedtext = spellcheckedtext + " " + token;
/*     */               }
/*     */               
/*     */ 
/*     */             }
/*  79 */             else if (Utils.isUserMention(token)) {
/*  80 */               if (begintocheck) {
/*  81 */                 spellcheckedtext = spellcheckedtext;
/*     */               } else {
/*  83 */                 spellcheckedtext = spellcheckedtext + " " + token;
/*     */               }
/*     */             }
/*     */             else {
/*  87 */               spellcheckedtext = spellcheckedtext;
/*     */             }
/*     */           } else {
/*  90 */             spellcheckedtext = spellcheckedtext + " " + token;
/*     */           }
/*     */         } else {
/*  93 */           spellcheckedtext = spellcheckedtext;
/*     */         }
/*     */       }
/*  96 */       spellcheckedtext = Utils.getSentimentWordFromEmoticon(spellcheckedtext);
/*     */     } catch (Exception e) {
/*  98 */       logger.error(e);
/*     */     }
/*     */     
/* 101 */     return spellcheckedtext;
/*     */   }
/*     */   
/*     */   @Autowired
/*     */   MastervalueRepository mvr;
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PreproccesingServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */