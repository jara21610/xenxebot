/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.CommandProcessStateEnum;
/*     */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*     */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*     */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*     */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*     */ import com.thoughtworks.xstream.XStream;
/*     */ import java.util.Date;
/*     */ import java.util.Map;
/*     */ import java.util.UUID;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.beans.factory.annotation.Qualifier;
/*     */ import org.springframework.context.MessageSource;
/*     */ import org.springframework.context.i18n.LocaleContextHolder;
/*     */ import org.springframework.scheduling.annotation.Async;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service
/*     */ public class PostProcessingServiceImpl
/*     */   implements PostProcessingService
/*     */ {
/*  39 */   private static final Logger logger = Logger.getLogger(PostProcessingServiceImpl.class);
/*     */   @Autowired
/*     */   @Qualifier("TwitterProcessPost")
/*     */   SourceProcessPostService ts;
/*     */   @Autowired
/*     */   @Qualifier("FacebookProcessPost")
/*     */   SourceProcessPostService fs;
/*     */   @Autowired
/*     */   PostRepository postr;
/*     */   @Autowired
/*     */   PreproccesingService scs;
/*     */   @Autowired
/*     */   SentimentService ss;
/*     */   
/*     */   @Async
/*     */   public void proccess(ComandDTO cdto)
/*     */   {
/*  56 */     CommandProcess cp = new CommandProcess();
/*  57 */     String channel = "";
/*  58 */     Users u = null;
/*  59 */     ResponseBotService rbs = this.rbsf.getResponseBotService(channel);
/*     */     try {
/*  61 */       channel = cdto.getChannel();
/*     */       
/*  63 */       String socialmediacource = (String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument());
/*     */       
/*  65 */       cp.setBegindate(new Date());
/*  66 */       cp.setId(UUID.randomUUID().toString());
/*  67 */       cp.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  68 */       String command = cdto.getCommand();
/*  69 */       cp.setComand(command);
/*     */       
/*  71 */       if (channel.equals(ChannelEnum.TELEGRAM.getChannel())) {
/*  72 */         u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
/*  73 */       } else if (channel.equals(ChannelEnum.EMAIL.getChannel())) {
/*  74 */         u = this.ur.isUserActivebyEmail(cdto.getCommunicationKey());
/*     */       }
/*     */       
/*  77 */       XStream xs = new XStream();
/*  78 */       String comanddetailarguments = xs.toXML(cdto);
/*     */       
/*  80 */       cp.setArguments(comanddetailarguments);
/*  81 */       cp.setState(CommandProcessStateEnum.ENPROCESO.getState());
/*  82 */       cp.setUser(u);
/*  83 */       this.cpr.saveAndFlush(cp);
/*     */       
/*  85 */       rbs.isprocessingResponseBot(cdto);
/*     */       
/*     */ 
/*  88 */       if (socialmediacource.equals(SocialMediaSourceEnum.TWITTER.getSocialmediasource())) {
/*  89 */         this.ts.process(cp, cdto);
/*     */       } else {
/*  91 */         this.fs.process(cp, cdto);
/*     */       }
/*     */       
/*  94 */       int total = this.postr.totalPost(cp.getId()).intValue();
/*  95 */       if (total > 0) {
/*  96 */         cp = (CommandProcess)this.cpr.findOne(cp.getId());
/*  97 */         if (!cp.getState().equals(CommandProcessStateEnum.CANCELADOXUSUARIO.getState())) {
/*  98 */           ReportDetailManager rdm = this.rdmf.getResponseBotService((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
/*  99 */           String reportpath = rdm.generateSentimentAnalysisReport(cp.getId());
/* 100 */           cp.setState(CommandProcessStateEnum.FINALIZADO.getState());
/* 101 */           cp.setFinishdate(new Date());
/* 102 */           this.cpr.saveAndFlush(cp);
/* 103 */           rbs.okResponseBot(reportpath, cdto);
/*     */         }
/*     */       }
/*     */       else {
/* 107 */         cp.setState(CommandProcessStateEnum.FINALIZADO.getState());
/* 108 */         cp.setFinishdate(new Date());
/* 109 */         this.cpr.saveAndFlush(cp);
/* 110 */         rbs.messageResponseBot(
/* 111 */           this.messageSource.getMessage("message.postnotfound.text", null, LocaleContextHolder.getLocale()), cdto);
/*     */       }
/*     */     } catch (Exception e) {
/* 114 */       logger.error(e);
/* 115 */       cp.setFinishdate(new Date());
/* 116 */       cp.setState(CommandProcessStateEnum.ERROR.getState());
/* 117 */       cp.setException(e.getMessage());
/* 118 */       this.cpr.saveAndFlush(cp);
/*     */       
/* 120 */       rbs.exceptionResponseBot(cdto, e); } }
/*     */   
/*     */   @Autowired
/*     */   CommandProcessRepository cpr;
/*     */   
/*     */   @Async
/* 126 */   public void cancel(String cpId) { CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
/* 127 */     cp.setFinishdate(new Date());
/* 128 */     cp.setState(CommandProcessStateEnum.CANCELADOXUSUARIO.getState());
/* 129 */     this.cpr.saveAndFlush(cp);
/*     */   }
/*     */   
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   @Autowired
/*     */   ReportDetailServiceFactory rdmf;
/*     */   @Autowired
/*     */   MessageSource messageSource;
/*     */   @Autowired
/*     */   UserRepository ur;
/*     */   @Autowired
/*     */   ResponseBotServiceFactory rbsf;
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PostProcessingServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */