package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.backend.dictionary.Dictionary;

public abstract interface DictionaryService
{
  public abstract Dictionary getDictionary();
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\DictionaryService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */