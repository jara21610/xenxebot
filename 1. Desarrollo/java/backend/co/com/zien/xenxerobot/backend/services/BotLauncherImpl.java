/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.scheduling.annotation.Async;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ @Service
/*    */ public class BotLauncherImpl implements BotLauncher
/*    */ {
/*    */   @Autowired
/*    */   EmailBot eb;
/*    */   
/*    */   @Async
/*    */   public void launchBots()
/*    */   {
/* 16 */     this.eb.checkNewCommands();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\BotLauncherImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */