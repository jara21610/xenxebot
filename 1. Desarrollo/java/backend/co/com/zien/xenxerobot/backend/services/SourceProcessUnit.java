package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public abstract interface SourceProcessUnit
{
  public abstract CompletableFuture<String> executeUnit(List paramList, CommandProcess paramCommandProcess, ComandDTO paramComandDTO);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\SourceProcessUnit.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */