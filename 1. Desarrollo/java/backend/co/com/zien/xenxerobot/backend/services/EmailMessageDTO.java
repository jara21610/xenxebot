/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class EmailMessageDTO
/*    */ {
/*    */   private String from;
/*    */   private String text;
/*    */   
/*    */   public String getFrom() {
/*  9 */     return this.from;
/*    */   }
/*    */   
/*    */   public void setFrom(String from) {
/* 13 */     this.from = from;
/*    */   }
/*    */   
/*    */   public String getText() {
/* 17 */     return this.text;
/*    */   }
/*    */   
/*    */   public void setText(String text)
/*    */   {
/* 22 */     this.text = text;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\EmailMessageDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */