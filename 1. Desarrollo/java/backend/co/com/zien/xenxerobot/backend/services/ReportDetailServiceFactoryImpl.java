/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*    */ 
/*    */ @org.springframework.stereotype.Service
/*    */ public class ReportDetailServiceFactoryImpl implements ReportDetailServiceFactory
/*    */ {
/*    */   @org.springframework.beans.factory.annotation.Autowired
/*    */   @org.springframework.beans.factory.annotation.Qualifier("XenxeReportDetailManager")
/*    */   ReportDetailManager rdmx;
/*    */   @org.springframework.beans.factory.annotation.Autowired
/*    */   @org.springframework.beans.factory.annotation.Qualifier("PreXtigeReportDetailManager")
/*    */   ReportDetailManager rdmp;
/*    */   
/*    */   public ReportDetailManager getResponseBotService(String servicetype)
/*    */   {
/* 17 */     if (servicetype.equals(ServiceTypeEnum.XENXE.getServicetype()))
/* 18 */       return this.rdmx;
/* 19 */     if (servicetype.equals(ServiceTypeEnum.PREXTIGE.getServicetype())) {
/* 20 */       return this.rdmp;
/*    */     }
/* 22 */     return this.rdmp;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\ReportDetailServiceFactoryImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */