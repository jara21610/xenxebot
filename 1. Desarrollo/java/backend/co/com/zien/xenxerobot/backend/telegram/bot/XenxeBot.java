/*      */ package co.com.zien.xenxerobot.backend.telegram.bot;
/*      */ 
/*      */ import co.com.zien.xenxerobot.backend.config.Config;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.ChannelEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandArgumentsEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.CommandProcessStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.FacebookSearchTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PlanStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PlanTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.PropertyEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.ServiceTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.SocialMediaSourceEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.TelegramStateEnum;
/*      */ import co.com.zien.xenxerobot.backend.enumerator.TwitterSearchTypeEnum;
/*      */ import co.com.zien.xenxerobot.backend.services.ComandDTO;
/*      */ import co.com.zien.xenxerobot.backend.services.FacebookService;
/*      */ import co.com.zien.xenxerobot.backend.services.PlanManagementService;
/*      */ import co.com.zien.xenxerobot.backend.services.PlanService;
/*      */ import co.com.zien.xenxerobot.backend.services.PostProcessingService;
/*      */ import co.com.zien.xenxerobot.backend.services.ReportDetailManager;
/*      */ import co.com.zien.xenxerobot.backend.services.ReportDetailServiceFactory;
/*      */ import co.com.zien.xenxerobot.backend.services.TokenService;
/*      */ import co.com.zien.xenxerobot.persistence.entity.CommandProcess;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Plan;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Userplan;
/*      */ import co.com.zien.xenxerobot.persistence.entity.Users;
/*      */ import co.com.zien.xenxerobot.persistence.repository.CommandProcessRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.PostRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*      */ import co.com.zien.xenxerobot.persistence.repository.UserRepository;
/*      */ import com.restfb.types.Post;
/*      */ import com.thoughtworks.xstream.XStream;
/*      */ import java.text.DateFormat;
/*      */ import java.text.ParseException;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Arrays;
/*      */ import java.util.Calendar;
/*      */ import java.util.Date;
/*      */ import java.util.HashMap;
/*      */ import java.util.List;
/*      */ import java.util.Map;
/*      */ import java.util.UUID;
/*      */ import org.springframework.context.ApplicationContext;
/*      */ import org.springframework.context.MessageSource;
/*      */ import org.springframework.context.i18n.LocaleContextHolder;
/*      */ import org.springframework.core.env.Environment;
/*      */ import org.telegram.telegrambots.api.methods.send.SendMessage;
/*      */ import org.telegram.telegrambots.api.objects.CallbackQuery;
/*      */ import org.telegram.telegrambots.api.objects.Contact;
/*      */ import org.telegram.telegrambots.api.objects.Message;
/*      */ import org.telegram.telegrambots.api.objects.Update;
/*      */ import org.telegram.telegrambots.api.objects.User;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
/*      */ import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
/*      */ import org.telegram.telegrambots.bots.TelegramLongPollingBot;
/*      */ import org.telegram.telegrambots.exceptions.TelegramApiException;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class XenxeBot
/*      */   extends TelegramLongPollingBot
/*      */   implements ServiceBot
/*      */ {
/*   81 */   private static Map<String, ComandDTO> usercommandstate = null;
/*      */   
/*      */   static
/*      */   {
/*   85 */     pr = (PropertiesRepository)Config.getContext().getBean(PropertiesRepository.class);
/*   86 */     messageSource = (MessageSource)Config.getContext().getBean(MessageSource.class);
/*   87 */     pps = (PostProcessingService)Config.getContext().getBean(PostProcessingService.class);
/*   88 */     ur = (UserRepository)Config.getContext().getBean(UserRepository.class);
/*   89 */     fs = (FacebookService)Config.getContext().getBean(FacebookService.class); }
/*   90 */   private static CommandProcessRepository cpr = (CommandProcessRepository)Config.getContext().getBean(CommandProcessRepository.class);
/*   91 */   private static PostRepository postr = (PostRepository)Config.getContext().getBean(PostRepository.class);
/*   92 */   private static ReportDetailServiceFactory rdmf = (ReportDetailServiceFactory)Config.getContext().getBean(ReportDetailServiceFactory.class);
/*   93 */   private static Environment env = (Environment)Config.getContext().getBean(Environment.class);
/*   94 */   private static PlanService ps = (PlanService)Config.getContext().getBean(PlanService.class);
/*   95 */   private static TokenService ts = (TokenService)Config.getContext().getBean(TokenService.class);
/*   96 */   private static PlanManagementService pm = (PlanManagementService)Config.getContext().getBean(PlanManagementService.class);
/*      */   private static PropertiesRepository pr;
/*      */   private static MessageSource messageSource;
/*      */   private static PostProcessingService pps;
/*      */   
/*      */   public void onUpdateReceived(Update update)
/*      */   {
/*  103 */     if (usercommandstate == null) {
/*  104 */       usercommandstate = new HashMap();
/*      */     }
/*      */     
/*  107 */     if ((update.hasMessage()) && (update.getMessage().hasText())) {
/*  108 */       String chatid = update.getMessage().getChatId().toString();
/*      */       
/*  110 */       Users u = ur.isUserActivebyChatid(chatid);
/*      */       
/*  112 */       if (chatid != null)
/*      */       {
/*  114 */         if (u != null)
/*      */         {
/*      */ 
/*  117 */           Userplan up = ps.getLastUserPlan(u.getId());
/*      */           
/*  119 */           if (up != null)
/*      */           {
/*  121 */             if (up.getState().equals(PlanStateEnum.ACTIVO.getState())) {
/*  122 */               String receivedmessage = update.getMessage().getText();
/*  123 */               List<String> receivedmessagelist = new ArrayList(Arrays.asList(receivedmessage.split(" ")));
/*  124 */               String command = (String)receivedmessagelist.get(0);
/*  125 */               if (CommandEnum.START.getCommand().equals(command)) {
/*  126 */                 handleStartCommand(chatid);
/*  127 */               } else if (CommandEnum.DELETEREGISTER.getCommand().equals(command)) {
/*  128 */                 u.setState("");
/*  129 */                 u.setMobilphone("");
/*  130 */                 u.setEmail("");
/*  131 */                 ur.saveAndFlush(u);
/*  132 */               } else if (CommandEnum.TELEGRAMSENTIMENT.getCommand().equals(command)) {
/*  133 */                 handleSentimentCommand(chatid);
/*  134 */               } else if (CommandEnum.TWITTER.getCommand().equals(command)) {
/*  135 */                 handleTwitterSentimentCommand(chatid);
/*  136 */               } else if (CommandEnum.FACEBOOK.getCommand().equals(command)) {
/*  137 */                 handleFacebookSentimentCommand(chatid);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               }
/*  146 */               else if (CommandEnum.PPALMENU.getCommand().equals(command)) {
/*  147 */                 handlePpalMenuCommand(chatid);
/*  148 */               } else if (CommandEnum.PARTIALREPORT.getCommand().equals(command)) {
/*  149 */                 handleReportDetailCommand(chatid);
/*  150 */               } else if (CommandEnum.CANCEL.getCommand().equals(command)) {
/*  151 */                 handleCancelCommand(chatid);
/*  152 */               } else if (CommandEnum.COMANDSTATUS.getCommand().equals(command)) {
/*  153 */                 handleStatusCommand(chatid);
/*  154 */               } else if (CommandEnum.HELP.getCommand().equals(command)) {
/*  155 */                 handleHelpCommand(chatid);
/*      */               } else {
/*  157 */                 ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  175 */                 if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()))
/*      */                 {
/*  177 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.TWITTER.getSocialmediasource())) {
/*  178 */                     handleTwitterSentimentSearchObjectCommand(chatid, receivedmessage);
/*  179 */                     return;
/*      */                   }
/*      */                 }
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  188 */                 if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()))
/*      */                 {
/*  190 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  191 */                     if (cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()).equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
/*  192 */                       handleFacebookSentimentSearchObjectCommand(chatid, receivedmessage);
/*  193 */                       return; }
/*  194 */                     handleFacebookSentimentSearchObjectPostCommand(chatid, receivedmessage);
/*      */                     
/*  196 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSINCE.getState()))
/*      */                 {
/*  198 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  199 */                     handleFacebookSentimentSinceCommand(chatid, receivedmessage);
/*  200 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTUNTIL.getState()))
/*      */                 {
/*  202 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  203 */                     handleFacebookSentimentUntilCommand(chatid, receivedmessage);
/*  204 */                     return; } } if (cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState()))
/*      */                 {
/*  206 */                   if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
/*  207 */                     handleFacebookSentimentChoosePostCommand(chatid, receivedmessage);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */                   }
/*      */                   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */                 }
/*      */                 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               }
/*      */               
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             }
/*  242 */             else if (up.getState().equals(PlanStateEnum.FINALIZADO.getState())) {
/*  243 */               if (up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType()))
/*      */               {
/*  245 */                 handleFreePlanFinished(chatid, u);
/*  246 */               } else if ((!up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType())) && 
/*  247 */                 (up.getTxpalused().intValue() >= up.getTxplancapacity().intValue())) {
/*  248 */                 handlePlanTxFinished(chatid, u);
/*      */               }
/*      */               
/*      */             }
/*      */             
/*      */           }
/*      */           
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  260 */           SendMessage message = new SendMessage()
/*  261 */             .setChatId(update.getMessage().getChatId())
/*  262 */             .setReplyMarkup(getRegistryboard())
/*  263 */             .setText(messageSource.getMessage("messsage.telegram.finalizarregistro.text", null, LocaleContextHolder.getLocale()));
/*      */           try {
/*  265 */             sendMessage(message);
/*      */           } catch (TelegramApiException e1) {
/*  267 */             e1.printStackTrace();
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */           }
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  290 */     else if (update.hasCallbackQuery()) {
/*  291 */       CallbackQuery callbackquery = update.getCallbackQuery();
/*  292 */       String[] data = callbackquery.getData().split(":");
/*      */       
/*  294 */       String chatid = callbackquery.getMessage().getChatId().toString();
/*      */       
/*      */ 
/*      */ 
/*  298 */       if (data[0].equals("detailreport")) {
/*  299 */         handleReportDetailCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
/*  300 */       } else if (data[0].equals("cancelreport")) {
/*  301 */         handleCancelCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
/*  302 */       } else if (data[0].equals("licenseinfluenceenroll")) {
/*  303 */         handleInfluencerEnrollAcceptance(callbackquery.getMessage().getChatId().toString(), data[1], callbackquery.getFrom());
/*  304 */       } else if (data[0].equals("renewplantoday")) {
/*  305 */         Users u = ur.isUserActivebyChatid(chatid);
/*  306 */         handleRenewPlanToday(chatid, u);
/*      */       }
/*      */     }
/*  309 */     else if ((update.hasMessage()) && (update.getMessage().getContact() != null)) {
/*  310 */       String chatid = update.getMessage().getChatId().toString();
/*  311 */       handleFinalizeRegisterCommand(update, chatid);
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleRenewPlanToday(String chatid, Users u) {
/*      */     try {
/*  317 */       pm.upgradePlanPaymentMethodPredeterminated(u);
/*      */     } catch (Exception e) {
/*  319 */       SendMessage message = new SendMessage()
/*  320 */         .setChatId(chatid)
/*  321 */         .setReplyMarkup(getMainMenuKeyboard())
/*  322 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  324 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  326 */         e1.printStackTrace();
/*      */       }
/*  328 */       e.printStackTrace();
/*      */       try {
/*  330 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  332 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handlePlanTxFinished(String chatid, Users u)
/*      */   {
/*      */     try
/*      */     {
/*  341 */       SendMessage message = new SendMessage()
/*  342 */         .setChatId(chatid)
/*  343 */         .setReplyMarkup(getPlanTxFinishedInlineMenuKeyboard(u))
/*  344 */         .setText(messageSource.getMessage("message.telegram.planfinish.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/*  347 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  349 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  369 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  352 */       message = 
/*      */       
/*      */ 
/*  355 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  357 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  359 */         e1.printStackTrace();
/*      */       }
/*  361 */       e.printStackTrace();
/*      */       try {
/*  363 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  365 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getPlanTxFinishedInlineMenuKeyboard(Users u)
/*      */   {
/*  372 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/*  374 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/*  376 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/*  377 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.planfinish.button.text", null, LocaleContextHolder.getLocale())).setCallbackData("renewplantoday"));
/*      */     
/*  379 */     rowsInline.add(rowInline);
/*  380 */     markupInline.setKeyboard(rowsInline);
/*      */     
/*  382 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleCancelCommandfromState(String chatid, String cpId)
/*      */   {
/*  387 */     pps.cancel(cpId);
/*      */     
/*  389 */     SendMessage message = new SendMessage()
/*  390 */       .setChatId(chatid)
/*  391 */       .setReplyMarkup(getMainMenuKeyboard())
/*  392 */       .setText(messageSource.getMessage("messsage.telegram.aftercancel.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  394 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  396 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleCancelCommand(String chatid)
/*      */   {
/*  402 */     String cpid = "";
/*      */     
/*  404 */     List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*  405 */     if (lcp.size() > 0) {
/*  406 */       cpid = ((CommandProcess)lcp.get(0)).getId();
/*  407 */       pps.cancel(cpid);
/*      */       
/*  409 */       SendMessage message = new SendMessage()
/*  410 */         .setChatId(chatid)
/*  411 */         .setReplyMarkup(getMainMenuKeyboard())
/*  412 */         .setText(messageSource.getMessage("messsage.telegram.aftercancel.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  414 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  416 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  421 */       SendMessage message = new SendMessage()
/*  422 */         .setChatId(chatid)
/*  423 */         .setReplyMarkup(getMainMenuKeyboard())
/*  424 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  426 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  428 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private static ReplyKeyboardMarkup getMainMenuKeyboard()
/*      */   {
/*  436 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  437 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  438 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  439 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  441 */     List<KeyboardRow> keyboard = new ArrayList();
/*  442 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  443 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.sentiment.text", null, LocaleContextHolder.getLocale()));
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  448 */     keyboard.add(keyboardFirstRow);
/*      */     
/*      */ 
/*  451 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  453 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getRegistryboard() {
/*  457 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  458 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  459 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  460 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  462 */     List<KeyboardRow> keyboard = new ArrayList();
/*  463 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*      */     
/*  465 */     KeyboardButton button = new KeyboardButton(messageSource.getMessage("messsage.telegram.command.finalizarregistro.text", null, LocaleContextHolder.getLocale()));
/*  466 */     button.setRequestContact(Boolean.valueOf(true));
/*  467 */     keyboardFirstRow.add(button);
/*  468 */     keyboard.add(keyboardFirstRow);
/*  469 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  471 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getSentimentMenuKeyboard() {
/*  475 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  476 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  477 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  478 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  480 */     List<KeyboardRow> keyboard = new ArrayList();
/*  481 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*      */     
/*  483 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.text", null, LocaleContextHolder.getLocale()));
/*      */     
/*      */ 
/*  486 */     keyboard.add(keyboardFirstRow);
/*      */     
/*  488 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  490 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getPpalMenuKeyboard() {
/*  494 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  495 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  496 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  497 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*  498 */     List<KeyboardRow> keyboard = new ArrayList();
/*  499 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  500 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.ppalmenu.text", null, LocaleContextHolder.getLocale()));
/*  501 */     keyboard.add(keyboardSecondRow);
/*  502 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  504 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getTwitterSearchTypeMenuKeyboard() {
/*  508 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  509 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  510 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  511 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  513 */     List<KeyboardRow> keyboard = new ArrayList();
/*  514 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  515 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.searchbyusertext", null, LocaleContextHolder.getLocale()));
/*  516 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.twitter.searchbyhastagtext", null, LocaleContextHolder.getLocale()));
/*  517 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  518 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/*  519 */     keyboard.add(keyboardFirstRow);
/*  520 */     keyboard.add(keyboardSecondRow);
/*  521 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  523 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private static ReplyKeyboardMarkup getFacebookSearchTypeMenuKeyboard() {
/*  527 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/*  528 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/*  529 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/*  530 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/*  532 */     List<KeyboardRow> keyboard = new ArrayList();
/*  533 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/*  534 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.facebook.searchbypagetext", null, LocaleContextHolder.getLocale()));
/*  535 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.facebook.searchbyposttext", null, LocaleContextHolder.getLocale()));
/*  536 */     KeyboardRow keyboardSecondRow = new KeyboardRow();
/*  537 */     keyboardSecondRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/*  538 */     keyboard.add(keyboardFirstRow);
/*  539 */     keyboard.add(keyboardSecondRow);
/*  540 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/*  542 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private void handleFinalizeRegisterCommand(Update update, String chatid)
/*      */   {
/*  547 */     Users u = ur.isUserActivebyMobilPhone(update.getMessage().getContact().getPhoneNumber().replace("+", ""));
/*      */     
/*  549 */     if (u != null) {
/*  550 */       u.setChatid(update.getMessage().getChatId().toString());
/*  551 */       ur.saveAndFlush(u);
/*  552 */       handleStartCommand(update.getMessage().getChatId().toString());
/*      */     }
/*      */     else {
/*      */       try {
/*  556 */         SendMessage message = new SendMessage()
/*  557 */           .setChatId(chatid)
/*  558 */           .setReplyMarkup(getRegisterUserInlineMenuKeyboard())
/*  559 */           .setText(messageSource.getMessage("message.telegram.registrarusuario.text", null, LocaleContextHolder.getLocale()));
/*      */         try
/*      */         {
/*  562 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  564 */           e1.printStackTrace();
/*      */         }
/*      */         SendMessage message;
/*      */         return; } catch (Exception e) { message = 
/*      */         
/*      */ 
/*  570 */           new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */         try {
/*  572 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  574 */           e1.printStackTrace();
/*      */         }
/*  576 */         e.printStackTrace();
/*      */         try {
/*  578 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/*  580 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleStartCommand(String chatid)
/*      */   {
/*  588 */     List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*      */     
/*  590 */     if (lcp.size() == 0) {
/*  591 */       ComandDTO cdto = new ComandDTO();
/*  592 */       cdto.setState(TelegramStateEnum.MAINMENU.getState());
/*  593 */       cdto.setArguments(new HashMap());
/*  594 */       cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), 
/*  595 */         ServiceTypeEnum.XENXE.getServicetype());
/*  596 */       usercommandstate.put(chatid, cdto);
/*  597 */       SendMessage message = new SendMessage()
/*  598 */         .setChatId(chatid)
/*  599 */         .setReplyMarkup(getMainMenuKeyboard())
/*  600 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()));
/*      */       try {
/*  602 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  604 */         e1.printStackTrace();
/*      */       }
/*      */     } else {
/*  607 */       SendMessage message = new SendMessage()
/*  608 */         .setChatId(chatid)
/*  609 */         .setReplyMarkup(getPushPartialMenuKeyboard())
/*  610 */         .setText(messageSource.getMessage("messsage.telegram.mainmenu.currentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  612 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  614 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handlePpalMenuCommand(String chatid) {
/*  620 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  621 */     cdto.setState(TelegramStateEnum.MAINMENU.getState());
/*  622 */     SendMessage message = new SendMessage()
/*  623 */       .setChatId(chatid)
/*  624 */       .setReplyMarkup(getMainMenuKeyboard())
/*  625 */       .setText(messageSource.getMessage("messsage.telegram.mainmenu.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  627 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  629 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleHelpCommand(String chatid) {
/*  634 */     SendMessage message = new SendMessage()
/*  635 */       .setChatId(chatid)
/*  636 */       .setReplyMarkup(getMainMenuKeyboard())
/*  637 */       .setText(messageSource.getMessage("messsage.telegram.help.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  639 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  641 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleSentimentCommand(String chatid) {
/*  646 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  647 */     cdto.setState(TelegramStateEnum.SENTIMENT.getState());
/*  648 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  649 */     SendMessage message = new SendMessage()
/*  650 */       .setChatId(chatid)
/*  651 */       .setReplyMarkup(getSentimentMenuKeyboard())
/*  652 */       .setText(messageSource.getMessage("messsage.telegram.sentiment.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  654 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  656 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentCommand(String chatid) {
/*  661 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  662 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  663 */     cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), 
/*  664 */       SocialMediaSourceEnum.TWITTER.getSocialmediasource());
/*  665 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  666 */       TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/*  667 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  668 */     SendMessage message = new SendMessage()
/*  669 */       .setChatId(chatid)
/*  670 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  671 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  673 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  675 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSearchbyUserCommand(String chatid) {
/*  680 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  681 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  682 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  683 */       TwitterSearchTypeEnum.USUARIO.getTwittersearchtype());
/*  684 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  685 */     SendMessage message = new SendMessage()
/*  686 */       .setChatId(chatid)
/*  687 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  689 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  691 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSearchbyHashtagCommand(String chatid) {
/*  696 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  697 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  698 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  699 */       TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
/*  700 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  701 */     SendMessage message = new SendMessage()
/*  702 */       .setChatId(chatid)
/*  703 */       .setReplyMarkup(getPpalMenuKeyboard())
/*  704 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  706 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  708 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentCommand(String chatid) {
/*  713 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  714 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHTYPE.getState());
/*  715 */     cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), 
/*  716 */       SocialMediaSourceEnum.FACEBOOK.getSocialmediasource());
/*  717 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  718 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
/*  719 */       cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  720 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  721 */         FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
/*      */     }
/*  723 */     SendMessage message = new SendMessage()
/*  724 */       .setChatId(chatid)
/*  725 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  726 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  728 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  730 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchbyPageCommand(String chatid)
/*      */   {
/*  736 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  737 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
/*  738 */       cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*  739 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  740 */         FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
/*      */     }
/*  742 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  743 */     SendMessage message = new SendMessage()
/*  744 */       .setChatId(chatid)
/*  745 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  746 */       .setText(messageSource.getMessage("messsage.telegram.searchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  748 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  750 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchbyPostCommand(String chatid) {
/*  755 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  756 */     if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState())) {
/*  757 */       cdto.setState(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState());
/*  758 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), 
/*  759 */         FacebookSearchTypeEnum.POST.getFacebooksearchtype());
/*      */     }
/*  761 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  762 */     SendMessage message = new SendMessage()
/*  763 */       .setChatId(chatid)
/*  764 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  765 */       .setText(messageSource.getMessage("messsage.telegram.facebook.choosepage.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  767 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  769 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSearchObjectCommand(String chatid, String searchobject) {
/*  774 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  775 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  776 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  777 */       searchobject);
/*  778 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  779 */     Calendar cal = Calendar.getInstance();
/*  780 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  781 */       cal.getTime());
/*      */     
/*  783 */     cal.add(5, -8);
/*      */     
/*  785 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  786 */       cal.getTime());
/*  787 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  788 */     cdto.setCommunicationKey(chatid);
/*  789 */     ComandDTO cdtocopy = new ComandDTO();
/*  790 */     cdtocopy.setArguments(cdto.getArguments());
/*  791 */     cdtocopy.setChannel(cdto.getChannel());
/*  792 */     cdtocopy.setCommand(cdto.getCommand());
/*  793 */     cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  794 */     cdtocopy.setState(cdto.getState());
/*  795 */     cdtocopy.setCratedAt(new Date());
/*  796 */     pps.proccess(cdtocopy);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private static UserRepository ur;
/*      */   
/*      */ 
/*      */   private static FacebookService fs;
/*      */   
/*      */   private static ReportDetailManager rdm;
/*      */   
/*      */   private void handleTwitterSentimentSearchObjectCommand(String chatid, String searchobject)
/*      */   {
/*  810 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  811 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  812 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  813 */       searchobject);
/*  814 */     Calendar cal = Calendar.getInstance();
/*  815 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  816 */       cal.getTime());
/*      */     
/*  818 */     cal.add(11, -24);
/*      */     
/*  820 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  821 */       cal.getTime());
/*  822 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  823 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  824 */     cdto.setCommunicationKey(chatid);
/*  825 */     ComandDTO cdtocopy = new ComandDTO();
/*  826 */     cdtocopy.setArguments(cdto.getArguments());
/*  827 */     cdtocopy.setChannel(cdto.getChannel());
/*  828 */     cdtocopy.setCommand(cdto.getCommand());
/*  829 */     cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  830 */     cdtocopy.setState(cdto.getState());
/*  831 */     cdtocopy.setCratedAt(new Date());
/*  832 */     pps.proccess(cdtocopy);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void handleFacebookSentimentSearchObjectPostCommand(String chatid, String postnumber)
/*      */   {
/*  846 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     
/*  848 */     cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
/*  849 */     cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), 
/*  850 */       ((Post)((List)cdto.getArguments().get(CommandArgumentsEnum.POSTLIST.getCommandargument())).get(new Integer(postnumber).intValue())).getId());
/*  851 */     cdto.getArguments().remove(CommandArgumentsEnum.POSTLIST.getCommandargument());
/*  852 */     cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
/*  853 */     SendMessage message = new SendMessage()
/*  854 */       .setChatId(chatid)
/*  855 */       .setText(messageSource.getMessage("messsage.telegram.since.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  857 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  859 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentChoosePostCommand(String chatid, String searchobject) {
/*  864 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*  865 */     List<Post> postlist = null;
/*      */     try {
/*  867 */       postlist = fs.searchlastPosts(searchobject, 10);
/*      */     } catch (Exception e) {
/*  869 */       SendMessage message = new SendMessage()
/*  870 */         .setChatId(chatid)
/*  871 */         .setReplyMarkup(getMainMenuKeyboard())
/*  872 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  874 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  876 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */     
/*  880 */     int i = 0;
/*  881 */     for (Post p : postlist) {
/*  882 */       SendMessage message = new SendMessage()
/*  883 */         .setChatId(chatid)
/*  884 */         .setReplyMarkup(new ReplyKeyboardRemove())
/*  885 */         .setText("Post " + i + " :" + p.getMessage());
/*      */       try {
/*  887 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  889 */         e1.printStackTrace();
/*      */       }
/*  891 */       i++;
/*      */     }
/*      */     
/*  894 */     SendMessage message = new SendMessage()
/*  895 */       .setChatId(chatid)
/*  896 */       .setReplyMarkup(new ReplyKeyboardRemove())
/*  897 */       .setText(messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.text", null, LocaleContextHolder.getLocale()));
/*      */     try {
/*  899 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/*  901 */       e1.printStackTrace();
/*      */     }
/*  903 */     cdto.getArguments().put(CommandArgumentsEnum.POSTLIST.getCommandargument(), 
/*  904 */       postlist);
/*  905 */     cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentSinceCommand(String chatid, String since)
/*      */   {
/*  910 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  911 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  913 */       cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
/*  914 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/*  915 */         dateFormat.parse(since));
/*  916 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  917 */       cdto.setCommunicationKey(chatid);
/*  918 */       SendMessage message = new SendMessage()
/*  919 */         .setChatId(chatid)
/*  920 */         .setReplyMarkup(getMainMenuKeyboard())
/*  921 */         .setText(messageSource.getMessage("messsage.telegram.until.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  923 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  925 */         e1.printStackTrace();
/*      */       }
/*      */       SendMessage message;
/*      */       return; } catch (ParseException e) { message = 
/*      */       
/*      */ 
/*  931 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  933 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  935 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentUntilCommand(String chatid, String since) {
/*  941 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  942 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  944 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  945 */         dateFormat.parse(since));
/*  946 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  947 */       cdto.setCommunicationKey(chatid);
/*  948 */       ComandDTO cdtocopy = new ComandDTO();
/*  949 */       cdtocopy.setArguments(cdto.getArguments());
/*  950 */       cdtocopy.setChannel(cdto.getChannel());
/*  951 */       cdtocopy.setCommand(cdto.getCommand());
/*  952 */       cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  953 */       cdtocopy.setState(cdto.getState());
/*  954 */       cdtocopy.setCratedAt(new Date());
/*  955 */       pps.proccess(cdtocopy);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (ParseException e)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  967 */       SendMessage message = new SendMessage()
/*  968 */         .setChatId(chatid)
/*  969 */         .setReplyMarkup(getMainMenuKeyboard())
/*  970 */         .setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/*  972 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/*  974 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleFacebookSentimentUntilCommand(String chatid, String since)
/*      */   {
/*  981 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/*  982 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/*  984 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), 
/*  985 */         dateFormat.parse(since));
/*  986 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/*  987 */       cdto.setCommunicationKey(chatid);
/*  988 */       ComandDTO cdtocopy = new ComandDTO();
/*  989 */       cdtocopy.setArguments(cdto.getArguments());
/*  990 */       cdtocopy.setChannel(cdto.getChannel());
/*  991 */       cdtocopy.setCommand(cdto.getCommand());
/*  992 */       cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
/*  993 */       cdtocopy.setState(cdto.getState());
/*  994 */       cdtocopy.setCratedAt(new Date());
/*  995 */       pps.proccess(cdtocopy);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (ParseException e)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1007 */       SendMessage message = new SendMessage()
/* 1008 */         .setChatId(chatid)
/* 1009 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1010 */         .setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1012 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1014 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleTwitterSentimentSinceCommand(String chatid, String since)
/*      */   {
/* 1021 */     DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
/* 1022 */     ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */     try {
/* 1024 */       cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
/* 1025 */       cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), 
/* 1026 */         dateFormat.parse(since));
/* 1027 */       cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 1028 */       cdto.setCommunicationKey(chatid);
/* 1029 */       SendMessage message = new SendMessage()
/* 1030 */         .setChatId(chatid)
/* 1031 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1032 */         .setText(messageSource.getMessage("messsage.telegram.until.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1034 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1036 */         e1.printStackTrace();
/*      */       }
/*      */       SendMessage message;
/*      */       return; } catch (ParseException e) { message = 
/*      */       
/*      */ 
/* 1042 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("messsage.telegram.datenotfomat.text", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1044 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1046 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void handlePushPartialReport(String chatid, int totalpost, String searchcriteria, String cpid)
/*      */   {
/* 1054 */     ComandDTO cdto = new ComandDTO();
/* 1055 */     cdto.setState(TelegramStateEnum.DETAILREPORT.getState());
/* 1056 */     cdto.setArguments(new HashMap());
/* 1057 */     cdto.getArguments().put(CommandArgumentsEnum.COMMANDPROCESSID.getCommandargument(), 
/* 1058 */       cpid);
/* 1059 */     cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
/* 1060 */     cdto.setCommunicationKey(chatid);
/*      */     
/* 1062 */     usercommandstate.replace(chatid, cdto);
/*      */     
/* 1064 */     SendMessage message = new SendMessage()
/* 1065 */       .setChatId(chatid)
/* 1066 */       .setReplyMarkup(getPushPartialMenuKeyboard())
/* 1067 */       .setText(messageSource.getMessage("messsage.telegram.pushnotification1.text", new Object[] { Integer.valueOf(totalpost), searchcriteria }, LocaleContextHolder.getLocale()));
/*      */     try {
/* 1069 */       sendMessage(message);
/*      */     } catch (TelegramApiException e1) {
/* 1071 */       e1.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private ReplyKeyboard getPushPartialMenuKeyboard()
/*      */   {
/* 1078 */     ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
/* 1079 */     replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
/* 1080 */     replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
/* 1081 */     replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
/*      */     
/* 1083 */     List<KeyboardRow> keyboard = new ArrayList();
/* 1084 */     KeyboardRow keyboardFirstRow = new KeyboardRow();
/* 1085 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", null, LocaleContextHolder.getLocale()));
/* 1086 */     keyboardFirstRow.add(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale()));
/* 1087 */     keyboard.add(keyboardFirstRow);
/* 1088 */     replyKeyboardMarkup.setKeyboard(keyboard);
/*      */     
/* 1090 */     return replyKeyboardMarkup;
/*      */   }
/*      */   
/*      */   private void handleReportDetailCommand(String chatid) {
/*      */     try {
/* 1095 */       String cpid = "";
/*      */       
/* 1097 */       List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/* 1098 */       if (lcp.size() > 0) {
/* 1099 */         cpid = ((CommandProcess)lcp.get(0)).getId();
/* 1100 */         rdm = rdmf.getResponseBotService(ServiceTypeEnum.XENXE.getServicetype());
/* 1101 */         rdm.reportdetailgenerateSentimentAnalysisReport(
/* 1102 */           cpid);
/*      */       } else {
/* 1104 */         SendMessage message = new SendMessage()
/* 1105 */           .setChatId(chatid)
/* 1106 */           .setReplyMarkup(getMainMenuKeyboard())
/* 1107 */           .setText(messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1109 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1111 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1129 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1119 */       message = 
/*      */       
/*      */ 
/* 1122 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1124 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1126 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleStatusCommand(String chatid)
/*      */   {
/*      */     try {
/* 1134 */       List<CommandProcess> lcp = cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid);
/*      */       
/* 1136 */       if (lcp.size() > 0) {
/* 1137 */         for (CommandProcess cp : lcp)
/*      */         {
/* 1139 */           XStream xs = new XStream();
/* 1140 */           ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
/*      */           
/* 1142 */           int total = postr.totalPost(cp.getId()).intValue();
/*      */           
/* 1144 */           Date date = new Date();
/* 1145 */           DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
/* 1146 */           SendMessage message = new SendMessage()
/* 1147 */             .setChatId(chatid)
/* 1148 */             .setReplyMarkup(getProcessStateInlineMenuKeyboard(cp))
/* 1149 */             .setText(messageSource.getMessage("messsage.telegram.procesrunning.text", new Object[] {
/* 1150 */             cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()), 
/* 1151 */             cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), 
/* 1152 */             df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())), 
/* 1153 */             df.format((Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())), 
/* 1154 */             Integer.valueOf(total) }, LocaleContextHolder.getLocale()));
/*      */           try {
/* 1156 */             sendMessage(message);
/*      */           } catch (TelegramApiException e1) {
/* 1158 */             e1.printStackTrace();
/*      */           }
/*      */         }
/*      */       } else {
/* 1162 */         SendMessage message = new SendMessage()
/* 1163 */           .setChatId(chatid)
/* 1164 */           .setReplyMarkup(getMainMenuKeyboard())
/* 1165 */           .setText(messageSource.getMessage("messsage.telegram.notprocesrunning.text", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1167 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1169 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1184 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1173 */       message = 
/*      */       
/*      */ 
/* 1176 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1178 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1180 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private ReplyKeyboard getProcessStateInlineMenuKeyboard(CommandProcess cp)
/*      */   {
/* 1188 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1190 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1192 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1193 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", null, LocaleContextHolder.getLocale())).setCallbackData("detailreport:cpid:" + cp.getId()));
/* 1194 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.command.cancel.text", null, LocaleContextHolder.getLocale())).setCallbackData("cancelreport:cpid:" + cp.getId()));
/*      */     
/* 1196 */     rowsInline.add(rowInline);
/* 1197 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1199 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleReportDetailCommandfromState(String chatid, String cpid) {
/*      */     try {
/* 1204 */       rdm = rdmf.getResponseBotService(ServiceTypeEnum.XENXE.getServicetype());
/* 1205 */       rdm.reportdetailgenerateSentimentAnalysisReport(
/* 1206 */         cpid);
/*      */       
/*      */ 
/* 1209 */       usercommandstate.replace(chatid, new ComandDTO());
/*      */     } catch (Exception e) {
/* 1211 */       SendMessage message = new SendMessage()
/* 1212 */         .setChatId(chatid)
/* 1213 */         .setReplyMarkup(getMainMenuKeyboard())
/* 1214 */         .setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1216 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1218 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnroll(String chatid)
/*      */   {
/*      */     try {
/* 1226 */       ComandDTO cdto = new ComandDTO();
/* 1227 */       cdto.setState(TelegramStateEnum.EMAILENRROLL.getState());
/* 1228 */       cdto.setCommand(CommandEnum.INFLUENCERENNROLL.getCommand());
/*      */       
/* 1230 */       usercommandstate.put(chatid, cdto);
/*      */       
/* 1232 */       SendMessage message = new SendMessage()
/* 1233 */         .setChatId(chatid)
/* 1234 */         .setText(messageSource.getMessage("messsage.telegram.influencerenroll.email.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1237 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1239 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1258 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1242 */       message = 
/*      */       
/*      */ 
/* 1245 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1247 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1249 */         e1.printStackTrace();
/*      */       }
/* 1251 */       e.printStackTrace();
/*      */       try {
/* 1253 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1255 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleFreePlanFinished(String chatid, Users u)
/*      */   {
/*      */     try {
/* 1263 */       SendMessage message = new SendMessage()
/* 1264 */         .setChatId(chatid)
/* 1265 */         .setReplyMarkup(getSelectPlanInlineMenuKeyboard(u))
/* 1266 */         .setText(messageSource.getMessage("message.telegram.freeplanfinish.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1269 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1271 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1290 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1274 */       message = 
/*      */       
/*      */ 
/* 1277 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1279 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1281 */         e1.printStackTrace();
/*      */       }
/* 1283 */       e.printStackTrace();
/*      */       try {
/* 1285 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1287 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnrollLicence(String chatid, String email)
/*      */   {
/*      */     try {
/* 1295 */       ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */       
/* 1297 */       cdto.setArguments(new HashMap());
/* 1298 */       cdto.getArguments().put(CommandArgumentsEnum.EMAIL.getCommandargument(), 
/* 1299 */         email);
/*      */       
/* 1301 */       String licence = pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
/*      */       
/* 1303 */       SendMessage message = new SendMessage()
/* 1304 */         .setChatId(chatid)
/* 1305 */         .setReplyMarkup(getlicenseinfluencerInlineMenuKeyboard())
/* 1306 */         .setText(licence);
/*      */       try {
/* 1308 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1310 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1329 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1313 */       message = 
/*      */       
/*      */ 
/* 1316 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1318 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1320 */         e1.printStackTrace();
/*      */       }
/* 1322 */       e.printStackTrace();
/*      */       try {
/* 1324 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1326 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getlicenseinfluencerInlineMenuKeyboard()
/*      */   {
/* 1333 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1335 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1337 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1338 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.influencerenroll.acept.text", null, LocaleContextHolder.getLocale())).setCallbackData("licenseinfluenceenroll:YES"));
/* 1339 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("messsage.telegram.influencerenroll.reject.text", null, LocaleContextHolder.getLocale())).setCallbackData("licenseinfluenceenroll:NO"));
/*      */     
/* 1341 */     rowsInline.add(rowInline);
/* 1342 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1344 */     return markupInline;
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getSelectPlanInlineMenuKeyboard(Users u)
/*      */   {
/* 1349 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1351 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1353 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1354 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.freeplanfinish.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("selectplan.url") + "?usertoken=" + ts.generateUserToken(u)));
/*      */     
/* 1356 */     rowsInline.add(rowInline);
/* 1357 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1359 */     return markupInline;
/*      */   }
/*      */   
/*      */   private ReplyKeyboard getRegisterUserInlineMenuKeyboard()
/*      */   {
/* 1364 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1366 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1368 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1369 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.registrarusuario.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("register.url")));
/*      */     
/* 1371 */     rowsInline.add(rowInline);
/* 1372 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1374 */     return markupInline;
/*      */   }
/*      */   
/*      */   private void handleInfluencerEnrollAcceptance(String chatid, String acceptance, User u)
/*      */   {
/*      */     try {
/* 1380 */       if (acceptance.equals("YES"))
/*      */       {
/* 1382 */         String licence = pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
/*      */         
/* 1384 */         Users user = new Users();
/* 1385 */         user.setChatid(chatid);
/* 1386 */         user.setId(UUID.randomUUID().toString());
/* 1387 */         user.setName(u.getFirstName());
/* 1388 */         user.setLastname(u.getLastName());
/* 1389 */         user.setState("activo");
/* 1390 */         user.setTestlicenceacepted("Y");
/* 1391 */         user.setTexttestlicence(licence);
/*      */         
/* 1393 */         ComandDTO cdto = (ComandDTO)usercommandstate.get(chatid);
/*      */         
/* 1395 */         user.setEmail((String)cdto.getArguments().get(CommandArgumentsEnum.EMAIL.getCommandargument()));
/* 1396 */         ur.saveAndFlush(user);
/*      */         
/*      */ 
/* 1399 */         handleStartCommand(chatid);
/*      */       }
/*      */       else {
/* 1402 */         SendMessage message = new SendMessage()
/* 1403 */           .setChatId(chatid)
/* 1404 */           .setText(messageSource.getMessage("messsage.telegram.influencerenroll.reject.text1", null, LocaleContextHolder.getLocale()));
/*      */         try {
/* 1406 */           sendMessage(message);
/*      */         } catch (TelegramApiException e1) {
/* 1408 */           e1.printStackTrace();
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1428 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1412 */       message = 
/*      */       
/*      */ 
/* 1415 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1417 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1419 */         e1.printStackTrace();
/*      */       }
/* 1421 */       e.printStackTrace();
/*      */       try {
/* 1423 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1425 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleUserTokenException(String chatid)
/*      */   {
/*      */     try {
/* 1433 */       SendMessage message = new SendMessage()
/* 1434 */         .setChatId(chatid)
/* 1435 */         .setText(messageSource.getMessage("message.telegram.usertokenexception.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1438 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1440 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1459 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1443 */       message = 
/*      */       
/*      */ 
/* 1446 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1448 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1450 */         e1.printStackTrace();
/*      */       }
/* 1452 */       e.printStackTrace();
/*      */       try {
/* 1454 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1456 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handleUpgradePlanOK(String chatid)
/*      */   {
/*      */     try {
/* 1464 */       SendMessage message = new SendMessage()
/* 1465 */         .setChatId(chatid)
/* 1466 */         .setText(messageSource.getMessage("message.telegram.upgradeplanok.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1469 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1471 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1490 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1474 */       message = 
/*      */       
/*      */ 
/* 1477 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1479 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1481 */         e1.printStackTrace();
/*      */       }
/* 1483 */       e.printStackTrace();
/*      */       try {
/* 1485 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1487 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void handlePaymentExceptionUpgradePlan(String chatid, Users u)
/*      */   {
/*      */     try {
/* 1495 */       SendMessage message = new SendMessage()
/* 1496 */         .setChatId(chatid)
/* 1497 */         .setReplyMarkup(gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(u))
/* 1498 */         .setText(messageSource.getMessage("message.telegram.paymentexceptionupgradeplan.text", null, LocaleContextHolder.getLocale()));
/*      */       try
/*      */       {
/* 1501 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1503 */         e1.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       SendMessage message;
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1522 */       return;
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1506 */       message = 
/*      */       
/*      */ 
/* 1509 */         new SendMessage().setChatId(chatid).setReplyMarkup(getMainMenuKeyboard()).setText(messageSource.getMessage("message.exception.runtimeexception.chat", null, LocaleContextHolder.getLocale()));
/*      */       try {
/* 1511 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1513 */         e1.printStackTrace();
/*      */       }
/* 1515 */       e.printStackTrace();
/*      */       try {
/* 1517 */         sendMessage(message);
/*      */       } catch (TelegramApiException e1) {
/* 1519 */         e1.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private ReplyKeyboard gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(Users u)
/*      */   {
/* 1526 */     InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
/*      */     
/* 1528 */     List<List<InlineKeyboardButton>> rowsInline = new ArrayList();
/*      */     
/* 1530 */     List<InlineKeyboardButton> rowInline = new ArrayList();
/* 1531 */     rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("message.telegram.freeplanfinish.button.text", null, LocaleContextHolder.getLocale())).setUrl(env.getProperty("paymentmethod.url") + "?usertoken=" + ts.generateUserToken(u)));
/*      */     
/* 1533 */     rowsInline.add(rowInline);
/* 1534 */     markupInline.setKeyboard(rowsInline);
/*      */     
/* 1536 */     return markupInline;
/*      */   }
/*      */   
/*      */   public String getBotUsername()
/*      */   {
/* 1541 */     return "xenxebot";
/*      */   }
/*      */   
/*      */   public String getBotToken()
/*      */   {
/* 1546 */     return env.getRequiredProperty("xenxebot.token");
/*      */   }
/*      */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\telegram\bot\XenxeBot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */