package co.com.zien.xenxerobot.backend.services.dataset;

import co.com.zien.xenxerobot.persistence.entity.Packages;
import co.com.zien.xenxerobot.persistence.entity.Post;
import co.com.zien.xenxerobot.persistence.entity.Worker;
import java.math.BigDecimal;
import java.util.List;

public abstract interface PackagesService
{
  public abstract List<Packages> findByWorker(Worker paramWorker);
  
  public abstract List<Packages> findByState(String paramString);
  
  public abstract List<Post> nextworkitembypackage(BigDecimal paramBigDecimal);
  
  public abstract Post change(Post paramPost);
  
  public abstract void savehumantask(Post paramPost);
  
  public abstract void savePackage(Packages paramPackages);
  
  public abstract Packages findPackage(BigDecimal paramBigDecimal);
  
  public abstract Integer packageSize(BigDecimal paramBigDecimal);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\dataset\PackagesService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */