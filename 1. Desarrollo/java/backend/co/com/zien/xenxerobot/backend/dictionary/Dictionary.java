/*    */ package co.com.zien.xenxerobot.backend.dictionary;
/*    */ 
/*    */ import java.io.BufferedReader;
/*    */ import java.io.File;
/*    */ import java.io.FileReader;
/*    */ import java.util.HashMap;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Dictionary
/*    */ {
/* 15 */   private static final Logger logger = Logger.getLogger(Dictionary.class);
/*    */   
/* 17 */   private HashMap<String, String> dictionary = new HashMap();
/*    */   private File plaintxtDictionary;
/*    */   
/*    */   public File getPlaintxtDictionary() {
/* 21 */     return this.plaintxtDictionary;
/*    */   }
/*    */   
/*    */   public void setPlaintxtDictionary(File plaintxtDictionary) {
/* 25 */     this.plaintxtDictionary = plaintxtDictionary;
/*    */   }
/*    */   
/*    */   public void loadPlainTextDictionary()
/*    */   {
/*    */     try
/*    */     {
/* 32 */       BufferedReader in = new BufferedReader(new FileReader(this.plaintxtDictionary));
/*    */       
/* 34 */       String line = "";
/* 35 */       while ((line = in.readLine()) != null) {
/* 36 */         this.dictionary.put(line, line);
/*    */       }
/*    */       
/* 39 */       in.close();
/*    */     }
/*    */     catch (Exception e) {
/* 42 */       logger.error(e);
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean existWord(String word) {
/* 47 */     return this.dictionary.containsKey(word);
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\dictionary\Dictionary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */