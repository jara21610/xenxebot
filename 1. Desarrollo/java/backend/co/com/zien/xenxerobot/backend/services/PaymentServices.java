package co.com.zien.xenxerobot.backend.services;

import java.math.BigInteger;

public abstract interface PaymentServices
{
  public abstract String getSignature(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract RegisterCreditCardTokenResponse createCreditCardToken(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract PayOutput payWithCreditCardToken(String paramString1, String paramString2, String paramString3, String paramString4, BigInteger paramBigInteger, String paramString5, String paramString6, String paramString7);
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PaymentServices.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */