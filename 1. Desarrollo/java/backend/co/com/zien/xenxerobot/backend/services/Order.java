/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ 
/*    */ public class Order
/*    */ {
/*    */   private String accountId;
/*    */   
/*    */   private String referenceCode;
/*    */   private String description;
/*    */   private String language;
/*    */   private String signature;
/*    */   private String notifyUrl;
/*    */   private Buyer buyer;
/*    */   private ShippingAddress shippingAddress;
/*    */   private AdditionalValues additionalValues;
/*    */   
/*    */   public String getAccountId()
/*    */   {
/* 19 */     return this.accountId;
/*    */   }
/*    */   
/*    */   public void setAccountId(String accountId) {
/* 23 */     this.accountId = accountId;
/*    */   }
/*    */   
/*    */   public String getReferenceCode() {
/* 27 */     return this.referenceCode;
/*    */   }
/*    */   
/*    */   public void setReferenceCode(String referenceCode) {
/* 31 */     this.referenceCode = referenceCode;
/*    */   }
/*    */   
/*    */   public String getDescription() {
/* 35 */     return this.description;
/*    */   }
/*    */   
/*    */   public void setDescription(String description) {
/* 39 */     this.description = description;
/*    */   }
/*    */   
/*    */   public String getLanguage() {
/* 43 */     return this.language;
/*    */   }
/*    */   
/*    */   public void setLanguage(String language) {
/* 47 */     this.language = language;
/*    */   }
/*    */   
/*    */   public String getSignature() {
/* 51 */     return this.signature;
/*    */   }
/*    */   
/*    */   public void setSignature(String signature) {
/* 55 */     this.signature = signature;
/*    */   }
/*    */   
/*    */   public String getNotifyUrl() {
/* 59 */     return this.notifyUrl;
/*    */   }
/*    */   
/*    */   public void setNotifyUrl(String notifyUrl) {
/* 63 */     this.notifyUrl = notifyUrl;
/*    */   }
/*    */   
/*    */   public AdditionalValues getAdditionalValues() {
/* 67 */     return this.additionalValues;
/*    */   }
/*    */   
/*    */   public void setAdditionalValues(AdditionalValues additionalValues) {
/* 71 */     this.additionalValues = additionalValues;
/*    */   }
/*    */   
/* 74 */   public Buyer getBuyer() { return this.buyer; }
/*    */   
/*    */   public void setBuyer(Buyer buyer)
/*    */   {
/* 78 */     this.buyer = buyer;
/*    */   }
/*    */   
/*    */   public ShippingAddress getShippingAddress() {
/* 82 */     return this.shippingAddress;
/*    */   }
/*    */   
/*    */   public void setShippingAddress(ShippingAddress shippingAddress) {
/* 86 */     this.shippingAddress = shippingAddress;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\Order.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */