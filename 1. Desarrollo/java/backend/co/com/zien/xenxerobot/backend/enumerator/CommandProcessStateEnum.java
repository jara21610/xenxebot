/*    */ package co.com.zien.xenxerobot.backend.enumerator;
/*    */ 
/*    */ 
/*    */ public enum CommandProcessStateEnum
/*    */ {
/*  6 */   ERROR("ERROR"),  ENPROCESO("ENPROCESO"),  FINALIZADO("FINALIZADO"),  CANCELADOXUSUARIO("CANCELADOXUSUARIO");
/*    */   
/*    */   private String state;
/*    */   
/*    */   public String getState() {
/* 11 */     return this.state;
/*    */   }
/*    */   
/*    */   private CommandProcessStateEnum(String state) {
/* 15 */     this.state = state;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\enumerator\CommandProcessStateEnum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */