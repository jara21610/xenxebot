/*    */ package co.com.zien.xenxerobot.backend.dictionary;
/*    */ 
/*    */ import java.io.BufferedReader;
/*    */ import java.io.File;
/*    */ import java.io.FileReader;
/*    */ import java.util.HashMap;
/*    */ import java.util.StringTokenizer;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ public class InflectionDictionary
/*    */ {
/* 13 */   private static final Logger logger = Logger.getLogger(InflectionDictionary.class);
/*    */   
/* 15 */   private HashMap<String, String> dictionary = new HashMap();
/*    */   private File plaintxtDictionary;
/*    */   
/*    */   public File getPlaintxtDictionary() {
/* 19 */     return this.plaintxtDictionary;
/*    */   }
/*    */   
/*    */   public void setPlaintxtDictionary(File plaintxtDictionary) {
/* 23 */     this.plaintxtDictionary = plaintxtDictionary;
/*    */   }
/*    */   
/*    */   public void loadPlainTextDictionary()
/*    */   {
/*    */     try
/*    */     {
/* 30 */       BufferedReader in = new BufferedReader(new FileReader(this.plaintxtDictionary));
/*    */       
/* 32 */       String line = "";
/* 33 */       StringTokenizer st1; for (; (line = in.readLine()) != null; 
/*    */           
/*    */ 
/* 36 */           st1.hasMoreElements())
/*    */       {
/* 34 */         st1 = new StringTokenizer(line, "=");
/* 35 */         String lemma = st1.nextToken();
/* 36 */         continue;
/* 37 */         StringTokenizer st2 = new StringTokenizer(st1.nextToken(), ";");
/* 38 */         while (st2.hasMoreElements()) {
/* 39 */           this.dictionary.put(st2.nextToken(), lemma);
/*    */         }
/*    */       }
/*    */       
/* 43 */       in.close();
/*    */     } catch (Exception e) {
/* 45 */       logger.error(e);
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean existWord(String word) {
/* 50 */     return this.dictionary.containsKey(word);
/*    */   }
/*    */   
/*    */   public String getLemma(String word) {
/* 54 */     return (String)this.dictionary.get(word);
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\dictionary\InflectionDictionary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */