/*    */ package co.com.zien.xenxerobot.backend.services.dataset;
/*    */ 
/*    */ import co.com.zien.xenxerobot.persistence.entity.Worker;
/*    */ import co.com.zien.xenxerobot.persistence.repository.WorkerRepository;
/*    */ import java.util.List;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;
/*    */ 
/*    */ 
/*    */ @Service("workerService")
/*    */ public class WorkerServiceImpl
/*    */   implements WorkerService
/*    */ {
/*    */   @Autowired
/*    */   WorkerRepository wr;
/*    */   
/*    */   public List<Worker> getavailableWorkers()
/*    */   {
/* 19 */     return this.wr.findAll();
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\dataset\WorkerServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */