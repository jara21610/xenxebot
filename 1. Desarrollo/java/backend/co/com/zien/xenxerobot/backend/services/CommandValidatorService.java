package co.com.zien.xenxerobot.backend.services;

public abstract interface CommandValidatorService
{
  public abstract ComandDTO validateCommand(String paramString)
    throws CommandMalformedException;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\CommandValidatorService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */