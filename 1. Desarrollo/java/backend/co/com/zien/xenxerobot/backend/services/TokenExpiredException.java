/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ 
/*    */ public class TokenExpiredException
/*    */   extends Exception
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   
/*    */   public TokenExpiredException() {}
/*    */   
/* 11 */   public TokenExpiredException(String message) { super(message); }
/* 12 */   public TokenExpiredException(String message, Throwable cause) { super(message, cause); }
/* 13 */   public TokenExpiredException(Throwable cause) { super(cause); }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\TokenExpiredException.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */