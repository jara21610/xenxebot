/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ 
/*    */ public class NotDataToPartialReportException
/*    */   extends Exception
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   
/*    */   public NotDataToPartialReportException() {}
/*    */   
/* 11 */   public NotDataToPartialReportException(String message) { super(message); }
/* 12 */   public NotDataToPartialReportException(String message, Throwable cause) { super(message, cause); }
/* 13 */   public NotDataToPartialReportException(Throwable cause) { super(cause); }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\NotDataToPartialReportException.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */