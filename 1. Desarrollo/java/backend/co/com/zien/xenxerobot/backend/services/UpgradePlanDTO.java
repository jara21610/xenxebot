/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ public class UpgradePlanDTO
/*    */ {
/*    */   private String usertoken;
/*    */   private String name;
/*    */   private String identificationNumber;
/*    */   private String number;
/*    */   private String expirationDate;
/*    */   private String ccv;
/*    */   private String idType;
/*    */   private String plan;
/*    */   
/*    */   public String getUsertoken() {
/* 15 */     return this.usertoken;
/*    */   }
/*    */   
/*    */   public void setUsertoken(String usertoken) {
/* 19 */     this.usertoken = usertoken;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 23 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 27 */     this.name = name;
/*    */   }
/*    */   
/*    */   public String getIdentificationNumber() {
/* 31 */     return this.identificationNumber;
/*    */   }
/*    */   
/*    */   public void setIdentificationNumber(String identificationNumber) {
/* 35 */     this.identificationNumber = identificationNumber;
/*    */   }
/*    */   
/*    */   public String getNumber() {
/* 39 */     return this.number;
/*    */   }
/*    */   
/*    */   public void setNumber(String number) {
/* 43 */     this.number = number;
/*    */   }
/*    */   
/*    */   public String getExpirationDate() {
/* 47 */     return this.expirationDate;
/*    */   }
/*    */   
/*    */   public void setExpirationDate(String expirationDate) {
/* 51 */     this.expirationDate = expirationDate;
/*    */   }
/*    */   
/*    */   public String getCcv() {
/* 55 */     return this.ccv;
/*    */   }
/*    */   
/*    */   public void setCcv(String ccv) {
/* 59 */     this.ccv = ccv;
/*    */   }
/*    */   
/*    */   public String getIdType() {
/* 63 */     return this.idType;
/*    */   }
/*    */   
/*    */   public void setIdType(String idType) {
/* 67 */     this.idType = idType;
/*    */   }
/*    */   
/*    */   public String getPlan() {
/* 71 */     return this.plan;
/*    */   }
/*    */   
/*    */   public void setPlan(String plan) {
/* 75 */     this.plan = plan;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\UpgradePlanDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */