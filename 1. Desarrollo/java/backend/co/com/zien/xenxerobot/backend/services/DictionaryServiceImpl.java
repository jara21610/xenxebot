/*    */ package co.com.zien.xenxerobot.backend.services;
/*    */ 
/*    */ import co.com.zien.xenxerobot.backend.dictionary.Dictionary;
/*    */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*    */ import java.io.File;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;
/*    */ import org.springframework.transaction.annotation.Transactional;
/*    */ 
/*    */ 
/*    */ 
/*    */ @Service
/*    */ @Transactional
/*    */ public class DictionaryServiceImpl
/*    */   implements DictionaryService
/*    */ {
/* 17 */   private Dictionary dictionary = null;
/*    */   @Autowired
/*    */   PropertiesRepository pr;
/*    */   
/* 21 */   public Dictionary getDictionary() { if (this.dictionary == null) {
/* 22 */       this.dictionary = new Dictionary();
/* 23 */       this.dictionary.setPlaintxtDictionary(new File("c:/listado-general.txt"));
/* 24 */       this.dictionary.loadPlainTextDictionary();
/*    */     }
/* 26 */     return this.dictionary;
/*    */   }
/*    */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\DictionaryServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */