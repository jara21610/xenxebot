package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.Paymentmethod;
import co.com.zien.xenxerobot.persistence.entity.Userplan;
import co.com.zien.xenxerobot.persistence.entity.Users;
import org.springframework.web.bind.annotation.RequestBody;

public abstract interface PlanManagementService
{
  public abstract Userplan upgradeplan(Users paramUsers, String paramString)
    throws Exception;
  
  public abstract Paymentmethod storePaymentMethod(Users paramUsers, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    throws StorePaymentmethodException;
  
  public abstract void makePlanPayment(Users paramUsers, Userplan paramUserplan, Paymentmethod paramPaymentmethod)
    throws PaymentException;
  
  public abstract void upgradePlanWithPaymentMethod(@RequestBody UpgradePlanDTO paramUpgradePlanDTO)
    throws Exception;
  
  public abstract void upgradePlanPaymentMethodPredeterminated(Users paramUsers)
    throws Exception;
}


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PlanManagementService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */