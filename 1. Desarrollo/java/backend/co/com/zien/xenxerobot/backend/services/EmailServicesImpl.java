/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import co.com.zien.xenxerobot.persistence.repository.PropertiesRepository;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Properties;
/*     */ import java.util.Set;
/*     */ import javax.mail.Authenticator;
/*     */ import javax.mail.BodyPart;
/*     */ import javax.mail.Flags;
/*     */ import javax.mail.Flags.Flag;
/*     */ import javax.mail.Folder;
/*     */ import javax.mail.Message;
/*     */ import javax.mail.Message.RecipientType;
/*     */ import javax.mail.MessagingException;
/*     */ import javax.mail.Multipart;
/*     */ import javax.mail.PasswordAuthentication;
/*     */ import javax.mail.Session;
/*     */ import javax.mail.Store;
/*     */ import javax.mail.Transport;
/*     */ import javax.mail.internet.InternetAddress;
/*     */ import javax.mail.internet.MimeBodyPart;
/*     */ import javax.mail.internet.MimeMessage;
/*     */ import javax.mail.internet.MimeMultipart;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ @Service
/*     */ public class EmailServicesImpl
/*     */   implements EmailService
/*     */ {
/*  37 */   private static final Logger logger = Logger.getLogger(EmailServicesImpl.class);
/*     */   @Autowired
/*     */   PropertiesRepository pr;
/*     */   private Store store;
/*     */   
/*     */   public void sendEmail(String from, String to, String subject, String text)
/*     */   {
/*     */     try
/*     */     {
/*  46 */       Message message = new MimeMessage(getSession());
/*  47 */       message.setFrom(new InternetAddress(from));
/*  48 */       message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
/*  49 */       message.setSubject(subject);
/*  50 */       message.setText(text);
/*     */       
/*  52 */       Transport.send(message);
/*     */       
/*  54 */       System.out.println("Done");
/*     */ 
/*     */     }
/*     */     catch (MessagingException e)
/*     */     {
/*  59 */       System.out.println("Exception e");
/*  60 */       throw new RuntimeException(e);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void sendHtmlEmail(String from, String to, String subject, String body, Map<String, String> mapInlineImages)
/*     */   {
/*     */     try
/*     */     {
/*  70 */       Message message = new MimeMessage(getSession());
/*  71 */       message.setFrom(new InternetAddress(from));
/*  72 */       message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
/*  73 */       message.setSubject(subject);
/*  74 */       message.setHeader("charset", "UTF-8");
/*     */       
/*     */ 
/*  77 */       MimeBodyPart messageBodyPart = new MimeBodyPart();
/*  78 */       messageBodyPart.setContent(body, "text/html");
/*     */       
/*     */ 
/*  81 */       Multipart multipart = new MimeMultipart();
/*  82 */       multipart.addBodyPart(messageBodyPart);
/*     */       
/*  84 */       if ((mapInlineImages != null) && (mapInlineImages.size() > 0)) {
/*  85 */         Set<String> setImageID = mapInlineImages.keySet();
/*  86 */         for (String contentId : setImageID) {
/*  87 */           MimeBodyPart imagePart = new MimeBodyPart();
/*  88 */           imagePart.setHeader("Content-ID", "<" + contentId + ">");
/*  89 */           imagePart.setDisposition("inline");
/*  90 */           String imageFilePath = (String)mapInlineImages.get(contentId);
/*     */           try {
/*  92 */             imagePart.attachFile(imageFilePath);
/*     */           }
/*     */           catch (IOException e) {
/*  95 */             e.printStackTrace();
/*     */           }
/*  97 */           multipart.addBodyPart(imagePart);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 102 */       message.setContent(multipart);
/* 103 */       Transport.send(message);
/*     */       
/* 105 */       System.out.println("Done");
/*     */ 
/*     */     }
/*     */     catch (MessagingException e)
/*     */     {
/* 110 */       System.out.println("Exception e");
/* 111 */       throw new RuntimeException(e);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void sendEmail(String from, String to, String subject, String text, String[] attachFiles)
/*     */   {
/*     */     try
/*     */     {
/* 120 */       Message message = new MimeMessage(getSession());
/* 121 */       message.setFrom(new InternetAddress(from));
/* 122 */       message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
/* 123 */       message.setSubject(subject);
/* 124 */       message.setSentDate(new Date());
/*     */       
/*     */ 
/* 127 */       MimeBodyPart messageBodyPart = new MimeBodyPart();
/* 128 */       messageBodyPart.setText(text);
/*     */       
/*     */ 
/* 131 */       Multipart multipart = new MimeMultipart();
/* 132 */       multipart.addBodyPart(messageBodyPart);
/*     */       
/*     */ 
/* 135 */       if ((attachFiles != null) && (attachFiles.length > 0)) { String[] arrayOfString;
/* 136 */         int j = (arrayOfString = attachFiles).length; for (int i = 0; i < j; i++) { String filePath = arrayOfString[i];
/* 137 */           MimeBodyPart attachPart = new MimeBodyPart();
/*     */           try {
/* 139 */             attachPart.attachFile(filePath);
/*     */           }
/*     */           catch (IOException localIOException) {}
/*     */           
/* 143 */           multipart.addBodyPart(attachPart);
/*     */         }
/*     */       }
/*     */       
/* 147 */       message.setContent(multipart);
/*     */       
/* 149 */       Transport.send(message);
/*     */       
/* 151 */       System.out.println("Done");
/*     */     }
/*     */     catch (MessagingException e)
/*     */     {
/* 155 */       System.out.println("Exception e" + e.getMessage());
/* 156 */       throw new RuntimeException(e);
/*     */     }
/*     */   }
/*     */   
/*     */   public List<EmailMessageDTO> getNewEmailsImap()
/*     */   {
/* 162 */     List<EmailMessageDTO> lm = new ArrayList();
/*     */     try {
/* 164 */       Session session = getSession();
/* 165 */       this.store = session.getStore("imaps");
/* 166 */       this.store.connect(this.pr.findByName("mail.smtp.host"), this.pr.findByName("mail.smtp.email"), this.pr.findByName("mail.smtp.email.password"));
/*     */       
/* 168 */       Folder inbox = this.store.getFolder("inbox");
/* 169 */       inbox.open(2);
/*     */       
/* 171 */       Message[] messages = inbox.getMessages();
/*     */       Message[] arrayOfMessage1;
/* 173 */       int j = (arrayOfMessage1 = messages).length; for (int i = 0; i < j; i++) { Message message = arrayOfMessage1[i];
/* 174 */         if (!message.getFlags().contains(Flags.Flag.SEEN)) {
/* 175 */           message.setFlag(Flags.Flag.SEEN, true);
/* 176 */           EmailMessageDTO emdto = new EmailMessageDTO();
/* 177 */           emdto.setFrom(((InternetAddress)message.getFrom()[0]).getAddress());
/* 178 */           emdto.setText(getTextFromMessage(message));
/* 179 */           lm.add(emdto);
/*     */         }
/*     */       }
/* 182 */       inbox.close(true);
/* 183 */       this.store.close();
/* 184 */       return lm;
/*     */     } catch (Exception e) {
/* 186 */       logger.error(e.getMessage()); }
/* 187 */     return null;
/*     */   }
/*     */   
/*     */   public Properties getServerPropertiesSmtp()
/*     */   {
/* 192 */     Properties props = new Properties();
/* 193 */     props.put("mail.smtp.auth", this.pr.findByName("mail.smtp.auth"));
/* 194 */     props.put("mail.smtp.starttls.enable", this.pr.findByName("mail.smtp.starttls.enable"));
/* 195 */     props.put("mail.smtp.host", this.pr.findByName("mail.smtp.host"));
/* 196 */     props.put("mail.smtp.port", this.pr.findByName("mail.smtp.port"));
/* 197 */     props.put("mail.debug", this.pr.findByName("mail.debug"));
/* 198 */     return props;
/*     */   }
/*     */   
/*     */   public Session getSession() {
/* 202 */     Session session = Session.getInstance(getServerPropertiesSmtp(), new Authenticator()
/*     */     {
/*     */       protected PasswordAuthentication getPasswordAuthentication()
/*     */       {
/* 206 */         return new PasswordAuthentication(EmailServicesImpl.this.pr.findByName("mail.smtp.email"), EmailServicesImpl.this.pr.findByName("mail.smtp.email.password"));
/*     */       }
/*     */       
/* 209 */     });
/* 210 */     return session;
/*     */   }
/*     */   
/*     */   private String getTextFromMessage(Message message) throws Exception {
/* 214 */     String result = "";
/* 215 */     if (message.isMimeType("text/plain")) {
/* 216 */       result = message.getContent().toString();
/* 217 */     } else if (message.isMimeType("multipart/*")) {
/* 218 */       MimeMultipart mimeMultipart = (MimeMultipart)message.getContent();
/* 219 */       result = getTextFromMimeMultipart(mimeMultipart);
/*     */     }
/* 221 */     return result;
/*     */   }
/*     */   
/*     */   private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception
/*     */   {
/* 226 */     String result = "";
/* 227 */     int count = mimeMultipart.getCount();
/* 228 */     for (int i = 0; i < count; i++) {
/* 229 */       BodyPart bodyPart = mimeMultipart.getBodyPart(i);
/* 230 */       if (bodyPart.isMimeType("text/plain")) {
/* 231 */         result = result + "\n" + bodyPart.getContent();
/* 232 */         break; }
/* 233 */       if (bodyPart.isMimeType("text/html")) {
/* 234 */         String html = (String)bodyPart.getContent();
/* 235 */         result = result + "\n" + html;
/* 236 */       } else if ((bodyPart.getContent() instanceof MimeMultipart)) {
/* 237 */         result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
/*     */       }
/*     */     }
/* 240 */     return result;
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\EmailServicesImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */