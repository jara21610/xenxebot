/*     */ package co.com.zien.xenxerobot.backend.services;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import com.sun.jersey.api.client.Client;
/*     */ import com.sun.jersey.api.client.ClientResponse;
/*     */ import com.sun.jersey.api.client.WebResource;
/*     */ import com.sun.jersey.api.client.WebResource.Builder;
/*     */ import java.io.PrintStream;
/*     */ import java.math.BigInteger;
/*     */ import java.util.UUID;
/*     */ import org.apache.commons.codec.digest.DigestUtils;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.core.env.Environment;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ @Service
/*     */ public class PaymentServicesImpl implements PaymentServices
/*     */ {
/*  19 */   private final String languaje = "es";
/*  20 */   private String CREATE__TOKEN_COMAND = "CREATE_TOKEN";
/*  21 */   private String SUBMIT_TRANSACTION = "SUBMIT_TRANSACTION";
/*  22 */   private String AUTHORIZATION_AND_CAPTURE = "AUTHORIZATION_AND_CAPTURE";
/*     */   @Autowired
/*     */   Environment env;
/*     */   
/*  26 */   public String getSignature(String apikey, String merchanid, String referenceCode, String value, String currency) { System.out.println("value" + value);
/*  27 */     return DigestUtils.md5Hex(apikey + "~" + merchanid + "~" + referenceCode + "~" + value + "~" + currency);
/*     */   }
/*     */   
/*     */   public String getPaymentMethodFromCreditCardNumber(String creditCardNumber)
/*     */   {
/*  32 */     if ((creditCardNumber.substring(0, 2).equals("51")) || (creditCardNumber.substring(0, 2).equals("52")) || (creditCardNumber.substring(0, 2).equals("53")) || 
/*  33 */       (creditCardNumber.substring(0, 2).equals("54")) || (creditCardNumber.substring(0, 2).equals("55")))
/*  34 */       return "MASTERCARD";
/*  35 */     if (creditCardNumber.substring(0, 2).equals("4"))
/*  36 */       return "VISA";
/*  37 */     if ((creditCardNumber.substring(0, 2).equals("34")) || (creditCardNumber.substring(0, 2).equals("37"))) {
/*  38 */       return "AMERICANEXPRESS";
/*     */     }
/*  40 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public RegisterCreditCardTokenResponse createCreditCardToken(String payerId, String name, String identificationNumber, String number, String expirationDate)
/*     */   {
/*  47 */     RegisterCreditCardTokenInput rccti = new RegisterCreditCardTokenInput();
/*     */     
/*  49 */     rccti.setLanguage("es");
/*  50 */     rccti.setCommand(this.CREATE__TOKEN_COMAND);
/*     */     
/*  52 */     MerchantInput mi = new MerchantInput();
/*     */     
/*  54 */     mi.setApiKey(this.env.getRequiredProperty("payu.apiKey"));
/*  55 */     mi.setApiLogin(this.env.getRequiredProperty("payu.apiLogin"));
/*     */     
/*  57 */     rccti.setMerchant(mi);
/*     */     
/*  59 */     CreditCardToken ccti = new CreditCardToken();
/*     */     
/*  61 */     ccti.setExpirationDate(expirationDate);
/*  62 */     ccti.setIdentificationNumber(identificationNumber);
/*  63 */     ccti.setName(name);
/*  64 */     ccti.setNumber(number);
/*  65 */     ccti.setPayerId(payerId);
/*  66 */     ccti.setPaymentMethod(getPaymentMethodFromCreditCardNumber(number));
/*     */     
/*     */ 
/*  69 */     rccti.setCreditCardToken(ccti);
/*     */     
/*  71 */     String uri = this.env.getRequiredProperty("payu.paymentsurl");
/*     */     
/*  73 */     Client client = Client.create();
/*     */     
/*  75 */     WebResource webResource = client
/*  76 */       .resource(uri);
/*     */     
/*     */ 
/*  79 */     Gson gson = new Gson();
/*  80 */     String input = gson.toJson(rccti);
/*     */     
/*  82 */     System.out.println(input);
/*     */     
/*  84 */     ClientResponse response = 
/*  85 */       (ClientResponse)((WebResource.Builder)webResource.type("application/json").accept(new String[] { "application/json" })).post(ClientResponse.class, input);
/*     */     
/*  87 */     if (response.getStatus() != 200) {
/*  88 */       throw new RuntimeException("Failed : HTTP error code : " + 
/*  89 */         response.getStatus());
/*     */     }
/*     */     
/*  92 */     String r = (String)response.getEntity(String.class);
/*  93 */     System.out.println(r);
/*  94 */     return (RegisterCreditCardTokenResponse)gson.fromJson(r, RegisterCreditCardTokenResponse.class);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PayOutput payWithCreditCardToken(String orderId, String paymentMethod, String creditCardTokenId, String currency, BigInteger amount, String payerId, String name, String identificationNumber)
/*     */   {
/* 102 */     PayInput pi = new PayInput();
/*     */     
/* 104 */     pi.setLanguage("es");
/* 105 */     pi.setCommand(this.SUBMIT_TRANSACTION);
/*     */     
/* 107 */     MerchantInput mi = new MerchantInput();
/*     */     
/* 109 */     mi.setApiKey(this.env.getRequiredProperty("payu.apiKey"));
/* 110 */     mi.setApiLogin(this.env.getRequiredProperty("payu.apiLogin"));
/*     */     
/* 112 */     pi.setMerchant(mi);
/*     */     
/* 114 */     Transaction t = new Transaction();
/*     */     
/* 116 */     Order o = new Order();
/*     */     
/* 118 */     o.setAccountId(this.env.getRequiredProperty("payu.accountId"));
/* 119 */     o.setReferenceCode(orderId);
/* 120 */     o.setDescription("pago plan");
/* 121 */     o.setLanguage("es");
/* 122 */     o.setSignature(getSignature(this.env.getRequiredProperty("payu.apiKey"), this.env.getRequiredProperty("payu.merchantId"), orderId, amount.toString(), currency));
/*     */     
/* 124 */     AdditionalValues additionalValues = new AdditionalValues();
/*     */     
/* 126 */     TXVALUE tXVALUE = new TXVALUE();
/*     */     
/* 128 */     tXVALUE.setValue(amount);
/* 129 */     tXVALUE.setCurrency(currency);
/*     */     
/*     */ 
/* 132 */     additionalValues.setTXVALUE(tXVALUE);
/*     */     
/* 134 */     o.setAdditionalValues(additionalValues);
/*     */     
/* 136 */     t.setOrder(o);
/*     */     
/* 138 */     Payer p = new Payer();
/*     */     
/* 140 */     p.setDniNumber(identificationNumber);
/* 141 */     p.setFullName(name);
/* 142 */     p.setMerchantPayerId(payerId);
/*     */     
/* 144 */     t.setPayer(p);
/* 145 */     t.setCreditCardTokenId(creditCardTokenId);
/*     */     
/* 147 */     ExtraParameters extraParameters = new ExtraParameters();
/*     */     
/* 149 */     extraParameters.setINSTALLMENTSNUMBER(1);
/*     */     
/* 151 */     t.setExtraParameters(extraParameters);
/*     */     
/* 153 */     t.setType(this.AUTHORIZATION_AND_CAPTURE);
/*     */     
/* 155 */     t.setPaymentMethod(paymentMethod);
/*     */     
/* 157 */     t.setPaymentCountry("CO");
/*     */     
/* 159 */     t.setDeviceSessionId(UUID.randomUUID().toString());
/*     */     
/* 161 */     t.setIpAddress(this.env.getRequiredProperty("payu.ipAddress"));
/*     */     
/* 163 */     t.setCookie(UUID.randomUUID().toString());
/*     */     
/* 165 */     t.setUserAgent("Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0");
/*     */     
/* 167 */     pi.setTransaction(t);
/*     */     
/* 169 */     pi.setTest(false);
/*     */     
/* 171 */     String uri = this.env.getRequiredProperty("payu.paymentsurl");
/*     */     
/* 173 */     Client client = Client.create();
/*     */     
/* 175 */     WebResource webResource = client
/* 176 */       .resource(uri);
/*     */     
/*     */ 
/* 179 */     Gson gson = new Gson();
/* 180 */     String input = gson.toJson(pi);
/*     */     
/* 182 */     System.out.println(input);
/*     */     
/* 184 */     ClientResponse response = 
/* 185 */       (ClientResponse)((WebResource.Builder)webResource.type("application/json").accept(new String[] { "application/json" })).post(ClientResponse.class, input);
/*     */     
/* 187 */     if (response.getStatus() != 200) {
/* 188 */       throw new RuntimeException("Failed : HTTP error code : " + 
/* 189 */         response.getStatus());
/*     */     }
/*     */     
/* 192 */     String r = (String)response.getEntity(String.class);
/* 193 */     System.out.println(r);
/* 194 */     return (PayOutput)gson.fromJson(r, PayOutput.class);
/*     */   }
/*     */ }


/* Location:              D:\Usuario\Downloads\xenxebot\WEB-INF\lib\backend-0.0.1-SNAPSHOT.jar!\co\com\zien\xenxerobot\backend\services\PaymentServicesImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */