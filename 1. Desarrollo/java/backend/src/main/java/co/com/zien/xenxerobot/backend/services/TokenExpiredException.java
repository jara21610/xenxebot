package co.com.zien.xenxerobot.backend.services;

public class TokenExpiredException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public TokenExpiredException() {
    }
    
    public TokenExpiredException(final String message) {
        super(message);
    }
    
    public TokenExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public TokenExpiredException(final Throwable cause) {
        super(cause);
    }
}
