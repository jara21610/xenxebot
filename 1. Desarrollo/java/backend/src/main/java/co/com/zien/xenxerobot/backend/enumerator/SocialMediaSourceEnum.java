package co.com.zien.xenxerobot.backend.enumerator;

public enum SocialMediaSourceEnum
{
    TWITTER("TWITTER", 0, "TWITTER"), 
    FACEBOOK("FACEBOOK", 1, "FACEBOOK"),
	INSTAGRAM("INSTAGRAM", 2, "INSTAGRAM");
    
    private String socialmediasource;
    
    public String getSocialmediasource() {
        return this.socialmediasource;
    }
    
    private SocialMediaSourceEnum(final String s, final int n, final String socialmediasource) {
        this.socialmediasource = socialmediasource;
    }
}
