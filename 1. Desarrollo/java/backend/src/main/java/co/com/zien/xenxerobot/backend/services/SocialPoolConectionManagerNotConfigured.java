package co.com.zien.xenxerobot.backend.services;

public class SocialPoolConectionManagerNotConfigured extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public SocialPoolConectionManagerNotConfigured(final String message) {
        super(message);
    }
}
