package co.com.zien.xenxerobot.backend.services;

public interface SentimentService
{
    SentimentAnalysisOutputDTO getSentimentAnalysis(final String p0, final String p1);
}
