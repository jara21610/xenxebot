package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import java.util.*;
import com.thoughtworks.xstream.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.io.*;
import org.springframework.context.i18n.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.scheduling.annotation.*;

@Service
public class PostProcessingServiceImpl implements PostProcessingService
{
    private static final Logger logger;
    @Autowired
    @Qualifier("TwitterProcessPost")
    SourceProcessPostService ts;
    @Autowired
    @Qualifier("FacebookProcessPost")
    SourceProcessPostService fs;
    @Autowired
    @Qualifier("InstagramProcessPost")
    SourceProcessPostService is;
    @Autowired
    PostRepository postr;
    @Autowired
    PreproccesingService scs;
    @Autowired
    SentimentService ss;
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    ReportDetailServiceFactory rdmf;
    @Autowired
    MessageSource messageSource;
    @Autowired
    UserRepository ur;
    @Autowired
    ResponseBotServiceFactory rbsf;
    
    static {
        logger = Logger.getLogger((Class)PostProcessingServiceImpl.class);
    }
    
    @Async
    @Override
    public void proccess(final ComandDTO cdto) {
        CommandProcess cp = new CommandProcess();
        String channel = "";
        Users u = null;
        final ResponseBotService rbs = this.rbsf.getResponseBotService(channel);
        try {
            channel = cdto.getChannel();
            final String socialmediacource = (String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument());
            cp.setBegindate(new Date());
            cp.setId(UUID.randomUUID().toString());
            cp.setChannel(ChannelEnum.TELEGRAM.getChannel());
            final String command = cdto.getCommand();
            cp.setComand(command);
            if (channel.equals(ChannelEnum.TELEGRAM.getChannel())) {
                u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
            }
            else if (channel.equals(ChannelEnum.EMAIL.getChannel())) {
                u = this.ur.isUserActivebyEmail(cdto.getCommunicationKey());
            }
            final XStream xs = new XStream();
            final String comanddetailarguments = xs.toXML((Object)cdto);
            cp.setArguments(comanddetailarguments);
            cp.setState(CommandProcessStateEnum.ENPROCESO.getState());
            cp.setUser(u);
            cp.setProductid((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            this.cpr.saveAndFlush(cp);
            rbs.isprocessingResponseBot(cdto);
            if (socialmediacource.equals(SocialMediaSourceEnum.TWITTER.getSocialmediasource())) {
                this.ts.process(cp, cdto);
            }
            else {
            	
            	if (socialmediacource.equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())){
            		this.fs.process(cp, cdto);
            	} else if (socialmediacource.equals(SocialMediaSourceEnum.INSTAGRAM.getSocialmediasource())) {
            		this.is.process(cp, cdto);
            	}
            }
            final int total = this.postr.totalPost(cp.getId());
            if (total > 0) {
                cp = (CommandProcess)this.cpr.findOne(cp.getId());
                if (!cp.getState().equals(CommandProcessStateEnum.CANCELADOXUSUARIO.getState())) {
                    final ReportDetailManager rdm = (ReportDetailManager) this.rdmf.getResponseBotService((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
                    final String reportpath = rdm.generateSentimentAnalysisReport(cp.getId());
                    cp.setState(CommandProcessStateEnum.FINALIZADO.getState());
                    cp.setFinishdate(new Date());
                    this.cpr.saveAndFlush(cp);
                    rbs.okResponseBot(reportpath, cdto);
                }
            }
            else {
                cp.setState(CommandProcessStateEnum.FINALIZADO.getState());
                cp.setFinishdate(new Date());
                this.cpr.saveAndFlush(cp);
                rbs.messageResponseBot(this.messageSource.getMessage("message.postnotfound.text", (Object[])null, LocaleContextHolder.getLocale()), cdto);
            }
        }
        catch (Exception e) {
            PostProcessingServiceImpl.logger.error((Object)e);
            e.printStackTrace();
            cp.setFinishdate(new Date());
            cp.setState(CommandProcessStateEnum.ERROR.getState());
            cp.setException(e.getMessage());
            this.cpr.saveAndFlush(cp);
            rbs.exceptionResponseBot(cdto, e);
        }
    }
    
    @Async
    @Override
    public void cancel(final String cpId) {
        final CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
        cp.setFinishdate(new Date());
        cp.setState(CommandProcessStateEnum.CANCELADOXUSUARIO.getState());
        this.cpr.saveAndFlush(cp);
    }
}
