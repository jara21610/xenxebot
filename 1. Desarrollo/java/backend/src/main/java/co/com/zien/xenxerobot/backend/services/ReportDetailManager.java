package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface ReportDetailManager
{
    void saveStatus(final CommandProcess p0, final ReportDetailDTO p1);
    
    String generateSentimentAnalysisReport(final String p0) throws Exception;
    
    void reportdetailgenerateSentimentAnalysisReport(final String p0) throws Exception;
}
