package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.scheduling.annotation.*;

@Service
public class BotLauncherImpl implements BotLauncher
{
    @Autowired
    EmailBot eb;
    
    @Async
    public void launchBots() {
        this.eb.checkNewCommands();
    }
}
