package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import java.util.concurrent.*;
import java.util.stream.*;
import com.restfb.types.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.io.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import co.com.zien.xenxerobot.persistence.entity.Post;

import java.util.*;
import org.springframework.social.twitter.api.*;

@Service("FacebookProcessUnit")
public class FacebookProcessUnitImpl implements SourceProcessUnit
{
    @Autowired
    PostRepository postr;
    @Autowired
    PreproccesingService scs;
    @Autowired
    SentimentService ss;
    @Autowired
    ConceptsRepository cr;
    @Autowired
    EmailService es;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    ReportDetailServiceFactory rdmf;
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    LoadStressTestService lsts;
    @Autowired
    ResponseBotServiceFactory rbsf;
    @Autowired
    TransactionCounterService tcs;
    @Autowired
    UserplanRepository upr;
    
    public CompletableFuture<String> executeUnit(final List currentlt, CommandProcess cp, final ComandDTO cdto) {
        final Date datesince = (Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument());
        final Date dateuntil = (Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument());
        final String servicetype = (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument());
        
        List<Comment> clist = currentlt;
        clist = (clist.stream().sorted(Comparator.comparing(Comment::getCreatedTime).reversed()).collect(Collectors.toList()));
        final int txoutofrange = (clist.stream().filter(t -> t.getCreatedTime().before(datesince)).collect(Collectors.toList())).size();
        int permitedtx = 0;
        final int txtoreserve = currentlt.size() - txoutofrange;
        try {
            permitedtx = this.tcs.addUsedTransactionPlan(cp.getUser().getId(), txtoreserve, servicetype);
        }
        catch (TxLimitException e2) {
            return CompletableFuture.completedFuture("stop");
        }
        
        for (final Comment c : clist) {
            final String isloadstreesstestactive = this.pr.findByName(PropertyEnum.LOADSTRESSTESTACTIVE.getProperty());
            
            if (c.getCreatedTime().after(datesince) && c.getCreatedTime().before(dateuntil)) {
                final long beforetotal = System.currentTimeMillis();
                final Post p = new Post();
                p.setIdpost(UUID.randomUUID().toString());
                p.setLanguagepost("");
                p.setPostdate(c.getCreatedTime());
                p.setOriginalmessage((c.getMessage().length() < 2500) ? c.getMessage() : c.getMessage().substring(0, 2450));
                final long beforepreprossecing = System.currentTimeMillis();
                String fm = this.scs.preproccesingText(p.getOriginalmessage());
                p.setFormattedmessage((fm.length() < 2500) ? fm : fm.substring(0, 2450));
                final double totalpreprocesing = (System.currentTimeMillis() - beforepreprossecing) / 1000.0;
                p.setUserpost("");
                cp = (CommandProcess)this.cpr.findOne(cp.getId());
                p.setCommandprocess(cp);
                this.postr.saveAndFlush(p);
                SentimentAnalysisOutputDTO sadto = null;
                double totalservice = 0.0;
                try {
                    final long beforeservice = System.currentTimeMillis();
                    sadto = this.ss.getSentimentAnalysis(p.getFormattedmessage(), servicetype);
                    totalservice = (System.currentTimeMillis() - beforeservice) / 1000.0;
                }
                catch (Exception ex) {}
                if (sadto != null) {
                    p.setSentiment(sadto.getSentiment());
                    p.setTopic(sadto.getCategory());
                    p.setConceptsCollection((Collection)new ArrayList());                    
                    if (sadto.getConceptlist() != null) {
	                    for (final SentimentAnalysisConceptOutputDTO co : sadto.getConceptlist()) {
	                        final Concepts concept = new Concepts();
	                        concept.setId(UUID.randomUUID().toString());
	                        concept.setIdPost(p);
	                        concept.setConceptType(co.getType());
	                        concept.setMachineLearningSentiment(co.getSentiment());
	                        concept.setName((co.getName().length() < 100) ? co.getName() : co.getName().substring(0, 100));
	                        this.cr.saveAndFlush(concept);
	                        p.getConceptsCollection().add(concept);
	                    }
                    }
                }
                this.postr.saveAndFlush(p);
                final double totalResponseTime = (System.currentTimeMillis() - beforetotal) / 1000.0;
                if (!isloadstreesstestactive.equals("Y")) {
                    continue;
                }
                final LoadStressProcessingPost lstdto = new LoadStressProcessingPost();
                lstdto.setTotalservice(totalservice);
                lstdto.setTotaltimepreprocesing(totalpreprocesing);
                lstdto.setTotaltime(totalResponseTime);
                try {
                    this.lsts.putDataProcessingPost(lstdto);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                if (c.getCreatedTime().before(datesince)) {
                    return CompletableFuture.completedFuture("stop");
                }
                continue;
            }
        }
        return CompletableFuture.completedFuture("");
    }
}
