package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface PlanService
{
	public Userplan getLastUserPlan(final String idUser, String productid);
}
