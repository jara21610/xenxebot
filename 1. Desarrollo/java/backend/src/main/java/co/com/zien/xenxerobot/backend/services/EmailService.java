package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public interface EmailService
{
    void sendEmail(final String p0, final String p1, final String p2, final String p3);
    
    void sendHtmlEmail(final String p0, final String p1, final String p2, final String p3, final Map<String, String> p4);
    
    void sendEmail(final String p0, final String p1, final String p2, final String p3, final String[] p4);
    
    List<EmailMessageDTO> getNewEmailsImap();
}
