package co.com.zien.xenxerobot.backend.services;

public class RegisterCreditCardTokenInput
{
    private String language;
    private String command;
    private MerchantInput merchant;
    private CreditCardToken creditCardToken;
    
    public String getLanguage() {
        return this.language;
    }
    
    public void setLanguage(final String language) {
        this.language = language;
    }
    
    public String getCommand() {
        return this.command;
    }
    
    public void setCommand(final String command) {
        this.command = command;
    }
    
    public MerchantInput getMerchant() {
        return this.merchant;
    }
    
    public void setMerchant(final MerchantInput merchant) {
        this.merchant = merchant;
    }
    
    public CreditCardToken getCreditCardToken() {
        return this.creditCardToken;
    }
    
    public void setCreditCardToken(final CreditCardToken creditCardToken) {
        this.creditCardToken = creditCardToken;
    }
}
