package co.com.zien.xenxerobot.backend.services;

import org.junit.runner.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.context.*;
import co.com.zien.xenxerobot.backend.config.*;
import org.springframework.test.context.support.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.*;
import org.junit.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class }, loader = AnnotationConfigContextLoader.class)
public class PostProcessingServiceImplTest
{
    @Autowired
    PostProcessingService pps;
    
    @Test
    public void testPerformanceProccess() {
        for (int threads = 1, i = 0; i < threads; ++i) {
            final ComandDTO cdto = new ComandDTO();
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
            cdto.setArguments(new HashMap<String, Object>());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), "@petrogustavo");
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
            cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
            cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
            final Calendar cal = Calendar.getInstance();
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), cal.getTime());
            cal.add(5, -8);
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), cal.getTime());
            this.pps.proccess(cdto);
        }
    }
}
