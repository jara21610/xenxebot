package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import co.com.zien.xenxerobot.backend.telegram.bot.ServiceBot;
import co.com.zien.xenxerobot.backend.telegram.bot.XenxeBot;

import org.springframework.context.i18n.*;
import java.io.*;
import org.telegram.telegrambots.bots.*;
import co.com.zien.xenxerobot.backend.config.*;

import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import co.com.zien.xenxerobot.persistence.entity.*;
import java.util.*;

@Service("TelegramResponseBot")
public class TelegramResponseBotServiceImpl implements ResponseBotService
{
    @Autowired
    PropertiesRepository pr;
    @Autowired
    MessageSource messageSource;
    @Autowired
    EmailService es;
    @Autowired
    UserRepository ur;
    @Autowired
    PlanService ps;
    
    @Override
    public void okResponseBot(final String reportpath, final ComandDTO cdto) {
        SendMessage message = new SendMessage();
        message.setChatId(cdto.getCommunicationKey());
        message.setReplyMarkup((ReplyKeyboard)this.getMainMenuKeyboard());
        message.setText(this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()) }, LocaleContextHolder.getLocale()));
        final SendDocument document = new SendDocument();
        document.setChatId(cdto.getCommunicationKey());
        document.setDocument(new InputFile(new File(reportpath)));
        try {
            final TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.execute(message);
            xb.execute(document);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
        final Users u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), u.getEmail(), this.messageSource.getMessage("message.finishsentimentanalysis.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), new String[] { reportpath });
        
        final String servicetype = (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument());
        final Userplan up = ps.getLastUserPlan(u.getId(), ServiceTypeEnum.XENXE.getServicetype());
        if (!up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType()) && up.getState().equals(PlanStateEnum.FINALIZADO.getState()) && up.getTxpalused() >= up.getTxplancapacity()) {
        	ServiceBot xb = (ServiceBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.handlePlanTxFinished(cdto.getCommunicationKey(), u);
        } 
    }
    
    @Override
    public void okPartialReportResponseBot(final String reportpath, final ComandDTO cdto) {
        final SendMessage message = new SendMessage();
        message.setChatId(cdto.getCommunicationKey());
        message.setText(this.messageSource.getMessage("message.reportdetailsentimentanalysis.text", (Object[])null, LocaleContextHolder.getLocale()));
        final SendDocument document = new SendDocument();
        document.setChatId(cdto.getCommunicationKey());
        document.setDocument(new InputFile(new File(reportpath)));
        try {
            final TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.execute(message);
            xb.execute(document);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
        final Users u = this.ur.isUserActivebyChatid(cdto.getCommunicationKey());
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), u.getEmail(), this.messageSource.getMessage("message.reportdetailsentimentanalysis.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.reportdetailsentimentanalysis.text", (Object[])null, LocaleContextHolder.getLocale()), new String[] { reportpath });
    }
    
    @Override
    public void exceptionResponseBot(final ComandDTO cdto, final Exception e) {
        final SendMessage message = new SendMessage();
        		message.setChatId(cdto.getCommunicationKey());
        		message.setReplyMarkup((ReplyKeyboard)this.getMainMenuKeyboard());
        		message.setText(this.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            final TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.execute(message);
        }
        catch (TelegramApiException e2) {
            e2.printStackTrace();
        }
    }
    
    @Override
    public void messageResponseBot(final String text, final ComandDTO cdto) {
        final SendMessage message = new SendMessage();
        message.setChatId(cdto.getCommunicationKey());
        message.setReplyMarkup((ReplyKeyboard)this.getMainMenuKeyboard());
        message.setText(text);
        try {
            final TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    @Override
    public void isprocessingResponseBot(final ComandDTO cdto) {
        final SendMessage message = new SendMessage();
        message.setChatId(cdto.getCommunicationKey());
        message.setReplyMarkup(this.getPushPartialMenuKeyboard());
        message.setText(this.messageSource.getMessage("message.isprocesing.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()));
        try {
            final TelegramLongPollingBot xb = (TelegramLongPollingBot)Config.getContext().getBean((String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()));
            xb.execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private ReplyKeyboard getPushPartialMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.cancel.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return (ReplyKeyboard)replyKeyboardMarkup;
    }
    
    private ReplyKeyboardMarkup getMainMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(this.messageSource.getMessage("messsage.telegram.command.sentiment.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
}
