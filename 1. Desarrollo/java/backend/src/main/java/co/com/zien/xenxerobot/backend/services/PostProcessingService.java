package co.com.zien.xenxerobot.backend.services;

public interface PostProcessingService
{
    void proccess(final ComandDTO p0);
    
    void cancel(final String p0);
}
