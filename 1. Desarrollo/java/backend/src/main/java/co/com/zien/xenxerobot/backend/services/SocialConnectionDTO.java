package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class SocialConnectionDTO
{
    private String keyData1;
    private String keyData2;
    private int remaining;
    private int limitcallsize;
    private int windowminutesize;
    private int secondsuntilreset;
    private Date windowStart;
    private Date windowFinish;
    private Object connection;
    private boolean active;
    
    public SocialConnectionDTO() {
        this.windowFinish = null;
    }
    
    public String getKeyData1() {
        return this.keyData1;
    }
    
    public void setKeyData1(final String keyData1) {
        this.keyData1 = keyData1;
    }
    
    public String getKeyData2() {
        return this.keyData2;
    }
    
    public void setKeyData2(final String keyData2) {
        this.keyData2 = keyData2;
    }
    
    public Date getWindowStart() {
        return this.windowStart;
    }
    
    public void setWindowStart(final Date windowStart) {
        this.windowStart = windowStart;
    }
    
    public Date getWindowFinish() {
        return this.windowFinish;
    }
    
    public void setWindowFinish(final Date windowFinish) {
        this.windowFinish = windowFinish;
    }
    
    public int getSecondsuntilreset() {
        return this.secondsuntilreset;
    }
    
    public void setSecondsuntilreset(final int secondsuntilreset) {
        this.secondsuntilreset = secondsuntilreset;
    }
    
    public Object getConnection() {
        return this.connection;
    }
    
    public void setConnection(final Object connection) {
        this.connection = connection;
    }
    
    public int getRemaining() {
        return this.remaining;
    }
    
    public void setRemaining(final int remaining) {
        this.remaining = remaining;
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void setActive(final boolean active) {
        this.active = active;
    }
    
    public int getWindowminutesize() {
        return this.windowminutesize;
    }
    
    public void setWindowminutesize(final int windowminutesize) {
        this.windowminutesize = windowminutesize;
    }
    
    public int getLimitcallsize() {
        return this.limitcallsize;
    }
    
    public void setLimitcallsize(final int limitcallsize) {
        this.limitcallsize = limitcallsize;
    }
}
