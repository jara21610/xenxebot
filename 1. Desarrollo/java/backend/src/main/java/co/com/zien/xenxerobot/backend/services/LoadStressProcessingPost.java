package co.com.zien.xenxerobot.backend.services;

public class LoadStressProcessingPost
{
    private double totaltime;
    private double totaltimepreprocesing;
    private double totalservice;
    
    public double getTotaltime() {
        return this.totaltime;
    }
    
    public void setTotaltime(final double totaltime) {
        this.totaltime = totaltime;
    }
    
    public double getTotaltimepreprocesing() {
        return this.totaltimepreprocesing;
    }
    
    public void setTotaltimepreprocesing(final double totaltimepreprocesing) {
        this.totaltimepreprocesing = totaltimepreprocesing;
    }
    
    public double getTotalservice() {
        return this.totalservice;
    }
    
    public void setTotalservice(final double totalservice) {
        this.totalservice = totalservice;
    }
}
