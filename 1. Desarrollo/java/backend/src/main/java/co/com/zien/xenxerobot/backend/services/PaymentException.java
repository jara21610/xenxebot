package co.com.zien.xenxerobot.backend.services;

public class PaymentException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public PaymentException() {
    }
    
    public PaymentException(final String message) {
        super(message);
    }
    
    public PaymentException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public PaymentException(final Throwable cause) {
        super(cause);
    }
}
