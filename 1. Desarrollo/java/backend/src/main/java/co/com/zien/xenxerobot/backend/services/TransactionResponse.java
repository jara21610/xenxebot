package co.com.zien.xenxerobot.backend.services;

import java.math.*;
import java.util.*;

public class TransactionResponse
{
    private int orderId;
    private String transactionId;
    private String state;
    private String paymentNetworkResponseCode;
    private Object paymentNetworkResponseErrorMessage;
    private String trazabilityCode;
    private String authorizationCode;
    private Object pendingReason;
    private String responseCode;
    private Object errorCode;
    private Object responseMessage;
    private String transactionDate;
    private String transactionTime;
    private BigInteger operationDate;
    private Object referenceQuestionnaire;
    private Object extraParameters;
    private AdditionalInfo additionalInfo;
    private Map<String, Object> additionalProperties;
    
    public TransactionResponse() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    public int getOrderId() {
        return this.orderId;
    }
    
    public void setOrderId(final int orderId) {
        this.orderId = orderId;
    }
    
    public String getTransactionId() {
        return this.transactionId;
    }
    
    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public String getPaymentNetworkResponseCode() {
        return this.paymentNetworkResponseCode;
    }
    
    public void setPaymentNetworkResponseCode(final String paymentNetworkResponseCode) {
        this.paymentNetworkResponseCode = paymentNetworkResponseCode;
    }
    
    public Object getPaymentNetworkResponseErrorMessage() {
        return this.paymentNetworkResponseErrorMessage;
    }
    
    public void setPaymentNetworkResponseErrorMessage(final Object paymentNetworkResponseErrorMessage) {
        this.paymentNetworkResponseErrorMessage = paymentNetworkResponseErrorMessage;
    }
    
    public String getTrazabilityCode() {
        return this.trazabilityCode;
    }
    
    public void setTrazabilityCode(final String trazabilityCode) {
        this.trazabilityCode = trazabilityCode;
    }
    
    public String getAuthorizationCode() {
        return this.authorizationCode;
    }
    
    public void setAuthorizationCode(final String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }
    
    public Object getPendingReason() {
        return this.pendingReason;
    }
    
    public void setPendingReason(final Object pendingReason) {
        this.pendingReason = pendingReason;
    }
    
    public String getResponseCode() {
        return this.responseCode;
    }
    
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }
    
    public Object getErrorCode() {
        return this.errorCode;
    }
    
    public void setErrorCode(final Object errorCode) {
        this.errorCode = errorCode;
    }
    
    public Object getResponseMessage() {
        return this.responseMessage;
    }
    
    public void setResponseMessage(final Object responseMessage) {
        this.responseMessage = responseMessage;
    }
    
    public String getTransactionDate() {
        return this.transactionDate;
    }
    
    public void setTransactionDate(final String transactionDate) {
        this.transactionDate = transactionDate;
    }
    
    public String getTransactionTime() {
        return this.transactionTime;
    }
    
    public void setTransactionTime(final String transactionTime) {
        this.transactionTime = transactionTime;
    }
    
    public BigInteger getOperationDate() {
        return this.operationDate;
    }
    
    public void setOperationDate(final BigInteger operationDate) {
        this.operationDate = operationDate;
    }
    
    public Object getReferenceQuestionnaire() {
        return this.referenceQuestionnaire;
    }
    
    public void setReferenceQuestionnaire(final Object referenceQuestionnaire) {
        this.referenceQuestionnaire = referenceQuestionnaire;
    }
    
    public Object getExtraParameters() {
        return this.extraParameters;
    }
    
    public void setExtraParameters(final Object extraParameters) {
        this.extraParameters = extraParameters;
    }
    
    public AdditionalInfo getAdditionalInfo() {
        return this.additionalInfo;
    }
    
    public void setAdditionalInfo(final AdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
}
