package co.com.zien.xenxerobot.backend.telegram.bot;

import org.telegram.telegrambots.bots.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import co.com.zien.xenxerobot.backend.config.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.*;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import co.com.zien.xenxerobot.backend.services.*;

public class StressLoadTestBot extends TelegramLongPollingBot
{
    private static PropertiesRepository pr;
    private static MessageSource messageSource;
    private static PostProcessingService pps;
    private static UserRepository ur;
    private static CommandProcessRepository cpr;
    private static FacebookService fs;
    private static ReportDetailManager rdm;
    private static PostRepository postr;
    private static ReportDetailServiceFactory rdmf;
    private static LoadStressTestService lsts;
    private static Map<String, ComandDTO> usercommandstate;
    private static Environment env;
    
    static {
        StressLoadTestBot.usercommandstate = null;
        StressLoadTestBot.pr = (PropertiesRepository)Config.getContext().getBean((Class)PropertiesRepository.class);
        StressLoadTestBot.messageSource = (MessageSource)Config.getContext().getBean((Class)MessageSource.class);
        StressLoadTestBot.pps = (PostProcessingService)Config.getContext().getBean((Class)PostProcessingService.class);
        StressLoadTestBot.ur = (UserRepository)Config.getContext().getBean((Class)UserRepository.class);
        StressLoadTestBot.fs = (FacebookService)Config.getContext().getBean((Class)FacebookService.class);
        StressLoadTestBot.cpr = (CommandProcessRepository)Config.getContext().getBean((Class)CommandProcessRepository.class);
        StressLoadTestBot.postr = (PostRepository)Config.getContext().getBean((Class)PostRepository.class);
        StressLoadTestBot.rdmf = (ReportDetailServiceFactory)Config.getContext().getBean((Class)ReportDetailServiceFactory.class);
        StressLoadTestBot.lsts = (LoadStressTestService)Config.getContext().getBean((Class)LoadStressTestService.class);
        StressLoadTestBot.env = (Environment)Config.getContext().getBean((Class)Environment.class);
    }
    
    public void onUpdateReceived(final Update update) {
        final String chatid = update.getMessage().getChatId().toString();
        if (update.hasMessage() && update.getMessage().hasText()) {
            final String receivedmessage = update.getMessage().getText();
            final List<String> receivedmessagelist = new ArrayList<String>(Arrays.asList(receivedmessage.split(" ")));
            final String command = receivedmessagelist.get(0);
            if (CommandEnum.START.getCommand().equals(command)) {
                final int threads = new Integer(receivedmessagelist.get(1));
                this.stressloadtestlaucher(threads, chatid);
            }
            else if (CommandEnum.PARTIALREPORT.getCommand().equals(command)) {
                this.partialreport(chatid);
            }
            else if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(command)) {
                final String search = receivedmessagelist.get(1);
                this.aplicationsentiment(chatid, search);
            }
        }
    }
    
    private void aplicationsentiment(final String chatid, final String search) {
        final ComandDTO cdto1 = new ComandDTO();
        cdto1.setChannel(ChannelEnum.TELEGRAM.getChannel());
        cdto1.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        cdto1.setCommunicationKey(chatid);
        cdto1.setArguments(new HashMap<String, Object>());
        cdto1.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), search);
        cdto1.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
        cdto1.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
        cdto1.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
        final Calendar cal = Calendar.getInstance();
        cdto1.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), cal.getTime());
        cal.add(11, -24);
        cdto1.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), cal.getTime());
        StressLoadTestBot.pps.proccess(cdto1);
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setText("Consulta Lanzada");
        try {
            this.execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void stressloadtestlaucher(final int threads, final String chatid) {
        for (int i = 0; i < threads; ++i) {
            final ComandDTO cdto = new ComandDTO();
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
            cdto.setArguments(new HashMap<String, Object>());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), "@petrogustavo");
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
            cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
            cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
            final Calendar cal = Calendar.getInstance();
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), cal.getTime());
            cal.add(5, -8);
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), cal.getTime());
            StressLoadTestBot.pps.proccess(cdto);
        }
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setText("La carga ha sido aplicada");
        try {
            this.execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void partialreport(final String chatid) {
        ResumeSnapshotProcessingPostDTO rspp = null;
        try {
            rspp = StressLoadTestBot.lsts.getResumeSnapshotProcessingPost();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setText("Tiempos promedio Total Post Analizados= " + rspp.getTotal() + " Promedio Servicio = " + rspp.getTotalservice() + " Promedio Preprocesamiento = " + rspp.getTotaltimepreprocesing() + " Promedio Total = " + rspp.getTotaltime());
        try {
            this.execute(message);
        }
        catch (TelegramApiException e2) {
            e2.printStackTrace();
        }
    }
    
    public String getBotUsername() {
        return "xenxeloadstresstestbot";
    }
    
    public String getBotToken() {
        return StressLoadTestBot.env.getRequiredProperty("loadstressbot.token");
    }
}
