package co.com.zien.xenxerobot.backend.services;

public class ExtraParameters
{
    private int iNSTALLMENTSNUMBER;
    
    public int getINSTALLMENTSNUMBER() {
        return this.iNSTALLMENTSNUMBER;
    }
    
    public void setINSTALLMENTSNUMBER(final int iNSTALLMENTSNUMBER) {
        this.iNSTALLMENTSNUMBER = iNSTALLMENTSNUMBER;
    }
}
