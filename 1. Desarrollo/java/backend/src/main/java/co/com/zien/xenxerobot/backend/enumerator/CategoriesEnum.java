package co.com.zien.xenxerobot.backend.enumerator;

public enum CategoriesEnum
{
    ENTRETENIMIENTO("ENTRETENIMIENTO", 0, "ENTRETENIMIENTO"), 
    OTROS("OTROS", 1, "OTROS"), 
    CINE("CINE", 2, "CINE"), 
    FUTBOL("FUTBOL", 3, "FUTBOL"), 
    TECNOLOGIA("TECNOLOGIA", 4, "TECNOLOGIA"), 
    DEPORTES("DEPORTES", 5, "DEPORTES"), 
    MUSICA("MUSICA", 6, "MUSICA"), 
    LITERATURA("LITERATURA", 7, "LITERATURA"), 
    POLITICA("POLITICA", 8, "POLITICA"), 
    ECONOMIA("ECONOMIA", 9, "ECONOMIA");
    
    private String category;
    
    public String getCategory() {
        return this.category;
    }
    
    private CategoriesEnum(final String s, final int n, final String category) {
        this.category = category;
    }
}
