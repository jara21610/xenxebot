package co.com.zien.xenxerobot.backend.services;

public interface ResponseBotServiceFactory
{
    ResponseBotService getResponseBotService(final String p0);
}
