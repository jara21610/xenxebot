package co.com.zien.xenxerobot.backend.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.com.zien.xenxerobot.backend.config.Config;
import co.com.zien.xenxerobot.persistence.entity.Paymentmethod;
import co.com.zien.xenxerobot.persistence.entity.Userplan;
import co.com.zien.xenxerobot.persistence.entity.Users;
import co.com.zien.xenxerobot.persistence.repository.PaymentmethodRepository;
import co.com.zien.xenxerobot.persistence.repository.UserRepository;
import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Config.class})
public class InvoiceReportImplTest {
	
	@Autowired
	InvoiceReport ir;
	
	
	@Autowired
	UserplanRepository upr;
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	PaymentmethodRepository pmr;
	
	@Test
	public void testGenerateInvoice() {
		
		Userplan up = upr.findOne("2a702b8d-d471-46cc-bbc8-9a115ae34d68");
		Paymentmethod pm = pmr.findOne("504dee85-e6a6-4090-bd28-fc4aeb52a2b4");
		Users u = ur.findOne("0148d9fd-9d57-4fc5-994d-1e3121974cd4");
		
		try {
			ir.generateInvoice(u, up, pm);
			assertTrue(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
