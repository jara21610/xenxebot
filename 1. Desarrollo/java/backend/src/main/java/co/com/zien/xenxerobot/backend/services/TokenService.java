package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface TokenService
{
	public String generateUserToken(final Users userId, String productid);
    
    Users validateUserToken(final String p0) throws TokenNotExistException, TokenExpiredException;
    
    Users getUserOwnerToken(final String p0) throws TokenNotExistException;
    
    String getProductIdToken(final String usertoken) throws TokenNotExistException;
}
