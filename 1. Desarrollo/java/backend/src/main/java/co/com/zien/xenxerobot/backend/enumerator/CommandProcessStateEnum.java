package co.com.zien.xenxerobot.backend.enumerator;

public enum CommandProcessStateEnum
{
    ERROR("ERROR", 0, "ERROR"), 
    ENPROCESO("ENPROCESO", 1, "ENPROCESO"), 
    FINALIZADO("FINALIZADO", 2, "FINALIZADO"), 
    CANCELADOXUSUARIO("CANCELADOXUSUARIO", 3, "CANCELADOXUSUARIO");
    
    private String state;
    
    public String getState() {
        return this.state;
    }
    
    private CommandProcessStateEnum(final String s, final int n, final String state) {
        this.state = state;
    }
}
