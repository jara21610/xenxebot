package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.env.*;
import org.springframework.context.i18n.*;
import java.util.*;
import java.io.*;
import java.text.*;

@Service
public class LoadStressTestServiceImpl implements LoadStressTestService
{
    int total;
    double totalservicetime;
    double totalpreprocessingtime;
    double totaltime;
    ResumeSnapshotProcessingPostDTO rsppdto;
    String filename;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    Environment env;
    
    public LoadStressTestServiceImpl() {
        this.total = 0;
        this.totalservicetime = 0.0;
        this.totalpreprocessingtime = 0.0;
        this.totaltime = 0.0;
        this.rsppdto = null;
        this.filename = "";
    }
    
    @Override
    public synchronized void putDataProcessingPost(final LoadStressProcessingPost lstdto) throws Exception {
        BufferedWriter bw = null;
        FileWriter fw = null;
        final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
        ++this.total;
        File file = null;
        final String ruta = this.env.getRequiredProperty("outputpath");
        if (this.rsppdto == null) {
            this.rsppdto = new ResumeSnapshotProcessingPostDTO();
            this.filename = String.valueOf(UUID.randomUUID().toString()) + "_" + df.format(new Date());
            file = new File(ruta, this.filename);
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
        }
        else {
            file = new File(ruta, this.filename);
            fw = new FileWriter(file, true);
            bw = new BufferedWriter(fw);
            bw.newLine();
        }
        this.totalservicetime += lstdto.getTotalservice();
        this.totalpreprocessingtime += lstdto.getTotaltimepreprocesing();
        this.totaltime += lstdto.getTotaltime();
        this.rsppdto.setTotal(this.total);
        this.rsppdto.setTotalservice(this.totalservicetime / this.total);
        this.rsppdto.setTotaltime(this.totaltime / this.total);
        this.rsppdto.setTotaltimepreprocesing(this.totalpreprocessingtime / this.total);
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        bw.write(String.valueOf(dateFormat.format(new Date())) + "," + lstdto.getTotalservice() + "," + lstdto.getTotaltimepreprocesing() + "," + lstdto.getTotaltime());
        bw.close();
    }
    
    @Override
    public ResumeSnapshotProcessingPostDTO getResumeSnapshotProcessingPost() throws Exception {
        return this.rsppdto;
    }
}
