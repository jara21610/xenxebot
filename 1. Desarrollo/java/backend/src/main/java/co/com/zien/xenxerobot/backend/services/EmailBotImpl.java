package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.context.i18n.*;
import java.util.*;
import co.com.zien.xenxerobot.persistence.entity.*;

@Service
public class EmailBotImpl implements EmailBot
{
    private static final Logger logger;
    @Autowired
    EmailService es;
    @Autowired
    UserRepository ur;
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    MessageSource messageSource;
    @Autowired
    CommandValidatorService cvs;
    @Autowired
    PostProcessingService pps;
    
    static {
        logger = Logger.getLogger((Class)EmailBotImpl.class);
    }
    
    public void checkNewCommands() {
        while (true) {
            final List<EmailMessageDTO> messages = this.es.getNewEmailsImap();
            if (messages != null) {
                for (final EmailMessageDTO message : messages) {
                    final String useremail = message.getFrom();
                    if (useremail != null) {
                        final Users u = this.ur.isUserActivebyEmail(useremail);
                        if (u != null) {
                            ComandDTO cdto = null;
                            try {
                                message.setText(message.getText().replace("\n", "").replace("\r", ""));
                                cdto = this.cvs.validateCommand(message.getText());
                                if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(cdto.getCommand())) {
                                    cdto.setChannel(ChannelEnum.EMAIL.getChannel());
                                    cdto.setCommunicationKey(useremail);
                                    cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.XENXE.getServicetype());
                                    this.pps.proccess(cdto);
                                }
                                else {
                                    if (!CommandEnum.HELP.getCommand().equals(cdto.getCommand())) {
                                        continue;
                                    }
                                    this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, this.messageSource.getMessage("message.emailbot.help.subject", (Object[])null, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.emailbot.help.text", (Object[])null, LocaleContextHolder.getLocale()));
                                }
                            }
                            catch (CommandMalformedException e) {
                                this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, this.messageSource.getMessage("message.commandmalformed.subject", new Object[] { message.getText() }, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.commandmalformed.text", (Object[])null, LocaleContextHolder.getLocale()));
                            }
                        }
                        else {
                            this.es.sendEmail(this.pr.findByName("mail.smtp.email"), useremail, this.messageSource.getMessage("message.notvalidemailsource.subject", (Object[])null, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.notvalidemailsource.text", (Object[])null, LocaleContextHolder.getLocale()));
                        }
                    }
                }
            }
        }
    }
}
