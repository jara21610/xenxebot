package co.com.zien.xenxerobot.backend.enumerator;

public enum CommandEnum
{
    APPLICATIONSENTIMENT("APPLICATIONSENTIMENT", 0, "/sentiment"), 
    START("START", 1, "/start"), 
    TELEGRAMSENTIMENT("TELEGRAMSENTIMENT", 2, "/AnalisisSentimiento"), 
    TWITTER("TWITTER", 3, "/Twitter"), 
    FACEBOOK("FACEBOOK", 4, "/Facebook"), 
    USUARIO("USUARIO", 5, "/Usuario"), 
    HASHTAG("HASHTAG", 6, "/Hashtag"), 
    PAGE("PAGE", 7, "/Pagina"), 
    POST("POST", 8, "/Post"), 
    PPALMENU("PPALMENU", 9, "/MenuPrincipal"), 
    HELP("HELP", 10, "/Help"), 
    PARTIALREPORT("PARTIALREPORT", 11, "/ReporteParcial"), 
    COMANDSTATUS("COMANDSTATUS", 12, "/MisAnalisis"), 
    INFLUENCERENNROLL("INFLUENCERENNROLL", 13, "/RegistroInfluenciador"), 
    CANCEL("CANCEL", 14, "/Cancelar"), 
    FINALIZEREGISTER("FINALIZEREGISTER", 15, "/FinalizarRegistro"), 
    DELETEREGISTER("DELETEREGISTER", 16, "/BorrarRegistro"),
	INSTAGRAM("INSTAGRAM", 17, "/Instagram");
    
    private String command;
    
    public String getCommand() {
        return this.command;
    }
    
    private CommandEnum(final String s, final int n, final String command) {
        this.command = command;
    }
}
