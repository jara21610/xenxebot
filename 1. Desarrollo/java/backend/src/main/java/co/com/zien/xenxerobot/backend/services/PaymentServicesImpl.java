package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.core.env.*;
import org.springframework.beans.factory.annotation.*;
import org.apache.commons.codec.digest.*;
import com.google.gson.*;
import com.sun.jersey.api.client.*;
import java.math.*;
import java.util.*;

@Service
public class PaymentServicesImpl implements PaymentServices
{
    private final String languaje = "es";
    private String CREATE__TOKEN_COMAND;
    private String SUBMIT_TRANSACTION;
    private String AUTHORIZATION_AND_CAPTURE;
    @Autowired
    Environment env;
    
    public PaymentServicesImpl() {
        this.CREATE__TOKEN_COMAND = "CREATE_TOKEN";
        this.SUBMIT_TRANSACTION = "SUBMIT_TRANSACTION";
        this.AUTHORIZATION_AND_CAPTURE = "AUTHORIZATION_AND_CAPTURE";
    }
    
    @Override
    public String getSignature(final String apikey, final String merchanid, final String referenceCode, final String value, final String currency) {
        System.out.println("value" + value);
        return DigestUtils.md5Hex(String.valueOf(apikey) + "~" + merchanid + "~" + referenceCode + "~" + value + "~" + currency);
    }
    
    public String getPaymentMethodFromCreditCardNumber(final String creditCardNumber) {
        if (creditCardNumber.substring(0, 2).equals("51") || creditCardNumber.substring(0, 2).equals("52") || creditCardNumber.substring(0, 2).equals("53") || creditCardNumber.substring(0, 2).equals("54") || creditCardNumber.substring(0, 2).equals("55")) {
            return "MASTERCARD";
        }
        if (creditCardNumber.substring(0, 1).equals("4")) {
            return "VISA";
        }
        if (creditCardNumber.substring(0, 2).equals("34") || creditCardNumber.substring(0, 2).equals("37")) {
            return "AMERICANEXPRESS";
        }
        return null;
    }
    
    @Override
    public RegisterCreditCardTokenResponse createCreditCardToken(final String payerId, final String name, final String identificationNumber, final String number, final String expirationDate) {
        final RegisterCreditCardTokenInput rccti = new RegisterCreditCardTokenInput();
        rccti.setLanguage("es");
        rccti.setCommand(this.CREATE__TOKEN_COMAND);
        final MerchantInput mi = new MerchantInput();
        mi.setApiKey(this.env.getRequiredProperty("payu.apiKey"));
        mi.setApiLogin(this.env.getRequiredProperty("payu.apiLogin"));
        rccti.setMerchant(mi);
        final CreditCardToken ccti = new CreditCardToken();
        ccti.setExpirationDate(expirationDate);
        ccti.setIdentificationNumber(identificationNumber);
        ccti.setName(name);
        ccti.setNumber(number);
        ccti.setPayerId(payerId);
        ccti.setPaymentMethod(this.getPaymentMethodFromCreditCardNumber(number));
        rccti.setCreditCardToken(ccti);
        final String uri = this.env.getRequiredProperty("payu.paymentsurl");
        final Client client = Client.create();
        final WebResource webResource = client.resource(uri);
        final Gson gson = new Gson();
        final String input = gson.toJson((Object)rccti);
        System.out.println(input);
        final ClientResponse response = (ClientResponse)((WebResource.Builder)webResource.type("application/json").accept(new String[] { "application/json" })).post((Class)ClientResponse.class, (Object)input);
        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
        final String r = (String)response.getEntity((Class)String.class);
        System.out.println(r);
        return (RegisterCreditCardTokenResponse)gson.fromJson(r, (Class)RegisterCreditCardTokenResponse.class);
    }
    
    @Override
    public PayOutput payWithCreditCardToken(final String orderId, final String paymentMethod, final String creditCardTokenId, final String currency, final BigInteger amount, final String payerId, final String name, final String identificationNumber) {
        final PayInput pi = new PayInput();
        pi.setLanguage("es");
        pi.setCommand(this.SUBMIT_TRANSACTION);
        final MerchantInput mi = new MerchantInput();
        mi.setApiKey(this.env.getRequiredProperty("payu.apiKey"));
        mi.setApiLogin(this.env.getRequiredProperty("payu.apiLogin"));
        pi.setMerchant(mi);
        final Transaction t = new Transaction();
        final Order o = new Order();
        o.setAccountId(this.env.getRequiredProperty("payu.accountId"));
        o.setReferenceCode(orderId);
        o.setDescription("pago plan");
        o.setLanguage("es");
        o.setSignature(this.getSignature(this.env.getRequiredProperty("payu.apiKey"), this.env.getRequiredProperty("payu.merchantId"), orderId, amount.toString(), currency));
        final AdditionalValues additionalValues = new AdditionalValues();
        final TXVALUE tXVALUE = new TXVALUE();
        tXVALUE.setValue(amount);
        tXVALUE.setCurrency(currency);
        additionalValues.setTXVALUE(tXVALUE);
        o.setAdditionalValues(additionalValues);
        t.setOrder(o);
        final Payer p = new Payer();
        p.setDniNumber(identificationNumber);
        p.setFullName(name);
        p.setMerchantPayerId(payerId);
        t.setPayer(p);
        t.setCreditCardTokenId(creditCardTokenId);
        final ExtraParameters extraParameters = new ExtraParameters();
        extraParameters.setINSTALLMENTSNUMBER(1);
        t.setExtraParameters(extraParameters);
        t.setType(this.AUTHORIZATION_AND_CAPTURE);
        t.setPaymentMethod(paymentMethod);
        t.setPaymentCountry("CO");
        t.setDeviceSessionId(UUID.randomUUID().toString());
        t.setIpAddress(this.env.getRequiredProperty("payu.ipAddress"));
        t.setCookie(UUID.randomUUID().toString());
        t.setUserAgent("Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0");
        pi.setTransaction(t);
        pi.setTest(false);
        final String uri = this.env.getRequiredProperty("payu.paymentsurl");
        final Client client = Client.create();
        final WebResource webResource = client.resource(uri);
        final Gson gson = new Gson();
        final String input = gson.toJson((Object)pi);
        System.out.println(input);
        final ClientResponse response = (ClientResponse)((WebResource.Builder)webResource.type("application/json").accept(new String[] { "application/json" })).post((Class)ClientResponse.class, (Object)input);
        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
        final String r = (String)response.getEntity((Class)String.class);
        System.out.println(r);
        return (PayOutput)gson.fromJson(r, (Class)PayOutput.class);
    }
}
