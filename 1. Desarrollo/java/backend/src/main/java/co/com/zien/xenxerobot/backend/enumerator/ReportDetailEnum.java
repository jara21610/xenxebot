package co.com.zien.xenxerobot.backend.enumerator;

public enum ReportDetailEnum
{
    TOTALPOST("TOTALPOST", 0, "TOTALPOST"), 
    TOTALPOSITIVE("TOTALPOSITIVE", 1, "TOTALPOSITIVE"), 
    TOTALNEGATIVE("TOTALNEGATIVE", 2, "TOTALNEGATIVE"), 
    TOTALNEUTRAL("TOTALNEUTRAL", 3, "TOTALNEUTRAL"), 
    USUARIO("USUARIO", 4, "USUARIO"), 
    COMMANDINFO("COMMANDINFO", 5, "COMMANDINFO");
    
    private String sentiment;
    
    public String getReportDetail() {
        return this.sentiment;
    }
    
    private ReportDetailEnum(final String s, final int n, final String sentiment) {
        this.sentiment = sentiment;
    }
}
