package co.com.zien.xenxerobot.backend.services;

public class Payer
{
    private String merchantPayerId;
    private String fullName;
    private String emailAddress;
    private String contactPhone;
    private String dniNumber;
    private BillingAddress billingAddress;
    
    public String getMerchantPayerId() {
        return this.merchantPayerId;
    }
    
    public void setMerchantPayerId(final String merchantPayerId) {
        this.merchantPayerId = merchantPayerId;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }
    
    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public String getContactPhone() {
        return this.contactPhone;
    }
    
    public void setContactPhone(final String contactPhone) {
        this.contactPhone = contactPhone;
    }
    
    public String getDniNumber() {
        return this.dniNumber;
    }
    
    public void setDniNumber(final String dniNumber) {
        this.dniNumber = dniNumber;
    }
    
    public BillingAddress getBillingAddress() {
        return this.billingAddress;
    }
    
    public void setBillingAddress(final BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }
}
