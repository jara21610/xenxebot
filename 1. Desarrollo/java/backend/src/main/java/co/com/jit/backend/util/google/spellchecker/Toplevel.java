package co.com.jit.backend.util.google.spellchecker;

import java.util.*;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "completeSuggestion" })
@XmlRootElement(name = "toplevel")
public class Toplevel
{
    @XmlElement(name = "CompleteSuggestion", required = true)
    protected List<CompleteSuggestion> completeSuggestion;
    
    public List<CompleteSuggestion> getCompleteSuggestion() {
        if (this.completeSuggestion == null) {
            this.completeSuggestion = new ArrayList<CompleteSuggestion>();
        }
        return this.completeSuggestion;
    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "suggestion" })
    public static class CompleteSuggestion
    {
        @XmlElement(required = true)
        protected Suggestion suggestion;
        
        public Suggestion getSuggestion() {
            return this.suggestion;
        }
        
        public void setSuggestion(final Suggestion value) {
            this.suggestion = value;
        }
        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Suggestion
        {
            @XmlAttribute(name = "data")
            protected String data;
            
            public String getData() {
                return this.data;
            }
            
            public void setData(final String value) {
                this.data = value;
            }
        }
    }
}
