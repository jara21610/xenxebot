package co.com.zien.xenxerobot.backend.services;

public class CommandMalformedException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public CommandMalformedException() {
    }
    
    public CommandMalformedException(final String message) {
        super(message);
    }
    
    public CommandMalformedException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public CommandMalformedException(final Throwable cause) {
        super(cause);
    }
}
