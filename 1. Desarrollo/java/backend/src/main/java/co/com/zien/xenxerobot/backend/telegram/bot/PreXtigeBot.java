package co.com.zien.xenxerobot.backend.telegram.bot;

import org.telegram.telegrambots.bots.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import co.com.zien.xenxerobot.backend.services.*;
import co.com.zien.xenxerobot.backend.config.*;
import org.springframework.context.i18n.*;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import co.com.zien.xenxerobot.persistence.entity.*;
import com.restfb.types.*;
import com.restfb.types.Post;
import com.restfb.types.instagram.IgMedia;

import java.text.*;
import com.thoughtworks.xstream.*;
import co.com.zien.xenxerobot.backend.enumerator.*;

import java.util.*;

public class PreXtigeBot extends TelegramLongPollingBot implements ServiceBot
{
    private static PropertiesRepository pr;
    private static MessageSource messageSource;
    private static PostProcessingService pps;
    private static UserRepository ur;
    private static CommandProcessRepository cpr;
    private static FacebookService fs;
    private static ReportDetailManager rdm;
    private static PostRepository postr;
    private static ReportDetailServiceFactory rdmf;
    private static Environment env;
    private static PlanService ps;
    private static TokenService ts;
    private static PlanManagementService pm;
    private static Map<String, ComandDTO> usercommandstate;
    
    static {
        PreXtigeBot.usercommandstate = null;
        PreXtigeBot.pr = (PropertiesRepository)Config.getContext().getBean((Class)PropertiesRepository.class);
        PreXtigeBot.messageSource = (MessageSource)Config.getContext().getBean((Class)MessageSource.class);
        PreXtigeBot.pps = (PostProcessingService)Config.getContext().getBean((Class)PostProcessingService.class);
        PreXtigeBot.ur = (UserRepository)Config.getContext().getBean((Class)UserRepository.class);
        PreXtigeBot.fs = (FacebookService)Config.getContext().getBean((Class)FacebookService.class);
        PreXtigeBot.cpr = (CommandProcessRepository)Config.getContext().getBean((Class)CommandProcessRepository.class);
        PreXtigeBot.postr = (PostRepository)Config.getContext().getBean((Class)PostRepository.class);
        PreXtigeBot.rdmf = (ReportDetailServiceFactory)Config.getContext().getBean((Class)ReportDetailServiceFactory.class);
        PreXtigeBot.env = (Environment)Config.getContext().getBean((Class)Environment.class);
        PreXtigeBot.ps = (PlanService)Config.getContext().getBean((Class)PlanService.class);
        PreXtigeBot.ts = (TokenService)Config.getContext().getBean((Class)TokenService.class);
        PreXtigeBot.pm = (PlanManagementService)Config.getContext().getBean((Class)PlanManagementService.class);
    }
    
    public void onUpdateReceived(final Update update) {
        if (PreXtigeBot.usercommandstate == null) {
            PreXtigeBot.usercommandstate = new HashMap<String, ComandDTO>();
        }
        if (update.hasMessage() && update.getMessage().hasText()) {
            final String chatid = update.getMessage().getChatId().toString();
            final Users u = PreXtigeBot.ur.isUserActivebyChatid(chatid);
            if (chatid != null) {
                if (u != null) {
                    final Userplan up = PreXtigeBot.ps.getLastUserPlan(u.getId(), ServiceTypeEnum.PREXTIGE.getServicetype());
                    if (up != null) {
                        if (up.getState().equals(PlanStateEnum.ACTIVO.getState())) {
                            final String receivedmessage = update.getMessage().getText();
                            final List<String> receivedmessagelist = new ArrayList<String>(Arrays.asList(receivedmessage.split(" ")));
                            final String command = receivedmessagelist.get(0);
                            if (CommandEnum.START.getCommand().equals(command)) {
                                this.handleStartCommand(chatid);
                            }
                            else if (CommandEnum.DELETEREGISTER.getCommand().equals(command)) {
                                u.setState("");
                                u.setMobilphone("");
                                u.setEmail("");
                                PreXtigeBot.ur.saveAndFlush(u);
                            }
                            else if (CommandEnum.TELEGRAMSENTIMENT.getCommand().equals(command)) {
                                this.handleSentimentCommand(chatid);
                            }
                            else if (CommandEnum.TWITTER.getCommand().equals(command)) {
                                this.handleTwitterSentimentCommand(chatid);
                            }
                            else if (CommandEnum.FACEBOOK.getCommand().equals(command)) {
                                this.handleFacebookSentimentCommand(chatid, u, SocialMediaSourceEnum.FACEBOOK.getSocialmediasource());
                            }
                            else if (CommandEnum.INSTAGRAM.getCommand().equals(command)) {
                                this.handleFacebookSentimentCommand(chatid, u, SocialMediaSourceEnum.INSTAGRAM.getSocialmediasource());
                            }
                            else if (CommandEnum.PPALMENU.getCommand().equals(command)) {
                                this.handlePpalMenuCommand(chatid);
                            }
                            else if (CommandEnum.PARTIALREPORT.getCommand().equals(command)) {
                                this.handleReportDetailCommand(chatid);
                            }
                            else if (CommandEnum.CANCEL.getCommand().equals(command)) {
                                this.handleCancelCommand(chatid);
                            }
                            else if (CommandEnum.COMANDSTATUS.getCommand().equals(command)) {
                                this.handleStatusCommand(chatid);
                            }
                            else if (CommandEnum.HELP.getCommand().equals(command)) {
                                this.handleHelpCommand(chatid);
                            }
                            else {
                                final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
                                if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()) && cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.TWITTER.getSocialmediasource())) {
                                    this.handleTwitterSentimentSearchObjectCommand(chatid, receivedmessage);
                                }
                                else if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState()) && cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
                                    if (cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()).equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
                                        //this.handleFacebookSentimentSearchObjectCommand(chatid, receivedmessage);
                                    }
                                    else {
                                        this.handleFacebookSentimentSearchObjectPostCommand(chatid, receivedmessage);
                                    }
                                }
                                else if (cdto.getState().equals(TelegramStateEnum.SENTIMENTSINCE.getState()) && cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
                                    this.handleFacebookSentimentSinceCommand(chatid, receivedmessage);
                                }
                                else if (cdto.getState().equals(TelegramStateEnum.SENTIMENTUNTIL.getState()) && cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
                                    this.handleFacebookSentimentUntilCommand(chatid, receivedmessage);
                                }
                                else if (cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState()) && cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())) {
                                    //this.handleFacebookSentimentChoosePostCommand(chatid, receivedmessage);
                                }
                            }
                        }
                        else if (up.getState().equals(PlanStateEnum.FINALIZADO.getState())) {
                            if (up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType())) {
                                this.handleFreePlanFinished(chatid, u);
                            }
                            else if (!up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType()) && up.getTxpalused() >= up.getTxplancapacity()) {
                                this.handlePlanTxFinished(chatid, u);
                            } 
                        }
                        else if (up.getState().equals(PlanStateEnum.NOTPAID.getState())) {
                            this.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
                        }
                    }
                }
                else {
                    final SendMessage message = new SendMessage();
                    		message.setChatId(update.getMessage().getChatId().toString());
                    		ReplyKeyboard keyboard = (ReplyKeyboard)getRegistryboard();
                    		message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.finalizarregistro.text", (Object[])null, LocaleContextHolder.getLocale()));
                    		message.setReplyMarkup(keyboard);
                    try {
                        this.execute(message);
                    }
                    catch (TelegramApiException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        else if (update.hasCallbackQuery()) {
            final CallbackQuery callbackquery = update.getCallbackQuery();
            final String[] data = callbackquery.getData().split(":");
            final String chatid2 = callbackquery.getMessage().getChatId().toString();
            if (data[0].equals("detailreport")) {
                this.handleReportDetailCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
            }
            else if (data[0].equals("cancelreport")) {
                this.handleCancelCommandfromState(callbackquery.getMessage().getChatId().toString(), data[2]);
            }
            else if (data[0].equals("licenseinfluenceenroll")) {
                this.handleInfluencerEnrollAcceptance(callbackquery.getMessage().getChatId().toString(), data[1], callbackquery.getFrom());
            }
            else if (data[0].equals("renewplantoday")) {
                final Users u2 = PreXtigeBot.ur.isUserActivebyChatid(chatid2);
                this.handleRenewPlanToday(chatid2, u2);
            } else if (data[0].equals("selectedfacebookaccount")) {
            	final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid2);                              
            	List<Account> al = (List<Account>) cdto.getArguments().get(CommandArgumentsEnum.TOKEN.getCommandargument());
            	
            	for (Account a:al) {
            		if (a.getId().equals(data[1])) {
            			cdto.getArguments().put(CommandArgumentsEnum.TOKEN.getCommandargument(), a.getAccessToken());
            			break;
            		}
            	}
            	            	
            	this.handleFacebookSelectSearchType(chatid2, data[1]);
            } else if (data[0].equals("facebooksearchtype")) {
            	
            	if (data[1].equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
            		this.handleFacebookSentimentSearchObjectCommand(chatid2);
            	}else if (data[1].equals(FacebookSearchTypeEnum.POST.getFacebooksearchtype())) {
            		final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid2);  
            		if (cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()).
            				equals(SocialMediaSourceEnum.FACEBOOK.getSocialmediasource())){
            			this.handleFacebookSentimentChoosePostCommand(chatid2);
            		} else {
            			this.handleInstagramSentimentChoosePostCommand(chatid2);
            		}
            	}
            	
            } else if (data[0].equals("postselected")) {
            	final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid2);  
                cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), data[1]);
            	this.handleFacebookSentimentSearchObjectCommand(chatid2);
            }
            
            
        }
        else if (update.hasMessage() && update.getMessage().getContact() != null) {
            final String chatid = update.getMessage().getChatId().toString();
            this.handleFinalizeRegisterCommand(update, chatid);
        }
    }
    
    private void handleRenewPlanToday(final String chatid, final Users u) {
        try {
            PreXtigeBot.pm.upgradePlanPaymentMethodPredeterminated(u, ServiceTypeEnum.PREXTIGE.getServicetype());
        }
        catch (Exception e2) {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
            e2.printStackTrace();
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    public void handlePlanTxFinished(final String chatid, final Users u) {
        try {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup(this.getPlanTxFinishedInlineMenuKeyboard(u));
            message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.planfinish.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private ReplyKeyboard getPlanTxFinishedInlineMenuKeyboard(final Users u) {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("message.telegram.planfinish.button.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setCallbackData("renewplantoday");
        rowInline.add(ikb);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private void handleCancelCommandfromState(final String chatid, final String cpId) {
        PreXtigeBot.pps.cancel(cpId);
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.aftercancel.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleCancelCommand(final String chatid) {
        String cpid = "";
        final List<CommandProcess> lcp = (List<CommandProcess>)PreXtigeBot.cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid, ServiceTypeEnum.PREXTIGE.getServicetype());
        if (lcp.size() > 0) {
            cpid = lcp.get(0).getId();
            PreXtigeBot.pps.cancel(cpid);
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.aftercancel.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        else {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    private static ReplyKeyboardMarkup getMainMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.sentiment.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
    
    private static ReplyKeyboardMarkup getRegistryboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        final KeyboardButton button = new KeyboardButton(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.finalizarregistro.text", (Object[])null, LocaleContextHolder.getLocale()));
        button.setRequestContact(true);
        keyboardFirstRow.add(button);
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
    
    private static ReplyKeyboardMarkup getSentimentMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.twitter.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.facebook.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.instagram.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
    
    private static ReplyKeyboardMarkup getPpalMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.ppalmenu.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardSecondRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
    
    private static ReplyKeyboardMarkup getTwitterSearchTypeMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.twitter.searchbyusertext", (Object[])null, LocaleContextHolder.getLocale()));
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.twitter.searchbyhastagtext", (Object[])null, LocaleContextHolder.getLocale()));
        final KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.cancel.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return replyKeyboardMarkup;
    }
    
    private static ReplyKeyboard getFacebookSearchTypeMenuKeyboard() {
    	
    	final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();    	
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.facebook.searchbypagetext", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setCallbackData("facebooksearchtype:"+FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
        rowInline.add(ikb);
        InlineKeyboardButton ikb1 = new InlineKeyboardButton();
        ikb1.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.facebook.searchbyposttext", (Object[])null, LocaleContextHolder.getLocale()));
        ikb1.setCallbackData("facebooksearchtype:"+FacebookSearchTypeEnum.POST.getFacebooksearchtype());
        rowInline.add(ikb1);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;               
    }
    
    private void handleFinalizeRegisterCommand(final Update update, final String chatid) {
        final Users u = PreXtigeBot.ur.isUserActivebyMobilPhone(update.getMessage().getContact().getPhoneNumber().replace("+", ""));
        if (u != null) {
            u.setChatid(update.getMessage().getChatId().toString());
            PreXtigeBot.ur.saveAndFlush(u);
            this.handleStartCommand(update.getMessage().getChatId().toString());
        }
        else {
            try {
                final SendMessage message = new SendMessage();
                message.setChatId(chatid);
                message.setReplyMarkup(this.getRegisterUserInlineMenuKeyboard());
                message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.registrarusuario.text", (Object[])null, LocaleContextHolder.getLocale()));
                try {
                    execute(message);
                }
                catch (TelegramApiException e1) {
                    e1.printStackTrace();
                }
            }
            catch (Exception e3) {
                final SendMessage message2 = new SendMessage();
                message2.setChatId(chatid);
                message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
                message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
                try {
                    execute(message2);
                }
                catch (TelegramApiException e2) {
                    e2.printStackTrace();
                }
                e3.printStackTrace();
                try {
                    execute(message2);
                }
                catch (TelegramApiException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
    
    private void handleStartCommand(final String chatid) {
        final List<CommandProcess> lcp = (List<CommandProcess>)PreXtigeBot.cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid, ServiceTypeEnum.PREXTIGE.getServicetype());
        if (lcp.size() == 0) {
            final ComandDTO cdto = new ComandDTO();
            cdto.setState(TelegramStateEnum.MAINMENU.getState());
            cdto.setArguments(new HashMap<String, Object>());
            cdto.getArguments().put(CommandArgumentsEnum.SERVICETYPE.getCommandargument(), ServiceTypeEnum.PREXTIGE.getServicetype());
            PreXtigeBot.usercommandstate.put(chatid, cdto);
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.mainmenu.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase() }, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        else {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup(this.getPushPartialMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.mainmenu.currentprocessrunnig", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private void handlePpalMenuCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.MAINMENU.getState());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.mainmenu.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleHelpCommand(final String chatid) {
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.help.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleSentimentCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENT.getState());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup((ReplyKeyboard)getSentimentMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.sentiment.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleTwitterSentimentCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), SocialMediaSourceEnum.TWITTER.getSocialmediasource());
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        ReplyKeyboardRemove rkr = new ReplyKeyboardRemove();
        rkr.setSelective(true);
        rkr.setRemoveKeyboard(false);
        message.setReplyMarkup(rkr);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.searchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleTwitterSentimentSearchbyUserCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.USUARIO.getTwittersearchtype());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.searchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleTwitterSentimentSearchbyHashtagCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup((ReplyKeyboard)getPpalMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.searchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSentimentCommand(final String chatid, Users u, String socialmediasurce) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHTYPE.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), socialmediasurce);
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
            cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
            //cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
        }
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup(getFLoguinInlineMenuKeyboard(u));
        message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.floguin.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSelectSearchType(final String chatid, final String searchobject) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);  
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), searchobject);
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setReplyMarkup(getFacebookSearchTypeMenuKeyboard());
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.searchtype.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSentimentSearchbyPageCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState())) {
            cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), FacebookSearchTypeEnum.PAGE.getFacebooksearchtype());
        }
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        ReplyKeyboardRemove rkr = new ReplyKeyboardRemove();
        rkr.setSelective(true);
        rkr.setRemoveKeyboard(false);
        message.setReplyMarkup(rkr);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.searchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSentimentSearchbyPostCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        if (!cdto.getState().equals(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState())) {
            cdto.setState(TelegramStateEnum.SENTIMENTCHOOSESEARCHOBJECT.getState());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), FacebookSearchTypeEnum.POST.getFacebooksearchtype());
        }
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        ReplyKeyboardRemove rkr = new ReplyKeyboardRemove();
        rkr.setSelective(true);
        rkr.setRemoveKeyboard(false);
        message.setReplyMarkup(rkr);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.facebook.choosepage.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSentimentSearchObjectCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());        
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final Calendar cal = Calendar.getInstance();
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), cal.getTime());
        cal.add(5, -8);
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), cal.getTime());
        cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
        cdto.setCommunicationKey(chatid);
        final ComandDTO cdtocopy = new ComandDTO();
        cdtocopy.setArguments(cdto.getArguments());
        cdtocopy.setChannel(cdto.getChannel());
        cdtocopy.setCommand(cdto.getCommand());
        cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
        cdtocopy.setState(cdto.getState());
        cdtocopy.setCratedAt(new Date());
        PreXtigeBot.pps.proccess(cdtocopy);
    }
    
    private void handleTwitterSentimentSearchObjectCommand(final String chatid, final String searchobject) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), searchobject);
        final Calendar cal = Calendar.getInstance();
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), cal.getTime());
        cal.add(11, -168);
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), cal.getTime());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
        cdto.setCommunicationKey(chatid);
        final ComandDTO cdtocopy = new ComandDTO();
        cdtocopy.setArguments(cdto.getArguments());
        cdtocopy.setChannel(cdto.getChannel());
        cdtocopy.setCommand(cdto.getCommand());
        cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
        cdtocopy.setState(cdto.getState());
        cdtocopy.setCratedAt(new Date());
        PreXtigeBot.pps.proccess(cdtocopy);
    }
    
    private void handleFacebookSentimentSearchObjectPostCommand(final String chatid, final String postnumber) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        cdto.setState(TelegramStateEnum.SENTIMENTSINCE.getState());
        cdto.getArguments().put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), ((List<Post>)cdto.getArguments().get(CommandArgumentsEnum.POSTLIST.getCommandargument())).get(new Integer(postnumber)).getId());
        cdto.getArguments().remove(CommandArgumentsEnum.POSTLIST.getCommandargument());
        cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.since.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private void handleFacebookSentimentChoosePostCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        String userToken = (String) cdto.getArguments().get(CommandArgumentsEnum.TOKEN.getCommandargument());
        String pageid = (String) cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
        List<Post> postlist = null;
        try {
            postlist = PreXtigeBot.fs.searchlastPostsFacebook(pageid, 10, userToken);
        }
        catch (Exception e3) {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        ReplyKeyboardRemove rkr = new ReplyKeyboardRemove();
        rkr.setSelective(true);
        rkr.setRemoveKeyboard(false);
        message.setReplyMarkup(rkr);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
        int i = 0;
        for (final Post p : postlist) {
        	
        	 final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
             final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
             final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
             InlineKeyboardButton ikb = new InlineKeyboardButton();
             ikb.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.button.text", (Object[])null, LocaleContextHolder.getLocale()));
             ikb.setCallbackData("postselected:" + p.getId());
             rowInline.add(ikb);             
             rowsInline.add(rowInline);
             markupInline.setKeyboard((List)rowsInline);
             
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup(markupInline);
            message2.setText(p.getMessage());
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            ++i;
        }                
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
    }
    
    private void handleInstagramSentimentChoosePostCommand(final String chatid) {
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        String userToken = (String) cdto.getArguments().get(CommandArgumentsEnum.TOKEN.getCommandargument());
        String pageid = (String) cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
        List<IgMedia> postlist = null;
        try {
        	pageid = fs.getInstAccountBussiness(pageid, userToken);
        	System.out.println("pageid " + pageid);
        	postlist = PreXtigeBot.fs.searchlastPostsInstagram(pageid, 10, userToken);
        }
        catch (Exception e3) {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
            e3.printStackTrace();            
        }
        final SendMessage message = new SendMessage();
        message.setChatId(chatid);
        ReplyKeyboardRemove rkr = new ReplyKeyboardRemove();
        rkr.setSelective(true);
        rkr.setRemoveKeyboard(false);
        message.setReplyMarkup(rkr);
        message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.text", (Object[])null, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
        int i = 0;
        for (final IgMedia p : postlist) {
        	
        	 final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
             final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
             final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
             InlineKeyboardButton ikb = new InlineKeyboardButton();
             ikb.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.facebook.choosesearchcriteria.button.text", (Object[])null, LocaleContextHolder.getLocale()));
             ikb.setCallbackData("postselected:" + p.getId());
             rowInline.add(ikb);             
             rowsInline.add(rowInline);
             markupInline.setKeyboard((List)rowsInline);
             
            final SendMessage message2 = new SendMessage();
            		message2.setChatId(chatid);
            		message2.setReplyMarkup(markupInline);
            		message2.setText(p.getCaption());
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            ++i;
        }                
        cdto.setState(TelegramStateEnum.SENTIMENTSEARCHOBJECT.getState());
    }
    
    private void handleFacebookSentimentSinceCommand(final String chatid, final String since) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        try {
            cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), dateFormat.parse(since));
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommunicationKey(chatid);
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.until.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (ParseException e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.datenotfomat.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private void handleTwitterSentimentUntilCommand(final String chatid, final String since) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        try {
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), dateFormat.parse(since));
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommunicationKey(chatid);
            final ComandDTO cdtocopy = new ComandDTO();
            cdtocopy.setArguments(cdto.getArguments());
            cdtocopy.setChannel(cdto.getChannel());
            cdtocopy.setCommand(cdto.getCommand());
            cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
            cdtocopy.setState(cdto.getState());
            cdtocopy.setCratedAt(new Date());
            PreXtigeBot.pps.proccess(cdtocopy);
        }
        catch (ParseException e2) {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.datenotfomat.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    private void handleFacebookSentimentUntilCommand(final String chatid, final String since) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        try {
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), dateFormat.parse(since));
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommunicationKey(chatid);
            final ComandDTO cdtocopy = new ComandDTO();
            cdtocopy.setArguments(cdto.getArguments());
            cdtocopy.setChannel(cdto.getChannel());
            cdtocopy.setCommand(cdto.getCommand());
            cdtocopy.setCommunicationKey(cdto.getCommunicationKey());
            cdtocopy.setState(cdto.getState());
            cdtocopy.setCratedAt(new Date());
            PreXtigeBot.pps.proccess(cdtocopy);
        }
        catch (ParseException e2) {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.datenotfomat.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    private void handleTwitterSentimentSinceCommand(final String chatid, final String since) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
        try {
            cdto.setState(TelegramStateEnum.SENTIMENTUNTIL.getState());
            cdto.getArguments().put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), dateFormat.parse(since));
            cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
            cdto.setCommunicationKey(chatid);
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.until.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (ParseException e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.datenotfomat.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public void handlePushPartialReport(final String chatid, final int totalpost, final String searchcriteria, final String cpid) {
        final ComandDTO cdto = new ComandDTO();
        cdto.setState(TelegramStateEnum.DETAILREPORT.getState());
        cdto.setArguments(new HashMap<String, Object>());
        cdto.getArguments().put(CommandArgumentsEnum.COMMANDPROCESSID.getCommandargument(), cpid);
        cdto.setChannel(ChannelEnum.TELEGRAM.getChannel());
        cdto.setCommunicationKey(chatid);
        PreXtigeBot.usercommandstate.replace(chatid, cdto);
        final SendMessage message = new SendMessage();
        		message.setChatId(chatid);
        		message.setReplyMarkup(this.getPushPartialMenuKeyboard());
        		message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.pushnotification1.text", new Object[] { totalpost, searchcriteria }, LocaleContextHolder.getLocale()));
        try {
            execute(message);
        }
        catch (TelegramApiException e1) {
            e1.printStackTrace();
        }
    }
    
    private ReplyKeyboard getPushPartialMenuKeyboard() {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        final List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        final KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboardFirstRow.add(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.cancel.text", (Object[])null, LocaleContextHolder.getLocale()));
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard((List)keyboard);
        return (ReplyKeyboard)replyKeyboardMarkup;
    }
    
    private void handleReportDetailCommand(final String chatid) {
        try {
            String cpid = "";
            final List<CommandProcess> lcp = (List<CommandProcess>)PreXtigeBot.cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid, ServiceTypeEnum.PREXTIGE.getServicetype());
            if (lcp.size() > 0) {
                cpid = lcp.get(0).getId();
                (PreXtigeBot.rdm = PreXtigeBot.rdmf.getResponseBotService(ServiceTypeEnum.PREXTIGE.getServicetype())).reportdetailgenerateSentimentAnalysisReport(cpid);
            }
            else {
                final SendMessage message = new SendMessage();
                message.setChatId(chatid);
                message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
                message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.mainmenu.notcurrentprocessrunnig", (Object[])null, LocaleContextHolder.getLocale()));
                try {
                    execute(message);
                }
                catch (TelegramApiException e1) {
                    e1.printStackTrace();
                }
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private void handleStatusCommand(final String chatid) {
        try {
            final List<CommandProcess> lcp = (List<CommandProcess>)PreXtigeBot.cpr.findMyRunningProcess(CommandProcessStateEnum.ENPROCESO.getState(), chatid, ServiceTypeEnum.PREXTIGE.getServicetype());
            if (lcp.size() > 0) {
                for (final CommandProcess cp : lcp) {
                    final XStream xs = new XStream();
                    final ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
                    final int total = PreXtigeBot.postr.totalPost(cp.getId());
                    final Date date = new Date();
                    final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
                    final SendMessage message = new SendMessage();
                    message.setChatId(chatid);
                    message.setReplyMarkup(this.getProcessStateInlineMenuKeyboard(cp));
                    message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.procesrunning.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()), cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()), df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())), df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())), total }, LocaleContextHolder.getLocale()));
                    try {
                        execute(message);
                    }
                    catch (TelegramApiException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            else {
                final SendMessage message2 = new SendMessage();
                		message2.setChatId(chatid);
                		message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
                		message2.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.notprocesrunning.text", (Object[])null, LocaleContextHolder.getLocale()));
                try {
                    execute(message2);
                }
                catch (TelegramApiException e2) {
                    e2.printStackTrace();
                }
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            		message2.setChatId(chatid);
            		message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private ReplyKeyboard getProcessStateInlineMenuKeyboard(final CommandProcess cp) {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.pushpartialreport.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setCallbackData("detailreport:cpid:" + cp.getId());
        rowInline.add(ikb);
        InlineKeyboardButton ikb1 = new InlineKeyboardButton();
        ikb1.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.command.cancel.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb1.setCallbackData("cancelreport:cpid:" + cp.getId());
        rowInline.add(ikb1);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private void handleReportDetailCommandfromState(final String chatid, final String cpid) {
        try {
            (PreXtigeBot.rdm = PreXtigeBot.rdmf.getResponseBotService(ServiceTypeEnum.PREXTIGE.getServicetype())).reportdetailgenerateSentimentAnalysisReport(cpid);
            PreXtigeBot.usercommandstate.replace(chatid, new ComandDTO());
        }
        catch (Exception e2) {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    private void handleInfluencerEnroll(final String chatid) {
        try {
            final ComandDTO cdto = new ComandDTO();
            cdto.setState(TelegramStateEnum.EMAILENRROLL.getState());
            cdto.setCommand(CommandEnum.INFLUENCERENNROLL.getCommand());
            PreXtigeBot.usercommandstate.put(chatid, cdto);
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.influencerenroll.email.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e2) {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            		message.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
            e2.printStackTrace();
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    public void handleFreePlanFinished(final String chatid, final Users u) {
        try {
            final SendMessage message = new SendMessage();
            		message.setChatId(chatid);
            		message.setReplyMarkup(this.getSelectPlanInlineMenuKeyboard(u));
            		message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.freeplanfinish.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private void handleInfluencerEnrollLicence(final String chatid, final String email) {
        try {
            final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
            cdto.setArguments(new HashMap<String, Object>());
            cdto.getArguments().put(CommandArgumentsEnum.EMAIL.getCommandargument(), email);
            final String licence = PreXtigeBot.pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup(this.getlicenseinfluencerInlineMenuKeyboard());
            message.setText(licence);
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private ReplyKeyboard getlicenseinfluencerInlineMenuKeyboard() {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.influencerenroll.acept.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setCallbackData("licenseinfluenceenroll:YES");
        rowInline.add(ikb);
        InlineKeyboardButton ikb1 = new InlineKeyboardButton();
        ikb1.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.influencerenroll.reject.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb1.setCallbackData("licenseinfluenceenroll:NO");
        rowInline.add(ikb1);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private ReplyKeyboard getSelectPlanInlineMenuKeyboard(final Users u) {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("message.telegram.freeplanfinish.button.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setUrl(String.valueOf(PreXtigeBot.env.getProperty("selectplan.url")) + "?usertoken=" + PreXtigeBot.ts.generateUserToken(u, ServiceTypeEnum.PREXTIGE.getServicetype()));
        rowInline.add(ikb);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private ReplyKeyboard getRegisterUserInlineMenuKeyboard() {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("message.telegram.registrarusuario.button.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setUrl(PreXtigeBot.env.getProperty("register.url") + "/?servicetype=" + ServiceTypeEnum.PREXTIGE.getServicetype());
        rowInline.add(ikb);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private void handleInfluencerEnrollAcceptance(final String chatid, final String acceptance, final org.telegram.telegrambots.meta.api.objects.User u) {
        try {
            if (acceptance.equals("YES")) {
                final String licence = PreXtigeBot.pr.findByName(PropertyEnum.INFLUENCEENROLLLICENCE.getProperty());
                final Users user = new Users();
                user.setChatid(chatid);
                user.setId(UUID.randomUUID().toString());
                user.setName(u.getFirstName());
                user.setLastname(u.getLastName());
                user.setState("activo");
                user.setTestlicenceacepted("Y");
                user.setTexttestlicence(licence);
                final ComandDTO cdto = PreXtigeBot.usercommandstate.get(chatid);
                user.setEmail((String)cdto.getArguments().get(CommandArgumentsEnum.EMAIL.getCommandargument()));
                PreXtigeBot.ur.saveAndFlush(user);
                this.handleStartCommand(chatid);
            }
            else {
                final SendMessage message = new SendMessage();
                message.setChatId(chatid);
                message.setText(PreXtigeBot.messageSource.getMessage("messsage.telegram.influencerenroll.reject.text1", (Object[])null, LocaleContextHolder.getLocale()));
                try {
                    execute(message);
                }
                catch (TelegramApiException e1) {
                    e1.printStackTrace();
                }
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public void handleUserTokenException(final String chatid) {
        try {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.usertokenexception.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public void handleUpgradePlanOK(final String chatid) {
        try {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.upgradeplanok.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public void handlePaymentExceptionUpgradePlan(final String chatid, final Users u) {
        try {
            final SendMessage message = new SendMessage();
            message.setChatId(chatid);
            message.setReplyMarkup(this.gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(u));
            message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.paymentexceptionupgradeplan.text", (Object[])null, LocaleContextHolder.getLocale()));
            try {
                execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(chatid);
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    private ReplyKeyboard gethandlePaymentExceptionUpgradePlanInlineMenuKeyboard(final Users u) {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("message.telegram.paymentexceptionupgradeplan.button.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setUrl(String.valueOf(PreXtigeBot.env.getProperty("paymentmethod.url")) + "?usertoken=" + PreXtigeBot.ts.generateUserToken(u, ServiceTypeEnum.PREXTIGE.getServicetype()));
        rowInline.add(ikb);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private ReplyKeyboard getFLoguinInlineMenuKeyboard(final Users u) {
        final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        InlineKeyboardButton ikb = new InlineKeyboardButton();
        ikb.setText(PreXtigeBot.messageSource.getMessage("message.telegram.floguin.button.text", (Object[])null, LocaleContextHolder.getLocale()));
        ikb.setUrl(String.valueOf(PreXtigeBot.env.getProperty("floguin.url")) + "?usertoken=" + PreXtigeBot.ts.generateUserToken(u, ServiceTypeEnum.PREXTIGE.getServicetype()) + "&servicetype=" + ServiceTypeEnum.PREXTIGE.getServicetype());
        rowInline.add(ikb);
        rowsInline.add(rowInline);
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    private ReplyKeyboard getSelectFAccountenuKeyboard(List<Account> al) {
    	final InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        final List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        for (Account a : al) {
        	final List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();
        	InlineKeyboardButton ikb = new InlineKeyboardButton();
        	ikb.setText(a.getName());
        	ikb.setCallbackData("selectedfacebookaccount:" + a.getId());
        	rowInline.add(ikb);        
        	rowsInline.add(rowInline);
        }
        markupInline.setKeyboard((List)rowsInline);
        return (ReplyKeyboard)markupInline;
    }
    
    @Override
	public void handleFacebookSelectAccount(List<Account> al, Users u) {
    	final ComandDTO cdto = PreXtigeBot.usercommandstate.get(u.getChatid());                              
    	try {
    		 cdto.getArguments().put(CommandArgumentsEnum.TOKEN.getCommandargument(), al);
    		
    		 SendMessage message = null;
    		 if (al == null){
    			 message = new SendMessage();
    			 message.setChatId(u.getChatid());
    			 message.setReplyMarkup(this.getPpalMenuKeyboard());
    			 message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.selectfacebookaccountempty.text", (Object[])null, LocaleContextHolder.getLocale()));
    		 } else {
    			 message = new SendMessage();
    			 message.setChatId(u.getChatid());
    			 message.setReplyMarkup(this.getSelectFAccountenuKeyboard(al));
    			 message.setText(PreXtigeBot.messageSource.getMessage("message.telegram.selectfacebookaccount.text", (Object[])null, LocaleContextHolder.getLocale()));
    		 }
            try {
            	execute(message);
            }
            catch (TelegramApiException e1) {
                e1.printStackTrace();
            }
        }
        catch (Exception e3) {
            final SendMessage message2 = new SendMessage();
            message2.setChatId(u.getChatid());                       
            message2.setReplyMarkup((ReplyKeyboard)getMainMenuKeyboard());
            message2.setText(PreXtigeBot.messageSource.getMessage("message.exception.runtimeexception.chat", (Object[])null, LocaleContextHolder.getLocale()));
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
            e3.printStackTrace();
            try {
            	execute(message2);
            }
            catch (TelegramApiException e2) {
                e2.printStackTrace();
            }
        }
		
	}    

    
    public String getBotUsername() {
        return "prextigebot";
    }
    
    public String getBotToken() {
        return PreXtigeBot.env.getRequiredProperty("prextigebot.token");
    }

	
}
