package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.backend.enumerator.*;

@Service
public class ResponseBotServiceFactoryImpl implements ResponseBotServiceFactory
{
    @Autowired
    @Qualifier("EmailResponseBot")
    ResponseBotService erbs;
    @Autowired
    @Qualifier("TelegramResponseBot")
    ResponseBotService trbs;
    
    @Override
    public ResponseBotService getResponseBotService(final String channel) {
        if (channel.equals(ChannelEnum.TELEGRAM.getChannel())) {
            return this.trbs;
        }
        if (channel.equals(ChannelEnum.EMAIL.getChannel())) {
            return this.erbs;
        }
        return this.trbs;
    }
}
