package co.com.zien.xenxerobot.backend.services;

public class AdditionalValues
{
    private TXVALUE TX_VALUE;
    
    public TXVALUE getTXVALUE() {
        return this.TX_VALUE;
    }
    
    public void setTXVALUE(final TXVALUE tXVALUE) {
        this.TX_VALUE = tXVALUE;
    }
}
