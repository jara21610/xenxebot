package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import co.com.zien.xenxerobot.persistence.entity.Properties;

import com.thoughtworks.xstream.*;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.context.i18n.*;
import java.text.*;
import java.util.*;
import java.io.*;
import co.com.jit.backend.util.*;
import org.apache.poi.ss.usermodel.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.scheduling.annotation.*;

@Service("InvoiceReport")
public class InvoiceReportImpl implements InvoiceReport
{
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    ResponseBotServiceFactory rbsf;
    @Autowired
    PostRepository postr;
    @Autowired
    Environment env;
       
    
    @Override
    public String generateInvoice(Users u, Userplan up, Paymentmethod pm) throws Exception {
        String path = "";
        String pathpdf = "";        

            
        	final Date date = new Date();
        	final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
            final String name = this.pr.findByName(PropertyEnum.INVOICETEMPLATE.getProperty());
            Properties p = this.pr.findOne(PropertyEnum.INVOICENUMBER.getProperty());            
            String invoicenumber = p.getValue();            
            Integer in = new Integer(invoicenumber);
            in = in + 1;
            p.setValue(in.toString());
            pr.saveAndFlush(p);
            
            final Workbook wb = (Workbook)new HSSFWorkbook((InputStream)new FileInputStream(name));
            final Sheet s1 = wb.getSheetAt(0);
            s1.getRow(4).getCell(1).setCellValue(String.valueOf(u.getName()) + " " + u.getLastname());
            //s1.getRow(5).getCell(1).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()));
            s1.getRow(6).getCell(1).setCellValue(u.getMobilphone());
            s1.getRow(7).getCell(1).setCellValue(u.getEmail());
            s1.getRow(2).getCell(4).setCellValue(invoicenumber);
            s1.getRow(3).getCell(4).setCellValue(df.format(date));
            s1.getRow(4).getCell(4).setCellValue(pm.getDninumber());
            
            s1.getRow(9).getCell(0).setCellValue(up.getIdplan().getPlanid());
            
            s1.getRow(12).getCell(3).setCellValue(up.getPrice().toString());
            
            
            final String ruta = this.env.getRequiredProperty("outputpath");
            
            final String filename = "Invoice_" + String.valueOf(UUID.randomUUID().toString()) + "_" + df.format(date);
            path = String.valueOf(ruta) + filename + ".xls";
            final FileOutputStream fileOut = new FileOutputStream(path);
            wb.write((OutputStream)fileOut);
            fileOut.close();
            pathpdf = String.valueOf(ruta) + filename + ".pdf";
            Utils.convertDocuments(path, pathpdf); 
            return pathpdf;
    }    
}
