package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import co.com.jit.backend.util.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.*;
import org.apache.commons.lang.*;

@Service
@Transactional
public class PreproccesingServiceImpl implements PreproccesingService
{
    private static final Logger logger;
    @Autowired
    InflectionDictionaryService ids;
    @Autowired
    DictionaryService ds;
    @Autowired
    MastervalueRepository mvr;
    @Autowired
    PropertiesRepository pr;
    
    static {
        logger = Logger.getLogger((Class)PreproccesingServiceImpl.class);
    }
    
    @Override
    public String preproccesingText(String tocheck) {
        String spellcheckedtext = "";
        try {
            if (tocheck.contains("on Twitter: ")) {
                tocheck = tocheck.split("on Twitter: ")[1];
            }
            tocheck = tocheck.toLowerCase();
            tocheck = Utils.deleteaccentmarkdieresis(tocheck);
            tocheck = Utils.cleanString(this.mvr.findBymastertypename(MasterListEnum.REGEXTOCLEANPUNTUATIONMARKS.getMasterlist()), tocheck);
            tocheck = Utils.replacePunctuationMarks(tocheck);
            tocheck = Utils.cleanRepeatedChars(tocheck);
            boolean begintocheck = true;
            final StringTokenizer st = new StringTokenizer(tocheck);
            while (st.hasMoreElements()) {
                final String token = st.nextToken();
                if (token.length() > 1) {
                    if (!NumberUtils.isNumber(token)) {
                        if (!Utils.isHashtag(token) && !Utils.isUserMention(token)) {
                            begintocheck = false;
                            final String lemma = this.ids.getDictionary().getLemma(token);
                            if (lemma != null) {
                                spellcheckedtext = String.valueOf(spellcheckedtext) + " " + lemma;
                            }
                            else {
                                spellcheckedtext = String.valueOf(spellcheckedtext) + " " + token;
                            }
                        }
                        else if (Utils.isUserMention(token)) {
                            if (begintocheck) {
                                spellcheckedtext = spellcheckedtext;
                            }
                            else {
                                spellcheckedtext = String.valueOf(spellcheckedtext) + " " + token;
                            }
                        }
                        else {
                            spellcheckedtext = spellcheckedtext;
                        }
                    }
                    else {
                        spellcheckedtext = String.valueOf(spellcheckedtext) + " " + token;
                    }
                }
                else {
                    spellcheckedtext = spellcheckedtext;
                }
            }
            spellcheckedtext = Utils.getSentimentWordFromEmoticon(spellcheckedtext);
        }
        catch (Exception e) {
            PreproccesingServiceImpl.logger.error((Object)e);
        }
        return spellcheckedtext;
    }
}
