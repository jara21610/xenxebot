package co.com.zien.xenxerobot.backend.enumerator;

public enum PackagesStateEnum
{
    AVAILABLE("AVAILABLE", 0, "AVAILABLE"), 
    PROCCESS("PROCCESS", 1, "PROCCESS"), 
    FINISHED("FINISHED", 2, "FINISHED");
    
    private String state;
    
    public String getState() {
        return this.state;
    }
    
    private PackagesStateEnum(final String s, final int n, final String state) {
        this.state = state;
    }
}
