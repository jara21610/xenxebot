package co.com.zien.xenxerobot.backend.services;

public interface RegistryService
{
    void registerUser(final RegisterUserDTO p0) throws UserAlreadyExistException;
    
    String getUserLicence();
}
