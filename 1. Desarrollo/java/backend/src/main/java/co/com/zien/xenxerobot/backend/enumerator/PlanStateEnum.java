package co.com.zien.xenxerobot.backend.enumerator;

public enum PlanStateEnum
{
    ACTIVO("ACTIVO", 0, "ACTIVO"), 
    FINALIZADO("FINALIZADO", 1, "FINALIZADO"), 
    NOTPAID("NOTPAID", 2, "NOTPAID");
    
    private String state;
    
    public String getState() {
        return this.state;
    }
    
    private PlanStateEnum(final String s, final int n, final String state) {
        this.state = state;
    }
}
