package co.com.zien.xenxerobot.backend.services;

public class Order
{
    private String accountId;
    private String referenceCode;
    private String description;
    private String language;
    private String signature;
    private String notifyUrl;
    private Buyer buyer;
    private ShippingAddress shippingAddress;
    private AdditionalValues additionalValues;
    
    public String getAccountId() {
        return this.accountId;
    }
    
    public void setAccountId(final String accountId) {
        this.accountId = accountId;
    }
    
    public String getReferenceCode() {
        return this.referenceCode;
    }
    
    public void setReferenceCode(final String referenceCode) {
        this.referenceCode = referenceCode;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getLanguage() {
        return this.language;
    }
    
    public void setLanguage(final String language) {
        this.language = language;
    }
    
    public String getSignature() {
        return this.signature;
    }
    
    public void setSignature(final String signature) {
        this.signature = signature;
    }
    
    public String getNotifyUrl() {
        return this.notifyUrl;
    }
    
    public void setNotifyUrl(final String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
    
    public AdditionalValues getAdditionalValues() {
        return this.additionalValues;
    }
    
    public void setAdditionalValues(final AdditionalValues additionalValues) {
        this.additionalValues = additionalValues;
    }
    
    public Buyer getBuyer() {
        return this.buyer;
    }
    
    public void setBuyer(final Buyer buyer) {
        this.buyer = buyer;
    }
    
    public ShippingAddress getShippingAddress() {
        return this.shippingAddress;
    }
    
    public void setShippingAddress(final ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
