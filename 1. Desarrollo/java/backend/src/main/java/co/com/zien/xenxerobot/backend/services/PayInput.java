package co.com.zien.xenxerobot.backend.services;

public class PayInput
{
    private String language;
    private String command;
    private MerchantInput merchant;
    private Transaction transaction;
    private boolean test;
    
    public String getLanguage() {
        return this.language;
    }
    
    public void setLanguage(final String language) {
        this.language = language;
    }
    
    public String getCommand() {
        return this.command;
    }
    
    public void setCommand(final String command) {
        this.command = command;
    }
    
    public MerchantInput getMerchant() {
        return this.merchant;
    }
    
    public void setMerchant(final MerchantInput merchant) {
        this.merchant = merchant;
    }
    
    public Transaction getTransaction() {
        return this.transaction;
    }
    
    public void setTransaction(final Transaction transaction) {
        this.transaction = transaction;
    }
    
    public boolean isTest() {
        return this.test;
    }
    
    public void setTest(final boolean test) {
        this.test = test;
    }
}
