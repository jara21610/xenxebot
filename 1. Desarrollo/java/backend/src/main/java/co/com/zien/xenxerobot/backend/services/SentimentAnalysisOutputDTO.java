package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class SentimentAnalysisOutputDTO
{
    private String sentiment;
    private String category;
    private List<SentimentAnalysisConceptOutputDTO> conceptlist;
    
    public String getSentiment() {
        return this.sentiment;
    }
    
    public void setSentiment(final String sentiment) {
        this.sentiment = sentiment;
    }
    
    public String getCategory() {
        return this.category;
    }
    
    public void setCategory(final String category) {
        this.category = category;
    }
    
    public List<SentimentAnalysisConceptOutputDTO> getConceptlist() {
        return this.conceptlist;
    }
    
    public void setConceptlist(final List<SentimentAnalysisConceptOutputDTO> conceptlist) {
        this.conceptlist = conceptlist;
    }
}
