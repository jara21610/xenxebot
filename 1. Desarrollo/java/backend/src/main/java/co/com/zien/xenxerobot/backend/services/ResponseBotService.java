package co.com.zien.xenxerobot.backend.services;

public interface ResponseBotService
{
    void okResponseBot(final String p0, final ComandDTO p1);
    
    void isprocessingResponseBot(final ComandDTO p0);
    
    void okPartialReportResponseBot(final String p0, final ComandDTO p1);
    
    void exceptionResponseBot(final ComandDTO p0, final Exception p1);
    
    void messageResponseBot(final String p0, final ComandDTO p1);
}
