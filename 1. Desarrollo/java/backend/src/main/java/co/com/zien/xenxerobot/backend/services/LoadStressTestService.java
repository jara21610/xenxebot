package co.com.zien.xenxerobot.backend.services;

public interface LoadStressTestService
{
    void putDataProcessingPost(final LoadStressProcessingPost p0) throws Exception;
    
    ResumeSnapshotProcessingPostDTO getResumeSnapshotProcessingPost() throws Exception;
}
