package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class ReportDetailDTO
{
    private String type;
    private Map<String, Object> reportDetails;
    
    public ReportDetailDTO() {
        this.reportDetails = new HashMap<String, Object>();
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public Map<String, Object> getReportDetails() {
        return this.reportDetails;
    }
    
    public void setReportDetails(final Map<String, Object> reportDetails) {
        this.reportDetails = reportDetails;
    }
}
