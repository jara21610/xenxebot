package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.Paymentmethod;
import co.com.zien.xenxerobot.persistence.entity.Userplan;
import co.com.zien.xenxerobot.persistence.entity.Users;

public interface InvoiceReport {
	
	public String generateInvoice(Users u, Userplan up, Paymentmethod pm) throws Exception;

}
