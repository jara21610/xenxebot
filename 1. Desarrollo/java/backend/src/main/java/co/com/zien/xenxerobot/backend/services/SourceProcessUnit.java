package co.com.zien.xenxerobot.backend.services;

import java.util.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.util.concurrent.*;

public interface SourceProcessUnit
{
    CompletableFuture<String> executeUnit(final List p0, final CommandProcess p1, final ComandDTO p2);
}
