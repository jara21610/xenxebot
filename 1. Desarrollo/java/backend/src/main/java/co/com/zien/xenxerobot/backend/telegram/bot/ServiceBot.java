package co.com.zien.xenxerobot.backend.telegram.bot;

import java.util.List;

import com.restfb.types.Account;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface ServiceBot
{
    void handleUserTokenException(final String p0);
    
    void handleUpgradePlanOK(final String p0);
    
    void handlePaymentExceptionUpgradePlan(final String p0, final Users p1);
    
    void handleFreePlanFinished(final String p0, final Users p1);
    
    public void handleFacebookSelectAccount(List<Account> al, Users u);
    
    public void handlePlanTxFinished(final String chatid, final Users u);
    
}
