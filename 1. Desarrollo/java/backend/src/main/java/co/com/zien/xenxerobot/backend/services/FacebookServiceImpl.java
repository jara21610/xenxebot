package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.env.Environment;

import com.restfb.types.*;
import com.restfb.types.instagram.IgMedia;
import com.restfb.*;
import java.util.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.lang.Thread;

@Service
@Transactional
public class FacebookServiceImpl implements FacebookService
{
    @Autowired
    @Qualifier("FacebookPoolConectionManager")
    SocialPoolConectionManager fpcm;
    @Autowired
    Environment env;
    
    private List<Comment> searchCommentsfromPage(final String pageid, final String since) throws Exception {
        final List<Comment> allcoments = new ArrayList<Comment>();
        final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction("");
        final Connection<Post> postConnection = (Connection<Post>)facebookClient.fetchConnection(String.valueOf(pageid) + "/posts", (Class)Post.class, new Parameter[] { Parameter.with("since", (Object)since) });
        for (final List<Post> myFeedPage : postConnection) {
            for (final Post post : myFeedPage) {
                final String postId = post.getId();
                final Connection<Comment> commentConnection = (Connection<Comment>)facebookClient.fetchConnection(String.valueOf(postId) + "/comments", (Class)Comment.class, new Parameter[0]);
                for (final List<Comment> commentPage : commentConnection) {
                    allcoments.addAll(commentPage);
                }
            }
        }
        return allcoments;
    }
    
    @Override
    public List<Post> searchlastPostsFacebook(final String pageid, final int numberPost, String userToken) throws Exception {
    	final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(userToken);
        final Connection<Post> postConnection = (Connection<Post>)facebookClient.fetchConnection(String.valueOf(pageid) + "/posts", (Class)Post.class, new Parameter[] { Parameter.with("limit", (Object)numberPost) });
        if (postConnection.iterator().hasNext()) {
            return (List<Post>)postConnection.iterator().next();
        }
        return null;
    }
    
    private List<Comment> searchCommentsfromPosts(final String postid, final String since) throws Exception {
        final List<Comment> allcoments = new ArrayList<Comment>();
        final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction("");
        final Connection<Comment> commentConnection = (Connection<Comment>)facebookClient.fetchConnection(String.valueOf(postid) + "/comments", (Class)Comment.class, new Parameter[0]);
        for (final List<Comment> commentPage : commentConnection) {
            allcoments.addAll(commentPage);
        }
        return allcoments;
    }
    
    private Object getAvailableConecction(String userToken) throws Exception {
        boolean success = false;
        while (!success) {
            try {
                success = true;
                return this.fpcm.getAvailableConecction(userToken);
            }
            catch (SocialPoolConectionManagerNotFoundAvailableException e) {
                Thread.currentThread();
                Thread.sleep(this.fpcm.getMinimalSecondResetConection() * 60000);
            }
        }
        return null;
    }
    
    public List<Comment> searchComments(final String searchType, final String searchCriteria, final String since) throws Exception {
        List<Comment> lc = null;
        if (searchType.equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
            lc = this.searchCommentsfromPage(searchCriteria, since);
        }
        else if (searchType.equals(FacebookSearchTypeEnum.POST.getFacebooksearchtype())) {
            lc = this.searchCommentsfromPosts(searchCriteria, since);
        }
        return lc;
    }

	public String getExtendedToken(String userToken) throws Exception {
		final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(userToken);
		String extendedToken = facebookClient.obtainExtendedAccessToken(this.env.getRequiredProperty("facebook.apiKey"), this.env.getRequiredProperty("facebook.secretKey"), userToken).getAccessToken();
		return extendedToken;
	}
	
	public List<Account> getAccountList(String userToken) throws Exception {
		
		final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(userToken);
		
		Connection<Account> postConnection = facebookClient.fetchConnection("me/accounts/", Account.class);
		
		if (postConnection.getData().size() > 0) {
			
			return postConnection.getData();
		} else {
			return null;
		}
		
		
	}
	
	public String getInstAccountBussiness(String idPage, String userToken) throws Exception {
		final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(userToken);		
		Page p = facebookClient.fetchObject(idPage, Page.class, Parameter.with("fields", "instagram_business_account"));
		
		
		
		if (p != null) {
			System.out.println("Page" + p.toString());
			if (p.getInstagramBusinessAccount() != null) {
				System.out.println("Page Instagram " + p.getInstagramBusinessAccount().toString());
				return p.getInstagramBusinessAccount().getId(); 
			} else {
				return null;
			}
		} else {
			return null;
		}
		
	}

	@Override
	public List<IgMedia> searchlastPostsInstagram(String pageid, int numberPost, String userToken) throws Exception {
		final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(userToken);
        final Connection<IgMedia> postConnection = (Connection<IgMedia>)facebookClient.fetchConnection(String.valueOf(pageid) + "/media", (Class)IgMedia.class, new Parameter[] { Parameter.with("limit", (Object)numberPost),Parameter.with("fields", "caption") });
        if (postConnection.iterator().hasNext()) {
            return (List<IgMedia>)postConnection.iterator().next();
        }
        return null;
	}
}
