package co.com.zien.xenxerobot.backend.services;

public interface TransactionCounterService
{
	public int addUsedTransactionPlan(final String userid, final int tx, String productid) throws TxLimitException;
}
