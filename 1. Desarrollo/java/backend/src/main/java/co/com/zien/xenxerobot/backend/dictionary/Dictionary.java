package co.com.zien.xenxerobot.backend.dictionary;

import org.apache.log4j.*;
import java.util.*;
import java.io.*;

public class Dictionary
{
    private static final Logger logger;
    private HashMap<String, String> dictionary;
    private File plaintxtDictionary;
    
    static {
        logger = Logger.getLogger((Class)Dictionary.class);
    }
    
    public Dictionary() {
        this.dictionary = new HashMap<String, String>();
    }
    
    public File getPlaintxtDictionary() {
        return this.plaintxtDictionary;
    }
    
    public void setPlaintxtDictionary(final File plaintxtDictionary) {
        this.plaintxtDictionary = plaintxtDictionary;
    }
    
    public void loadPlainTextDictionary() {
        try {
            final BufferedReader in = new BufferedReader(new FileReader(this.plaintxtDictionary));
            String line = "";
            while ((line = in.readLine()) != null) {
                this.dictionary.put(line, line);
            }
            in.close();
        }
        catch (Exception e) {
            Dictionary.logger.error((Object)e);
        }
    }
    
    public boolean existWord(final String word) {
        return this.dictionary.containsKey(word);
    }
}
