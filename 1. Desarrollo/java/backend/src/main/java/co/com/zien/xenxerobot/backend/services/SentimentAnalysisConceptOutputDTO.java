package co.com.zien.xenxerobot.backend.services;

public class SentimentAnalysisConceptOutputDTO
{
    private String type;
    private String name;
    private String sentiment;
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getSentiment() {
        return this.sentiment;
    }
    
    public void setSentiment(final String sentiment) {
        this.sentiment = sentiment;
    }
}
