package co.com.zien.xenxerobot.backend.enumerator;

public enum ConceptsEnum
{
    ENTIDAD("ENTIDAD", 0, "ENTIDAD"), 
    TOPICO("TOPICO", 1, "TOPICO");
    
    private String concept;
    
    public String getConcept() {
        return this.concept;
    }
    
    private ConceptsEnum(final String s, final int n, final String concept) {
        this.concept = concept;
    }
}
