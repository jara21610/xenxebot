package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.stereotype.Service;
import org.apache.log4j.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import javax.mail.internet.*;
import java.io.*;
import javax.mail.*;
import java.util.*;

@Service
public class EmailServicesImpl implements EmailService
{
    private static final Logger logger;
    @Autowired
    PropertiesRepository pr;
    private Store store;
    
    static {
        logger = Logger.getLogger((Class)EmailServicesImpl.class);
    }
    
    public void sendEmail(final String from, final String to, final String subject, final String text) {
        try {
            final Message message = (Message)new MimeMessage(this.getSession());
            message.setFrom((Address)new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, (Address[])InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            System.out.println("Done");
        }
        catch (MessagingException e) {
            System.out.println("Exception e");
            throw new RuntimeException((Throwable)e);
        }
    }
    
    public void sendHtmlEmail(final String from, final String to, final String subject, final String body, final Map<String, String> mapInlineImages) {
        try {
            final Message message = (Message)new MimeMessage(this.getSession());
            message.setFrom((Address)new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, (Address[])InternetAddress.parse(to));
            message.setSubject(subject);
            message.setHeader("charset", "UTF-8");
            final MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent((Object)body, "text/html");
            final Multipart multipart = (Multipart)new MimeMultipart();
            multipart.addBodyPart((BodyPart)messageBodyPart);
            if (mapInlineImages != null && mapInlineImages.size() > 0) {
                final Set<String> setImageID = mapInlineImages.keySet();
                for (final String contentId : setImageID) {
                    final MimeBodyPart imagePart = new MimeBodyPart();
                    imagePart.setHeader("Content-ID", "<" + contentId + ">");
                    imagePart.setDisposition("inline");
                    final String imageFilePath = mapInlineImages.get(contentId);
                    try {
                        imagePart.attachFile(imageFilePath);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    multipart.addBodyPart((BodyPart)imagePart);
                }
            }
            message.setContent(multipart);
            Transport.send(message);
            System.out.println("Done");
        }
        catch (MessagingException e2) {
            System.out.println("Exception e");
            throw new RuntimeException((Throwable)e2);
        }
    }
    
    public void sendEmail(final String from, final String to, final String subject, final String text, final String[] attachFiles) {
        try {
            final Message message = (Message)new MimeMessage(this.getSession());
            message.setFrom((Address)new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, (Address[])InternetAddress.parse(to));
            message.setSubject(subject);
            message.setSentDate(new Date());
            final MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(text);
            final Multipart multipart = (Multipart)new MimeMultipart();
            multipart.addBodyPart((BodyPart)messageBodyPart);
            if (attachFiles != null && attachFiles.length > 0) {
                for (final String filePath : attachFiles) {
                    final MimeBodyPart attachPart = new MimeBodyPart();
                    try {
                        attachPart.attachFile(filePath);
                    }
                    catch (IOException ex) {}
                    multipart.addBodyPart((BodyPart)attachPart);
                }
            }
            message.setContent(multipart);
            Transport.send(message);
            System.out.println("Done");
        }
        catch (MessagingException e) {
            System.out.println("Exception e" + e.getMessage());
            throw new RuntimeException((Throwable)e);
        }
    }
    
    public List<EmailMessageDTO> getNewEmailsImap() {
        final List<EmailMessageDTO> lm = new ArrayList<EmailMessageDTO>();
        try {
            final Session session = this.getSession();
            (this.store = session.getStore("imaps")).connect(this.pr.findByName("mail.smtp.host"), this.pr.findByName("mail.smtp.email"), this.pr.findByName("mail.smtp.email.password"));
            final Folder inbox = this.store.getFolder("inbox");
            inbox.open(2);
            final Message[] messages = inbox.getMessages();
            Message[] array;
            for (int length = (array = messages).length, i = 0; i < length; ++i) {
                final Message message = array[i];
                if (!message.getFlags().contains(Flags.Flag.SEEN)) {
                    message.setFlag(Flags.Flag.SEEN, true);
                    final EmailMessageDTO emdto = new EmailMessageDTO();
                    emdto.setFrom(((InternetAddress)message.getFrom()[0]).getAddress());
                    emdto.setText(this.getTextFromMessage(message));
                    lm.add(emdto);
                }
            }
            inbox.close(true);
            this.store.close();
            return lm;
        }
        catch (Exception e) {
            EmailServicesImpl.logger.error((Object)e.getMessage());
            return null;
        }
    }
    
    public Properties getServerPropertiesSmtp() {
        final Properties props = new Properties();
        props.put("mail.smtp.auth", this.pr.findByName("mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", this.pr.findByName("mail.smtp.starttls.enable"));
        props.put("mail.smtp.host", this.pr.findByName("mail.smtp.host"));
        props.put("mail.smtp.port", this.pr.findByName("mail.smtp.port"));
        props.put("mail.debug", this.pr.findByName("mail.debug"));
        return props;
    }
    
    public Session getSession() {
        final Session session = Session.getInstance(this.getServerPropertiesSmtp(), (Authenticator)new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(EmailServicesImpl.this.pr.findByName("mail.smtp.email"), EmailServicesImpl.this.pr.findByName("mail.smtp.email.password"));
            }
        });
        return session;
    }
    
    private String getTextFromMessage(final Message message) throws Exception {
        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        }
        else if (message.isMimeType("multipart/*")) {
            final MimeMultipart mimeMultipart = (MimeMultipart)message.getContent();
            result = this.getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }
    
    private String getTextFromMimeMultipart(final MimeMultipart mimeMultipart) throws Exception {
        String result = "";
        for (int count = mimeMultipart.getCount(), i = 0; i < count; ++i) {
            final BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = String.valueOf(result) + "\n" + bodyPart.getContent();
                break;
            }
            if (bodyPart.isMimeType("text/html")) {
                final String html = (String)bodyPart.getContent();
                result = String.valueOf(result) + "\n" + html;
            }
            else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = String.valueOf(result) + this.getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }
}
