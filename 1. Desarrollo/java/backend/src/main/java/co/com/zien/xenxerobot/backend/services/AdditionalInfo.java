package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class AdditionalInfo
{
    private String paymentNetwork;
    private String rejectionType;
    private Object responseNetworkMessage;
    private Object travelAgencyAuthorizationCode;
    private Object cardType;
    private Map<String, Object> additionalProperties;
    
    public AdditionalInfo() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    public String getPaymentNetwork() {
        return this.paymentNetwork;
    }
    
    public void setPaymentNetwork(final String paymentNetwork) {
        this.paymentNetwork = paymentNetwork;
    }
    
    public String getRejectionType() {
        return this.rejectionType;
    }
    
    public void setRejectionType(final String rejectionType) {
        this.rejectionType = rejectionType;
    }
    
    public Object getResponseNetworkMessage() {
        return this.responseNetworkMessage;
    }
    
    public void setResponseNetworkMessage(final Object responseNetworkMessage) {
        this.responseNetworkMessage = responseNetworkMessage;
    }
    
    public Object getTravelAgencyAuthorizationCode() {
        return this.travelAgencyAuthorizationCode;
    }
    
    public void setTravelAgencyAuthorizationCode(final Object travelAgencyAuthorizationCode) {
        this.travelAgencyAuthorizationCode = travelAgencyAuthorizationCode;
    }
    
    public Object getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final Object cardType) {
        this.cardType = cardType;
    }
    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
}
