package co.com.zien.xenxerobot.backend.services;

public class SocialPoolConectionManagerNotFoundAvailableException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public SocialPoolConectionManagerNotFoundAvailableException(final String message) {
        super(message);
    }
}
