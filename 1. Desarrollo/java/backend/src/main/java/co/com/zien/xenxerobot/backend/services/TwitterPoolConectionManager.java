package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.apache.log4j.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.io.*;
import org.springframework.context.i18n.*;
import org.springframework.social.twitter.api.impl.*;
import org.springframework.social.twitter.api.*;
import java.util.*;
import co.com.zien.xenxerobot.persistence.entity.*;

@Service("TwitterPoolConectionManager")
@Transactional
public class TwitterPoolConectionManager implements SocialPoolConectionManager
{
    private static final Logger logger;
    List<SocialConnectionDTO> scdtolist;
    int windowsizeminutes;
    int limitcallsallowed;
    @Autowired
    SocialConnectionRepository scr;
    @Autowired
    MessageSource messageSource;
    
    static {
        logger = Logger.getLogger((Class)TwitterPoolConectionManager.class);
    }
    
    public TwitterPoolConectionManager() {
        this.scdtolist = null;
        this.windowsizeminutes = 0;
        this.limitcallsallowed = 0;
    }
    
    @Override
    public synchronized void init() throws SocialPoolConectionManagerNotConfigured {
        this.scdtolist = new ArrayList<SocialConnectionDTO>();
        final SocialConnection sc = (SocialConnection)this.scr.findOne(SocialMediaSourceEnum.TWITTER.getSocialmediasource());
        if (sc == null) {
            final String message = this.messageSource.getMessage("message.exception.notconfiguredsocialpoolconnectionmanager", new Object[] { SocialMediaSourceEnum.TWITTER.getSocialmediasource() }, LocaleContextHolder.getLocale());
            TwitterPoolConectionManager.logger.error((Object)message);
            throw new SocialPoolConectionManagerNotConfigured(message);
        }
        this.windowsizeminutes = sc.getWindowsizeminutes();
        this.limitcallsallowed = sc.getLimitcallsallowed();
        if (sc.getSocialconecctiondetails().size() > 0) {                        
            sc.getSocialconecctiondetails().forEach(scd -> {
            	SocialConnectionDTO scdto = new SocialConnectionDTO();
                scdto.setKeyData1(scd.getKeydata1());
                scdto.setKeyData2(scd.getKeydata2());
                scdto.setActive(true);
                scdto.setRemaining(sc.getLimitcallsallowed());
                scdto.setWindowminutesize(sc.getWindowsizeminutes());
                scdto.setLimitcallsize(sc.getLimitcallsallowed());
                scdto.setConnection(new TwitterTemplate(scdto.getKeyData1(), scdto.getKeyData2()));
                this.scdtolist.add(scdto);
            });
            return;
        }
        final String message = this.messageSource.getMessage("message.exception.notconfiguredsocialpoolconnectionmanager", new Object[] { SocialMediaSourceEnum.TWITTER.getSocialmediasource() }, LocaleContextHolder.getLocale());
        TwitterPoolConectionManager.logger.error((Object)message);
        throw new SocialPoolConectionManagerNotConfigured(message);
    }
    
    @Override
    public Object getAvailableConecction(final String... parameter) throws SocialPoolConectionManagerNotConfigured, SocialPoolConectionManagerNotFoundAvailableException {
        if (this.scdtolist == null) {
            this.init();
        }
        return this.getAvailableConecctionAgainsServerInfo();
    }
    
    @Override
    public List<SocialConnectionDTO> getCurrentState() {
        return this.scdtolist;
    }
    
    @Transactional
    @Override
    public Object getSocialServerLimitStatus() {
        if (this.scdtolist == null) {
            try {
                this.init();
            }
            catch (SocialPoolConectionManagerNotConfigured e) {
                TwitterPoolConectionManager.logger.error((Object)e.getMessage());
                return e.getMessage();
            }
        }
        final List<Map<ResourceFamily, List<RateLimitStatus>>> ratelimitstatusMapList = new ArrayList<Map<ResourceFamily, List<RateLimitStatus>>>();
        for (final SocialConnectionDTO scdto : this.scdtolist) {
            ratelimitstatusMapList.add(new TwitterTemplate(scdto.getKeyData1(), scdto.getKeyData2()).userOperations().getRateLimitStatus(new ResourceFamily[] { ResourceFamily.SEARCH, ResourceFamily.APPLICATION, ResourceFamily.BLOCKS }));
        }
        return ratelimitstatusMapList;
    }
    
    private Object getAvailableConecctionAgainsServerInfo() throws SocialPoolConectionManagerNotFoundAvailableException {
        for (final SocialConnectionDTO scdto : this.scdtolist) {
            if (scdto.isActive()) {
                int remaininghits = scdto.getRemaining();
                if (scdto.getWindowFinish() == null) {
                    --remaininghits;
                    scdto.setRemaining(remaininghits);
                    final Calendar cal = Calendar.getInstance();
                    cal.add(12, scdto.getWindowminutesize());
                    scdto.setWindowFinish(cal.getTime());
                    System.out.println("WF" + scdto.getWindowFinish());
                    return scdto.getConnection();
                }
                if (!new Date().before(scdto.getWindowFinish())) {
                    scdto.setRemaining(scdto.getLimitcallsize());
                    final Calendar cal = Calendar.getInstance();
                    cal.add(12, scdto.getWindowminutesize());
                    scdto.setWindowFinish(cal.getTime());
                    System.out.println("WF" + scdto.getWindowFinish());
                    return scdto.getConnection();
                }
                System.out.println("RH" + scdto.getRemaining());
                if (remaininghits >= 5) {
                    --remaininghits;
                    scdto.setRemaining(remaininghits);
                    return scdto.getConnection();
                }
                final Long timetoReset = scdto.getWindowFinish().getTime() - new Date().getTime();
                scdto.setSecondsuntilreset(timetoReset.intValue() + 5000);
                scdto.setRemaining(scdto.getLimitcallsize());
                scdto.setActive(false);
                System.out.println("SUR" + scdto.getSecondsuntilreset());
            }
            else {
                if (new Date().after(scdto.getWindowFinish())) {
                    scdto.setRemaining(scdto.getLimitcallsize());
                    scdto.setActive(true);
                    final Calendar cal2 = Calendar.getInstance();
                    cal2.add(12, scdto.getWindowminutesize());
                    scdto.setWindowFinish(cal2.getTime());
                    System.out.println("WF" + scdto.getWindowFinish());
                    return scdto.getConnection();
                }
                continue;
            }
        }
        System.out.println("No hay conexiones disponibles");
        throw new SocialPoolConectionManagerNotFoundAvailableException("");
    }
    
    @Override
    public int getMinimalSecondResetConection() {
        this.scdtolist.sort((scdto1, scdto2) -> scdto1.getSecondsuntilreset() - scdto2.getSecondsuntilreset());
        return this.scdtolist.get(0).getSecondsuntilreset();
    }
}
