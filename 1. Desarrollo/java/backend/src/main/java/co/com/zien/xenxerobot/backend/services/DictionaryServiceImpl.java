package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import co.com.zien.xenxerobot.backend.dictionary.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import java.io.*;

@Service
@Transactional
public class DictionaryServiceImpl implements DictionaryService
{
    private Dictionary dictionary;
    @Autowired
    PropertiesRepository pr;
    
    public DictionaryServiceImpl() {
        this.dictionary = null;
    }
    
    public Dictionary getDictionary() {
        if (this.dictionary == null) {
            (this.dictionary = new Dictionary()).setPlaintxtDictionary(new File("c:/listado-general.txt"));
            this.dictionary.loadPlainTextDictionary();
        }
        return this.dictionary;
    }
}
