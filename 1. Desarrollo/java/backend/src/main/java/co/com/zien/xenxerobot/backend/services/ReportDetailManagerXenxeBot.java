package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import com.thoughtworks.xstream.*;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.context.i18n.*;
import java.text.*;
import java.util.*;
import java.io.*;
import java.nio.file.Files;

import co.com.jit.backend.util.*;
import org.apache.poi.ss.usermodel.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.scheduling.annotation.*;

@Service("XenxeReportDetailManager")
public class ReportDetailManagerXenxeBot implements ReportDetailManager
{
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    ResponseBotServiceFactory rbsf;
    @Autowired
    PostRepository postr;
    @Autowired
    Environment env;
    
    @Override
    public void saveStatus(final CommandProcess cp, final ReportDetailDTO rrdto) {
        final XStream xs = new XStream();
        final String reportdetail = xs.toXML((Object)rrdto);
        cp.setReportdetail(reportdetail);
        this.cpr.saveAndFlush(cp);
        System.out.println("PASE");
    }
    
    @Override
    public String generateSentimentAnalysisReport(final String cpId) throws Exception {
        String path = "";
        String pathpdf = "";
        final CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
        final int total = this.postr.totalPost(cp.getId());
        if (total > 0) {
        	final String ruta = this.env.getRequiredProperty("outputpath");
            final XStream xs = new XStream();
            final ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
            final String name = this.pr.findByName(PropertyEnum.XENXETEMPLATE.getProperty());
            final Workbook wb = (Workbook)new HSSFWorkbook((InputStream)new FileInputStream(name));
            final Sheet s1 = wb.getSheetAt(0);
            s1.getRow(9).getCell(2).setCellValue(String.valueOf(cp.getUser().getName()) + " " + cp.getUser().getLastname());
            s1.getRow(10).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()));
            s1.getRow(11).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()));
            s1.getRow(12).getCell(2).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()));
            final Date date = new Date();
            final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
            s1.getRow(9).getCell(7).setCellValue(df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())));
            s1.getRow(10).getCell(7).setCellValue(df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())));
            s1.getRow(11).getCell(7).setCellValue(df.format(date));
            s1.getRow(12).getCell(7).setCellValue((double)total);
            final int positive = this.postr.totalXSentiment(cp.getId(), SentimentEnum.POSITIVE.getSentiment());
            final int negative = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEGATIVE.getSentiment());
            final int neutral = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEUTRAL.getSentiment());
            final HashMap<String, Object> sentimentpiecontext = new HashMap<String, Object>();
            sentimentpiecontext.put("positivo", new Integer(positive).toString());
            sentimentpiecontext.put("negativo", new Integer(negative).toString());
            sentimentpiecontext.put("neutral", new Integer(neutral).toString());
            String filenamejpg = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            String filenamehtml = String.valueOf(UUID.randomUUID().toString()) + ".html";
            Utils.trasformTemplate(this.env.getRequiredProperty("sentimentpie.template.path"), String.valueOf(ruta) + filenamehtml, sentimentpiecontext);
            Utils.coverthtmltojpeg(String.valueOf(ruta) + filenamehtml, String.valueOf(ruta) + filenamejpg, 600, 310);
            Thread.currentThread();
            Thread.sleep(10000L);
            final int my_picture_id = wb.addPicture(Files.readAllBytes(new File(String.valueOf(ruta) + filenamejpg).toPath()), 5);
            final HSSFPatriarch drawing = (HSSFPatriarch)s1.createDrawingPatriarch();
            final ClientAnchor my_anchor = (ClientAnchor)new HSSFClientAnchor();
            my_anchor.setCol1(2);
            my_anchor.setRow1(17);
            final HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
            my_picture.resize();            
            final String filename = String.valueOf(UUID.randomUUID().toString()) + "_" + cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) + "_" + df.format(date);
            path = String.valueOf(ruta) + filename + ".xls";
            final FileOutputStream fileOut = new FileOutputStream(path);
            wb.write((OutputStream)fileOut);
            fileOut.close();
            pathpdf = String.valueOf(ruta) + filename + ".pdf";
            Utils.convertDocuments(path, pathpdf);
        }
        return pathpdf;
    }
    
    @Async
    @Override
    public void reportdetailgenerateSentimentAnalysisReport(final String cpId) throws Exception {
        final String path = this.generateSentimentAnalysisReport(cpId);
        final ResponseBotService rbs = this.rbsf.getResponseBotService(ChannelEnum.TELEGRAM.getChannel());
        final CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
        final XStream xs = new XStream();
        final ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
        rbs.okPartialReportResponseBot(path, cdto);
    }
}
