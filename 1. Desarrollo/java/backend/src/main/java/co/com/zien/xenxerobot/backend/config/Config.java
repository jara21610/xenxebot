package co.com.zien.xenxerobot.backend.config;

import org.springframework.scheduling.annotation.*;
import co.com.zien.xenxerobot.persistence.config.*;
import co.com.zien.xenxerobot.backend.telegram.bot.*;
import org.telegram.telegrambots.*;
import org.springframework.context.*;
import org.springframework.context.annotation.*;
import java.util.concurrent.*;
import org.springframework.scheduling.concurrent.*;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.LongPollingBot;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import org.springframework.context.support.*;
import org.springframework.aop.interceptor.*;
import org.springframework.beans.*;

@Configuration
@EnableAsync
@ComponentScan({ "co.com.zien.xenxerobot.backend", "co.com.jit.backend" })
@Import({ PersistenceConfig.class })
public class Config implements AsyncConfigurer, ApplicationContextAware
{
    static ApplicationContext applicationContext;
    private PreXtigeBot PreXtigeBot;
    private XenxeBot XenxeBot;
    private StressLoadTestBot StressLoadTestBot;
    TelegramBotsApi botsApi;
    
    static {
        Config.applicationContext = null;        
    }
    
    public Config() {
        this.PreXtigeBot = null;
        this.XenxeBot = null;
        this.StressLoadTestBot = null;
        try {
			this.botsApi = new TelegramBotsApi(DefaultBotSession.class);
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Bean(name = { "messageSource" })
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/resources/usermsg");
        messageSource.setDefaultEncoding("UTF-8");
        return (MessageSource)messageSource;
    }
    
    public Executor getAsyncExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(Integer.MAX_VALUE);
        executor.setQueueCapacity(0);
        executor.setThreadNamePrefix("Spring-");
        executor.initialize();
        return (Executor)executor;
    }
    
    @Bean(name = { "PreXtige" })
    public PreXtigeBot PreXtigeBot() {
        if (this.PreXtigeBot == null) {
            this.PreXtigeBot = new PreXtigeBot();
            try {
                this.botsApi.registerBot(this.PreXtigeBot);
            }
            catch (TelegramApiException e) {
                e.printStackTrace();
            }
            return this.PreXtigeBot;
        }
        return this.PreXtigeBot;
    }
    
    //@Bean(name = { "Xenxe" })
    /*public XenxeBot xenxeBot() {
        if (this.XenxeBot == null) {
            this.XenxeBot = new XenxeBot();
            try {
                this.botsApi.registerBot(this.XenxeBot);
            }
            catch (TelegramApiException e) {
                e.printStackTrace();
            }
            return this.XenxeBot;
        }
        return this.XenxeBot;
    }*/
    
    /*@Bean(name = { "StressLoadTestBot" })
    public StressLoadTestBot stressLoadTestBot() {
        if (this.StressLoadTestBot == null) {
            this.StressLoadTestBot = new StressLoadTestBot();
            try {
                this.botsApi.registerBot((LongPollingBot)this.StressLoadTestBot);
            }
            catch (TelegramApiException e) {
                e.printStackTrace();
            }
            return this.StressLoadTestBot;
        }
        return this.StressLoadTestBot;
    }*/
    
    @Bean
    public PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
    
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }
    
    public void setApplicationContext(final ApplicationContext context) throws BeansException {
        Config.applicationContext = context;
    }
    
    public static ApplicationContext getContext() {
        return Config.applicationContext;
    }
}
