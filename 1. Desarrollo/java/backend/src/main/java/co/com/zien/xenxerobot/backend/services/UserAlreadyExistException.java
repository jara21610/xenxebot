package co.com.zien.xenxerobot.backend.services;

public class UserAlreadyExistException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public UserAlreadyExistException() {
    }
    
    public UserAlreadyExistException(final String message) {
        super(message);
    }
    
    public UserAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public UserAlreadyExistException(final Throwable cause) {
        super(cause);
    }
}
