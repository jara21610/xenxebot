package co.com.zien.xenxerobot.backend.services.dataset;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import java.util.*;
import co.com.zien.xenxerobot.persistence.entity.*;

@Service("workerService")
public class WorkerServiceImpl implements WorkerService
{
    @Autowired
    WorkerRepository wr;
    
    public List<Worker> getavailableWorkers() {
        return (List<Worker>)this.wr.findAll();
    }
}
