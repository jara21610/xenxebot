package co.com.jit.backend.util;

import java.util.*;
import java.security.*;

public class RandomPassword
{
    private static final Random RANDOM;
    public static final int PASSWORD_LENGTH = 6;
    
    static {
        RANDOM = new SecureRandom();
    }
    
    public static String generateRandomPassword() {
        final String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
        String pw = "";
        for (int i = 0; i < 6; ++i) {
            final int index = (int)(RandomPassword.RANDOM.nextDouble() * letters.length());
            pw = String.valueOf(pw) + letters.substring(index, index + 1);
        }
        return pw;
    }
}
