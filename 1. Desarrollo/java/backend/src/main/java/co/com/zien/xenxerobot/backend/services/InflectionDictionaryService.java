package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.backend.dictionary.*;

public interface InflectionDictionaryService
{
    InflectionDictionary getDictionary();
}
