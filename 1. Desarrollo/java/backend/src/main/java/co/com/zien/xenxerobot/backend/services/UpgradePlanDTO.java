package co.com.zien.xenxerobot.backend.services;

public class UpgradePlanDTO
{
    private String usertoken;
    private String name;
    private String identificationNumber;
    private String number;
    private String expirationDate;
    private String ccv;
    private String idType;
    private String plan;
    private String servicetype;
    
    public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getUsertoken() {
        return this.usertoken;
    }
    
    public void setUsertoken(final String usertoken) {
        this.usertoken = usertoken;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getIdentificationNumber() {
        return this.identificationNumber;
    }
    
    public void setIdentificationNumber(final String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(final String number) {
        this.number = number;
    }
    
    public String getExpirationDate() {
        return this.expirationDate;
    }
    
    public void setExpirationDate(final String expirationDate) {
        this.expirationDate = expirationDate;
    }
    
    public String getCcv() {
        return this.ccv;
    }
    
    public void setCcv(final String ccv) {
        this.ccv = ccv;
    }
    
    public String getIdType() {
        return this.idType;
    }
    
    public void setIdType(final String idType) {
        this.idType = idType;
    }
    
    public String getPlan() {
        return this.plan;
    }
    
    public void setPlan(final String plan) {
        this.plan = plan;
    }
}
