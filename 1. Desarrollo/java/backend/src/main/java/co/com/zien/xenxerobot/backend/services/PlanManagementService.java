package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.web.bind.annotation.*;

public interface PlanManagementService
{
	public Userplan upgradeplan(final Users u, final String plan, String productid) throws Exception;
    
    Paymentmethod storePaymentMethod(final Users p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6) throws StorePaymentmethodException;
    
    void makePlanPayment(final Users p0, final Userplan p1, final Paymentmethod p2) throws PaymentException;
    
    void upgradePlanWithPaymentMethod(@RequestBody final UpgradePlanDTO p0) throws Exception;
    
    public void upgradePlanPaymentMethodPredeterminated(final Users u, String productid) throws Exception;
    
    public void planManagementDailyVerifications() throws Exception;
}
