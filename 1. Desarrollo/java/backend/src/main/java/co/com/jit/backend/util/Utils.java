package co.com.jit.backend.util;

import org.springframework.core.env.*;
import co.com.zien.xenxerobot.backend.config.*;
import java.text.*;
import com.artofsolving.jodconverter.openoffice.converter.*;
import com.artofsolving.jodconverter.openoffice.connection.*;
import com.artofsolving.jodconverter.*;
import org.apache.velocity.app.*;
import org.apache.velocity.runtime.resource.loader.*;
import org.apache.velocity.context.*;
import org.apache.velocity.*;
import java.net.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import com.vdurmont.emoji.*;

public class Utils
{
    private static Environment env;
    
    static {
        Utils.env = (Environment)Config.getContext().getBean((Class)Environment.class);
    }
    
    public static String ConvertDateToString(final String formato, final Date date) {
        final DateFormat fecha = new SimpleDateFormat(formato);
        final String convertido = fecha.format(date);
        return convertido;
    }
    
    public static GregorianCalendar convertStringToDate(final String fec) {
        final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final GregorianCalendar cal = new GregorianCalendar();
        try {
            final Date date = format.parse(fec);
            cal.setTime(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }
    
    public static int calculateAge(final GregorianCalendar fecnac) {
        final Calendar now = new GregorianCalendar();
        int res = now.get(1) - fecnac.get(1);
        if (fecnac.get(2) > now.get(2) || (fecnac.get(2) == now.get(2) && fecnac.get(5) > now.get(5))) {
            --res;
        }
        return res;
    }
    
    public static void convertDocuments(final String source, final String dst) throws ConnectException {
        final File inputFile = new File(source);
        final File outputFile = new File(dst);
        final OpenOfficeConnection connection = (OpenOfficeConnection)new SocketOpenOfficeConnection(8100);
        connection.connect();
        final DocumentConverter converter = (DocumentConverter)new OpenOfficeDocumentConverter(connection);
        converter.convert(inputFile, outputFile);
        connection.disconnect();
        //inputFile.delete();
    }
    
    /*public static long coverthtmltojpeg(final String source, final String dst, final int width, final int height) throws Exception {
    	final String command = String.valueOf(Utils.env.getRequiredProperty("webdriver.chrome.driver")) + " --width " + width + " --height " + height + " " + "file:///" + source + " " + dst;
    	File jpeg = new File( dst);
        long jpegsize = jpeg.length();
        int i = 0;
        System.out.println("tamaño" + jpeg.length());
        while (true){
        	System.out.println(command);
            Runtime.getRuntime().exec(command);
            Thread.currentThread();                        
    		Thread.sleep(10000L);        	
        	jpegsize = jpeg.length();        	
        	if (i < 5 && jpegsize < 5000) {
        		i++;
        	} else {
        		break;
        	}
        	
        } 
       System.out.println("tamaño" + jpeg.length()); 
       return jpegsize; 
    }*/
    
    public static void coverthtmltojpeg(final String source, final String dst, final int width, final int height) throws Exception {
    	
    	 System.setProperty("webdriver.chrome.driver", String.valueOf(Utils.env.getRequiredProperty("webdriver.chrome.driver")));
    	 
    	 ChromeOptions options = new ChromeOptions();
    	 options.addArguments("--headless", "--disable-gpu","--ignore-certificate-errors","--hide-scrollbars", "--no-sandbox");
         
    	 WebDriver driver = new ChromeDriver(options);       
    	 //driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
    	 driver.manage().window().setSize(new Dimension(width, height));
    	 System.out.println("CHROME DRIVE SOURCE =" + source);
    	 Path path = Paths.get(source);
    	 driver.navigate().to(path.toUri().toString());
    	 
    	 //WebDriverWait wait = new WebDriverWait(driver,30);
    	 //WebElement chart;
    	 //chart= wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("chart")));    	     	     	     	     	     	
         long jpegsize = 0;
         int i = 0;
        
         while (true){
         	
            Thread.currentThread();                        
     		Thread.sleep(5000L);     
     		
     		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
       	 
     		// Now you can do whatever you need to do with it, for example copy somewhere
     		FileUtils.copyFile(scrFile, new File(dst));
     		File jpeg = new File( dst);
         	jpegsize = jpeg.length();        	
         	if (i < 5 && jpegsize < 5000) {
         		i++;
         	} else {
         		break;
         	}
         	
         } 
    	 
    	 driver.close();
    	 driver.quit();    	     	 
        
    }
    
    public static void trasformTemplate(final String sourcetemplate, final String dstfilename, final HashMap<String, Object> context) throws Exception {
        final VelocityEngine ve = new VelocityEngine();
        ve.setProperty("resource.loader", (Object)"classpath");
        ve.setProperty("classpath.resource.loader.class", (Object)ClasspathResourceLoader.class.getName());
        ve.init();
        final Template t = ve.getTemplate(sourcetemplate);
        final VelocityContext vc = new VelocityContext();
        for (final String key : context.keySet()) {
            vc.put(key, context.get(key));
        }
        final StringWriter sw = new StringWriter();
        t.merge((Context)vc, (Writer)sw);
        final FileWriter fw = new FileWriter(dstfilename);
        fw.write(sw.toString());
        fw.close();
    }
    
    public static String sendHttpRequest(final String targetURL) {
        HttpURLConnection connection = null;
        final StringBuilder response = new StringBuilder();
        try {
            final URL url = new URL(targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setUseCaches(false);
            final InputStream is = connection.getInputStream();
            final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }
    
    public static String deleteaccentmarkdieresis(final String input) {
        final String original = "\u00e1\u00e0\u00e4\u00e9\u00e8\u00eb\u00ed\u00ec\u00ef\u00f3\u00f2\u00f6\u00fa\u00fc\u00c1\u00c0\u00c4\u00c9\u00c8\u00cb\u00cd\u00cc\u00cf\u00d3\u00d2\u00d6\u00da\u00d9\u00dc";
        final String ascii = "aaaeeeiiiooouuuAAAEEEIIIOOOUUU";
        String output = input;
        for (int i = 0; i < original.length(); ++i) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }
    
    public static String replacePunctuationMarks(String input) {
        input = input.replace("�", " signointerrogacion ");
        input = input.replace("?", " signointerrogacion ");
        input = input.replace("�", " signoadmiracion ");
        input = input.replace("!", " signoadmiracion ");
        input = input.replace("...", " signopuntossuspensivos ");
        input = input.replace(".", " signopunto ");
        input = input.replace(";", " signopuntoycoma ");
        input = input.replace(",", " signocoma ");
        return input;
    }
    
    public static String cleanString(final List<String> regextoclean, String input) {
        for (final String regex : regextoclean) {
            input = input.replaceAll(regex, "");
        }
        return input;
    }
    
    public static String cleanRepeatedChars(String input) {
        input = input.replaceAll("(.)\\1+", "$1");
        return input;
    }
    
    public static boolean isHashtag(final String posibleHastag) {
        return posibleHastag.substring(0, 1).equals("#");
    }
    
    public static boolean isUserMention(final String posibleHastag) {
        return posibleHastag.substring(0, 1).equals("@");
    }
    
    public static void copyRow(final HSSFWorkbook workbook, final HSSFSheet worksheet, final int sourceRowNum, final int destinationRowNum) {
        HSSFRow newRow = worksheet.getRow(destinationRowNum);
        final HSSFRow sourceRow = worksheet.getRow(sourceRowNum);
        if (newRow != null) {
            worksheet.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
        }
        else {
            newRow = worksheet.createRow(destinationRowNum);
        }
        for (int i = 0; i < sourceRow.getLastCellNum(); ++i) {
            final HSSFCell oldCell = sourceRow.getCell(i);
            HSSFCell newCell = newRow.createCell(i);
            if (oldCell == null) {
                newCell = null;
            }
            else {
                newCell.setCellStyle(oldCell.getCellStyle());
                if (oldCell.getCellComment() != null) {
                    newCell.setCellComment((Comment)oldCell.getCellComment());
                }
                if (oldCell.getHyperlink() != null) {
                    newCell.setHyperlink((Hyperlink)oldCell.getHyperlink());
                }
                newCell.setCellType(oldCell.getCellType());
                switch (oldCell.getCellType()) {
                    case 3: {
                        newCell.setCellValue(oldCell.getStringCellValue());
                        break;
                    }
                    case 4: {
                        newCell.setCellValue(oldCell.getBooleanCellValue());
                        break;
                    }
                    case 5: {
                        newCell.setCellErrorValue(oldCell.getErrorCellValue());
                        break;
                    }
                    case 2: {
                        newCell.setCellFormula(oldCell.getCellFormula());
                        break;
                    }
                    case 0: {
                        newCell.setCellValue(oldCell.getNumericCellValue());
                        break;
                    }
                    case 1: {
                        newCell.setCellValue((RichTextString)oldCell.getRichStringCellValue());
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < worksheet.getNumMergedRegions(); ++i) {
            final CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
            if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
                final CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(), newRow.getRowNum() + (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow()), cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn());
                worksheet.addMergedRegion(newCellRangeAddress);
            }
        }
    }
    
    public static Date addMinutesToDate(final int minutes, final Date beforeTime) {
        final long ONE_MINUTE_IN_MILLIS = 60000L;
        final long curTimeInMs = beforeTime.getTime();
        final Date afterAddingMins = new Date(curTimeInMs + minutes * 60000L);
        return afterAddingMins;
    }
    
    public static String getOriginalResponseTweet(final String url) throws IOException {
        final Document doc = Jsoup.connect(url).get();
        final String text = doc.title();
        System.out.println(text);
        return text;
    }
    
    public static String getSentimentWordFromEmoticon(final String text) throws IOException {
        final EmojiParser.EmojiTransformer emojiTransformer = (EmojiParser.EmojiTransformer)new EmojiParser.EmojiTransformer() {
            public String transform(final EmojiParser.UnicodeCandidate unicodeCandidate) {
                return " " + unicodeCandidate.getEmoji().getAliases().get(0) + unicodeCandidate.getFitzpatrickType() + " ";
            }
        };
        return EmojiParser.parseFromUnicode(text, emojiTransformer);
    }
}
