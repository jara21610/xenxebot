package co.com.zien.xenxerobot.backend.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import co.com.zien.xenxerobot.backend.config.Config;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class }, loader = AnnotationConfigContextLoader.class)
public class ReportDetailManagerPreXtigeBotTest {
	
	 @Autowired 
	 @Qualifier("PreXtigeReportDetailManager")
	 ReportDetailManager rdm;

	@Test
	public void testGenerateSentimentAnalysisReport() {
		try {
			this.rdm.generateSentimentAnalysisReport("f1d5db55-9d4d-4c0e-a2fe-ba363b8c7c59");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
