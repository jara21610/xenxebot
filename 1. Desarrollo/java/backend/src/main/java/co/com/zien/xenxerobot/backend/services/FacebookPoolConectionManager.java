package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.apache.log4j.*;
import java.util.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import com.restfb.types.*;
import com.restfb.*;

@Service("FacebookPoolConectionManager")
@Transactional
public class FacebookPoolConectionManager implements SocialPoolConectionManager
{
    private static final Logger logger;
    List<SocialConnectionDTO> scdtolist;
    int windowsizeminutes;
    int limitcallsallowed;
    @Autowired
    SocialConnectionRepository scr;
    @Autowired
    MessageSource messageSource;
    
    static {
        logger = Logger.getLogger((Class)FacebookPoolConectionManager.class);
    }
    
    public FacebookPoolConectionManager() {
        this.scdtolist = null;
        this.windowsizeminutes = 0;
        this.limitcallsallowed = 0;
    }
    
    public void init() throws SocialPoolConectionManagerNotConfigured {
    }

    public Object getAvailableConecction(final String... parameter) throws SocialPoolConectionManagerNotConfigured, SocialPoolConectionManagerNotFoundAvailableException {
        final FacebookClient facebookClient = (FacebookClient)new DefaultFacebookClient(parameter[0], Version.LATEST);
        
        if (facebookClient.getWebRequestor().getDebugHeaderInfo() != null) {
	        if (facebookClient.getWebRequestor().getDebugHeaderInfo().getPageUsage().getCallCount() > 80 || 
	        		facebookClient.getWebRequestor().getDebugHeaderInfo().getPageUsage().getTotalCputime() > 80 || 
	        		facebookClient.getWebRequestor().getDebugHeaderInfo().getPageUsage().getTotalTime() > 80) {
	        	throw new SocialPoolConectionManagerNotFoundAvailableException("");
	        } else {
	        	return facebookClient;	        
	        }
        } else {
        	return facebookClient;
        }
        
    }
    

    public List<SocialConnectionDTO> getCurrentState() {
        return this.scdtolist;
    }
    

    public Object getSocialServerLimitStatus() {
        return null;
    }
    

    public int getMinimalSecondResetConection() {
        return 216000;
    }
}
