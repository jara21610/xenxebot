package co.com.zien.xenxerobot.backend.services;

import co.com.zien.xenxerobot.persistence.entity.*;

public interface SourceProcessPostService
{
    void process(final CommandProcess p0, final ComandDTO p1) throws Exception;
}
