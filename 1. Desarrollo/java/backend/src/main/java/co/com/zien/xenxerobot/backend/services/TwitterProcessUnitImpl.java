package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import java.util.concurrent.*;
import java.util.stream.*;
import org.springframework.social.twitter.api.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.regex.*;
import co.com.jit.backend.util.*;
import java.io.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.util.*;
import org.springframework.scheduling.annotation.*;

@Service("TwitterProcessUnit")
public class TwitterProcessUnitImpl implements SourceProcessUnit
{
    private static final Logger logger;
    @Autowired
    PostRepository postr;
    @Autowired
    PreproccesingService scs;
    @Autowired
    SentimentService ss;
    @Autowired
    ConceptsRepository cr;
    @Autowired
    EmailService es;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    ReportDetailServiceFactory rdmf;
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    LoadStressTestService lsts;
    @Autowired
    ResponseBotServiceFactory rbsf;
    @Autowired
    TransactionCounterService tcs;
    @Autowired
    UserplanRepository upr;
    
    static {
        logger = Logger.getLogger((Class)TwitterProcessUnitImpl.class);
    }
    
    @Async    
    public CompletableFuture<String> executeUnit(final List currentlt, CommandProcess cp, final ComandDTO cdto) {
        final Date datesince = (Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument());
        final Date dateuntil = (Date)cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument());
        final String servicetype = (String) cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument());
        List<Tweet> tlist = currentlt;
        final int txoutofrange = ((List)tlist.stream().filter(t -> t.getCreatedAt().before(datesince)).collect(Collectors.toList())).size();
        int permitedtx = 0;
        final int txtoreserve = currentlt.size() - txoutofrange;
        try {
            permitedtx = this.tcs.addUsedTransactionPlan(cp.getUser().getId(), txtoreserve, servicetype);
        }
        catch (TxLimitException e2) {
            return CompletableFuture.completedFuture("stop");
        }
        tlist = currentlt.subList(0, permitedtx);
        for (final Tweet t2 : tlist) {
            final String isloadstreesstestactive = this.pr.findByName(PropertyEnum.LOADSTRESSTESTACTIVE.getProperty());
            
            if (t2.getCreatedAt().after(datesince) && t2.getCreatedAt().before(dateuntil)) {
                final long beforetotal = System.currentTimeMillis();
                final Post p = new Post();
                p.setIdpost(UUID.randomUUID().toString());
                p.setLanguagepost(t2.getLanguageCode());
                p.setPostdate(t2.getCreatedAt());
                String om = "";
                if (t2.getRetweetedStatus() != null) {
                    if (t2.getRetweetedStatus().getInReplyToUserId() == null) {
                        final String[] strArray = t2.getRetweetedStatus().getText().split(Pattern.quote("\u2026"));
                        if (strArray.length > 1) {
                            try {
                                om = Utils.getOriginalResponseTweet(strArray[strArray.length - 1].trim());
                            }
                            catch (Exception e3) {
                                om = t2.getRetweetedStatus().getText();
                            }
                        }
                        else {
                            om = t2.getRetweetedStatus().getText();
                        }
                    }
                    else {
                        final String[] strArray = t2.getRetweetedStatus().getText().split(Pattern.quote("\u2026"));
                        try {
                            om = Utils.getOriginalResponseTweet(strArray[strArray.length - 1].trim());
                        }
                        catch (Exception e3) {
                            om = t2.getRetweetedStatus().getText();
                        }
                    }
                }
                else if (t2.getInReplyToUserId() == null) {
                    final String[] strArray = t2.getText().split(Pattern.quote("\u2026"));
                    if (strArray.length > 1) {
                        try {
                            om = Utils.getOriginalResponseTweet(strArray[strArray.length - 1].trim());
                        }
                        catch (Exception e3) {
                            om = t2.getText();
                        }
                    }
                    else {
                        om = t2.getText();
                    }
                }
                else {
                    final String[] strArray = t2.getText().split(Pattern.quote("\u2026"));
                    try {
                        om = Utils.getOriginalResponseTweet(strArray[strArray.length - 1].trim());
                    }
                    catch (Exception e3) {
                        om = t2.getText();
                    }
                }
                
                p.setOriginalmessage((om.length() < 2500) ? om : om.substring(0, 2450));
                final long beforepreprossecing = System.currentTimeMillis();
                String fm = this.scs.preproccesingText(p.getOriginalmessage());
                p.setFormattedmessage((fm.length() < 2500) ? fm : fm.substring(0, 2450));
                final double totalpreprocesing = (System.currentTimeMillis() - beforepreprossecing) / 1000.0;
                p.setUserpost(t2.getFromUser());
                cp = (CommandProcess)this.cpr.findOne(cp.getId());
                p.setCommandprocess(cp);
                this.postr.saveAndFlush(p);
                SentimentAnalysisOutputDTO sadto = null;
                double totalservice = 0.0;
                try {
                    final long beforeservice = System.currentTimeMillis();
                    sadto = this.ss.getSentimentAnalysis(p.getFormattedmessage(), servicetype);
                    totalservice = (System.currentTimeMillis() - beforeservice) / 1000.0;
                }
                catch (Exception ex) {}
                if (sadto != null) {
                    p.setSentiment(sadto.getSentiment());
                    p.setTopic(sadto.getCategory());
                    p.setConceptsCollection((Collection)new ArrayList());
                    for (final SentimentAnalysisConceptOutputDTO c : sadto.getConceptlist()) {
                        final Concepts concept = new Concepts();
                        concept.setId(UUID.randomUUID().toString());
                        concept.setIdPost(p);
                        concept.setConceptType(c.getType());
                        concept.setMachineLearningSentiment(c.getSentiment());
                        concept.setName((c.getName().length() < 100) ? c.getName() : c.getName().substring(0, 100));
                        this.cr.saveAndFlush(concept);
                        p.getConceptsCollection().add(concept);
                    }
                }
                this.postr.saveAndFlush(p);
                final double totalResponseTime = (System.currentTimeMillis() - beforetotal) / 1000.0;
                if (!isloadstreesstestactive.equals("Y")) {
                    continue;
                }
                final LoadStressProcessingPost lstdto = new LoadStressProcessingPost();
                lstdto.setTotalservice(totalservice);
                lstdto.setTotaltimepreprocesing(totalpreprocesing);
                lstdto.setTotaltime(totalResponseTime);
                try {
                    this.lsts.putDataProcessingPost(lstdto);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                if (t2.getCreatedAt().before(datesince)) {
                    return CompletableFuture.completedFuture("stop");
                }
                continue;
            }
        }
        return CompletableFuture.completedFuture("");
    }
}
