package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class ComandDTO
{
    private String communicationKey;
    private String channel;
    private String state;
    private String command;
    private Date cratedAt;
    private Map<String, Object> arguments;
    
    public String getCommand() {
        return this.command;
    }
    
    public void setCommand(final String command) {
        this.command = command;
    }
    
    public Map<String, Object> getArguments() {
        return this.arguments;
    }
    
    public void setArguments(final Map<String, Object> arguments) {
        this.arguments = arguments;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public String getCommunicationKey() {
        return this.communicationKey;
    }
    
    public void setCommunicationKey(final String communicationKey) {
        this.communicationKey = communicationKey;
    }
    
    public String getChannel() {
        return this.channel;
    }
    
    public void setChannel(final String channel) {
        this.channel = channel;
    }
    
    public Date getCratedAt() {
        return this.cratedAt;
    }
    
    public void setCratedAt(final Date cratedAt) {
        this.cratedAt = cratedAt;
    }
}
