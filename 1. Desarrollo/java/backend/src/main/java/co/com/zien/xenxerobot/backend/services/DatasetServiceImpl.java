package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import java.math.*;
import org.apache.poi.poifs.filesystem.*;
import java.util.stream.*;
import co.com.jit.backend.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.context.i18n.*;
import java.text.*;
import org.apache.poi.ss.usermodel.*;
import java.util.*;
import java.io.*;
import org.apache.commons.lang3.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.data.domain.*;

@Service
@Transactional
public class DatasetServiceImpl implements DatasetService
{
    @Autowired
    PostRepository pr;
    @Autowired
    PropertiesRepository propr;
    @Autowired
    PackagesRepository packr;
    @Autowired
    SentimentService ss;
    @Autowired
    PreproccesingService scs;
    @Autowired
    Environment env;
        
    public void export(final BigDecimal... packages) throws Exception {
        final List<Post> postlist = new ArrayList<Post>();
        for (final BigDecimal id : packages) {
            final List<Post> packagepost = (List<Post>)this.pr.postbypackage(id);
            if (packagepost.size() > 0) {
                postlist.addAll(packagepost);
            }
        }
        final String name = this.propr.findByName(PropertyEnum.DATASETEXPORTTEMPLATE.getProperty());
        final POIFSFileSystem fs = new POIFSFileSystem((InputStream)new FileInputStream(name));
        final Workbook wb = (Workbook)new HSSFWorkbook(fs, true);
        final Sheet s1 = wb.getSheetAt(0);
        int i = 1;
        for (final Post p : postlist) {
            String text = this.scs.preproccesingText(p.getFormattedmessage());
            s1.getRow(i).getCell(1).setCellValue(p.getHumanSentiment());
            s1.getRow(i).getCell(2).setCellValue(p.getHumanCategory());
            s1.getRow(i).getCell(7).setCellValue(p.getIdPackage().getId().toString());
            if (p.getConceptsCollection().size() > 0) {
                final Collection<Concepts> c = (Collection<Concepts>)p.getConceptsCollection();
                final List<Concepts> topics = c.stream().filter(x -> ConceptsEnum.TOPICO.getConcept().equals(x.getConceptType())).collect(Collectors.toList());
                String topicsnames = "";
                String topicssentiments = "";
                for (final Concepts t : topics) {
                    topicsnames = String.valueOf(topicsnames) + t.getName() + ",";
                    topicssentiments = String.valueOf(topicssentiments) + t.getHumanSentiment() + ",";
                }
                final List<Concepts> entities = c.stream().filter(x -> ConceptsEnum.ENTIDAD.getConcept().equals(x.getConceptType())).collect(Collectors.toList());
                String entitiesnames = "";
                String entitiessentiments = "";
                for (final Concepts e : entities) {
                    if (e.getName() != null && text != null) {
                        text = text.replace(Utils.deleteaccentmarkdieresis(e.getName().toLowerCase().trim()), "ENTIDAD");
                        entitiesnames = String.valueOf(entitiesnames) + e.getName() + ",";
                        entitiessentiments = String.valueOf(entitiessentiments) + e.getHumanSentiment() + ",";
                    }
                }
                s1.getRow(i).getCell(3).setCellValue(topicsnames);
                s1.getRow(i).getCell(4).setCellValue(topicssentiments);
                s1.getRow(i).getCell(5).setCellValue(entitiesnames);
                s1.getRow(i).getCell(6).setCellValue(entitiessentiments);
            }
            s1.getRow(i).getCell(0).setCellValue(text);
            ++i;
            Utils.copyRow((HSSFWorkbook)wb, (HSSFSheet)s1, 1, i);
        }
        final Date date = new Date();
        final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
        final String ruta = this.env.getRequiredProperty("outputpath");
        final String filename = String.valueOf(UUID.randomUUID().toString()) + "_" + "datasetexport" + "_" + df.format(date);
        final String path = String.valueOf(ruta) + filename + ".xls";
        final FileOutputStream fileOut = new FileOutputStream(path);
        wb.write((OutputStream)fileOut);
        fileOut.close();
    }
        
    public void generatePackgage(final BigDecimal idpackage, final String... idpost) throws Exception {
        final int psize = new Integer(this.propr.findByName(PropertyEnum.PACKAGESIZE.getProperty()));
        final List<Post> postlist = new ArrayList<Post>();
        int i = 0;
        for (final String id : idpost) {
            final Pageable page = (Pageable)new PageRequest(0, 1500);
            final List<Object[]> packagepost = (List<Object[]>)this.pr.posttopackage(id, page);
            if (packagepost.size() > 0) {
                for (final Object[] obj : packagepost) {
                    if (i >= psize + 100) {
                        break;
                    }
                    final Post current = (Post)this.pr.findOne((String)obj[0]);
                    boolean similartoany = false;
                    for (final Post p : postlist) {
                        final int distance = StringUtils.getLevenshteinDistance((CharSequence)p.getOriginalmessage(), (CharSequence)current.getOriginalmessage());
                        final double similarity = 100.0 - distance / Math.max(p.getOriginalmessage().length(), current.getOriginalmessage().length()) * 100.0;
                        if (similarity > 80.0) {
                            similartoany = true;
                            p.setDiscartedtopack("Y");
                            this.pr.saveAndFlush(p);
                            break;
                        }
                    }
                    if (similartoany) {
                        continue;
                    }
                    if (i > psize) {
                        current.setOptiontochange("Y");
                    }
                    postlist.add(current);
                    ++i;
                }
            }
        }
        final Packages packages = (Packages)this.packr.findOne(idpackage);
        for (final Post p2 : postlist) {
            p2.setIdPackage(packages);
            this.pr.saveAndFlush(p2);
        }
        packages.setState(PackagesStateEnum.AVAILABLE.getState());
        this.packr.saveAndFlush(packages);
    }
}
