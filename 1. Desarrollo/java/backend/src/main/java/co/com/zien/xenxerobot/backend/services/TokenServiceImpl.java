package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.io.*;
import java.util.*;

@Service
public class TokenServiceImpl implements TokenService
{
    @Autowired
    UsertokenRepository utr;
    
    @Override
    public String generateUserToken(final Users userId, String productid) {
        final Usertoken ut = new Usertoken();
        ut.setIduser(userId);
        final String token = UUID.randomUUID().toString();
        ut.setToken(token);
        final Calendar cal = Calendar.getInstance();
        cal.add(12, 10);
        ut.setUntilvaliddate(cal.getTime());
        ut.setProductid(productid);
        this.utr.saveAndFlush(ut);
        return token;
    }
    
    @Override
    public Users validateUserToken(final String usertoken) throws TokenNotExistException, TokenExpiredException {
        final Usertoken ut = (Usertoken)this.utr.findOne(usertoken);
        if (ut == null) {
            throw new TokenNotExistException();
        }
        if (ut.getUntilvaliddate().after(new Date())) {
            return ut.getIduser();
        }
        throw new TokenExpiredException();
    }
    
    @Override
    public Users getUserOwnerToken(final String usertoken) throws TokenNotExistException {
        final Usertoken ut = (Usertoken)this.utr.findOne(usertoken);
        if (ut != null) {
            return ut.getIduser();
        }
        throw new TokenNotExistException();
    }
    
    public String getProductIdToken(final String usertoken) throws TokenNotExistException {
        final Usertoken ut = (Usertoken)this.utr.findOne(usertoken);
        if (ut != null) {
            return ut.getProductid();
        }
        throw new TokenNotExistException();
    }
}
