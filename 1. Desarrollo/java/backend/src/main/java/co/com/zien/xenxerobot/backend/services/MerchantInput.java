package co.com.zien.xenxerobot.backend.services;

public class MerchantInput
{
    private String apiLogin;
    private String apiKey;
    
    public String getApiLogin() {
        return this.apiLogin;
    }
    
    public void setApiLogin(final String apiLogin) {
        this.apiLogin = apiLogin;
    }
    
    public String getApiKey() {
        return this.apiKey;
    }
    
    public void setApiKey(final String apiKey) {
        this.apiKey = apiKey;
    }
}
