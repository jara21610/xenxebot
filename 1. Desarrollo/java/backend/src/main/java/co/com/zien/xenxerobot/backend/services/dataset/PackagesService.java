package co.com.zien.xenxerobot.backend.services.dataset;

import java.util.*;
import java.math.*;
import co.com.zien.xenxerobot.persistence.entity.*;

public interface PackagesService
{
    List<Packages> findByWorker(final Worker p0);
    
    List<Packages> findByState(final String p0);
    
    List<Post> nextworkitembypackage(final BigDecimal p0);
    
    Post change(final Post p0);
    
    void savehumantask(final Post p0);
    
    void savePackage(final Packages p0);
    
    Packages findPackage(final BigDecimal p0);
    
    Integer packageSize(final BigDecimal p0);
}
