package co.com.zien.xenxerobot.backend.services;

import java.util.*;
import com.restfb.types.*;
import com.restfb.types.instagram.IgMedia;

public interface FacebookService
{
    List<Comment> searchComments(final String p0, final String p1, final String p2) throws Exception;
    
    public List<Post> searchlastPostsFacebook(final String pageid, final int numberPost, String userToken) throws Exception;
    
    public List<IgMedia> searchlastPostsInstagram(final String pageid, final int numberPost, String userToken) throws Exception;
    
    String getExtendedToken(String userToken) throws Exception;
    
    public List<Account> getAccountList(String userToken) throws Exception;
    
    public String getInstAccountBussiness(String idPage, String userToken) throws Exception;
}
