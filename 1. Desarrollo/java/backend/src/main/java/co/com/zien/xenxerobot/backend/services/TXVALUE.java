package co.com.zien.xenxerobot.backend.services;

import java.math.*;

public class TXVALUE
{
    private BigInteger value;
    private String currency;
    
    public BigInteger getValue() {
        return this.value;
    }
    
    public void setValue(final BigInteger value) {
        this.value = value;
    }
    
    public String getCurrency() {
        return this.currency;
    }
    
    public void setCurrency(final String currency) {
        this.currency = currency;
    }
}
