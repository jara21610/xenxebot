package co.com.zien.xenxerobot.backend.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.com.zien.xenxerobot.backend.config.Config;
import co.com.zien.xenxerobot.persistence.repository.UserplanRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Config.class})
public class PlanManagementServiceImplTest {
	
	@Autowired
	PlanManagementService pms;

	@Test
	public void testPlanManagementDailyVerifications() {
		try {
			pms.planManagementDailyVerifications();
			assertTrue(true);
		} catch (Exception e) {			
			e.printStackTrace();
			fail();
		}
	}
	
}
