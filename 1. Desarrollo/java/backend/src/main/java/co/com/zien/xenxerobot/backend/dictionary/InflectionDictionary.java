package co.com.zien.xenxerobot.backend.dictionary;

import org.apache.log4j.*;
import java.io.*;
import java.util.*;

public class InflectionDictionary
{
    private static final Logger logger;
    private HashMap<String, String> dictionary;
    private File plaintxtDictionary;
    
    static {
        logger = Logger.getLogger((Class)InflectionDictionary.class);
    }
    
    public InflectionDictionary() {
        this.dictionary = new HashMap<String, String>();
    }
    
    public File getPlaintxtDictionary() {
        return this.plaintxtDictionary;
    }
    
    public void setPlaintxtDictionary(final File plaintxtDictionary) {
        this.plaintxtDictionary = plaintxtDictionary;
    }
    
    public void loadPlainTextDictionary() {
        try {
            final BufferedReader in = new BufferedReader(new FileReader(this.plaintxtDictionary));
            String line = "";
            while ((line = in.readLine()) != null) {
                final StringTokenizer st1 = new StringTokenizer(line, "=");
                final String lemma = st1.nextToken();
                while (st1.hasMoreElements()) {
                    final StringTokenizer st2 = new StringTokenizer(st1.nextToken(), ";");
                    while (st2.hasMoreElements()) {
                        this.dictionary.put(st2.nextToken(), lemma);
                    }
                }
            }
            in.close();
        }
        catch (Exception e) {
            InflectionDictionary.logger.error((Object)e);
        }
    }
    
    public boolean existWord(final String word) {
        return this.dictionary.containsKey(word);
    }
    
    public String getLemma(final String word) {
        return this.dictionary.get(word);
    }
}
