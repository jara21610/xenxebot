package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.core.env.*;
import org.springframework.context.*;
import com.thoughtworks.xstream.*;
import org.springframework.context.i18n.*;
import java.text.*;
import co.com.jit.backend.util.*;
import java.nio.file.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.io.*;
import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.data.domain.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.scheduling.annotation.*;

@Service("PreXtigeReportDetailManager")
public class ReportDetailManagerPreXtigeBot implements ReportDetailManager
{
    @Autowired
    CommandProcessRepository cpr;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    PostRepository postr;
    @Autowired
    ResponseBotServiceFactory rbsf;
    @Autowired
    Environment env;
    @Autowired
    MessageSource messageSource;
    
    @Override
    public void saveStatus(CommandProcess cp, final ReportDetailDTO rrdto) {
        cp = (CommandProcess)this.cpr.findOne(cp.getId());
        final XStream xs = new XStream();
        final String reportdetail = xs.toXML((Object)rrdto);
        cp.setReportdetail(reportdetail);
        this.cpr.saveAndFlush(cp);
        System.out.println("PASE");
    }
    
    @Override
    public String generateSentimentAnalysisReport(final String cpId) throws Exception {
    	
        String pathpdf = "";
        String path = "";
        final CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
        final int total = this.postr.totalPost(cp.getId());
        if (total > 0) {
            final String ruta = this.env.getRequiredProperty("outputpath");
            final XStream xs = new XStream();
            final ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
            final String name = this.pr.findByName(PropertyEnum.PREXTIGETEMPLATE.getProperty());
            final Workbook wb = (Workbook)new HSSFWorkbook((InputStream)new FileInputStream(name));
            final Sheet s1 = wb.getSheetAt(0);
            s1.getRow(6).getCell(1).setCellValue(String.valueOf(cp.getUser().getName()) + " " + cp.getUser().getLastname());
            s1.getRow(7).getCell(1).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument()));
            s1.getRow(8).getCell(1).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHTYPE.getCommandargument()));
            s1.getRow(9).getCell(1).setCellValue((String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()));
            final Date date = new Date();
            final DateFormat df = DateFormat.getDateInstance(1, LocaleContextHolder.getLocale());
            s1.getRow(6).getCell(5).setCellValue(df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHSINCE.getCommandargument())));
            s1.getRow(7).getCell(5).setCellValue(df.format(cdto.getArguments().get(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument())));
            s1.getRow(8).getCell(5).setCellValue(df.format(date));
            s1.getRow(9).getCell(5).setCellValue((double)total);
            final int positive = this.postr.totalXSentiment(cp.getId(), SentimentEnum.POSITIVE.getSentiment());
            final int negative = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEGATIVE.getSentiment());
            final int neutral = this.postr.totalXSentiment(cp.getId(), SentimentEnum.NEUTRAL.getSentiment());
            final HashMap<String, Object> sentimentpiecontext = new HashMap<String, Object>();
            sentimentpiecontext.put("positivo", new Integer(positive).toString());
            sentimentpiecontext.put("negativo", new Integer(negative).toString());
            sentimentpiecontext.put("neutral", new Integer(neutral).toString());
            final String filenamejpg = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml = String.valueOf(UUID.randomUUID().toString()) + ".html";
            //imprimir ruta
            final HSSFPatriarch drawing = (HSSFPatriarch)s1.createDrawingPatriarch();                                          
            final Pageable pag = (Pageable)new PageRequest(0, 10);
            /*
            final List<ElementSentimentDTO> sentimentXCategory = (List<ElementSentimentDTO>)this.postr.sentimentXCategory(cp.getId());
            final HashMap<String, Object> categorybarchartcontext = new HashMap<String, Object>();
            categorybarchartcontext.put("elementsentimentlist", sentimentXCategory);
            categorybarchartcontext.put("title", "Sentimiento por Categoria");
            filenamejpg = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            filenamehtml = String.valueOf(UUID.randomUUID().toString()) + ".html";
            Utils.trasformTemplate(this.env.getRequiredProperty("categorybarchart.template.path"), String.valueOf(ruta) + filenamehtml, categorybarchartcontext);
            Utils.coverthtmltojpeg(String.valueOf(ruta) + filenamehtml, String.valueOf(ruta) + filenamejpg, 600, 310);
            Thread.currentThread();
            Thread.sleep(10000L);
            final int my_picture_id_1 = wb.addPicture(Files.readAllBytes(new File(String.valueOf(ruta) + filenamejpg).toPath()), 5);
            final ClientAnchor my_anchor_1 = (ClientAnchor)new HSSFClientAnchor();
            my_anchor_1.setCol1(2);
            my_anchor_1.setRow1(68);
            final HSSFPicture my_picture_1 = drawing.createPicture(my_anchor_1, my_picture_id_1);
            my_picture_1.resize(); */
            
            final List<Object[]> topfrecuencyXconceptTypeE = (List<Object[]>)this.postr.topfrecuencyXconceptType(cp.getId(), ConceptsEnum.ENTIDAD.getConcept(), pag);
            final List<ElementSentimentDTO> sentimentXTopic = new ArrayList<ElementSentimentDTO>();
            final List<ElementSentimentDTO> maximunsentimentpositiveXTopic = new ArrayList<ElementSentimentDTO>();
            final List<ElementSentimentDTO> maximunsentimentnegativeXTopic = new ArrayList<ElementSentimentDTO>();            
            int positiveentity = 0;
            int negativeentity = 0;                
            for (final Object[] obj : topfrecuencyXconceptTypeE) {
                final long total2 = (long)obj[1];
                if (total2 > 10L) {
                    final ElementSentimentDTO sentimentXbyConceptName = this.postr.sentimentXbyConceptName(cp.getId(), ConceptsEnum.ENTIDAD.getConcept(), (String)obj[0]);
                    if (sentimentXbyConceptName == null) {
                        continue;
                    }
                    sentimentXbyConceptName.setPositivo(sentimentXbyConceptName.getPositivo().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXbyConceptName.setNegativo(sentimentXbyConceptName.getNegativo().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXbyConceptName.setNeutral(sentimentXbyConceptName.getNeutral().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXTopic.add(sentimentXbyConceptName);                    
                    if ((sentimentXbyConceptName.getPositivo().compareTo((new BigDecimal(50))) > 0)) {
                        if (positiveentity > 4) {
                            continue;
                        }
                        //positiveentitiyanalisis = String.valueOf(positiveentitiyanalisis) + sentimentXbyConceptName.getName() + " ,";                        
                        //aqui lleno el arreglo para la nueva grafica
                        sentimentXbyConceptName.setMaxsentiment(sentimentXbyConceptName.getPositivo());
                        maximunsentimentpositiveXTopic.add(0, sentimentXbyConceptName);
                        ++positiveentity;
                    }
                    else {
                        if (((sentimentXbyConceptName.getNegativo().compareTo((new BigDecimal(50))) > 0)) ) {                                              
                    	if (negativeentity > 4) {
                            continue;
                    	}
                        //negativeentitiyanalisis = String.valueOf(negativeentitiyanalisis) + sentimentXbyConceptName.getName() + " ,";
                        //aqui lleno el arreglo para la nueva grafica
                        sentimentXbyConceptName.setMaxsentiment(sentimentXbyConceptName.getNegativo().negate());
                        maximunsentimentnegativeXTopic.add(maximunsentimentnegativeXTopic.size(), sentimentXbyConceptName);
                        ++negativeentity;
                        }
                    }
                }
            }
            
            final String filenamejpg1 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml1 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            final HashMap<String, Object> entitybarchartcontext1 = new HashMap<String, Object>();
                        
            if (maximunsentimentpositiveXTopic.size() > 0) {
                
                entitybarchartcontext1.put("elementsentimentlist", maximunsentimentpositiveXTopic);
                entitybarchartcontext1.put("title", "% SENTIMIENTO POSITIVO");
                entitybarchartcontext1.put("color", "#00FFFF");                                               
            } 
            
            final HashMap<String, Object> entitybarchartcontext2 = new HashMap<String, Object>();           
            final String filenamejpg2 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml2 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            
            if (maximunsentimentnegativeXTopic.size() > 0) {                
                entitybarchartcontext2.put("elementsentimentlist", maximunsentimentnegativeXTopic);
                entitybarchartcontext2.put("title", "% SENTIMIENTO NEGATIVO");
                entitybarchartcontext2.put("color", "#152E51");                                
            } 
            
            final String filenamejpg3 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml3 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            
            final HashMap<String, Object> entitybarchartcontext3 = new HashMap<String, Object>();
            if (sentimentXTopic.size() > 0) {
                
                entitybarchartcontext3.put("elementsentimentlist", sentimentXTopic);                                                
            }
            
            final List<Object[]> topfrecuencyXconceptType = (List<Object[]>)this.postr.topfrecuencyXconceptType(cp.getId(), ConceptsEnum.TOPICO.getConcept(), pag);
            final List<ElementSentimentDTO> sentimentXEntity = new ArrayList<ElementSentimentDTO>();
            final List<ElementSentimentDTO> maximunsentimentpositiveXEntity = new ArrayList<ElementSentimentDTO>();
            final List<ElementSentimentDTO> maximunsentimentnegativeXEntity = new ArrayList<ElementSentimentDTO>();            
            int positivetopic = 0;
            int negativetopic = 0;
            for (final Object[] obj2 : topfrecuencyXconceptType) {
                final long total3 = (long)obj2[1];
                if (total3 > 10L) {
                    final ElementSentimentDTO sentimentXbyConceptName2 = this.postr.sentimentXbyConceptName(cp.getId(), ConceptsEnum.TOPICO.getConcept(), (String)obj2[0]);
                    if (sentimentXbyConceptName2 == null) {
                        continue;
                    }
                    sentimentXbyConceptName2.setPositivo(sentimentXbyConceptName2.getPositivo().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXbyConceptName2.setNegativo(sentimentXbyConceptName2.getNegativo().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXbyConceptName2.setNeutral(sentimentXbyConceptName2.getNeutral().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_DOWN));
                    sentimentXEntity.add(sentimentXbyConceptName2);
                    if ((sentimentXbyConceptName2.getPositivo().compareTo((new BigDecimal(50))) > 0)) {
                        if (positivetopic > 4) {
                            continue;
                        }
                        sentimentXbyConceptName2.setMaxsentiment(sentimentXbyConceptName2.getPositivo());
                        maximunsentimentpositiveXEntity.add(0, sentimentXbyConceptName2);
                        ++positivetopic;
                    }
                    else {
                    	if (((sentimentXbyConceptName2.getNegativo().compareTo((new BigDecimal(50))) > 0))) {
                    	
                    	if (negativetopic > 4) {
                            continue;
                        }
                    	
                    	sentimentXbyConceptName2.setMaxsentiment(sentimentXbyConceptName2.getNegativo().negate());
                        maximunsentimentnegativeXEntity.add(maximunsentimentnegativeXEntity.size(), sentimentXbyConceptName2);
                        ++negativetopic;
                    }
                }
                }
            }
            
            final String filenamejpg4 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml4 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            final HashMap<String, Object> entitybarchartcontext4 = new HashMap<String, Object>();
            if (maximunsentimentpositiveXEntity.size() > 0) {
                
                entitybarchartcontext4.put("elementsentimentlist", maximunsentimentpositiveXEntity);
                entitybarchartcontext4.put("title", "% SENTIMIENTO POSITIVO");
                entitybarchartcontext4.put("color", "#00FFFF");
                                
            } 
            
            final String filenamejpg5 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml5 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            final HashMap<String, Object> entitybarchartcontext5 = new HashMap<String, Object>();
            if (maximunsentimentnegativeXEntity.size() > 0) {
               
                entitybarchartcontext5.put("elementsentimentlist", maximunsentimentnegativeXEntity);
                entitybarchartcontext5.put("title", "% SENTIMIENTO NEGATIVO");
                entitybarchartcontext5.put("color", "#152E51");                                
            } 
            
            final HashMap<String, Object> topicbarchartcontext = new HashMap<String, Object>();
            final String filenamejpg6 = String.valueOf(UUID.randomUUID().toString()) + ".jpeg";
            final String filenamehtml6 = String.valueOf(UUID.randomUUID().toString()) + ".html";
            if (sentimentXEntity.size() > 0) {                
                topicbarchartcontext.put("elementsentimentlist", sentimentXEntity);                
                                                
            }
                        
            //llamar metodo
            long startTime = System.currentTimeMillis();
            
            ExecutorService pool = Executors.newFixedThreadPool(10);
            
            CompletableFuture<Boolean> cf = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("sentimentpie.template.path"), 600, 420, 12, 3, ruta, filenamehtml, filenamejpg, sentimentpiecontext, wb, drawing),pool);
            CompletableFuture<Boolean> cf1 = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart.template.path"), 370, 490, 29, 0,ruta, filenamehtml1, filenamejpg1, entitybarchartcontext1, wb, drawing),pool);
            CompletableFuture<Boolean> cf2 = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart.template.path"), 370, 490, 29, 4,ruta, filenamehtml2, filenamejpg2, entitybarchartcontext2, wb, drawing),pool);
            CompletableFuture<Boolean> cf3 = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart2.template.path"), 400, 460, 56, 1,ruta, filenamehtml3, filenamejpg3, entitybarchartcontext3, wb, drawing),pool);
            CompletableFuture<Boolean> cf4 = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart.template.path"), 370, 490, 74, 0,ruta, filenamehtml4, filenamejpg4, entitybarchartcontext4, wb, drawing),pool);
            CompletableFuture<Boolean> cf5 =CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart.template.path"), 370, 490, 74, 4,ruta, filenamehtml5, filenamejpg5, entitybarchartcontext5, wb, drawing),pool);
            CompletableFuture<Boolean> cf6 = CompletableFuture.supplyAsync(()-> drawandPrintChart(this.env.getRequiredProperty("categorybarchart2.template.path"), 650, 460, 103, 1,ruta, filenamehtml6, filenamejpg6, topicbarchartcontext, wb, drawing),pool);
            
            CompletableFuture<Boolean>[] cfs = new CompletableFuture[]{cf, cf1, cf2, cf3, cf4, cf5, cf6};
            
            CompletableFuture<Void> allCf = CompletableFuture.allOf(cfs);
            
            allCf.join();            
            long endTime = System.currentTimeMillis() - startTime;
            System.out.println("Duración " + endTime + " milisegundos.");
            
            final String filename = String.valueOf(UUID.randomUUID().toString()) + "_" + cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) + "_" + df.format(date);
            path = String.valueOf(ruta) + filename + ".xls";
            final FileOutputStream fileOut = new FileOutputStream(path);
            wb.write((OutputStream)fileOut);
            fileOut.close();
            pathpdf = String.valueOf(ruta) + filename + ".pdf";
            Utils.convertDocuments(path, pathpdf);
            System.out.println("Genere excel y pdf");
            return pathpdf;            
        }
        throw new NotDataToPartialReportException();
    }
    
    @Async
    @Override
    public void reportdetailgenerateSentimentAnalysisReport(final String cpId) throws Exception {
        final CommandProcess cp = (CommandProcess)this.cpr.findOne(cpId);
        String path = "";
        final ResponseBotService rbs = this.rbsf.getResponseBotService(ChannelEnum.TELEGRAM.getChannel());
        try {
            path = this.generateSentimentAnalysisReport(cpId);
            final XStream xs = new XStream();
            final ComandDTO cdto = (ComandDTO)xs.fromXML(cp.getArguments());
            rbs.okPartialReportResponseBot(path, cdto);
        }
        catch (NotDataToPartialReportException e) {
            final XStream xs2 = new XStream();
            final ComandDTO cdto2 = (ComandDTO)xs2.fromXML(cp.getArguments());
            rbs.messageResponseBot(this.messageSource.getMessage("messsage.telegram.notdatatopartialreport.text", (Object[])null, LocaleContextHolder.getLocale()), cdto2);
        }
    }
    
    private boolean drawandPrintChart(String template, int width, int heigth, int row, int col, String ruta, final String filenamehtml, final String filenamejpg, HashMap<String, Object> context, Workbook wb,  HSSFPatriarch drawing) {
    	try {
	    	if (context.size() > 0){
		    	long startTime = System.currentTimeMillis();
				Utils.trasformTemplate(template, String.valueOf(ruta) + filenamehtml, context);
				        
		        //Valido que ya el grafico se haya generado validando que la imagen no haya quedado en blanco
		        
		        Utils.coverthtmltojpeg(String.valueOf(ruta) + filenamehtml, String.valueOf(ruta) + filenamejpg, width, heigth);                       
		        
		        File jpeg = new File( String.valueOf(ruta) + filenamejpg);
		        long jpegsize = jpeg.length();
		        System.out.println("jpegsize drawandPrintChart " + jpegsize + ".");
		        
		        final int my_picture_id = wb.addPicture(Files.readAllBytes(new File(String.valueOf(ruta) + filenamejpg).toPath()), 5);
		        
		        final ClientAnchor my_anchor = (ClientAnchor)new HSSFClientAnchor();
		        my_anchor.setCol1(col);
		        my_anchor.setRow1(row);
		        final HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
		        my_picture.resize();
		        long endTime = System.currentTimeMillis() - startTime;
		        System.out.println("Duración drawandPrintChart " + endTime + " milisegundos.");
	    	}
       
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return true;
    }
}
