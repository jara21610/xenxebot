package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import com.restfb.types.*;
import com.restfb.types.Post;
import com.google.common.collect.*;
import java.util.concurrent.*;
import com.restfb.*;
import java.util.*;
import java.lang.Thread;

import org.springframework.social.twitter.api.*;

@Service("FacebookProcessPost")
public class FacebookProcessPostServiceImpl implements SourceProcessPostService
{
    private static final Logger logger;
    @Autowired
    PostRepository postr;
    @Autowired
    PreproccesingService scs;
    @Autowired
    SentimentService ss;
    @Autowired
    @Qualifier("FacebookPoolConectionManager")
    SocialPoolConectionManager tpcm;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    @Qualifier("FacebookProcessUnit")
    SourceProcessUnit tpt;
    @Autowired
    @Qualifier("EmailResponseBot")
    ResponseBotService erbs;
    @Autowired
    @Qualifier("TelegramResponseBot")
    ResponseBotService trbs;
    @Autowired
    MessageSource messageSource;
    @Autowired
    CommandProcessRepository cpr;
    
    static {
        logger = Logger.getLogger((Class)TwitterProcessPostServiceImpl.class);
    }
    
    public void process(final CommandProcess cp, final ComandDTO cdto) throws Exception {
        final String searchCriteria = (String)cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
        final String token = (String)cdto.getArguments().get(CommandArgumentsEnum.TOKEN.getCommandargument());
        final String searchType = (String)cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument());
        final List<Comment> currentlt = new ArrayList<Comment>();
        final int pagesize = new Integer(this.pr.findByName(PropertyEnum.TWITTERPAGESIZE.getProperty()));
        
        if (searchType.equals(FacebookSearchTypeEnum.PAGE.getFacebooksearchtype())) {
	        final FacebookClient facebookClient = (FacebookClient)this.getAvailableConecction(token);
	        final Connection<Post> postConnection = (Connection<Post>)facebookClient.fetchConnection(String.valueOf(searchCriteria) + "/posts", Post.class, new Parameter[] { Parameter.with("limit", (Object)100)});
	        for (final List<Post> myFeedPage : postConnection) {
	            if (myFeedPage.isEmpty()) {
	                return;
	            }
	            
	            for (final Post post : myFeedPage) {
	            	if(processpost(post.getId(),token, cp, cdto).equals("stop")){
	            		return;
	            	}
	            }
	        }
        } else {
        	if(processpost(searchCriteria,token, cp, cdto).equals("stop")){
        		return;
        	}
        }
    }
    
    private String processpost(String postId, String token,  CommandProcess cp, ComandDTO cdto) throws Exception{
    	FacebookClient facebookClient2 = (FacebookClient)this.getAvailableConecction(token);
    	facebookClient2 = (FacebookClient)this.getAvailableConecction(token);        
        final Connection<Comment> commentConnection = (Connection<Comment>)facebookClient2.fetchConnection(String.valueOf(postId) + "/comments", Comment.class, new Parameter[] { Parameter.with("limit", (Object)100) });
        for (final List<Comment> commentPage : commentConnection) {
            facebookClient2 = (FacebookClient)this.getAvailableConecction(token);
            final List<List<Comment>> smallerLists = (List<List<Comment>>)Lists.partition((List)commentPage, 25);
            final CompletableFuture[] fs = new CompletableFuture[smallerLists.size()];
            int i = 0;
            for (final List<Comment> lt : smallerLists) {
                final CompletableFuture<String> page = this.tpt.executeUnit(lt, cp, cdto);
                fs[i] = page;
                ++i;
            }
            CompletableFuture.allOf((CompletableFuture<?>[])fs);
            CompletableFuture[] array;
            for (int length = (array = fs).length, j = 0; j < length; ++j) {
                final CompletableFuture<String> page2 = (CompletableFuture<String>)array[j];
                if ("stop".equals(page2.get())) {
                    return "stop";
                }
            }
        }
        return "";
    }
    private Object getAvailableConecction(String token) throws Exception {
        boolean success = false;
        while (!success) {
            try {
                success = true;
                return this.tpcm.getAvailableConecction(token);
            }
            catch (SocialPoolConectionManagerNotFoundAvailableException e) {
                success = false;                
                Thread.currentThread();
                Thread.sleep(this.tpcm.getMinimalSecondResetConection());
            }
        }
        return null;
    }
}
