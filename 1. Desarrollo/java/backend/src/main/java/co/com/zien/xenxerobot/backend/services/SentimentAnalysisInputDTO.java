package co.com.zien.xenxerobot.backend.services;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class SentimentAnalysisInputDTO
{
    private String tweet;
    private String servicetype;
    
    public String getTweet() {
        return this.tweet;
    }
    
    public void setTweet(final String tweet) {
        this.tweet = tweet;
    }
    
    public String getServicetype() {
        return this.servicetype;
    }
    
    public void setServicetype(final String servicetype) {
        this.servicetype = servicetype;
    }
}
