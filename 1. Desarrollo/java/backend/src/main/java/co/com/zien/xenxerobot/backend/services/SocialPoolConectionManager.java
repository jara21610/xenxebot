package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public interface SocialPoolConectionManager
{
    void init() throws SocialPoolConectionManagerNotConfigured;
    
    Object getAvailableConecction(final String... p0) throws SocialPoolConectionManagerNotConfigured, SocialPoolConectionManagerNotFoundAvailableException;
    
    List<SocialConnectionDTO> getCurrentState();
    
    Object getSocialServerLimitStatus();
    
    int getMinimalSecondResetConection();
}
