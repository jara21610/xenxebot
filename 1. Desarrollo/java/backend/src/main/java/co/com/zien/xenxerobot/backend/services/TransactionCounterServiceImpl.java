package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.entity.*;

@Service
public class TransactionCounterServiceImpl implements TransactionCounterService
{
    @Autowired
    UserplanRepository upr;
    
    @Override
    public synchronized int addUsedTransactionPlan(final String userid, final int tx, String productid) throws TxLimitException {
        final Userplan up = this.upr.activePlanByUseridandstate(userid, "ACTIVO", productid);
        if (up == null) {
            throw new TxLimitException();
        }
        if (up.getTxplancapacity() == -1) {
            up.setTxpalused(up.getTxpalused() + tx);
            this.upr.saveAndFlush(up);
            return tx;
        }
        final int txavailable = up.getTxplancapacity() - up.getTxpalused();
        if (txavailable <= 0) {
            up.setState("FINALIZADO");
            this.upr.saveAndFlush(up);
            throw new TxLimitException();
        }
        if (txavailable >= tx) {
            up.setTxpalused(up.getTxpalused() + tx);
            this.upr.saveAndFlush(up);
            return tx;
        }
        up.setTxpalused(up.getTxpalused() + txavailable);
        this.upr.saveAndFlush(up);
        return txavailable;
    }
}
