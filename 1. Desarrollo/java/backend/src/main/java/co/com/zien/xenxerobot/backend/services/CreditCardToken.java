package co.com.zien.xenxerobot.backend.services;

public class CreditCardToken
{
    public String payerId;
    public String name;
    public String identificationNumber;
    public String paymentMethod;
    public String number;
    public String expirationDate;
    public String maskedNumber;
    public String creditCardTokenId;
    public String errorDescription;
    
    public String getPayerId() {
        return this.payerId;
    }
    
    public void setPayerId(final String payerId) {
        this.payerId = payerId;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getIdentificationNumber() {
        return this.identificationNumber;
    }
    
    public void setIdentificationNumber(final String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    
    public String getPaymentMethod() {
        return this.paymentMethod;
    }
    
    public void setPaymentMethod(final String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(final String number) {
        this.number = number;
    }
    
    public String getExpirationDate() {
        return this.expirationDate;
    }
    
    public void setExpirationDate(final String expirationDate) {
        this.expirationDate = expirationDate;
    }
    
    public String getMaskedNumber() {
        return this.maskedNumber;
    }
    
    public void setMaskedNumber(final String maskedNumber) {
        this.maskedNumber = maskedNumber;
    }
    
    public String getCreditCardTokenId() {
        return this.creditCardTokenId;
    }
    
    public void setCreditCardTokenId(final String creditCardTokenId) {
        this.creditCardTokenId = creditCardTokenId;
    }
    
    public String getErrorDescription() {
        return this.errorDescription;
    }
    
    public void setErrorDescription(final String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
