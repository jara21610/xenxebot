package co.com.zien.xenxerobot.backend.enumerator;

public enum ChannelEnum
{
    EMAIL("EMAIL", 0, "EMAIL"), 
    TELEGRAM("TELEGRAM", 1, "TELEGRAM");
    
    private String channel;
    
    public String getChannel() {
        return this.channel;
    }
    
    private ChannelEnum(final String s, final int n, final String channel) {
        this.channel = channel;
    }
}
