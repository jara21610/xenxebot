package co.com.zien.xenxerobot.backend.enumerator;

public enum FacebookSearchTypeEnum
{
    PAGE("PAGE", 0, "PAGE"), 
    POST("POST", 1, "POST");
    
    private String facebooksearchtype;
    
    public String getFacebooksearchtype() {
        return this.facebooksearchtype;
    }
    
    private FacebookSearchTypeEnum(final String s, final int n, final String facebooksearchtype) {
        this.facebooksearchtype = facebooksearchtype;
    }
}
