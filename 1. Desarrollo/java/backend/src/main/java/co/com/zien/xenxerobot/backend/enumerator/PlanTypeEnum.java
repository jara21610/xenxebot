package co.com.zien.xenxerobot.backend.enumerator;

public enum PlanTypeEnum
{
    FREEPLAN("FREEPLAN", 0, "FREEPLAN");
    
    private String type;
    
    public String getType() {
        return this.type;
    }
    
    private PlanTypeEnum(final String s, final int n, final String type) {
        this.type = type;
    }
}
