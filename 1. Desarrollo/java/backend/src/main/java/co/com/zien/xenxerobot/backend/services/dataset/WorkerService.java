package co.com.zien.xenxerobot.backend.services.dataset;

import java.util.*;
import co.com.zien.xenxerobot.persistence.entity.*;

public interface WorkerService
{
    List<Worker> getavailableWorkers();
}
