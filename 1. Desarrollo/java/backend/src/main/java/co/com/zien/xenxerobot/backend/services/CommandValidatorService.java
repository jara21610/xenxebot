package co.com.zien.xenxerobot.backend.services;

public interface CommandValidatorService
{
    ComandDTO validateCommand(final String p0) throws CommandMalformedException;
}
