package co.com.zien.xenxerobot.backend.services;

public class TokenNotExistException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public TokenNotExistException() {
    }
    
    public TokenNotExistException(final String message) {
        super(message);
    }
    
    public TokenNotExistException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public TokenNotExistException(final Throwable cause) {
        super(cause);
    }
}
