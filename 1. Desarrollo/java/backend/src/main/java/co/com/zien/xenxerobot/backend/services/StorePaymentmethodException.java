package co.com.zien.xenxerobot.backend.services;

public class StorePaymentmethodException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public StorePaymentmethodException() {
    }
    
    public StorePaymentmethodException(final String message) {
        super(message);
    }
    
    public StorePaymentmethodException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public StorePaymentmethodException(final Throwable cause) {
        super(cause);
    }
}
