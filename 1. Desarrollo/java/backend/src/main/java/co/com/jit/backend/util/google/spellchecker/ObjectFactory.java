package co.com.jit.backend.util.google.spellchecker;

import javax.xml.bind.annotation.*;

@XmlRegistry
public class ObjectFactory
{
    public Toplevel createToplevel() {
        return new Toplevel();
    }
    
    public Toplevel.CompleteSuggestion createToplevelCompleteSuggestion() {
        return new Toplevel.CompleteSuggestion();
    }
    
    public Toplevel.CompleteSuggestion.Suggestion createToplevelCompleteSuggestionSuggestion() {
        return new Toplevel.CompleteSuggestion.Suggestion();
    }
}
