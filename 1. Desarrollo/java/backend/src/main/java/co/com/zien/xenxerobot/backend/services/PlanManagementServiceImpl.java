package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.context.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.io.*;
import java.util.*;
import com.google.gson.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.web.bind.annotation.*;
import co.com.zien.xenxerobot.backend.telegram.bot.*;
import co.com.zien.xenxerobot.backend.config.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.scheduling.annotation.*;

@Service
public class PlanManagementServiceImpl implements PlanManagementService
{
    @Autowired
    PaymentServices ps;
    @Autowired
    UserplanRepository upr;
    @Autowired
    PlanRepository planr;
    @Autowired
    UserRepository ur;
    @Autowired
    PaymentmethodRepository pmr;
    @Autowired
    PaymenthistoryRepository phr;
    @Autowired
    TokenService ts;
    @Autowired
    MessageSource messageSource;
    @Autowired
    PlanService plans;
    @Autowired
	InvoiceReport ir;
	
        
    public Userplan upgradeplan(final Users u, final String plan, String productid) throws Exception {
        final Plan templateplan = (Plan)this.planr.findByIdAndProductid(plan, productid);
        final Userplan up = new Userplan();
        up.setId(UUID.randomUUID().toString());
        up.setIdplan(templateplan);
        up.setIduser(u);
        up.setTxplancapacity(templateplan.getTxplancapacity());
        up.setTxpalused(0);
        up.setStartdate(new Date());
        up.setState("NOTPAID");
        up.setPrice(templateplan.getPrice());
        up.setProductid(productid);
        final Calendar cal = Calendar.getInstance();
        cal.add(2, new Integer(templateplan.getTimequantity()));
        up.setFinishdate(cal.getTime());
        this.upr.saveAndFlush(up);
        this.ur.saveAndFlush(u);
        return up;
    }
        
    public Paymentmethod storePaymentMethod(final Users u, final String name, final String identificationNumber, final String number, final String expirationDate, final String ccv, final String idType) throws StorePaymentmethodException {
        final RegisterCreditCardTokenResponse tokenresponse = this.ps.createCreditCardToken(u.getId(), name, identificationNumber, number, expirationDate);
        if (tokenresponse.getCode().equals("SUCCESS")) {
            final Paymentmethod pm = new Paymentmethod();
            pm.setUserid(u);
            pm.setId(UUID.randomUUID().toString());
            pm.setCreditcardtoken(tokenresponse.getCreditCardToken().getCreditCardTokenId());
            pm.setPaymentmethod(tokenresponse.getCreditCardToken().getPaymentMethod());
            pm.setMaskednumber(tokenresponse.getCreditCardToken().getMaskedNumber());
            pm.setCardname(name);
            pm.setDninumber(identificationNumber);
            pm.setPredeterminate("Y");
            this.pmr.saveAndFlush(pm);
            return pm;
        }
        throw new StorePaymentmethodException();
    }
        
    public void makePlanPayment(final Users u, final Userplan up, final Paymentmethod pm) throws PaymentException {
        final Gson gson = new Gson();
        final PayOutput payresponse = this.ps.payWithCreditCardToken(up.getId(), pm.getPaymentmethod(), pm.getCreditcardtoken(), "COP", up.getPrice(), u.getId(), pm.getCardname(), pm.getDninumber());
        final Paymenthistory ph = new Paymenthistory();
        ph.setId(UUID.randomUUID().toString());
        ph.setDetail(gson.toJson((Object)payresponse.getTransactionResponse()));
        ph.setPaymentdate(new Date());
        ph.setPaymentmethodid(pm);
        ph.setUserplanid(up);
        if (payresponse.getTransactionResponse().getState().equals("APPROVED")) {
            ph.setStatus("SUCCESS");
            this.phr.saveAndFlush(ph);
            
            try {
				ir.generateInvoice(u, up, pm);
			} catch (Exception e) {				
				e.printStackTrace();
			}
            
            return;
        }
        ph.setStatus("REJECTED");
        this.phr.saveAndFlush(ph);
        pm.setPredeterminate("N");
        this.pmr.saveAndFlush(pm);
        throw new PaymentException();
    }
    
    @Async  
    public void upgradePlanWithPaymentMethod(@RequestBody final UpgradePlanDTO updto) throws Exception {
        Userplan up = null;
        Paymentmethod pm = null;
        Users u = null;
        String servicetype = this.ts.getProductIdToken(updto.getUsertoken());
        final ServiceBot xb = (ServiceBot)Config.getContext().getBean(servicetype);
        try {
            u = this.ts.validateUserToken(updto.getUsertoken());
            if (updto.getPlan() != null) {
                up = this.upgradeplan(u, updto.getPlan(), servicetype);
            }
            else {
                up = this.upr.activePlanByUseridandstate(u.getId(), "NOTPAID", updto.getServicetype());
            }
            pm = this.storePaymentMethod(u, updto.getName(), updto.getIdentificationNumber(), updto.getNumber().replace(" ", ""), updto.getExpirationDate(), updto.getCcv(), updto.getIdType());
            this.makePlanPayment(u, up, pm);
            up.setState("ACTIVO");
            this.upr.saveAndFlush(up);
            xb.handleUpgradePlanOK(u.getChatid());
        }
        catch (TokenNotExistException ex) {}
        catch (TokenExpiredException e) {
            u = this.ts.getUserOwnerToken(updto.getUsertoken());
            xb.handleUserTokenException(u.getChatid());
            
            up = this.plans.getLastUserPlan(u.getId(), servicetype);
            
            if (up.getIdplan().equals(PlanTypeEnum.FREEPLAN.getType())) {
            	xb.handleFreePlanFinished(u.getChatid(), u);
            } else {
            	xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
            }
        }
        catch (StorePaymentmethodException e2) {
            up.setState("NOTPAID");
            this.upr.saveAndFlush(up);
            this.ur.saveAndFlush(u);
            xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
        }
        catch (PaymentException e3) {
            up.setState("NOTPAID");
            this.upr.saveAndFlush(up);
            this.ur.saveAndFlush(u);
            xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
        }
        catch (Exception ex2) {}
    }
    
    @Override
    public void upgradePlanPaymentMethodPredeterminated(final Users u, String productid) throws Exception {
        Userplan up = null;
        Userplan up2 = null;
        Paymentmethod pm = null;
        final ServiceBot xb = (ServiceBot)Config.getContext().getBean(productid);
        try {
            up = this.plans.getLastUserPlan(u.getId(), productid);
            up2 = this.upgradeplan(u, up.getIdplan().getPlanid(), productid);
            pm = this.pmr.findPrederminateMethod(u.getId());
            this.makePlanPayment(u, up2, pm);
            up2.setState("ACTIVO");
            this.upr.saveAndFlush(up2);
            xb.handleUpgradePlanOK(u.getChatid());
        }
        catch (StorePaymentmethodException e) {
            up.setState("NOTPAID");
            this.upr.saveAndFlush(up);
            this.ur.saveAndFlush(u);
            xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
        }
        catch (PaymentException e2) {
            up.setState("NOTPAID");
            this.upr.saveAndFlush(up);
            this.ur.saveAndFlush(u);
            xb.handlePaymentExceptionUpgradePlan(u.getChatid(), u);
        }
        catch (Exception ex) {}
    }
    
    public void planManagementDailyVerifications() throws Exception {
    	
    	int i = 0;
    	boolean hasmore = true;
    	while (hasmore) {
    		Pageable pageable = (Pageable)new PageRequest(i, 100);
    		i++;
    		List<Userplan> uplist = upr.findPlanstoFinish(pageable);
    		if (uplist != null && uplist.size() > 0) {    			
    			for (Userplan up: uplist){    				
    				
    				if (up.getIduser().getId().equals("846e6fe6-4476-4204-9a47-d68dad482ed6")) {
	    				up.setState(PlanStateEnum.FINALIZADO.getState());
	    				upr.saveAndFlush(up);
	    				
	    				if (up.getIdplan().getPlanid().equals(PlanTypeEnum.FREEPLAN.getType())) {
	    					final ServiceBot xb = (ServiceBot)Config.getContext().getBean(up.getProductid());
	    					xb.handleFreePlanFinished(up.getIduser().getChatid(), up.getIduser());
	    				} else {
	    					upgradePlanPaymentMethodPredeterminated(up.getIduser(), up.getProductid());
	    				}
    				}
    				
    			}    			
    		} else {
    			hasmore = false;
    		}
    	
    	}
    }
}
