package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;
import co.com.zien.xenxerobot.backend.dictionary.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.env.*;
import java.io.*;

@Service
@Transactional
public class InflectionDictionaryServiceImpl implements InflectionDictionaryService
{
    private InflectionDictionary dictionary;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    Environment env;
    
    public InflectionDictionaryServiceImpl() {
        this.dictionary = null;
    }
    
    @Override
    public InflectionDictionary getDictionary() {
        if (this.dictionary == null) {
            (this.dictionary = new InflectionDictionary()).setPlaintxtDictionary(new File(this.env.getRequiredProperty("lemma.path")));
            this.dictionary.loadPlainTextDictionary();
        }
        return this.dictionary;
    }
}
