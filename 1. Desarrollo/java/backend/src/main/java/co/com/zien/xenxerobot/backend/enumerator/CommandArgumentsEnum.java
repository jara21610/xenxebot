package co.com.zien.xenxerobot.backend.enumerator;

public enum CommandArgumentsEnum
{
    SOCIALMEDIASOURCE("SOCIALMEDIASOURCE", 0, "SOCIALMEDIASOURCE"), 
    SEARCHTYPE("SEARCHTYPE", 1, "SEARCHTYPE"), 
    SEARCHCRITERIA("SEARCHCRITERIA", 2, "SEARCHCRITERIA"), 
    SEARCHSINCE("SEARCHSINCE", 3, "SEARCHSINCE"), 
    SEARCHUNTIL("SEARCHUNTIL", 4, "SEARCHUNTIL"), 
    POSTLIST("POSTLIST", 5, "POSTLIST"), 
    COMMANDPROCESSID("COMMANDPROCESSID", 6, "COMMANDPROCESSID"), 
    SERVICETYPE("SERVICETYPE", 7, "SERVICETYPE"), 
    EMAIL("EMAIL", 8, "EMAIL"),
    TOKEN("EMAIL", 9, "TOKEN");
    
    private String commandargument;
    
    public String getCommandargument() {
        return this.commandargument;
    }
    
    private CommandArgumentsEnum(final String s, final int n, final String commandargument) {
        this.commandargument = commandargument;
    }
}
