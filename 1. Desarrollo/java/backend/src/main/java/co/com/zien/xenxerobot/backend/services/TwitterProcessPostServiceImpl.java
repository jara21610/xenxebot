package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.apache.log4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.io.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import com.google.common.collect.*;
import java.util.concurrent.*;
import org.springframework.social.twitter.api.*;
import java.util.*;

@Service("TwitterProcessPost")
public class TwitterProcessPostServiceImpl implements SourceProcessPostService
{
    private static final Logger logger;
    @Autowired
    PostRepository postr;
    @Autowired
    PreproccesingService scs;
    @Autowired
    SentimentService ss;
    @Autowired
    @Qualifier("TwitterPoolConectionManager")
    SocialPoolConectionManager tpcm;
    @Autowired
    PropertiesRepository pr;
    @Autowired
    @Qualifier("TwitterProcessUnit")
    SourceProcessUnit tpt;
    @Autowired
    @Qualifier("EmailResponseBot")
    ResponseBotService erbs;
    @Autowired
    @Qualifier("TelegramResponseBot")
    ResponseBotService trbs;
    @Autowired
    MessageSource messageSource;
    @Autowired
    CommandProcessRepository cpr;
    
    static {
        logger = Logger.getLogger((Class)TwitterProcessPostServiceImpl.class);
    }

    public void process(CommandProcess cp, final ComandDTO cdto) throws Exception {
        final String searchCriteria = (String) cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument());
        List<Tweet> currentlt = new ArrayList<Tweet>();
        final int pagesize = new Integer(this.pr.findByName(PropertyEnum.TWITTERPAGESIZE.getProperty()));
        Twitter twitter = (Twitter)this.getAvailableConecction();
        boolean twitteroverload = true;
        SearchResults sr = null;
        while (twitteroverload) {
            try {
                sr = twitter.searchOperations().search(new SearchParameters(searchCriteria).count(pagesize).lang("es"));
            }
            catch (Exception e) {
                twitteroverload = true;
            }
            twitteroverload = false;
        }
        if (sr.getTweets().size() <= 0) {
            return;
        }
        final CompletableFuture<String> page = this.tpt.executeUnit(sr.getTweets(), cp, cdto);
        page.join();
        if ("stop".equals(page.get())) {
            return;
        }
        while (true) {
            cp = (CommandProcess)this.cpr.findOne(cp.getId());
            if (cp.getState().equals(CommandProcessStateEnum.CANCELADOXUSUARIO.getState()) || cp.getState().equals(CommandProcessStateEnum.ERROR.getState())) {
                return;
            }
            twitter = (Twitter)this.getAvailableConecction();
            for (twitteroverload = true; twitteroverload; twitteroverload = false) {
                try {
                    sr = twitter.searchOperations().search(new SearchParameters(searchCriteria).includeEntities(true).count(pagesize).lang("es").maxId(sr.getTweets().get(sr.getTweets().size() - 1).getId() - 1L));
                }
                catch (Exception e) {
                    twitteroverload = true;
                }
            }
            currentlt = (List<Tweet>)sr.getTweets();
            if (currentlt.size() <= 0) {
                return;
            }
            final List<List<Tweet>> smallerLists = (List<List<Tweet>>)Lists.partition((List)currentlt, 25);
            final CompletableFuture[] fs = new CompletableFuture[smallerLists.size()];
            int i = 0;
            for (final List<Tweet> lt : smallerLists) {
                final CompletableFuture<String> page2 = this.tpt.executeUnit(lt, cp, cdto);
                fs[i] = page2;
                ++i;
            }
            CompletableFuture.allOf((CompletableFuture<?>[])fs);
            CompletableFuture[] array;
            for (int length = (array = fs).length, j = 0; j < length; ++j) {
                final CompletableFuture<String> page3 = (CompletableFuture<String>)array[j];
                if ("stop".equals(page3.get())) {
                    return;
                }
            }
        }
    }
    
    private Object getAvailableConecction() throws Exception {
        boolean success = false;
        while (!success) {
            try {
                success = true;
                return this.tpcm.getAvailableConecction(new String[0]);
            }
            catch (SocialPoolConectionManagerNotFoundAvailableException e) {
                success = false;
                Thread.currentThread();
                Thread.sleep(this.tpcm.getMinimalSecondResetConection());
            }
        }
        return null;
    }
}
