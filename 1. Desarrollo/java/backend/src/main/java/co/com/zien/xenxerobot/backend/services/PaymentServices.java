package co.com.zien.xenxerobot.backend.services;

import java.math.*;

public interface PaymentServices
{
    String getSignature(final String p0, final String p1, final String p2, final String p3, final String p4);
    
    RegisterCreditCardTokenResponse createCreditCardToken(final String p0, final String p1, final String p2, final String p3, final String p4);
    
    PayOutput payWithCreditCardToken(final String p0, final String p1, final String p2, final String p3, final BigInteger p4, final String p5, final String p6, final String p7);
}
