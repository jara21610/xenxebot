package co.com.zien.xenxerobot.backend.services;

public class Transaction
{
    private Order order;
    private Payer payer;
    private String creditCardTokenId;
    private ExtraParameters extraParameters;
    private String type;
    private String paymentMethod;
    private String paymentCountry;
    private String deviceSessionId;
    private String ipAddress;
    private String cookie;
    private String userAgent;
    
    public Order getOrder() {
        return this.order;
    }
    
    public void setOrder(final Order order) {
        this.order = order;
    }
    
    public Payer getPayer() {
        return this.payer;
    }
    
    public void setPayer(final Payer payer) {
        this.payer = payer;
    }
    
    public String getCreditCardTokenId() {
        return this.creditCardTokenId;
    }
    
    public void setCreditCardTokenId(final String creditCardTokenId) {
        this.creditCardTokenId = creditCardTokenId;
    }
    
    public ExtraParameters getExtraParameters() {
        return this.extraParameters;
    }
    
    public void setExtraParameters(final ExtraParameters extraParameters) {
        this.extraParameters = extraParameters;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public String getPaymentMethod() {
        return this.paymentMethod;
    }
    
    public void setPaymentMethod(final String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public String getPaymentCountry() {
        return this.paymentCountry;
    }
    
    public void setPaymentCountry(final String paymentCountry) {
        this.paymentCountry = paymentCountry;
    }
    
    public String getDeviceSessionId() {
        return this.deviceSessionId;
    }
    
    public void setDeviceSessionId(final String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }
    
    public String getIpAddress() {
        return this.ipAddress;
    }
    
    public void setIpAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }
    
    public String getCookie() {
        return this.cookie;
    }
    
    public void setCookie(final String cookie) {
        this.cookie = cookie;
    }
    
    public String getUserAgent() {
        return this.userAgent;
    }
    
    public void setUserAgent(final String userAgent) {
        this.userAgent = userAgent;
    }
}
