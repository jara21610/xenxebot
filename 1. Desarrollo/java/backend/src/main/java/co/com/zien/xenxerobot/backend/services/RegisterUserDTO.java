package co.com.zien.xenxerobot.backend.services;

public class RegisterUserDTO
{
    private String name;
    private String lastname;
    private String email;
    private String mobilphone;
    private String servicetype;
    
    public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getLastname() {
        return this.lastname;
    }
    
    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getMobilphone() {
        return this.mobilphone;
    }
    
    public void setMobilphone(final String mobilphone) {
        this.mobilphone = mobilphone;
    }
}
