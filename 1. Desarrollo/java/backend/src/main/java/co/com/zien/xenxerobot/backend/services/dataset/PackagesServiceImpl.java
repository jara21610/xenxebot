package co.com.zien.xenxerobot.backend.services.dataset;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import java.math.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import java.util.*;
import java.io.*;
import org.apache.commons.lang3.*;
import org.springframework.data.domain.*;

@Service("packagesService")
public class PackagesServiceImpl implements PackagesService
{
    @Autowired
    PackagesRepository pr;
    @Autowired
    PostRepository postr;
    @Autowired
    ConceptsRepository cr;
    
    public List<Packages> findByWorker(final Worker worker) {
        return (List<Packages>)this.pr.findByWorker(worker);
    }
    
    public List<Packages> findByState(final String state) {
        return (List<Packages>)this.pr.findByState(state);
    }
    
    public List<Post> nextworkitembypackage(final BigDecimal id) {
        return (List<Post>)this.postr.nextworkitembypackage(id);
    }
    
    public void savehumantask(final Post p) {
        for (final Concepts c : p.getConceptsCollection()) {
            this.cr.saveAndFlush(c);
        }
        this.postr.saveAndFlush(p);
    }
    
    public void savePackage(final Packages p) {
        this.pr.saveAndFlush(p);
    }
    
    public Packages findPackage(final BigDecimal id) {
        return (Packages)this.pr.findOne(id);
    }
    
    public Integer packageSize(final BigDecimal id) {
        return this.postr.packageSize(id);
    }
    
    public Post change(final Post current) {
        int i = 0;
        Post newpost = null;
        Pageable pageable = (Pageable)new PageRequest(i, 1);
        final List<Post> changelist = (List<Post>)this.postr.posttochange(current.getIdPackage().getId(), pageable);
        if (changelist != null) {
            newpost = changelist.get(0);
        }
        else {
            boolean similartoany = true;
            final List<Post> packagepost = (List<Post>)this.postr.postbypackage(current.getIdPackage().getId());
            while (similartoany) {
                pageable = (Pageable)new PageRequest(i, 1);
                newpost = (Post)this.postr.findOne((String)((Object[])this.postr.posttopackage(current.getCommandprocess().getId(), pageable).get(0))[0]);
                similartoany = false;
                for (final Post p : packagepost) {
                    final int distance = StringUtils.getLevenshteinDistance((CharSequence)p.getOriginalmessage(), (CharSequence)newpost.getOriginalmessage());
                    final double similarity = 100.0 - distance / Math.max(p.getOriginalmessage().length(), current.getOriginalmessage().length()) * 100.0;
                    if (similarity > 80.0) {
                        similartoany = true;
                        newpost.setDiscartedtopack("Y");
                        this.postr.saveAndFlush(newpost);
                        break;
                    }
                }
                if (!similartoany) {
                    break;
                }
                ++i;
            }
        }
        newpost.setIdPackage(current.getIdPackage());
        newpost.setOptiontochange((String)null);
        current.setDiscartedtopack("Y");
        current.setIdPackage((Packages)null);
        this.postr.saveAndFlush(newpost);
        this.postr.saveAndFlush(current);
        return newpost;
    }
}
