package co.com.zien.xenxerobot.backend.enumerator;

public enum TwitterSearchTypeEnum
{
    HASHTAG("HASHTAG", 0, "HASHTAG"), 
    USUARIO("USUARIO", 1, "USUARIO");
    
    private String twittersearchtype;
    
    public String getTwittersearchtype() {
        return this.twittersearchtype;
    }
    
    private TwitterSearchTypeEnum(final String s, final int n, final String twittersearchtype) {
        this.twittersearchtype = twittersearchtype;
    }
}
