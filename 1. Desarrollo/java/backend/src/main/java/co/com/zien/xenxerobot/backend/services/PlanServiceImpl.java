package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.persistence.entity.*;
import org.springframework.data.domain.*;
import java.util.*;

@Service
public class PlanServiceImpl implements PlanService
{
    @Autowired
    UserplanRepository upr;
    
    @Override
    public Userplan getLastUserPlan(final String idUser, String productid) {
        final Pageable pageable = (Pageable)new PageRequest(0, 1);
        final List<Userplan> userplanlist = (List<Userplan>)this.upr.historicByUserid(idUser, productid, pageable);
        if (userplanlist.size() > 0) {
            return userplanlist.get(0);
        }
        return null;
    }
}
