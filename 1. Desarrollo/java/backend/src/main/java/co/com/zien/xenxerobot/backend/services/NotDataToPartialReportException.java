package co.com.zien.xenxerobot.backend.services;

public class NotDataToPartialReportException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public NotDataToPartialReportException() {
    }
    
    public NotDataToPartialReportException(final String message) {
        super(message);
    }
    
    public NotDataToPartialReportException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public NotDataToPartialReportException(final Throwable cause) {
        super(cause);
    }
}
