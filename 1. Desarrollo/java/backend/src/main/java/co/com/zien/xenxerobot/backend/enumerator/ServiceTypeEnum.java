package co.com.zien.xenxerobot.backend.enumerator;

public enum ServiceTypeEnum
{
    PREXTIGE("PREXTIGE", 0, "PreXtige"), 
    XENXE("XENXE", 1, "Xenxe");
    
    private String servicetype;
    
    public String getServicetype() {
        return this.servicetype;
    }
    
    private ServiceTypeEnum(final String s, final int n, final String servicetype) {
        this.servicetype = servicetype;
    }
}
