package co.com.zien.xenxerobot.backend.services;

public interface BotLauncher
{
    public void launchBots();
}
