package co.com.zien.xenxerobot.backend.services;

public class RegisterCreditCardTokenResponse
{
    private String code;
    private String error;
    private CreditCardToken creditCardToken;
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    public CreditCardToken getCreditCardToken() {
        return this.creditCardToken;
    }
    
    public void setCreditCardToken(final CreditCardToken creditCardToken) {
        this.creditCardToken = creditCardToken;
    }
    
    public String getError() {
        return this.error;
    }
    
    public void setError(final String error) {
        this.error = error;
    }
}
