package co.com.zien.xenxerobot.backend.services;

public class EmailMessageDTO
{
    private String from;
    private String text;
    
    public String getFrom() {
        return this.from;
    }
    
    public void setFrom(final String from) {
        this.from = from;
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(final String text) {
        this.text = text;
    }
}
