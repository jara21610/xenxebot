package co.com.zien.xenxerobot.backend.services;

public class TxLimitException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public TxLimitException() {
    }
    
    public TxLimitException(final String message) {
        super(message);
    }
    
    public TxLimitException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public TxLimitException(final Throwable cause) {
        super(cause);
    }
}
