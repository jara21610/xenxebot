package co.com.zien.xenxerobot.backend.services;

import java.math.*;

public interface DatasetService
{
    void export(final BigDecimal... p0) throws Exception;
    
    void generatePackgage(final BigDecimal p0, final String... p1) throws Exception;
}
