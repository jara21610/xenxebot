package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import co.com.zien.xenxerobot.persistence.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import org.springframework.context.i18n.*;

@Service("EmailResponseBot")
public class EmailResponseBotServiceImpl implements ResponseBotService
{
    @Autowired
    PropertiesRepository pr;
    @Autowired
    MessageSource messageSource;
    @Autowired
    EmailService es;
    
    public void okResponseBot(final String reportpath, final ComandDTO cdto) {
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), this.messageSource.getMessage("message.finishsentimentanalysis.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.finishsentimentanalysis.text", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()).toString().toUpperCase(), cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), new String[] { reportpath });
    }
    
    public void exceptionResponseBot(final ComandDTO cdto, final Exception e) {
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), this.messageSource.getMessage("message.exception.runtimeexception.subject", (Object[])null, LocaleContextHolder.getLocale()), e.getMessage());
    }
    
    public void messageResponseBot(final String message, final ComandDTO cdto) {
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), this.messageSource.getMessage("message.postnotfound.subject", new Object[] { cdto.getArguments().get(CommandArgumentsEnum.SERVICETYPE.getCommandargument()), cdto.getArguments().get(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument()) }, LocaleContextHolder.getLocale()), message);
    }
    
    public void okPartialReportResponseBot(final String reportpath, final ComandDTO cdto) {
    }
    
    public void isprocessingResponseBot(final ComandDTO cdto) {
        this.es.sendEmail(this.pr.findByName("mail.smtp.email"), cdto.getCommunicationKey(), this.messageSource.getMessage("message.isprocesing.subject", new Object[] { cdto.getArguments().get(2) }, LocaleContextHolder.getLocale()), this.messageSource.getMessage("message.isprocesing.text", (Object[])null, LocaleContextHolder.getLocale()));
    }
}
