package co.com.zien.xenxerobot.backend.services;

public class Buyer
{
    private String merchantBuyerId;
    private String fullName;
    private String emailAddress;
    private String contactPhone;
    private String dniNumber;
    private ShippingAddress shippingAddress;
    
    public String getMerchantBuyerId() {
        return this.merchantBuyerId;
    }
    
    public void setMerchantBuyerId(final String merchantBuyerId) {
        this.merchantBuyerId = merchantBuyerId;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }
    
    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public String getContactPhone() {
        return this.contactPhone;
    }
    
    public void setContactPhone(final String contactPhone) {
        this.contactPhone = contactPhone;
    }
    
    public String getDniNumber() {
        return this.dniNumber;
    }
    
    public void setDniNumber(final String dniNumber) {
        this.dniNumber = dniNumber;
    }
    
    public ShippingAddress getShippingAddress() {
        return this.shippingAddress;
    }
    
    public void setShippingAddress(final ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
