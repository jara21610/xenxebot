package co.com.zien.xenxerobot.backend.services;

import java.util.*;

public class PayOutput
{
    private String code;
    private Object error;
    private TransactionResponse transactionResponse;
    private Map<String, Object> additionalProperties;
    
    public PayOutput() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    public Object getError() {
        return this.error;
    }
    
    public void setError(final Object error) {
        this.error = error;
    }
    
    public TransactionResponse getTransactionResponse() {
        return this.transactionResponse;
    }
    
    public void setTransactionResponse(final TransactionResponse transactionResponse) {
        this.transactionResponse = transactionResponse;
    }
    
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
}
