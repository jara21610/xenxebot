package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import co.com.zien.xenxerobot.backend.enumerator.*;

@Service
public class ReportDetailServiceFactoryImpl implements ReportDetailServiceFactory
{
    @Autowired
    @Qualifier("XenxeReportDetailManager")
    ReportDetailManager rdmx;
    @Autowired
    @Qualifier("PreXtigeReportDetailManager")
    ReportDetailManager rdmp;
    
    @Override
    public ReportDetailManager getResponseBotService(final String servicetype) {
        if (servicetype.equals(ServiceTypeEnum.XENXE.getServicetype())) {
            return this.rdmx;
        }
        if (servicetype.equals(ServiceTypeEnum.PREXTIGE.getServicetype())) {
            return this.rdmp;
        }
        return this.rdmp;
    }
}
