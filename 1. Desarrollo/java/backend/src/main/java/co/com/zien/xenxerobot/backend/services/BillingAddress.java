package co.com.zien.xenxerobot.backend.services;

public class BillingAddress
{
    private String street1;
    private String street2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    
    public String getStreet1() {
        return this.street1;
    }
    
    public void setStreet1(final String street1) {
        this.street1 = street1;
    }
    
    public String getStreet2() {
        return this.street2;
    }
    
    public void setStreet2(final String street2) {
        this.street2 = street2;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(final String city) {
        this.city = city;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(final String country) {
        this.country = country;
    }
    
    public String getPostalCode() {
        return this.postalCode;
    }
    
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }
    
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(final String phone) {
        this.phone = phone;
    }
}
