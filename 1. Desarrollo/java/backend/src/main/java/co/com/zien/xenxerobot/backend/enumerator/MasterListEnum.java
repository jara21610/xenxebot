package co.com.zien.xenxerobot.backend.enumerator;

public enum MasterListEnum
{
    REGEXTOCLEANPUNTUATIONMARKS("REGEXTOCLEANPUNTUATIONMARKS", 0, "REGEXTOCLEANPUNTUATIONMARKS"), 
    REGEXTOKEPPPUNTUATIONMARKS("REGEXTOKEPPPUNTUATIONMARKS", 1, "REGEXTOKEPPPUNTUATIONMARKS");
    
    private String masterlist;
    
    public String getMasterlist() {
        return this.masterlist;
    }
    
    private MasterListEnum(final String s, final int n, final String masterlist) {
        this.masterlist = masterlist;
    }
}
