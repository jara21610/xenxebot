package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.core.env.*;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.*;
import com.google.gson.*;
import com.sun.jersey.api.client.*;

@Service
public class SentimentServicesImpl implements SentimentService
{
    @Autowired
    Environment env;
    
    @Override
    public SentimentAnalysisOutputDTO getSentimentAnalysis(final String texttoanalize, final String servicetype) {
        final String uri = this.env.getRequiredProperty("mlservice.url");
        final SentimentAnalysisInputDTO sadto = new SentimentAnalysisInputDTO();
        sadto.setTweet(texttoanalize);
        sadto.setServicetype(servicetype);
        final Client client = Client.create();
        final WebResource webResource = client.resource(uri);
        final Gson gson = new Gson();
        final String input = gson.toJson((Object)sadto);
        final ClientResponse response = (ClientResponse)webResource.type("application/json").post((Class)ClientResponse.class, (Object)input);
        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
        return (SentimentAnalysisOutputDTO)gson.fromJson((String)response.getEntity((Class)String.class), (Class)SentimentAnalysisOutputDTO.class);
    	
    	/*System.out.println("MOCK SERVICIO SENTIMIENTO");
    	SentimentAnalysisOutputDTO output = new SentimentAnalysisOutputDTO();
    	
    	output.setSentiment("P");
    	output.setCategory("ENTRETENIMIENTO");*/    	
    	//return output;
    }
}
