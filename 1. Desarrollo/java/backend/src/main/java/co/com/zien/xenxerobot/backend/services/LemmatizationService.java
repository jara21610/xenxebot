package co.com.zien.xenxerobot.backend.services;

public interface LemmatizationService
{
    String getWordLemma(final String p0);
}
