package co.com.zien.xenxerobot.backend.enumerator;

public enum SentimentEnum
{
    POSITIVE("POSITIVE", 0, "P"), 
    NEGATIVE("NEGATIVE", 1, "N"), 
    NEUTRAL("NEUTRAL", 2, "NEU");
    
    private String sentiment;
    
    public String getSentiment() {
        return this.sentiment;
    }
    
    private SentimentEnum(final String s, final int n, final String sentiment) {
        this.sentiment = sentiment;
    }
}
