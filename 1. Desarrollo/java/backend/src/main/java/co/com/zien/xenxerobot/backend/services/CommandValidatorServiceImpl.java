package co.com.zien.xenxerobot.backend.services;

import org.springframework.stereotype.*;
import org.springframework.context.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.i18n.*;
import co.com.zien.xenxerobot.backend.enumerator.*;
import java.util.*;
import java.text.*;

@Service
public class CommandValidatorServiceImpl implements CommandValidatorService
{
    @Autowired
    MessageSource messageSource;
        
    public ComandDTO validateCommand(final String command) throws CommandMalformedException {
        try {
            final List<String> commanddetail = new ArrayList<String>(Arrays.asList(command.split(" ")));
            final ComandDTO cdto = new ComandDTO();
            if (CommandEnum.APPLICATIONSENTIMENT.getCommand().equals(commanddetail.get(0))) {
                cdto.setCommand(CommandEnum.APPLICATIONSENTIMENT.getCommand());
                if (!SocialMediaSourceEnum.TWITTER.getSocialmediasource().equals(commanddetail.get(1))) {
                    throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidsocialmediasource", (Object[])null, LocaleContextHolder.getLocale()));
                }
                if (TwitterSearchTypeEnum.HASHTAG.getTwittersearchtype().equals(commanddetail.get(2)) || TwitterSearchTypeEnum.USUARIO.getTwittersearchtype().equals(commanddetail.get(2))) {
                    final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                    final HashMap<String, Object> arguments = new HashMap<String, Object>();
                    arguments.put(CommandArgumentsEnum.SOCIALMEDIASOURCE.getCommandargument(), commanddetail.get(1));
                    arguments.put(CommandArgumentsEnum.SEARCHTYPE.getCommandargument(), commanddetail.get(2));
                    String searchcriteria = "";
                    for (int i = 3; i < commanddetail.size() - 4; ++i) {
                        searchcriteria = String.valueOf(searchcriteria) + commanddetail.get(i) + " ";
                    }
                    arguments.put(CommandArgumentsEnum.SEARCHCRITERIA.getCommandargument(), searchcriteria);
                    arguments.put(CommandArgumentsEnum.SEARCHSINCE.getCommandargument(), dateFormat.parse(String.valueOf(commanddetail.get(commanddetail.size() - 4)) + " " + commanddetail.get(commanddetail.size() - 3)));
                    arguments.put(CommandArgumentsEnum.SEARCHUNTIL.getCommandargument(), dateFormat.parse(String.valueOf(commanddetail.get(commanddetail.size() - 2)) + " " + commanddetail.get(commanddetail.size() - 1)));
                    cdto.setArguments(arguments);
                    return cdto;
                }
                throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidtwittersearchtype", (Object[])null, LocaleContextHolder.getLocale()));
            }
            else {
                if (CommandEnum.HELP.getCommand().equals(commanddetail.get(0))) {
                    cdto.setCommand(CommandEnum.HELP.getCommand());
                    return cdto;
                }
                throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidcommand", (Object[])null, LocaleContextHolder.getLocale()));
            }
        }
        catch (ParseException e) {
            throw new CommandMalformedException(this.messageSource.getMessage("message.exception.notvalidcommand", (Object[])null, LocaleContextHolder.getLocale()));
        }
    }
}
