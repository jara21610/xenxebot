#!/bin/bash
python3.7 -m venv env
cd ./usr
cd ./src
source env/bin/activate
cd ./env/textclassification/deployment
python ml_serverV2.py