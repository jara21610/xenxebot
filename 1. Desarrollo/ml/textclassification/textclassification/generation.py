import pandas as pd
df = pd.read_csv('dataset.csv')

from sklearn.preprocessing import LabelEncoder

le_sentiments = LabelEncoder()
le_category = LabelEncoder()
le_topics = LabelEncoder()

# ACA AGREGAR/ CAMBIAR EL NOMBRE DE LA COLUMNA O SI AGREGAN OTRA PONERLA
sentiments = le_sentiments.fit_transform(df["Sentimiento"].values.astype(str))
category = le_category.fit_transform(df["Categoria"].values.astype(str))
topics = le_topics.fit_transform(df["Topicos"].values.astype(str))

tweets = df["Post"].values.astype(str)

# TODO REEMPLAZAR ACA EL Y POR LA COLUMNA QUE QUIEREN QUE EL MODELO UTILICE Y SI USAR DNN!!
Y = topics
USE_DNN = False

def generate_dnn_model(tweets, targets):
    MAX_SEQUENCE_LENGTH = 1000
    MAX_NB_WORDS = 20000
    EMBEDDING_DIM = 100
    TOP_WORDS = 5000
    from keras.preprocessing.text import Tokenizer
    from keras.preprocessing.sequence import pad_sequences
    from keras.utils import to_categorical
    import numpy as np
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import LSTM
    from keras.layers.embeddings import Embedding

    tokenizer = Tokenizer(nb_words=MAX_NB_WORDS)
    tokenizer.fit_on_texts(tweets)
    sequences = tokenizer.texts_to_sequences(tweets)

    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))

    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

    labels = targets#to_categorical(np.asarray(targets))
    print('Shape of data tensor:', data.shape)
    print('Shape of label tensor:', labels.shape)

    # create the model
    embedding_vecor_length = 32
    model = Sequential()
    model.add(Embedding(TOP_WORDS, EMBEDDING_DIM, input_length=MAX_SEQUENCE_LENGTH))
    model.add(LSTM(100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    model.fit(data, labels, epochs=35, batch_size=64)
    return model


# from keras.wrappers.scikit_learn import KerasClassifier
# model = KerasClassifier(build_fn=generate_dnn_model, epochs=5, batch_size=10, verbose=10)
if USE_DNN:
    dnn = generate_dnn_model(tweets, Y)

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import Normalizer
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import VotingClassifier
from sklearn.model_selection import GridSearchCV
from time import time

vectorizer = TfidfVectorizer()
svd = TruncatedSVD()
normalizer = Normalizer()
linear_svc = LinearSVC()
logistic_regression = LogisticRegression()
sgd = SGDClassifier()
mnnb = MultinomialNB()

pipeline1 = Pipeline([
    ('vectorizer', vectorizer),
    ('svd', svd),
    ('normalizer', normalizer),
    ('linear_svc', linear_svc),
])

pipeline2 = Pipeline([
    ('vectorizer', vectorizer),
    ('logistic_regression', logistic_regression)
])

pipeline3 = Pipeline([
    ('vectorizer', vectorizer),
    ('svd', svd),
    ('normalizer', normalizer),
    ('sgd', sgd),
])

pipeline4 = Pipeline([
    ('vectorizer', vectorizer),
    ('mnnb', mnnb)
])

# specify parameters and distributions to sample from

def param_set(init, final, step):
    l = []
    while init <= final:
        l.append(min(init, final))
        init += step
    return tuple(l)


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


vectorizer_parameters = {
    'vectorizer__max_df': param_set(50, 5000, 100),
    'vectorizer__min_df': param_set(3, 100, 5),
    'vectorizer__ngram_range': ((1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7)),
    'vectorizer__analyzer' : ('word', 'char'),
    'vectorizer__use_idf': param_set(0, 1, 1),
    'vectorizer__smooth_idf': param_set(0, 1, 1),
    'vectorizer__sublinear_tf': param_set(0, 1, 1),
}


parameters1 = {
    'svd__n_components': param_set(5, 300, 10),
    'linear_svc__C': (0.05, 0.1)
}

parameters1 = merge_dicts(vectorizer_parameters, parameters1)

parameters2 = {
    'logistic_regression__C': param_set(0.1, 5.0, 0.2),
    'logistic_regression__penalty' : ('l2', 'l1'),
    'logistic_regression__fit_intercept' : param_set(0, 1, 1),
    'logistic_regression__intercept_scaling' : param_set(0.1, 5.0, 0.2)
}

parameters2 = merge_dicts(vectorizer_parameters, parameters2)

parameters3 = {
    'svd__n_components': param_set(5, 300, 10),
    'sgd__loss': ('hinge', 'log', 'modified_huber', 'squared_hinge', 
        'perceptron', 'squared_loss', 'huber', 'epsilon_insensitive', 'squared_epsilon_insensitive'),
    'sgd__penalty': ('elasticnet', 'l2', 'l1'),
    'sgd__class_weight': ('balanced' , None)
}

parameters3 = merge_dicts(vectorizer_parameters, parameters3)

#parameters4 = {
#    'mnnb__alpha': param_set(0.0, 5.0, 0.5)
#}

#parameters4 = merge_dicts(vectorizer_parameters, parameters4)

parameters4 = {
    'mnnb__alpha': (2.0,),
	'vectorizer__analyzer': ('char',),
	'vectorizer__max_df': (1050,),
	'vectorizer__min_df':(98,0),
	'vectorizer__ngram_range' :((1,7),),
	'vectorizer__smooth_idf': (0,),
	'vectorizer__sublinear_tf' : (0,),
	'vectorizer__use_idf' : (0,)
}

# run grid search
def perform_hyper_search_and_return_best(pipeline, X, Y, parameters):
    grid_search = GridSearchCV(pipeline, param_grid=parameters, n_jobs=-1, verbose=10, cv=5, error_score=0.0)
    start = time()
    grid_search.fit(X, Y)
    print("GridSearchCV took %.2f seconds"
          " parameter settings." % ((time() - start)))
    print("Best estimator:")
    print(grid_search.best_estimator_)
    print()
    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))
    return grid_search.best_estimator_

print("paso 1")
estimator4 = perform_hyper_search_and_return_best(pipeline4, tweets, Y, parameters4)
print("paso 2")
estimator3 = perform_hyper_search_and_return_best(pipeline3, tweets, Y, parameters3)
print("paso 3")
estimator1 = perform_hyper_search_and_return_best(pipeline2, tweets, Y, parameters2)
print("paso 4")
estimator2 = perform_hyper_search_and_return_best(pipeline1, tweets, Y, parameters1)
print("paso 5")

ensamble = VotingClassifier(estimators=[('est1', estimator1), ('est2', estimator2), ('est3', estimator3), ('est4', estimator4)], weights=[0.3, 0.3, 0.2, 0.1])
print("paso 6")
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score

# evaluate using 10-fold cross validation
kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=3)
results = cross_val_score(ensamble, tweets, Y, cv=kfold)
print(results.mean())

print("Persisting model to disk...")
from sklearn.externals import joblib
import time
timestr = time.strftime("%Y%m%d-%H%M%S")
joblib.dump(ensamble, timestr + '.gz', compress=('gzip', 3))
joblib.dump(le_sentiments, timestr + '-encoder.gz', compress=('gzip', 3))

ensamble = joblib.load(timestr + '.gz')
print(ensamble)
