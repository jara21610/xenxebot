import os, io, base64, json
from flask import Flask, request, redirect, url_for
from werkzeug.utils import secure_filename

import numpy as np
from sklearn.externals import joblib

from ner1 import process_entities_tweet

sentiment_clf = joblib.load('./sentiment/model.gz')
sentiment_decoder = joblib.load('./sentiment/encoder.gz')

#topic = joblib.load('./topic/model.gz')
#sentiment_decoder = joblib.load('./topic/encoder.gz')

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        if 'tweet' in request.json:
            tweet = request.json['tweet']
            entities = process_entities_tweet(tweet)
            sentiment = sentiment_clf.predict([tweet])
            sentiment_decoded = sentiment_decoder.inverse_transform(sentiment)
            return json.dumps({'entities':entities, 'sentiment': sentiment_decoded.tolist()[0]})
            return json.dumps({'entities':entities})
        else:
            return json.dumps({'message': 'No valid tweet was found in field "tweet"'})
    else:
        return json.dumps({'default': 'send tweet thru POST with ID "tweet"'})

@app.route('/topic', methods=['GET', 'POST'])
def todo():
    pass

if __name__ == "__main__":
    app.run('0.0.0.0', port=8182)
