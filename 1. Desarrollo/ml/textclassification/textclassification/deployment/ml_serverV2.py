import os, io, base64, json
from flask import Flask, request, redirect, url_for
from werkzeug.utils import secure_filename

import numpy as np
from sklearn.externals import joblib

from ner1 import process_entities_tweet
from textprocessV2 import ml_process

from flask import json



sentiment_clf = joblib.load('./sentiment/model.gz')
sentiment_decoder = joblib.load('./sentiment/encoder.gz')

#topic = joblib.load('./topic/model.gz')
#sentiment_decoder = joblib.load('./topic/encoder.gz')

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        if 'tweet' in request.json:
            tweet = request.json['tweet']              
            return json.dumps(ml_process(tweet,'PreXtige'))
        else:
            return json.dumps({'message': 'No valid tweet was found in field "tweet"'})
    else:
        return json.dumps({'default': 'send tweet thru POST with ID "tweet"'})

if __name__ == "__main__":
    app.run('0.0.0.0', port=8183)
