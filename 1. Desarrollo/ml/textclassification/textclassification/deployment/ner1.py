import os, io, base64, json
import nltk
from nltk import word_tokenize

# ESTO DEBE DESCOMENTARSE AL MENOS UNA VEZ!!!!!!!!!!!!!!!!!!!!!!!!!
#nltk.download("conll2002")
#nltk.download("punkt")
#nltk.download('perluniprops')
#nltk.download('averaged_perceptron_tagger')
#nltk.download('maxent_ne_chunker')
#nltk.download('words')
#nltk.download('averaged_perceptron_tagger')
#from polyglot.downloader import downloader
#downloader.download("embeddings2.es")
#downloader.download("ner2.es")
#downloader.download("sentiment2.es")

from polyglot.text import Text

def extract_entities_model(tweet):
    text = Text(tweet,"es")
    e = list()
    for entity in text.entities: 
        print (entity)
        sentiment = ''
        if entity.positive_sentiment > entity.positive_sentiment:
           sentiment = 'P'
        else: 
           sentiment = 'N'
        print (sentiment)
        entitytext = ''
        for enttext in entity: 
            entitytext = entitytext + ' ' + enttext
        x = entidad(entitytext, sentiment)        
        print(json.dumps(x.dump()))
        e.append(x)        
    for item in e:
        print(json.dumps(item.dump()))
    return e

def process_entities_tweet(tweet):
    return extract_entities_model(tweet)

class entidad:

    def __init__(self, name, sentiment):
        self.name = name
        self.sentiment = sentiment
    
    def dump(self):
        return {"entidad": {'name': self.name,
                            'sentiment': self.sentiment}}
