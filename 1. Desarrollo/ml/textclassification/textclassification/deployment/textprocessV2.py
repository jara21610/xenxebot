import os, io, base64, json
import nltk
from nltk import word_tokenize

# ESTO DEBE DESCOMENTARSE AL MENOS UNA VEZ!!!!!!!!!!!!!!!!!!!!!!!!!
nltk.download("conll2002")
nltk.download("punkt")
nltk.download('perluniprops')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('averaged_perceptron_tagger')
from polyglot.downloader import downloader
downloader.download("embeddings2.es")
downloader.download("embeddings2.en")
downloader.download("pos2.en")
downloader.download("pos2.es")
downloader.download("ner2.es")
downloader.download("sentiment2.es")

from polyglot.text import Text
from werkzeug.utils import secure_filename

import numpy as np
from time import time
from sklearn.externals import joblib
sentiment_clf = joblib.load('./sentiment/model.gz')
sentiment_decoder = joblib.load('./sentiment/encoder.gz')
#import nltk
#from nltk.tag.stanford import StanfordTagger
#from nltk.tag.stanford import StanfordPOSTagger

def extract_entities_model(tweet):
    sent1 = tweet.split(" signopunto 	")
    e = list()    
    for s1 in sent1:
        if len(s1) > 0:
            print ("Sentencia = " + s1 + " size = " + str(len(s1)))
            sent2 = s1.split("signocoma")
            for s2 in sent2:
                print ("Sentencia = " + s2)
                text = Text(s2,"es")    
                sentiment = extract_sentiment_text(s2)
                for entity in text.entities: 
                    print (entity)
                    #sentiment = ''
                    #print (entity.positive_sentiment)
                    #print (entity.negative_sentiment)
                    #if entity.positive_sentiment > entity.negative_sentiment:
                        #sentiment = 'P'
                    #else: 
                        #sentiment = 'N'
                    #print (sentiment)                    
                    entitytext = ''
                    for enttext in entity: 
                        enttext.strip()
                        if (enttext != 'signopunto' and enttext != 'signointerogacion' and  enttext != 'signoadmiracion' and  enttext != 'signopuntossuspensivos' and  enttext != 'signopuntoycoma' and 'arrow_right' not in enttext and '?' not in enttext):
                            entitytext = entitytext + ' ' + enttext
                            x = concept(entitytext, sentiment, 'ENTIDAD')
                            print(json.dumps(x.dump()))
                            e.append(x)        
    for item in e:
        print(json.dumps(item.dump()))
    return e

def process_entities_tweet(tweet):
    return extract_entities_model(tweet)

def extract_sentiment_text(tweet):
    #text = Text(tweet,"es")
    #if text.polarity == 0:
    #       sentiment = 'NEU'
    #elif text.polarity == 1:
    #       sentiment = 'P'
    #else: 
    #       sentiment = 'N'
    #return sentiment
    tiempo_inicial = time()     
    sentiment = sentiment_clf.predict([tweet])	
    sentiment_decoded = sentiment_decoder.inverse_transform(sentiment)
    print(sentiment_decoded.tolist()[0])
    tiempo_final = time()
    tiempo_ejecucion = tiempo_final - tiempo_inicial
    print(tiempo_ejecucion)
    return sentiment_decoded.tolist()[0]

def extract_category_text(tweet):
    #category_clf = joblib.load('./category/model.gz')
    #category_decoder = joblib.load('./category/encoder.gz')
    #category = category_clf.predict([tweet])
    #category_decoded = sentiment_decoder.inverse_transform(sentiment)
    #print(category_decoded.tolist()[0])
    #return category_decoded.tolist()[0]
    return "FUTBOL"

def extract_topic_text(tweet):
    sent1 = tweet.split(" signopunto ")
    e = list()    
    for s1 in sent1:
        if len(s1) > 0:
            print (s1)
            sent2 = s1.split("signocoma")
            for s2 in sent2:
                print (s2)
                text = Text(s2,"es")
                topic = ''
                for postag in text.pos_tags: 
                    print(postag)
                    if (postag[1]) == 'NOUN':
                        try:
                            topic = topic + str(postag[0] + ' ')
                            print ("encontre noun")
                        except Exception as ex: 
                            print(ex)
                    elif (postag[1]) == 'ADJ':
                        try:
                            if topic:
                                topic = topic + str(postag[0] + ' ')
                                print ("adj")
                            else:                                
                                if topic:
                                    print (topic)
                                    if (topic != 'signointerrogacion' and  topic != 'signoadmiracion' and  topic != 'signopuntossuspensivos' and  topic != 'signopuntoycoma'):
                                      x = concept(topic, extract_sentiment_text(s2), 'TOPICO')
                                      print(json.dumps(x.dump()))
                                      e.append(x)        
                                      topic = ''
                        except Exception as ex: 
                            print(ex)
                    else:
                        try:
                            if topic:
                                print (topic)
                                if (topic != 'signointerrogacion' and  topic != 'signoadmiracion' and  topic != 'signopuntossuspensivos' and  topic != 'signopuntoycoma'):
                                  x = concept(topic, extract_sentiment_text(s2), 'TOPICO')
                                  print(json.dumps(x.dump()))
                                  e.append(x)   
                                  topic = ''
                        except Exception as ex: 
                            print(ex)
    return e
    #spanish_postagger =  StanfordPOSTagger('models/spanish.tagger', 'stanford-postagger.jar')
    #spanish_postagger.tag('esta es una oracion de prueba'.split())

def ml_process(tweet, servicetype):
    print (tweet)
    mr = mlresponse(tweet, servicetype)
    return mr.dump()

class concept:

    def __init__(self, name, sentiment, type):
        self.name = name
        self.sentiment = sentiment
        self.type = type
    
    def dump(self):
        return {'name': self.name,
                'sentiment': self.sentiment,
                'type': self.type}

class mlresponse:

    def __init__(self, tweet, servicetype):
        self.sentiment = extract_sentiment_text(tweet)
        self.category = ''
        self.topics = list()
        self.entities = list()
        if servicetype == 'PreXtige':
            self.category = extract_category_text(tweet)
            self.entities = extract_entities_model(tweet)
            self.topics = extract_topic_text(tweet)
            self.entities.extend(self.topics)
    
    def dump(self):
        return {'sentiment': self.sentiment,
                'category': self.category,
                'conceptlist' : [item.dump() for item in self.entities]}
