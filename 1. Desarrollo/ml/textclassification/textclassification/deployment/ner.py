import nltk
from nltk import word_tokenize

# ESTO DEBE DESCOMENTARSE AL MENOS UNA VEZ!!!!!!!!!!!!!!!!!!!!!!!!!
nltk.download("conll2002")
nltk.download("punkt")
nltk.download('perluniprops')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('averaged_perceptron_tagger')
from polyglot.downloader import downloader
downloader.download("embeddings2.es")
downloader.download("ner2.es")
downloader.download("sentiment2.es")

from polyglot.text import Text

def extract_entities_simple(tweet):
    try:
        response = []
        if "@" in tweet:
            words = word_tokenize(tweet, language='spanish')
            add = False
            for word in words:
                if add:
                    response.append(word)
                    add = False
                if word == "@":
                    add = True
            return {"I-ACCOUNT": response}
            # words = tweet.split(" ")
            # for word in words:
            #     if "@" in word:
            #         response += [a for a in word.split("@") if a != ""]
        return response
    except Exception as e:
        return {}

def transform_collection(collection, entity):
    if len(collection) == 1:
        return {"entity": collection[0], "P" : entity.positive_sentiment, "N": entity.negative_sentiment }
    else:
        return {"entity": ' '.join(collection), "P" : entity.positive_sentiment, "N": entity.negative_sentiment }

def extract_entities_model(tweet):
    response = {}
    try:
        text = Text(tweet)
        for entity in text.entities:
            if entity.tag in response:
                temp = response[entity.tag]
                if transform_collection(entity._collection, entity) not in temp:
                    temp.append(transform_collection(entity._collection, entity))
                    response[entity.tag] = temp
            else:
                response[entity.tag] = []
        return response
    except Exception as e:
        return response

def process_entities_tweet(tweet):
    return { extract_entities_simple(tweet), extract_entities_model(tweet)}
