import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';
import ChoiceRegister from './components/ChoiceRegister/ChoiceRegister';
import Register from './components/Register/Register';
import Welcome from './components/Welcome/Welcome';
import SelectPlan from './components/SelectPlan/SelectPlan';
import PaymentMethod from './components/PaymentMethod/PaymentMethod';
import PrivatePolicy from './components/PrivatePolicy/PrivatePolicy';
import Fbloguin from './components/Fbloguin/Fbloguin';
import SelectPlanPrextige from './components/SelectPlan/SelectPlanPrextige';



const Routes = () => ( <
    BrowserRouter basename = {
        process.env.REACT_APP_ROUTER_BASE || ''
    } >
    <Switch>
    <
    Route exact path = "/"
    component = {
        ChoiceRegister
    }
    /> <
    Route path = "/register"
    component = {
        Register
    }
    />     <
    Route path = "/welcome"
    component = {
        Welcome
    }
    /> <
    Route path = "/selectplan"
    component = {
        SelectPlan
    }
    /><
    Route path = "/prextigeselectplan"
    component = {
        SelectPlanPrextige
    }
    /> <
    Route path = "/paymentmethod"
    component = {
        PaymentMethod
    }
    /> <
    Route path = "/privatepolicy"
    component = {
        PrivatePolicy
    }
	/>
	<
    Route path = "/fbloguin"
    component = {
        Fbloguin
    }
    /> 
	</Switch> 
	</BrowserRouter>
);
export default Routes;