import React, {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {Redirect} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container,Row,Col,Button} from 'reactstrap'
import logoxenxe from '../../images/xenxebot-blanco-color-logo.png'
import logoprextige from '../../images/prextigebot-blanco-color.png'
import {GetTermsandConditionsPost} from '../../services/GetTermsandConditionsPost';
import '../../index.css';


class ChoiceRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginError: false,
            redirect: false
        };
        this.signup = this.signup.bind(this);
		sessionStorage.setItem("servicetype", this.props.location.search.replace('?servicetype=', ''));
        
			let postData = {
                        productid : sessionStorage.getItem('servicetype'),
                    };
            GetTermsandConditionsPost(postData).then((result) => {
                console.log(result);

                sessionStorage.setItem("license", JSON.stringify(result));
            });
        
    }

    responseRegistro() {
        console.log("response console");
        console.log("response");
        this.signup("", 'registro');
    }

    signup(res, type) {
        let postData;
        if (type === 'registro') {
            postData = {
                name: '',
                lastname: '',
                provider: '',
                email: '',
                provider_id: '',
                token: '',
            };
        } else {
            if (type === 'facebook' && res.email) {
                postData = {
                    name: res.first_name,
                    lastname: res.last_name,
                    provider: type,
                    email: res.email,
                    provider_id: res.id,
                    token: res.accessToken,
                };
            }

            if (type === 'google' && res.w3.U3) {
                postData = {
                    name: res.w3.ofa,
                    lastname: res.w3.wea,
                    provider: type,
                    email: res.w3.U3,
                    provider_id: res.El,
                    token: res.Zi.access_token,
                    provider_pic: res.w3.Paa
                };
            }

        }

        if (postData) {
            sessionStorage.setItem("userData", JSON.stringify(postData));
            this.setState({
                redirect: true
            });
        }
    }

    render() {
		
		let logo;

		if (sessionStorage.getItem('servicetype') == process.env.REACT_APP_PRODUCTID_XENXEBOT){
			logo = <img width="358" src={logoxenxe}/>
		} else {
			logo = <img width="358" src={logoprextige}/>
		}

        if (this.state.redirect) {
            return ( < Redirect to = {
                    '/register'
                }
                />)
            }

            const responseFacebook = (response) => {
                console.log("facebook console");
                console.log(response);
                this.signup(response, 'facebook');
            }

            const responseGoogle = (response) => {
                console.log("google console");
                console.log(response);
                this.signup(response, 'google');
            }



            return ( <Container >
                <Row>
                <Col >
                <br / >
                <br / >
                <br / >
                <br / >
                <br / >
                <br / >
				<br / >
				<br / >                
                </Col> 
				</Row> 				
                <Row >
				<Col xs="12" md="4">
				</Col>
                <Col xs="12" md="4"> 
				{logo}
				</Col>
				<Col xs="12" md="4"> 				
				</Col>
                </Row> 
				<Row >
                <Col xs="12" md="4"> 
				< GoogleLogin clientId = "312321685668-utoijsi13llc02dockntuav5bcjrvmnc.apps.googleusercontent.com" onSuccess = {responseGoogle} onFailure = {responseGoogle} render={renderProps => (
					<Button color = "google" size = "md" block onClick = {renderProps.onClick} > Registrarse con Google < /Button> 
				)} / >
                </Col> 
				<Col xs="12" md="4"> 
				< FacebookLogin appId = "617902555240032" autoLoad = {false} textButton = "Registrarse con Facebook" fields = "first_name,last_name,email" callback = {responseFacebook} cssClass = "btn btn-facebook btn-md btn-block" / >
                </Col> 
				<Col xs="12" md="4">
                <Button color = "register" size = "md" block onClick = {this.responseRegistro.bind(this)} > Registrarse < /Button> 
				</Col> 
				</Row> 				
				</Container>
            );
        }
    }
    export default ChoiceRegister;