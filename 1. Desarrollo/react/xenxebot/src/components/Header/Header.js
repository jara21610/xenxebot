import React, {
    Component
} from 'react';
import logo from '../../images/zien-tech-oscuro-e1526986190419.jpg'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return ( <
            div >
            <
            Navbar color = "dark"
            dark expand = "lg" >
            <
            NavbarBrand href = "/" > < img src = {
                logo
            }
            /></NavbarBrand >
            <
            NavbarToggler onClick = {
                this.toggle
            }
            />           <
            /Navbar> <
            /div>
        );
    }
}

export default Header;