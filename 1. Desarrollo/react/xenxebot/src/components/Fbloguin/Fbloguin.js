import React, {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {Redirect} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container,Row,Col,Button} from 'reactstrap'
import logo from '../../images/xenxebot-blanco-color-logo.png'
import {GetTermsandConditionsPost} from '../../services/GetTermsandConditionsPost';
import '../../index.css';
import Header from '../../components/Header/Header';
import {
    SocialLoguinFacebookPost
} from '../../services/SocialLoguinFacebookPost';


class Fbloguin extends Component {
        constructor(props) {
        super(props);
		
		if (!sessionStorage.getItem('redirectfbloguin')) {
			const search = this.props.location.search;
			const params = new URLSearchParams(search);
			sessionStorage.setItem("usertoken", params.get('usertoken'));
			sessionStorage.setItem("servicetype", params.get('servicetype'));
			sessionStorage.setItem("redirectfbloguin", 'usertokensaved');
		} 
        this.signup = this.signup.bind(this);        
    }

    signup(res, type) {
        let postData;
        
            if (type === 'facebook' && res.email) {
                postData = {
					usertoken:  sessionStorage.getItem('usertoken'),
                    token: res.accessToken,
					servicetype: sessionStorage.getItem('servicetype')
                };
            }            

        if (postData) {
				SocialLoguinFacebookPost(postData).then((result) => {
                        console.log(result);                       
                            sessionStorage.clear();
							alert("En unos minutos te informaremos el resultado de la transacción a traves del chat bot")
							var win = window.open(process.env.REACT_APP_CHATBOTURL, "_self");                       
                    });
        }
    }

    render() {				
		
			if (sessionStorage.getItem('redirectfbloguin') == 'usertokensaved'){
				sessionStorage.setItem("redirectfbloguin", 'false');
                return ( < Redirect to = {
                        '/fbloguin'
                    }
                    />)
            }  else {
				const responseFacebook = (response) => {
					console.log("facebook console");
					console.log(response);
					this.signup(response, 'facebook');
				}
				
				return ( <div >
					 <Header >
					</Header> 
					<Container >
					<Row>
					<Col >
					<br / >
					<br / >
					<br / >
					<br / >
					<br / >
					<br / >
					<br / >
					<br / >                
					</Col> 
					</Row> 				
					<Row >
					<Col > 				
					</Col> 
					<Col > 
					< FacebookLogin appId = "617902555240032" autoLoad = {false} textButton = "Haz click Aquí" fields = "first_name,last_name,email" callback = {responseFacebook} cssClass = "btn btn-facebook btn-md btn-block" redirectUri={process.env.REACT_APP_FLOGUIN_URI}/ >
					</Col> 
					<Col >                
					</Col> 
					</Row> 				
					</Container>
					</div>
				);
			}

				
	}	
}

    export default Fbloguin;