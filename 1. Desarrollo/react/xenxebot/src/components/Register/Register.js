import React, {
    Component
} from 'react';
import {
    Fragment
} from 'react';
import {
    Redirect,
    Link
} from 'react-router-dom';
import {
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormFeedback,
    FormText
} from 'reactstrap'
import {
    RegisterUserPost
} from '../../services/RegisterUserPost';
import Header from '../../components/Header/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import IntlTelInput from 'react-bootstrap-intl-tel-input'
import validator from 'validator';

class Register extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            lastname: '',
            email: '',
            mobilphone: '',
            license: '',
            redirect: false,
            modal: false,
            redirecttowelcome: false,
            redirecttochoice: false,
            emailisinvalid: false
        };
        this.toggle = this.toggle.bind(this);
        this.handlemobilphoneChange = this.handlemobilphoneChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLastnameChange = this.handleLastnameChange.bind(this);
        this.handleemailChange = this.handleemailChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleNameChange(event) {
        this.setState({
            name: event.target.value
        });
    }

    handleLastnameChange(event) {
        this.setState({
            lastname: event.target.value
        });
    }

    handlemobilphoneChange(event) {
        this.setState({
            mobilphone: event.target.value
        });
    }

    handleemailChange(event) {
        this.setState({
            email: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.state.mobilphone == "") {
            alert("El campo celular no debe estar vacio");
        } else {

            if (this.state.mobilphone.valid == false) {
                alert("El campo celular no es valido");
            } else {
                var r = window.confirm("Esta seguro que los datos ingresados son correctos?");
                if (r == true) {
                    let postData = {
                        name: this.state.name,
                        lastname: this.state.lastname,
                        email: this.state.email,
                        mobilphone: this.state.mobilphone.intlPhoneNumber.slice(1).split(' ').join(''),
						servicetype: sessionStorage.getItem('servicetype'),
                    };
                    RegisterUserPost(postData).then((result) => {
                        console.log(result);
                        if (result.code == 0) {
                            this.setState({
                                redirecttowelcome: true
                            });
                        } else {
                            alert("Ya existe una cuenta creada con tu datos");
                            this.setState({
                                redirecttochoice: true
                            });
                        }
                    });
                }
            }


        }




    }

    componentDidMount() {
        let userData = JSON.parse(sessionStorage.getItem('userData'));
        let license = JSON.parse(sessionStorage.getItem('license'));
        let licencetext = license.license;
        this.setState({
            name: userData.name,
            lastname: userData.lastname,
            email: userData.email,
            license: licencetext
        })

    }
    render() {

            const emailisvalid = this.state.emailisvalid;

            if (!sessionStorage.getItem('userData') || this.state.redirect) {
                return ( < Redirect to = {
                        '/'
                    }
                    />)
                }

                if (this.state.redirecttowelcome) {
                    return ( < Redirect to = {
                            '/welcome'
                        }
                        />)
                    }
                    if (this.state.redirecttochoice) {
                        return ( < Redirect to = {
                                '/?servicetype=' + sessionStorage.getItem('servicetype')
                            }
                            />)
                        }
						
			let productname = '';
		
			if (sessionStorage.getItem('servicetype') == process.env.REACT_APP_PRODUCTID_XENXEBOT){
				productname = process.env.REACT_APP_PRODUCTNAME_XENXEBOT				
			} else {
				productname = process.env.REACT_APP_PRODUCTNAME_PREXTIGEBOT				
			}

                        return ( <
                            div >
                            <
                            Header >
                            <
                            /Header> <
                            Container >
                            <
                            Row >
                            <
                            Col >
                            <
                            br / >
                            <
                            br / >
                            <
                            br / >
                            <
                            br / >
                            <
                            br / >
                            <
                            /Col> <
                            /Row> <
                            Row >
                            <
                            Col xs="1" md="3" >
                            <
                            /Col> <
                            Col xs="10" md="6">
                            <
                            Form onSubmit = {
                                this.handleSubmit
                            } >
                            <
                            FormGroup >
                            <
                            Label
                            for = "name" > Nombre < /Label> <
                            Input name = "name"
                            id = "name"
                            value = {
                                this.state.name
                            }
                            onChange = {
                                this.handleNameChange
                            }
                            required / >
                            <
                            /FormGroup>         <
                            FormGroup >
                            <
                            Label
                            for = "lastname" > Apellidos < /Label> <
                            Input name = "lastname"
                            id = "lastname"
                            value = {
                                this.state.lastname
                            }
                            onChange = {
                                this.handleLastnameChange
                            }
                            required / >
                            <
                            /FormGroup>         <
                            FormGroup >
                            <
                            Label
                            for = "email" > Email < /Label> <
                            Input type = "email"
                            name = "email"
                            id = "email"
                            value = {
                                this.state.email
                            }
                            onChange = {
                                this.handleemailChange
                            }
                            required / >
                            <
                            /FormGroup>         <
                            FormGroup >
                            <
                            Label
                            for = "mobilphone" > Celular (Primero selecciona la bandera de tu país y luego digita tu número de celular)< /Label>                           <
                            IntlTelInput preferredCountries = {
                                ['CO']
                            }
                            onChange = {
                                mobilphone => this.setState({
                                    mobilphone
                                })
                            }
                            />                           <
                            /FormGroup>  <
                            FormGroup check >
                            <
                            Label check >
                            <
                            Input type = "checkbox"
                            required / > {
                                ' '
                            }
                            Acepto los términos y condiciones del < Button color = "link"
                            onClick = {
                                this.toggle
                            } > Acuerdo para Clientes de {productname} < /Button>         <
                            /Label> <
                            /FormGroup> <
                            br / >
                            <
                            br / >
                            <
                            Button > Submit < /Button> <
                            /Form>                 <
                            /Col> <
                            Col xs="1" md="3">
                            <
                            /Col> <
                            /Row> <
                            Modal isOpen = {
                                this.state.modal
                            }
                            toggle = {
                                this.toggle
                            }
                            centered = 'true'
                            size = 'lg' >
                            <
                            ModalHeader toggle = {
                                this.toggle
                            } > Terminos y Condiciones del Servicio < /ModalHeader> <
                            ModalBody >
                            <
                            div dangerouslySetInnerHTML = {
                                {
                                    __html: this.state.license
                                }
                            }
                            />                             <
                            /ModalBody> <
                            ModalFooter >
                            <
                            Button color = "secondary"
                            onClick = {
                                this.toggle
                            } > Regresar < /Button> <
                            /ModalFooter> <
                            /Modal> <
                            /Container> <
                            /div>
                        );
                    }
                }
                export default Register;