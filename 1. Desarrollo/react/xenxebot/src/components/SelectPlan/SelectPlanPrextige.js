import React, {
    Component
} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {
    Redirect
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardText,
    CardImg,
    Button,
    Table,
    CardHeader,
    CardFooter,
    CardBody,
    Form,
    Input
} from 'reactstrap'
import Header from '../../components/Header/Header';


class SelectPlanPrextige extends Component {
    constructor(props) {
        super(props);
        this.state = {
            plan: "",
            usertoken: this.props.location.search.replace('?usertoken=', ''),
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        console.log(this.props);
    }

    handleSubmit(event) {
        event.preventDefault();


    }

    render() {
        if (this.state.plan != '') {
            sessionStorage.setItem("plan", this.state.plan);
            sessionStorage.setItem("usertoken", this.state.usertoken);
            return ( < Redirect to = {
                    '/paymentmethod'
                }
                />)
            }



            return ( <
                div >
                <
                Header >
                <
                /Header>

                <
                Row >
                <
                Col >
                <
                br / >
                <
                br / >
                <
                br / >
                <
                /Col>         <
                /Row> <
                Row >
                <
                Col >
                <
                Card outline color = "primary" >
                <
                CardHeader > < CardTitle > Startup < /CardTitle></CardHeader >
                <
                CardBody >
                <
                div >
                <
                h1 >
                $59900 < span > /MES</span > < /h1>                <
                /div> <
                Table striped >
                <
                tbody >
                <
                tr >
                <
                td > 3000 Transacciones < /td>             <
                /tr> <
                tr >
                <
                td > Analisis de Sentimiento < /td>            <
                /tr> <
                tr >
                <
                td > Consultas en Twitter < /td>             <
                /tr> <
                tr >
                <
                td > Reportes < /td>             <
                /tr> <
                /tbody> <
                /Table>

                <
                /CardBody> <
                CardFooter > < Form onSubmit = {
                    () => this.setState({
                        plan: "STARTUP"
                    })
                } > < Button color = "primary" > Comprar < /Button></Form > < /CardFooter>                       <
                /Card> <
                /Col> <
                Col >
                <
                Card outline color = "primary" >
                <
                CardHeader > < CardTitle > Profesional < /CardTitle></CardHeader >
                <
                CardBody >
                <
                div >
                <
                h1 >
                $99900 < span > /MES</span > < /h1>                <
                /div> <
                Table striped >
                <
                tbody >
                <
                tr >
                <
                td > 10000 Transacciones < /td>             <
                /tr> <
                tr >
                <
                td > Analisis de Sentimiento < /td>            <
                /tr> <
                tr >
                <
                td > Consultas en Twitter < /td>             <
                /tr> <
                tr >
                <
                td > Reportes < /td>             <
                /tr> <
                /tbody> <
                /Table>

                <
                /CardBody> <
                CardFooter > < Form onSubmit = {
                    () => this.setState({
                        plan: "PROFESIONAL"
                    })
                } > < Button color = "primary" > Comprar < /Button></Form > < /CardFooter>                      <
                /Card>       <
                /Col> <
                Col >
                <
                Card outline color = "primary" >
                <
                CardHeader > < CardTitle > Negocio < /CardTitle></CardHeader >
                <
                CardBody >
                <
                div >
                <
                h1 >
                $349900 < span > /MES</span > < /h1>                <
                /div> <
                Table striped >
                <
                tbody >
                <
                tr >
                <
                td > 50000 Transacciones < /td>             <
                /tr> <
                tr >
                <
                td > Analisis de Sentimiento < /td>            <
                /tr> <
                tr >
                <
                td > Consultas en Twitter < /td>             <
                /tr> <
                tr >
                <
                td > Reportes < /td>             <
                /tr> <
                /tbody> <
                /Table>

                <
                /CardBody> <
                CardFooter > < Form onSubmit = {
                    () => this.setState({
                        plan: "BUSSINESS"
                    })
                } > < Button color = "primary" > Comprar < /Button></Form > < /CardFooter>             <
                /Card>       <
                /Col> <
                Col >
                <
                Card outline color = "primary" >
                <
                CardHeader > < CardTitle > Empresarial < /CardTitle></CardHeader >
                <
                CardBody >
                <
                div >
                <
                h1 >
                $649900 / MES < /h1>                <
                /div> <
                Table striped >
                <
                tbody >
                <
                tr >
                <
                td > 50000 Transacciones < /td>             <
                /tr> <
                tr >
                <
                td > Analisis de Sentimiento < /td>            <
                /tr> <
                tr >
                <
                td > Consultas en Twitter < /td>             <
                /tr> <
                tr >
                <
                td > Reportes < /td>             <
                /tr> <
                /tbody> <
                /Table>

                <
                /CardBody> <
                CardFooter > < Form onSubmit = {
                    () => this.setState({
                        plan: "ENTERPRISE"
                    })
                } > < Button color = "primary" > Comprar < /Button></Form > < /CardFooter> <
                /Card>       <
                /Col> <
                /Row>         <
                /div>
            );
        }
    }
    export default SelectPlanPrextige;


    // WEBPACK FOOTER //
    // ./src/components/SelectPlanPrextige/SelectPlanPrextige.js