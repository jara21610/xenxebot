import React, {
    Component
} from 'react';
import {
    Fragment
} from 'react';
import {
    Redirect,
    Link
} from 'react-router-dom';
import {
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormFeedback,
    FormText
} from 'reactstrap'
import {
    UpgradePlanWithPaymentMethod
} from '../../services/UpgradePlanWithPaymentMethod';
import Header from '../../components/Header/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import CreditCardInput from 'react-credit-card-input';
import validator from 'validator';

class PaymentMethod extends Component {

    constructor(props) {
        super(props);

        this.state = {
            idtype: 'CEDULA',
            idnumber: '',
            cardname: '',
            cardnumber: '',
            expiry: '',
            cvc: ''
        };

        if (!sessionStorage.getItem('usertoken')) {
            sessionStorage.setItem("usertoken", this.props.location.search.replace('?usertoken=', ''));
        }
        this.handleCardCVCChange = this.handleCardCVCChange.bind(this);
        this.handleCardNumberChange = this.handleCardNumberChange.bind(this);
        this.handleCardExpiryChange = this.handleCardExpiryChange.bind(this);
        this.handleCardNameChange = this.handleCardNameChange.bind(this);
        this.handleIdNumberChange = this.handleIdNumberChange.bind(this);
        this.handleIdTypeChange = this.handleIdTypeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCardNumberChange(event) {
        this.setState({
            cardnumber: event.target.value
        });
    }

    handleCardExpiryChange(event) {
        this.setState({
            expiry: event.target.value
        });
    }

    handleCardCVCChange(event) {
        this.setState({
            cvc: event.target.value
        });
    }

    handleCardNameChange(event) {
        this.setState({
            cardname: event.target.value
        });
    }
    handleIdNumberChange(event) {
        this.setState({
            idnumber: event.target.value
        });
    }
    handleIdTypeChange(event) {
        this.setState({
            idtype: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.cardnumber == "") {
            alert("Debes ingresar los datos de tu tarjeta de crédito");
        } else {
            let ed = this.state.expiry;
            let fields = ed.split("/")

            ed = '20' + fields[1].replace(" ", "") + '/' + fields[0].replace(" ", "")

            let postData = {
                plan: sessionStorage.getItem("plan"),
                usertoken: sessionStorage.getItem("usertoken"),
                idtype: this.state.idtype,
                identificationNumber: this.state.idnumber,
                name: this.state.cardname,
                number: this.state.cardnumber.trim(),
                expirationDate: ed,
                cvc: this.state.cvc,
            };
            UpgradePlanWithPaymentMethod(postData).then((result) => {
                console.log(result);
                if (result.code == 0) {
                    sessionStorage.clear();
                    alert("En unos minutos te informaremos el resultado de la transacción a traves del chat bot")
                    var win = window.open(process.env.REACT_APP_CHATBOTURL, "_self");
                } else {
                    alert(result.message);
                }
            });
        }
    }

    componentDidMount() {

    }
    render() {

        return ( <
            div >
            <
            Header >
            <
            /Header> <
            Container >
            <
            Row >
            <
            Col >
            <
            br / >
            <
            br / >
            <
            br / >
            <
            br / >
            <
            br / >
            <
            /Col> <
            /Row> <
            Row >
            <
            Col >
            <
            Form onSubmit = {
                this.handleSubmit
            } >
            <
            FormGroup >
            <
            Label
            for = "idtype" > Tipo de Identificacion < /Label> <
            Input type = "select"
            name = "idtype"
            id = "idtype"
            onClick = {
                this.handleIdTypeChange
            } >
            <
            option value = "CEDULA" > CEDULA < /option> <
            option value = "TARJETADEIDENTIDAD" > TARJETADEIDENTIDAD < /option> <
            option value = "PASAPORTE" > PASAPORTE < /option> <
            option value = "CEDULAEXTRANJERIA" > CEDULAEXTRANJERIA < /option>                             <
            /Input> <
            /FormGroup> <
            FormGroup >
            <
            Label
            for = "idnumber" > Numero de Identificacion < /Label> <
            Input name = "idnumber"
            id = "idnumber"
            value = {
                this.state.idnumber
            }
            onChange = {
                this.handleIdNumberChange
            }
            required / >
            <
            /FormGroup>         <
            FormGroup >
            <
            Label
            for = "cardname" > Nombre en la tarjeta < /Label> <
            Input name = "cardname"
            id = "cardname"
            value = {
                this.state.cardname
            }
            onChange = {
                this.handleCardNameChange
            }
            required / >
            <
            /FormGroup>                                      <
            FormGroup >
            <
            Label
            for = "mobilphone" > Tarjeta de Credito < /Label>                           <
            CreditCardInput cardNumberInputProps = {
                {
                    value: this.state.cardNumber,
                    onChange: this.handleCardNumberChange
                }
            }
            cardExpiryInputProps = {
                {
                    value: this.state.expiry,
                    onChange: this.handleCardExpiryChange
                }
            }
            cardCVCInputProps = {
                {
                    value: this.state.cvc,
                    onChange: this.handleCardCVCChange
                }
            }
            fieldClassName = "input" /
            >
            <
            /FormGroup>                          <
            br / >
            <
            br / >
            <
            Button > Submit < /Button> <
            /Form>                 <
            /Col>        <
            /Row>          <
            /Container> <
            /div>
        );
    }
}
export default PaymentMethod;