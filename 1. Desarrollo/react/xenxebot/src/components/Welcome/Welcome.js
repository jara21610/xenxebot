import React, {
    Component
} from 'react';
import {
    Fragment
} from 'react';
import {
    Redirect,
    Link
} from 'react-router-dom';
import {
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardText,
    CardImg
} from 'reactstrap'
import Header from '../../components/Header/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import paso1 from '../../images/paso1.jpeg'
import paso2 from '../../images/paso2.jpeg'
import paso3 from '../../images/paso3.jpeg'
class Welcome extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            lastname: '',
            email: '',
            mobilphone: '',
            license: '',
            redirect: false,
            modal: false
        };
    }

    render() {

        let REACT_APP_CHATBOTURL = ''
		

        if (!sessionStorage.getItem('userData') || this.state.redirect) {
            return ( < Redirect to = {
                    '/'
                }
                />)
            }
		let productname = '';
		
		if (sessionStorage.getItem('servicetype') == process.env.REACT_APP_PRODUCTID_XENXEBOT){
			productname = process.env.REACT_APP_PRODUCTNAME_XENXEBOT
			REACT_APP_CHATBOTURL = process.env.REACT_APP_CHATBOTURL_XENXEBOT;
		} else {
			productname = process.env.REACT_APP_PRODUCTNAME_PREXTIGEBOT
			REACT_APP_CHATBOTURL = process.env.REACT_APP_CHATBOTURL_PREXTIGEBOT;
		}
            return ( <
                div >
                <
                Header >
                <
                /Header> <
                Container >
                <
                Row >
                <
                Col >
                <
                br / >
                <
                /Col>          <
                /Row> <
                Row >
                <
                Col >
                <
                h4 class = "card-title text-center" > Ahora puedes usar la Aplicación {productname}, solo sigue estos 3 pasos < /h4>                  <
                /Col>          <
                /Row>          <
                Row >
                <
                Col >
                <
                br / >
                <
                /Col>          <
                /Row> <
                Row >
                <
                Col >
                <
                Card body outline color = "primary" >
                <
                CardTitle > Paso 1 < /CardTitle> <
                br / >
                <
                CardText > Descarga e instala Telegram en tu dispositivo movil < /CardText> <
                CardImg top src = {
                    paso1
                }
                alt = "Card image cap" / >
                <
                /Card>  <
                /Col> <
                Col >
                <
                Card body outline color = "primary" >
                <
                CardTitle > Paso 2 < /CardTitle> <
                br / >
                <
                CardText > Ingresa a Telegram y busca nuestro bot {productname} o < a href = {
                    REACT_APP_CHATBOTURL
                }
                class = "btn btn btn-outline-dark btn-sm active"
                role = "button" > Haz Click Aquí < /a> </CardText >
                <
                CardImg top src = {
                    paso2
                }
                alt = "Card image cap" / >
                <
                /Card>            <
                /Col> <
                Col >
                <
                Card body outline color = "primary" >
                <
                CardTitle > Paso 3 < /CardTitle> <
                br / >
                <
                CardText >Una vez abierto nuestro chatbot, haz click en Iniciar y sigue las instrucciones para comenzar a Analizar Sentimientos! < /CardText> <
                CardImg top src = {
                    paso3
                }
                alt = "Card image cap" / >
                <
                /Card>           <
                /Col> <
                /Row>                     <
                /Container> <
                /div>
            );
        }
    }
    export default Welcome;


    // WEBPACK FOOTER //
    // ./src/components/Welcome/Welcome.js