export function GetTermsandConditionsPost(userData) {
    let BaseURL = process.env.REACT_APP_GETUSERLICENSEURL;

    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
                    "Content-Type": "application/json",
                    'Accept': 'application/json'
                },
				body: JSON.stringify(userData)
            }).then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });
    });
}
